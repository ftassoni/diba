import db_access
import tools
import itertools
import time
import anno_stagione_lib
from Common import business_object, errors
import math


from facon.ordpf.dao import legacy, decorators, richiesteDB
from facon.ordpf.errors import ForwardException
from facon.ordpf.facade.pool_manager import Request, db_provided, ConnPool, CONN_KEY
from facon.ordpf.utils.email import EmailHelper
from facon.diba.dao import fabbisogni_dao, dwg_dao, diba_dao
from facon.diba.bo.fabbisogni import Fabbisogno, Impegno
from facon.pao.facade.giacenza_mp import GiacenzaMPFacade
from facon.pao.facade.ordine_mp import OrdineMateriaPrimaFacade
from facon.magmp.dao import prelievoDB
from facon.pao.dao import ordiniDB



class FabbFacade(business_object.FacadeObject):

	@db_provided()
	def autocomplete_classi(self):
		classi = legacy.DaoLegacy().get_classi()
		return classi

	@db_provided()
	def autocomplete_colore(self, params):
		colori = fabbisogni_dao.get_colore_capocmat(params)
		return colori

	@db_provided()
	def autocomplete_misura(self, params):
		misure = fabbisogni_dao.get_misura_capocmat(params)
		return misure

	@db_provided()
	def ricerca_fabbisogni(self, dati):
		result_list = []
		result = fabbisogni_dao.ricerca_fabbisogni(dati)
		if dati.get('tipo_vista', '') == 'articolo':
			result = sorted(result, key=lambda k: (k['societa'], k['anno_stagione'], k['gm'], k['articolo']))
			for k, g in itertools.groupby(result,key=lambda k: (k['societa'], k['anno_stagione'], k['gm'], k['articolo'])):
				gruppo = list(g)
				r = gruppo[0].copy()
				r['id_fabbs'] = ','.join(list(set([g['id_fabb'] for g in gruppo])))
				r['id_dibas'] = ','.join(list(set([g['id_diba'] for g in gruppo])))
				r['da_soddisfare'] = 0
				r['impegno'] = 0
				if float(r['qta_reale']) > 0:
					r.update(fabbisogni_dao.get_qta_reale_by_id_fabb(dati, r['id_fabbs']))
				r.update(fabbisogni_dao.get_qta_capi(dati, gruppo))
				imp = fabbisogni_dao.get_qta_impegno_by_id_fabb(dati, r['id_fabbs'])
				if imp:
					r['impegno'] = imp[0]['qta_imp']
				if r['impegno'] and r['fabbisogno']:
					r['da_soddisfare'] = round(float(r['fabbisogno']) - float(r['impegno']), 2)
				r['dettaglio'] = fabbisogni_dao.get_dettaglio_x_articolo(r)
				for d in r['dettaglio']:
					d['da_soddisfare'] = 0
					d['impegno'] = 0
					d['rif_bp'] = []
					# if d['id_artcomm_list'] and d['var_dt_list'] and int(d['capi_lanciati'])>0:
					# 	rif_bp = fabbisogni_dao.get_bp_by_id_artcomm_righe(d['id_artcomm_list'], d['var_dt_list'])
					# 	if rif_bp:
					# 		d['rif_bp'] = ['%s - %s' % (time.strftime('%d/%m/%Y', time.strptime(b[:6], '%y%m%d')), b[-6:]) for b in rif_bp[0]['rif_bp'].split(',') if b]
					imp = fabbisogni_dao.get_qta_impegno_by_id_fabb(dati, d['id_fabbs_dett'])
					if imp:
						d['impegno'] = imp[0]['qta_imp']
					if float(d['qta_reale'])> 0:
						d.update(fabbisogni_dao.get_qta_reale_by_id_fabb(dati, d['id_fabbs_dett']))
						d['rif_bp'] = list(set(['%s - %s' % (time.strftime('%d/%m/%Y', time.strptime(b[:6], '%y%m%d')), b[-6:]) for b in d['rif_bp'].split(',') if b]))
					if d['impegno'] and d['fabbisogno']:
						d['da_soddisfare'] = round(float(d['fabbisogno']) - float(d['impegno']), 2)
				result_list.append(r)
		elif dati.get('tipo_vista', '') == 'modello':
			result = sorted(result, key=lambda k: (k['id_art_comm'], k['societa_orig'], k['modello_orig'], k['variante']))
			for k, g in itertools.groupby(result, key=lambda k: (k['id_art_comm'], k['societa_orig'], k['modello_orig'], k['variante'])):
				gruppo = list(g)
				r = gruppo[0].copy()
				r['fabbisogno_perc'] = 0
				r['id_fabbs'] = ','.join(list(set([g['id_fabb'] for g in gruppo])))
				r['id_dibas'] = ','.join(list(set([g['id_diba'] for g in gruppo])))
				r['dettaglio'] = fabbisogni_dao.get_dettaglio_x_modello(r)
				r['impegno'] = 0
				if float(r['qta_reale']) > 0:
					r.update(fabbisogni_dao.get_qta_reale_by_id_fabb(dati, r['id_fabbs']))
				r.update(fabbisogni_dao.get_qta_capi(dati, gruppo))
				imp = fabbisogni_dao.get_qta_impegno_by_id_fabb(dati, r['id_fabbs'])
				if imp:
					r['impegno'] = imp[0]['qta_imp']
				if r['impegno'] and r['fabbisogno'] and float(r['fabbisogno']) > 0.00:
					r['fabbisogno_perc'] = round(float(r['impegno']) / float(r['fabbisogno']), 2) * 100
					if r['fabbisogno_perc'] > 100:
						r['fabbisogno_perc'] = 100
				for d in r['dettaglio']:
					d['da_soddisfare'] = 0
					d['impegno'] = 0
					d['rif_bp'] = []
					# if d['id_artcomm_list'] and d['var_dt_list'] and int(d['capi_lanciati'])>0:
					# 	rif_bp = fabbisogni_dao.get_bp_by_id_artcomm_righe(d['id_artcomm_list'], d['var_dt_list'])
					# 	if rif_bp:
					# 		d['rif_bp'] = ['%s - %s' %(time.strftime('%d/%m/%Y', time.strptime(b[:6],'%y%m%d')), b[-6:]) for b in rif_bp[0]['rif_bp'].split(',') if b]
					imp = fabbisogni_dao.get_qta_impegno_by_id_fabb(dati, d['id_fabbs_dett'])
					if imp:
						d['impegno'] = imp[0]['qta_imp']
					if float(d['qta_reale'])> 0:
						d.update(fabbisogni_dao.get_qta_reale_by_id_fabb(dati, d['id_fabbs_dett']))
						d['rif_bp'] = list(set(['%s - %s' % (time.strftime('%d/%m/%Y', time.strptime(b[:6], '%y%m%d')), b[-6:]) for b in d['rif_bp'].split(',') if b]))
					if d['impegno'] and d['fabbisogno']:
						d['da_soddisfare'] = round(float(d['fabbisogno']) - float(d['impegno']), 2)
				result_list.append(r)
		else:
			raise ForwardException('Vista non gestita %s' %dati.get('tipo_vista', ''))

		return result_list


	@db_provided()
	def esiste_fabb_prev(self, id_art_rich_comm):
		res = fabbisogni_dao.get_fabbisogno_by_id_art_rich_comm(id_art_rich_comm)
		if res:
			return res[0]
		else:
			return {}


	@db_provided(transactional=True)
	def crea_fabbisogno_prev(self, dati_tessuto, dati_rig, dati_art):
		pard = Request().get_pard()
		conn = ConnPool().get_connection(Request().get_val(CONN_KEY))
		try:
			dati_prod = dwg_dao.get_modelli_variante_prod(pard, dati_rig['societa_orig'], dati_rig['modello_orig'], dati_art['variante'])
		except:
			raise ForwardException('Modello non trovato %(societa_orig)s-%(modello_orig)s-%%(variante)s' % dati_rig %dati_art)
		else:
			ret = dwg_dao.get_diba_esplosa_originale(dati_prod[0]['societa'], dati_prod[0]['modello_produzione'], dati_prod[0]['variante'], debug=False)
			diba_esplosa = []
			result = sorted(ret[0]['data'], key=lambda k: (k['prodotto'], k['variante_prodotto'], k['prodotto_componente'],
			                k['misura_prodotto'], k['misura_prodotto_componente'], k['variante_prodotto_componente']))
			for k, g in itertools.groupby(result, key=lambda k: (k['prodotto'], k['variante_prodotto'], k['prodotto_componente'],
			                k['misura_prodotto'], k['misura_prodotto_componente'], k['variante_prodotto_componente'])):
				db_list = list(g)
				materiale = db_list[0].copy()
				if str(materiale['misura_prodotto']) in ('42', 'SM', 'M', 'null'):
					# if (str(materiale['gruppo_merceologico']), str(dati_rig['societa_orig'])) in (('20', 'MM'), ('21', 'MM'), ('20', 'MA'), ('21', 'MA'), ('30', 'MN'), ('31', 'MN'), ('32', 'MN')):
					materiale['consumo_unitario_lordo'] = sum(float(d['consumo_unitario_lordo']) for d in db_list)
					materiale['posizione'] = ','.join(set([str(d['posizione']) for d in db_list if d.get('posizione','') and d['posizione'] and d['posizione']!=None]))
					materiale['nota'] = ''
					materiale['consumo_dettaglio'] = ''
					if len(db_list) > 1:
						materiale['nota'] = 'Consumo composto: %s' % '+'.join([str(d['consumo_unitario_lordo']) for d in db_list])
						materiale['consumo_dettaglio'] = ','.join([str(d['consumo_unitario_lordo']) for d in db_list])
					diba_esplosa.append(materiale)
					# else:
					# 	diba_esplosa.extend(db_list)
			for d in diba_esplosa:
				if str(d['gruppo_merceologico']) != '80':
					diz = {'diba': {},}
					diz['diba']['societa'] = dati_rig['societa_orig']
					diz['diba']['articolo'] = d['progr_articolo']
					if d['anno'] != '0' and d['stagione'] != '0':
						if str(d['stagione']) == '1':
							stagione = 'P/E'
						else:
							stagione = 'A/I'
						diz['diba']['anno_stagione'] = "%s %s" %(d['anno'], stagione)
						diz['diba']['a_s'] = anno_stagione_lib.decodifica_as(diz['diba']['anno_stagione'])
					else:
						diz['diba']['anno_stagione'] = anno_stagione_lib.decode_as('PP')
						diz['diba']['a_s'] = 'PP'

					diz['diba']['gm'] = d['gruppo_merceologico']
					diz['diba']['pezzo'] = d['progr_pezzo']
					diz['diba']['colonna'] = d.get('progr_componente_coloritura', '')
					diz['diba']['posizione'] = d['posizione'] if d['posizione'] else ''
					diz['diba']['id_articolo'] = ''
					diz['diba']['descrizione'] = ''
					dati_art_orig = diba_dao.get_id_art_orig(diz['diba'])
					if dati_art_orig:
						diz['diba']['id_articolo'] = dati_art_orig[0]['id_articolo']
						diz['diba']['descrizione'] = dati_art_orig[0]['descrizione']

					diz['diba']['numero_riga'] = d['numero_riga']
					diz['diba']['taglia_base'] = d['misura_prodotto']
					diz['diba']['consumo'] = float(d['consumo_unitario_lordo']) if d.get('consumo_unitario_lordo','') != None else 1
					if diz['diba']['societa'] in ('MM', 'MA') and diz['diba']['numero_riga'] == '1110':
						diz['diba']['consumo'] = diba_dao.calcola_consumo_netto(Request().get_pard(), diz['diba']['societa'], d['materiale_componente'], diz['diba']['consumo'])
					diz['id_art_rich_comm'] = dati_art['id_art']
					diz['stato'] = 'bozza'
					diz['nota'] = d.get('nota', '')
					diz['diba']['consumo_dettaglio'] = d.get('consumo_dettaglio', '')
					diz['diba']['societa_orig'] = dati_rig['societa_orig']
					diz['diba']['modello_orig'] = dati_rig['modello_orig']
					diz['diba']['nome_modello_orig'] = dati_prod[0]['nome']
					diz['diba']['misura'] = d['misura_prodotto_componente'][-2:] if d.get('misura_prodotto_componente','') != None else '00'
					if str(dati_rig['flag_variante_orig']) == '0':
						diz['diba']['colore'] = str(d['variante_prodotto_componente']).zfill(4)
						diz['diba']['macrocolore'] = ''
						diz['diba']['colore_descr'] = ''
						macrocol = diba_dao.get_macrocolore(diz['diba'])
						if macrocol:
							diz['diba']['macrocolore'] = macrocol[0]['macrocolore']
							diz['diba']['colore_descr'] = macrocol[0]['colore_descr']
						diz['diba']['variante_orig'] = dati_rig['variante_orig']
						diz['diba']['variante_dt'] = dati_rig['variante_orig']
					else:
						diz['diba']['colore'] = '0000'
						diz['diba']['macrocolore'] = ''
						diz['diba']['colore_descr'] = ''
						diz['diba']['variante_orig'] = ''
						diz['diba']['variante_dt'] = dati_rig['variante_orig']
					diz['capi_prev'] = dati_art['tot_capi']
					diz['diba']['capocmat'] = diz['diba']['societa'] + diz['diba']['gm'] + diz['diba']['a_s'] + diz['diba']['articolo'] + diz['diba']['misura'] + diz['diba']['colore']
					if (diz['diba']['societa'] in ('MM', 'MA') and diz['diba']['numero_riga'] == '1110') or \
							(diz['diba']['societa'] in ('MN') and diz['diba']['numero_riga'] == '100'):
						# metto il tessuto scelto
						diz['diba']['societa'] = dati_tessuto['societa']
						diz['diba']['articolo'] = dati_tessuto['articolo']
						diz['diba']['anno_stagione'] = dati_tessuto['anno_stagione']
						diz['diba']['a_s'] = anno_stagione_lib.decodifica_as(dati_tessuto['anno_stagione'])
						diz['diba']['colore'] = str(dati_tessuto['colore']).zfill(4)
						diz['diba']['macrocolore'] = ''
						diz['diba']['misura'] = '00'
						diz['diba']['colore_descr'] = dati_tessuto['colore_descrizione']
						macrocol = diba_dao.get_macrocolore(diz['diba'])
						if macrocol:
							diz['diba']['macrocolore'] = macrocol[0]['macrocolore']
							diz['diba']['colore_descr'] = macrocol[0]['colore_descr']
						diz['diba']['descrizione'] = dati_tessuto['articolo_descrizione']
						diz['diba']['capocmat'] = diz['diba']['societa'] + diz['diba']['gm'] + diz['diba']['a_s'] + \
						                          diz['diba']['articolo'] + diz['diba']['misura'] + diz['diba']['colore']
						diz['diba']['consumo'] = dati_rig['consumo']
						diz['qta_prev'] = float(dati_rig['consumo']) * int(dati_art['tot_capi'])
					else:
						diz['qta_prev'] = float(diz['diba']['consumo']) * int(dati_art['tot_capi'])


					dati_capocmat = diba_dao.exist_capocmat(pard, conn, diz['diba'])
					if not dati_capocmat.get('capocmat', ''):
						from facon.origana import importa_anagrafica
						from facon.origana.importa_anagrafica import societa_list
						# tools.dump('qui')
						db_riga = diz['diba'].copy()
						db_riga['societa_old'] = ''
						for s in societa_list:
							if s['soc_dt'] == db_riga['societa']:
								db_riga['societa_old'] = s['soc_orig']
						db_riga['id_articolo_old'] = ''
						db_riga['id_articolo_new'] = ''
						dati_art_orig = importa_anagrafica.importa_solo_articolo(pard, db_riga, conn)
						if dati_art_orig:
							diz['diba']['id_articolo'] = dati_art_orig['id_articolo']
							diz['diba']['descrizione'] = dati_art_orig['descrizione']

					fabb = Fabbisogno(diz)
					fabb.salva()


	@db_provided(transactional=True)
	def aggiorna_fabbisogno_prev(self, dati_tessuto, dati_rig, dati_art):
		pard = Request().get_pard()
		conn = ConnPool().get_connection(Request().get_val(CONN_KEY))
		fabb_diba = Fabbisogno.get_fabbisogni_diba(dati_art['id_art'])
		for fabb in fabb_diba:
			fabb.capi_prev = dati_art['tot_capi']
			if str(dati_rig['flag_variante_orig']) == '1':
				fabb.diba.variante_dt = dati_rig['variante_orig']
			if (fabb.diba.societa_orig in ('MM', 'MA') and fabb.diba.numero_riga == '1110') or \
					(fabb.diba.societa_orig in ('MN') and fabb.diba.numero_riga == '100'):
				# aggiorno il tessuto scelto
				fabb.diba.societa = dati_tessuto['societa']
				fabb.diba.articolo = dati_tessuto['articolo']
				fabb.diba.anno_stagione = dati_tessuto['anno_stagione']
				a_s = anno_stagione_lib.decodifica_as(dati_tessuto['anno_stagione'])
				fabb.diba.colore = str(dati_tessuto['colore']).zfill(4)
				fabb.diba.macrocolore = ''
				fabb.diba.misura = '00'
				fabb.diba.colore_descr = dati_tessuto['colore_descrizione']
				macrocol = diba_dao.get_macrocolore(dati_tessuto)
				if macrocol:
					fabb.diba.macrocolore = macrocol[0]['macrocolore']
					fabb.diba.colore_descr = macrocol[0]['colore_descr']
				fabb.diba.descrizione = dati_tessuto['articolo_descrizione']
				fabb.diba.capocmat = fabb.diba.societa + fabb.diba.gm + a_s + \
				                     fabb.diba.articolo + fabb.diba.misura + fabb.diba.colore
				fabb.diba.consumo = float(dati_rig['consumo'])
				fabb.qta_prev = dati_rig['consumo'] * dati_art['tot_capi']
			else:
				fabb.qta_prev = fabb.diba.consumo * dati_art['tot_capi']

			# Importazione dati anagrafici non necessari in aggiornamento
			# Da usare solo in caso di mancata creazione

			#dati_diba = fabb.diba.to_dictionary()
			#dati_diba['a_s'] = anno_stagione_lib.decodifica_as(fabb.diba.anno_stagione) if fabb.diba.anno_stagione else ''
			#dati_capocmat = diba_dao.exist_capocmat(pard, conn, dati_diba)
			#if not dati_capocmat.get('capocmat', ''):
			#	from facon.origana import importa_anagrafica
			#	from facon.origana.importa_anagrafica import societa_list
			#	# tools.dump('qui')
			#	dati_diba['societa_old'] = ''
			#	for s in societa_list:
			#		if s['soc_dt'] == dati_diba['societa']:
			#			dati_diba['societa_old'] = s['soc_orig']
			#	dati_diba['id_articolo_old'] = ''
			#	dati_diba['id_articolo_new'] = ''
			#	dati_art_orig = importa_anagrafica.importa_solo_articolo(pard, dati_diba, conn)
			#	if dati_art_orig:
			#		fabb.diba.id_articolo = dati_art_orig['id_articolo']
			#		fabb.diba.descrizione = dati_art_orig['descrizione']
			fabb.salva()



	@db_provided(transactional=True)
	def salva_fabbisogno_prev(self, rcomm_diz):
		for tes in rcomm_diz['list_tessuti']:
			for rig in tes['list_righe']:
				for art in rig['list_art']:
					dati_fabb_prev = self.esiste_fabb_prev(art['id_art'])
					if not dati_fabb_prev:
						if tes['tipo_tessuto'] == 'T':
							self.crea_fabbisogno_prev(tes, rig, art)
						else:
							self.crea_fabbisogno_prev_singolo(tes, rig, art)
					elif int(dati_fabb_prev['capi_prev']) != int(art['tot_capi']) or \
							float(dati_fabb_prev['consumo'] != float(rig['consumo'])):
						self.aggiorna_fabbisogno_prev(tes, rig, art)



	@db_provided(transactional=True)
	def crea_fabbisogno_prev_singolo(self, dati_tessuto, dati_rig, dati_art):
		try:
			dati_prod = dwg_dao.get_modelli_variante_prod(Request().get_pard(), dati_rig['societa_orig'],
			                                              dati_rig['modello_orig'], dati_art['variante'])
		except:
			raise ForwardException('Modello non trovato %(societa_orig)s-%(modello_orig)s-%%(variante)s' % dati_rig % dati_art)
		diz = {'diba': {},}
		# metto il tessuto scelto
		diz['id_art_rich_comm'] = dati_art['id_art']
		diz['stato'] = 'bozza'
		diz['diba']['societa_orig'] = dati_rig['societa_orig']
		diz['diba']['modello_orig'] = dati_rig['modello_orig']
		diz['diba']['nome_modello_orig'] = dati_prod[0]['nome']
		if str(dati_rig['flag_variante_orig']) == '0':
			diz['diba']['variante_orig'] = dati_rig['variante_orig']
			diz['diba']['variante_dt'] = dati_rig['variante_orig']
		else:
			diz['diba']['variante_orig'] = ''
			diz['diba']['variante_dt'] = dati_rig['variante_orig']
		diz['diba']['societa'] = dati_tessuto['societa']
		diz['diba']['articolo'] = dati_tessuto['articolo']
		diz['diba']['gm'] = dati_tessuto['gm']
		diz['diba']['anno_stagione'] = dati_tessuto['anno_stagione']
		diz['diba']['a_s'] = anno_stagione_lib.decodifica_as(dati_tessuto['anno_stagione'])
		diz['diba']['colore'] = str(dati_tessuto['colore']).zfill(4)
		diz['diba']['macrocolore'] = ''
		diz['diba']['misura'] = '00'
		diz['diba']['capocmat'] = diz['diba']['societa'] + diz['diba']['gm'] + diz['diba']['a_s'] + diz['diba'][
			'articolo'] + diz['diba']['misura'] + diz['diba']['colore']
		diz['diba']['colore_descr'] = dati_tessuto['colore_descrizione']
		macrocol = diba_dao.get_macrocolore(diz['diba'])
		if macrocol:
			diz['diba']['macrocolore'] = macrocol[0]['macrocolore']
			diz['diba']['colore_descr'] = macrocol[0]['colore_descr']
		diz['diba']['capocmat'] = diz['diba']['societa'] + diz['diba']['gm'] + diz['diba']['a_s'] + \
		                          diz['diba']['articolo'] + diz['diba']['misura'] + diz['diba']['colore']
		diz['diba']['consumo'] = dati_rig['consumo']
		dati_art_orig = diba_dao.get_id_art_orig(diz['diba'])
		if dati_art_orig:
			diz['diba']['id_articolo'] = dati_art_orig[0]['id_articolo']
			diz['diba']['descrizione'] = dati_art_orig[0]['descrizione']
		diz['diba']['pezzo'] = ''
		diz['diba']['colonna'] = ''
		diz['diba']['posizione'] = ''
		diz['diba']['numero_riga'] = ''
		diz['diba']['taglia_base'] = ''
		diz['capi_prev'] = dati_art['tot_capi']
		diz['qta_prev'] = float(dati_rig['consumo']) * int(dati_art['tot_capi'])
		fabb = Fabbisogno(diz)
		fabb.salva()





	@db_provided(transactional=True)
	def elimina_fabbisogno_prev(self, id_art_eliminate):
		self._elimina_fabbisogno_prev(id_art_eliminate)


	def _elimina_fabbisogno_prev(self, id_art_eliminate):
		for id_art in id_art_eliminate:
			fabb_diba = Fabbisogno.get_fabbisogni_diba(id_art)
			for fabb in fabb_diba:
				fabb.elimina()


	@db_provided(transactional=True)
	def salva_fabb_reale(self, bp_diz):
		self._salva_fabb_reale(bp_diz)


	@db_provided()
	def _salva_fabb_reale(self, bp_diz, conn=None):
		# prendo le nuove varianti del bp
		bp_diz['collez'] = bp_diz.get('collez', '').replace('%26', '&')

		varianti_list = []
		for riga_var in bp_diz['list_varianti']:
			if riga_var['variante']:
				var_new = {'variante_dt': str(riga_var['variante']).zfill(3),
						'tot_lanciati': riga_var['qta_tot_capi'],
						'rif_intr': bp_diz['rif_intr'],
						'modello_originale': bp_diz['modello_originale'][:8],
						'societa_originale': bp_diz['societa'],
						'nome_originale': bp_diz['nome_originale'],
				        'collez': bp_diz['collez'],
				        'anno': bp_diz['anno_stagione'][3],
				        'stag': '2' if bp_diz['anno_stagione'][-3:] == 'P/E' else '4'}
				varianti_list.append(var_new)

		# prendo le vecchie varianti del bp, salvate sui fabbisogni
		varianti_old = fabbisogni_dao.get_varianti_fabb_by_bp(bp_diz)
		for v in varianti_old:
			# filtro per ciascuna variante gli id art rcomm,
			# prendendo solo quelli legati alla richiesta standard che e' stata lanciata
			# gli altri id li escludo per non duplicare il fabbisogno reale
			new_list_art = []
			res = fabbisogni_dao.get_id_art_rich_std_by_variante_bp(bp_diz['rif_intr'], v['variante_dt'])
			id_art_standard = [r['id_art_rcomm'] for r in res]
			if id_art_standard:
				for id_art in v['id_art_rcomm'].split(','):
					if id_art in id_art_standard:
						new_list_art.append(id_art)
				if new_list_art:
					v['id_art_rcomm'] = ','.join(new_list_art)

		var_trovate = []
		for var_new in varianti_list:
			trovata = False
			for var_old in varianti_old:
				# se trovo una variante gia' presente: aggiorno le qta sui fabbisogni
				if str(var_new['variante_dt']).zfill(3) == str(var_old['variante_dt']).zfill(3):
					trovata = True
					var_trovate.append(str(var_new['variante_dt']))
					self._aggiorna_fabbisogno_reale(var_old, var_new, bp_diz['list_righe'])
			# se non la trovo creo una nuova serie di fabbisogni per quella variante
			if not trovata:
				self._crea_fabbisogno_reale(var_new, bp_diz['list_righe'])

		# per le varianti vecchie che non trovo piu', aggiorno (o azzero) il fabbisogno
		for var_old in varianti_old:
			if str(var_old['variante_dt']) not in var_trovate:
				self._aggiorna_fabbisogno_reale(var_old, {}, [])


	def _crea_fabbisogno_reale(self, var_diz, bp_righe):
		facade_giacenza = GiacenzaMPFacade(Request().get_pard())

		dati_ordpf = fabbisogni_dao.get_id_art_rich_by_variante_bp(var_diz['rif_intr'], var_diz['variante_dt'])
		if not dati_ordpf:
			raise ForwardException('Impossibile creare il fabbisogno di una variante non ordinata.')

		bp_righe_var = []
		for riga in bp_righe:
			variante = str(riga['variante']).zfill(3) if (riga['variante'] and str(riga['variante']) != '0') else ''
			if not variante or (variante and str(var_diz['variante_dt']).zfill(3) == variante):
				bp_righe_var.append(riga)

		s_bp_righe_var = sorted(bp_righe_var, key=lambda riga: (riga['capocmat']))
		for k, g in itertools.groupby(s_bp_righe_var, key=lambda riga: (riga['capocmat'])):
			bp_riga_list = list(g)
			riga = bp_riga_list[0].copy()

			# per ogni articolo creo il fabbisogno reale e lo salvo
			diz = {'diba': {},}
			capocmat = riga['capocmat'] if riga['tipo_riga'] not in ('T','E') else riga['capocmat_cmat']# MM1062883000001
			diz['diba']['capocmat'] = capocmat
			diz['diba']['societa'] = capocmat[:2]
			diz['diba']['articolo'] = capocmat[6:9]
			diz['diba']['a_s'] = capocmat[4:6]
			diz['diba']['anno_stagione'] = anno_stagione_lib.decode_as(diz['diba']['a_s'])
			diz['diba']['gm'] = capocmat[2:4]
			diz['diba']['misura'] = capocmat[9:11]
			diz['diba']['colore'] = capocmat[11:]
			diz['diba']['colore_descr'] = riga['d_mat_col']
			diz['diba']['macrocolore'] = riga['macrogruppo']
			diz['diba']['pezzo'] = '1'
			diz['diba']['colonna'] = ''
			diz['diba']['descrizione'] = riga['d_mat']
			diz['diba']['posizione'] = ''
			diz['diba']['articolo_fornitore'] = ''
			diz['diba']['id_articolo'] = ''
			diz['diba']['id_articolo_fornitore'] = ''
			id_art = diba_dao.get_id_art_orig(diz['diba'])
			if id_art:
				diz['diba']['id_articolo'] = id_art[0]['id_articolo']
			dati_art = diba_dao.get_dati_articolo_orig(diz['diba'])
			if dati_art:
				diz['diba']['articolo_fornitore'] = dati_art[0]['articolo_fornitore']
				diz['diba']['id_articolo_fornitore'] = dati_art[0]['id_articolo_fornitore']
			diz['diba']['numero_riga'] = riga['nr_r']
			diz['diba']['taglia_base'] = ''
			diz['diba']['consumo'] = sum(float(d['consumo']) for d in bp_riga_list)
			if len(bp_riga_list) > 1:
				diz['nota'] = 'Consumo composto: %s' % '+'.join([str(float(d['consumo'])) for d in bp_riga_list])
				diz['diba']['consumo_dettaglio'] = ','.join([str(float(d['consumo'])) for d in bp_riga_list])
			diz['stato'] = 'valutazione'
			diz['diba']['societa_orig'] = var_diz['societa_originale']
			diz['diba']['modello_orig'] = var_diz['modello_originale'][:8]
			diz['diba']['nome_modello_orig'] = var_diz['nome_originale']
			diz['diba']['variante_dt'] = var_diz['variante_dt']
			diz['diba']['variante_orig'] = ''
			diz['id_art_rich_comm'] = ''
			diz['capi_teor'] = 0
			if dati_ordpf:
				diz['diba']['variante_orig'] = dati_ordpf[0]['variante_orig']
				diz['id_art_rich_comm'] = dati_ordpf[0]['id_art_rcomm']
				diz['capi_teor'] = dati_ordpf[0]['capi_ordpf']

			diz['list_impegni'] = []
			diz['qta_reale'] = 0
			diz['capi_lanciati'] = 0

			for d in bp_riga_list:
				id_faccom = d['ti_rowid']
				qta = d['qta']
				qta_capi = d['qta_capi']
				dati_riga = fabbisogni_dao.get_qta_bp_by_impegno({'id_faccom': id_faccom, 'variante_dt': var_diz['variante_dt'], 'qta': qta, 'qta_capi': qta_capi})
				if dati_riga['tot_lanciati'] > 0:
					diz['qta_reale'] += round(float(dati_riga['qta_reale']), 2)
					diz['capi_lanciati'] += float(dati_riga['tot_lanciati'])

				if d.get('tipo_riga') in ('T', 'E'):
					# disimpegna su me stesso
					qta_da_imp = round(float(dati_riga['qta_reale']), 2)
					diff = round(float(dati_riga['qta_reale']), 2)
					if diff > 0:
						giac_diz = facade_giacenza._get_giacenza_tessuto(diz['diba'], rif_bp=var_diz['rif_intr'], id_fabb='')
						# verifico giacenza libera
						giac_free = giac_diz['GIAC'] - giac_diz['GIAC_PRENOTATA'] + qta_da_imp
						if giac_free > 0:
							diff -= giac_free
						# disimpegna altri modelli
						if diff > 0:
							for id in giac_diz['ID_FABB_ALTRO']:
								if diff > 0:
									fabb_altro = Fabbisogno.get_by_key(id)
									diff, qta_da_imp = fabb_altro.disimpegna_dt(diff, id_faccom)
									fabb_altro.salva()
						if diff > 0:
							raise ForwardException('Giacenza insufficiente per %s' % diz['diba']['capocmat'])
					# infine aggiorno l'impegno pari alla qta messa nel bp
					imp = {}
					imp['qta'] = round(float(dati_riga['qta_reale']), 2)
					imp['qta_faccom'] = round(float(dati_riga['qta_reale']), 2)
					imp['capi_faccom'] = float(dati_riga['tot_lanciati'])
					imp['id_faccom'] = id_faccom
					imp['qta_prelevata'] = imp['qta']
					imp['tipo'] = 'DT'
					diz['list_impegni'].append(imp)
				elif (d.get('giacenza', '') == '1' and d.get('da_ordinare', '') == ''):
					imp = {}
					giac_art = facade_giacenza._get_giacenza_dt_by_capocmat(diz['diba'])
					if giac_art:
						if float(giac_art[0]['qta']) >= float(diz['qta_reale']):
							imp['qta'] = round(float(dati_riga['qta_reale']), 2)
						else:
							imp['qta'] = giac_art[0]['qta']
					else:
						raise ForwardException('Nessuna giacenza trovata per %s' %diz['diba']['capocmat'])
					imp['qta_faccom'] = round(float(dati_riga['qta_reale']), 2)
					imp['capi_faccom'] = float(dati_riga['tot_lanciati'])
					imp['id_faccom'] = id_faccom
					imp['qta_prelevata'] = d['qta_prelevata']
					imp['tipo'] = 'DT'
					diz['list_impegni'].append(imp)
				elif riga.get('da_ordinare', '') == '*':
					imp = {}
					imp['qta'] = 0
					imp['qta_faccom'] = round(float(dati_riga['qta_reale']), 2)
					imp['capi_faccom'] = float(dati_riga['tot_lanciati'])
					imp['id_faccom'] = id_faccom
					imp['tipo'] = 'DT'
					diz['list_impegni'].append(imp)
				else:
					imp = {}
					imp['qta'] = 0
					imp['qta_faccom'] = round(float(dati_riga['qta_reale']), 2)
					imp['capi_faccom'] = float(dati_riga['tot_lanciati'])
					imp['id_faccom'] = id_faccom
					imp['tipo'] = 'DT'
					diz['list_impegni'].append(imp)

			fabb = Fabbisogno(diz.copy())
			fabb.salva()


	def _aggiorna_fabbisogno_reale(self, var_old, var_new, bp_righe):
		facade_giacenza = GiacenzaMPFacade(Request().get_pard())
		# prendo i fabbisogni gia' presenti a db (vecchi)
		fabb_diba_old = Fabbisogno.get_fabbisogni_diba(var_old.get('id_art_rcomm', ''))
		# possono esserci piu' varianti per outlet estero in quanto il codice variante e' deciso successivamente
		var_old_list = list(set([f.diba.variante_dt for f in fabb_diba_old]))
		fabb_trovati = []
		#id_righe_bp_list = []
		id_righe_all_bp = []
		for riga in bp_righe:
			trovato = False
			id_righe_all_bp.append(str(riga['ti_rowid']))
			variante = str(riga['variante']).zfill(3) if (riga['variante'] and str(riga['variante']) != '0') else ''
			if not variante or (variante and variante in var_old_list):
				#id_righe_bp_list.append(str(riga['ti_rowid']))
				# per ogni articolo nel bp verifico se c'e' un fabbisogno vecchio
				for fabb in fabb_diba_old:
					capocmat = riga['capocmat'] if riga['tipo_riga'] not in ('T', 'E') else riga['capocmat_cmat']
					if fabb.diba.capocmat and fabb.diba.capocmat == capocmat:
						# se lo trovo, ricalcolo i capi lanciati e la qta del fabbisogno
						# aggiorno anche il collegamento con la rich comm
						# se non presente do errore
						dati = fabbisogni_dao.calcola_lanciato(var_new, fabb.diba.capocmat)
						if not dati:
							raise ForwardException('Impossibile creare il fabbisogno di una variante non ordinata.')
						fabb.capi_lanciati = dati['tot_lanciati']
						fabb.qta_reale = dati['qta_reale']
						fabb.id_art_rich_comm = dati['id_art_rcomm']

						riga['id_faccom'] = riga['ti_rowid']
						riga['variante_dt'] = var_new['variante_dt']
						dati_riga = fabbisogni_dao.get_qta_bp_by_impegno(riga)

						if riga.get('tipo_riga') in ('T', 'E'):
							# disimpegna su me stesso
							qta_da_imp = dati_riga['qta_reale']
							diff, qta_da_imp = fabb.disimpegna_dt(qta_da_imp, id_faccom = riga['ti_rowid'])
							if diff > 0:
								giac_diz = facade_giacenza._get_giacenza_tessuto(fabb.diba.to_dictionary(), rif_bp=var_new['rif_intr'], id_fabb=fabb.id)
								# verifico giacenza libera
								giac_free = giac_diz['GIAC'] - giac_diz['GIAC_PRENOTATA'] + qta_da_imp
								if giac_free > 0:
									diff -= giac_free
								# disimpegna altri modelli
								if diff > 0:
									for id in giac_diz['ID_FABB_ALTRO']:
										if diff > 0:
											fabb_altro = Fabbisogno.get_by_key(id)
											diff, qta_da_imp = fabb_altro.disimpegna_dt(diff, riga['ti_rowid'])
											fabb_altro.salva()
								if diff > 0:
									raise ForwardException('Giacenza insufficiente per %s' % fabb.diba.capocmat)
							# infine aggiorno l'impegno pari alla qta messa nel bp
							fabb.aggiorna_impegno({'DT': dati_riga['qta_reale'],
							                       'id_faccom': riga['ti_rowid'],
							                       'qta_faccom': dati_riga['qta_reale'],
							                       'capi_faccom': dati_riga['tot_lanciati'],
							                       'qta_prelevata': dati_riga['qta_reale']
							                       })
						elif (riga.get('giacenza', '') == '1' and riga.get('da_ordinare', '') in ('', '-')):
							giac_art = facade_giacenza._get_giacenza_dt_by_capocmat(fabb.diba.to_dictionary())
							if giac_art:
								if float(giac_art[0]['qta']) >= float(dati_riga['qta_reale']):
									qta = dati_riga['qta_reale']
								else:
									qta = giac_art[0]['qta']
							else:
								raise ForwardException('Nessuna giacenza trovata per %s' % fabb.diba.capocmat)
							fabb.aggiorna_impegno({'DT': qta,
							                       'id_faccom': riga['ti_rowid'],
							                       'qta_faccom': dati_riga['qta_reale'],
							                       'capi_faccom': dati_riga['tot_lanciati'],
							                       'qta_prelevata': riga['qta_prelevata']
							})
							fabb.scala_impegni_prev_e_teor()
						elif riga.get('da_ordinare', '') == '*':
							# in caso in cui la riga fosse prelevabile e poi l'utente scelga,
							# in un secondo momento, di metterla come 'da ordinare'
							fabb.aggiorna_impegno({'DT': 0,
							                       'id_faccom': riga['ti_rowid'],
							                       'qta_faccom': dati_riga['qta_reale'],
							                       'capi_faccom': dati_riga['tot_lanciati']
							                       })
							fabb.scala_impegni_prev_e_teor()
						fabb_trovati.append(str(fabb.id))
						fabb.stato = 'valutazione'
						fabb.salva()
						trovato = True
				if not trovato:
					# se non lo trovo, creo il fabbisogno per quell'articolo (nuovo)
					self._crea_fabbisogno_reale(var_new, [riga])

		for fabb in fabb_diba_old:
			if str(fabb.id) not in fabb_trovati:
				# per tutti i fabbisogni presenti che non trovo piu'
				# ricalcolo il fabb reale e aggiorno anche il collegamento con la rich comm
				# se non presente lo azzero ed annullo gli impegni
				dati = fabbisogni_dao.calcola_lanciato(var_old, fabb.diba.capocmat)
				if dati:
					fabb.capi_lanciati = dati['tot_lanciati']
					fabb.qta_reale = dati['qta_reale']
					fabb.id_art_rich_comm = dati['id_art_rcomm']
				else:
					fabb.capi_lanciati = 0
					fabb.qta_reale = 0
					fabb.id_art_rich_comm = 0
				fabb.elimina_impegni_bp(id_righe_all_bp)
				fabb.salva()


	@db_provided()
	def _elimina_fabb_reale(self, bp_diz, conn=None):
		varianti_old = fabbisogni_dao.get_varianti_fabb_by_bp(bp_diz)
		for var_old in varianti_old:
			# prendo i fabbisogni gia' presenti a db (vecchi)
			fabb_diba_old = Fabbisogno.get_fabbisogni_diba(var_old.get('id_art_rcomm', ''))
			for fabb in fabb_diba_old:
				# per tutti i fabbisogni che non trovo piu'
				# ricalcolo il fabb reale e aggiorno anche il collegamento con la rich comm
				# se non presente lo azzero ed annullo gli impegni
				dati = fabbisogni_dao.calcola_lanciato(var_old, fabb.diba.capocmat)
				if dati:
					fabb.capi_lanciati = dati['tot_lanciati']
					fabb.qta_reale = dati['qta_reale']
					fabb.id_art_rich_comm = dati['id_art_rcomm']
				else:
					fabb.capi_lanciati = 0
					fabb.qta_reale = 0
					fabb.id_art_rich_comm = 0
				fabb.elimina_impegni_bp([str(riga['ti_rowid']) for riga in bp_diz['list_righe']])
				fabb.salva()



	@db_provided(transactional=True)
	def crea_fabbisogno_teor(self, dati):
		self._crea_fabbisogno_teor(dati)


	def _crea_fabbisogno_teor(self, dati, dati_art):
		try:
			dati_prod = dwg_dao.get_modelli_variante_prod(Request().get_pard(), dati['societa_orig'],
			                                              dati['modello_orig'], dati_art['variante'])
		except:
			raise ForwardException('Modello non trovato %(societa_orig)s-%(modello_orig)s-%%(variante)s' % dati % dati_art)
		else:
			ret = dwg_dao.get_diba_esplosa_originale(dati_prod[0]['societa'], dati_prod[0]['modello_produzione'],
			                                                  dati_prod[0]['variante'], debug=False)
			diba_esplosa = []
			result = sorted(ret[0]['data'], key=lambda k: (k['prodotto'], k['variante_prodotto'], k['prodotto_componente'],
			                k['misura_prodotto'], k['misura_prodotto_componente'], k['variante_prodotto_componente']))
			for k, g in itertools.groupby(result, key=lambda k: (k['prodotto'], k['variante_prodotto'], k['prodotto_componente'],
			                k['misura_prodotto'], k['misura_prodotto_componente'], k['variante_prodotto_componente'])):
				db_list = list(g)
				materiale = db_list[0].copy()
				if str(materiale['misura_prodotto']) in ('42', 'SM', 'M', 'null'):
					# if (str(materiale['gruppo_merceologico']), str(dati['societa_orig'])) in (('20', 'MM'), ('21', 'MM'), ('20', 'MA'), ('21', 'MA'), ('30', 'MN'), ('31', 'MN'), ('32', 'MN')):
					materiale['consumo_unitario_lordo'] = sum(float(d['consumo_unitario_lordo']) for d in db_list)
					materiale['posizione'] = ','.join(set([str(d['posizione']) for d in db_list if d.get('posizione','') and d['posizione'] and d['posizione']!=None]))
					materiale['nota'] = ''
					materiale['consumo_dettaglio'] = ''
					if len(db_list) > 1:
						materiale['nota'] = 'Consumo composto: %s' % '+'.join([str(d['consumo_unitario_lordo']) for d in db_list])
						materiale['consumo_dettaglio'] = ','.join([str(d['consumo_unitario_lordo']) for d in db_list])
					diba_esplosa.append(materiale)
					# else:
					# 	diba_esplosa.extend(db_list)
			for d in diba_esplosa:
				if str(d['gruppo_merceologico']) != '80':
					diz = {'diba': {},}
					diz['diba']['societa'] = dati['societa_orig']
					diz['diba']['articolo'] = d['progr_articolo']
					if d['anno'] != '0' and d['stagione'] != '0':
						if str(d['stagione']) == '1':
							stagione = 'P/E'
						else:
							stagione = 'A/I'
						diz['diba']['anno_stagione'] = "%s %s" %(d['anno'], stagione)
						diz['diba']['a_s'] = anno_stagione_lib.decodifica_as(diz['diba']['anno_stagione'])
					else:
						diz['diba']['anno_stagione'] = anno_stagione_lib.decode_as('PP')
						diz['diba']['a_s'] = 'PP'

					diz['diba']['gm'] = d['gruppo_merceologico']
					diz['diba']['pezzo'] = d['progr_pezzo']
					diz['diba']['colonna'] = d.get('progr_componente_coloritura', '')
					diz['diba']['posizione'] = d['posizione'] if d['posizione'] else ''
					diz['diba']['id_articolo'] = ''
					diz['diba']['descrizione'] = ''
					dati_art_orig = diba_dao.get_id_art_orig(diz['diba'])
					if dati_art_orig:
						diz['diba']['id_articolo'] = dati_art_orig[0]['id_articolo']
						diz['diba']['descrizione'] = dati_art_orig[0]['descrizione']

					diz['diba']['numero_riga'] = d['numero_riga']
					diz['diba']['taglia_base'] = d['misura_prodotto']
					diz['diba']['consumo'] = float(d['consumo_unitario_lordo']) if d.get('consumo_unitario_lordo','') != None else 1
					if diz['diba']['societa'] in ('MM', 'MA') and diz['diba']['numero_riga'] == '1110':
						diz['diba']['consumo'] = diba_dao.calcola_consumo_netto(Request().get_pard(), diz['diba']['societa'], d['materiale_componente'], diz['diba']['consumo'])
					diz['id_art_rich_comm'] = dati_art['id_art']
					diz['stato'] = 'valutazione'
					diz['nota'] = d.get('nota', '')
					diz['diba']['consumo_dettaglio'] = d.get('consumo_dettaglio', '')
					diz['diba']['societa_orig'] = dati['societa_orig']
					diz['diba']['modello_orig'] = dati['modello_orig']
					diz['diba']['nome_modello_orig'] = dati_prod[0]['nome']
					diz['diba']['misura'] = d['misura_prodotto_componente'][-2:] if d.get('misura_prodotto_componente','') != None else '00'
					if dati.get('flag_variante_orig', '') != '1':
						diz['diba']['colore'] = str(d['variante_prodotto_componente']).zfill(4)
						diz['diba']['macrocolore'] = ''
						diz['diba']['colore_descr'] = ''
						macrocol = diba_dao.get_macrocolore(diz['diba'])
						if macrocol:
							diz['diba']['macrocolore'] = macrocol[0]['macrocolore']
							diz['diba']['colore_descr'] = macrocol[0]['colore_descr']
						diz['diba']['variante_orig'] = dati_art['variante']
						diz['diba']['variante_dt'] = dati_art['variante']
					else:
						diz['diba']['colore'] = '0000'
						diz['diba']['macrocolore'] = ''
						diz['diba']['colore_descr'] = ''
						diz['diba']['variante_orig'] = ''
						diz['diba']['variante_dt'] = dati_art['variante']
					diz['capi_teor'] = int(dati_art['tot_capi'])
					diz['diba']['capocmat'] = str(diz['diba']['societa']) + str(diz['diba']['gm']) + str(diz['diba']['a_s']) + \
					                          str(diz['diba']['articolo']) + str(diz['diba']['misura']) + str(diz['diba']['colore'])
					diz['qta_teor'] = float(diz['diba']['consumo']) * int(diz['capi_teor'])
					fabb = Fabbisogno(diz)
					fabb.salva()


	@db_provided(transactional=True)
	def aggiorna_fabbisogno_teor(self, dati):
		self._aggiorna_fabbisogno_teor(dati)


	def _aggiorna_fabbisogno_teor(self, dati):
		lista_righe = []
		for rr in dati.get('list_righe', []):
			r_diz = rr.copy()
			r_diz['list_art'] = []
			for art in rr.get('list_art', []):
				if str(art.get('flag_originale', '')) != '0' and art['variante'] != '':
					r_diz['list_art'].append(art)
			if len(r_diz['list_art']) > 0:
				lista_righe.append(r_diz)
		for riga in lista_righe:
			for art in riga['list_art']:
				trovato = False
				if art.get('id_art_padre', '') != '' and str(art.get('id_art_padre', '')) != '0':
					fabb_diba = Fabbisogno.get_fabbisogni_diba(art.get('id_art_padre', ''))
					if fabb_diba:
						trovato = True
					for fabb in fabb_diba:
						# ricalcolo le qta capi dalle richieste standard e aggiorno il fabbisogno
						fabb.aggiorna_teorico(param={'collezione': dati['collezione'],
						                             'id_col': dati['id_col'],
						                             'anno_stagione': dati['anno_stagione'],
						                             'as_orig': riga['as_orig'],
						                             'societa_orig': riga['societa'],
						                             'modello_orig': riga['modello_orig'],
						                             'variante_orig': art['variante'],
						                             'variante_dt': art['variante']})
						fabb.salva()
				# vedo se avevo gia' inserito il fabbisogno per la rich produttiva
				# se si lo aggiorno
				elif art.get('id_art', '') != '' and str(art.get('id_art', '')) != '0':
					fabb_diba = Fabbisogno.get_fabbisogni_diba(art.get('id_art', ''))
					if fabb_diba:
						trovato = True
					for fabb in fabb_diba:
						# ricalcolo le qta capi dalle richieste standard e aggiorno il fabbisogno
						fabb.aggiorna_teorico(param={'collezione': dati['collezione'],
						                             'id_col': dati['id_col'],
						                             'anno_stagione': dati['anno_stagione'],
						                             'as_orig': riga['as_orig'],
						                             'societa_orig': riga['societa'],
						                             'modello_orig': riga['modello_orig'],
						                             'variante_orig': art['variante'],
						                             'variante_dt': art['variante']})
						fabb.salva()

				# se non esiste gia' un fabbisogno lo creo
				if not trovato:
					riga['societa_orig'] = riga.get('societa', '')
					self._crea_fabbisogno_teor(riga, art)


	@db_provided(transactional=True)
	def elimina_fabbisogno_teorico(self, testata, righe):
		self._elimina_fabbisogno_teorico(testata, righe)


	def _elimina_fabbisogno_teorico(self, testata, righe_list):
		for riga in righe_list:
			for art in riga['list_art']:
				if art.get('id_art_padre', '') != '' and str(art.get('id_art_padre', '')) != '0':
					fabb_diba = Fabbisogno.get_fabbisogni_diba(art.get('id_art_padre', ''))
					for fabb in fabb_diba:
						#ricalcolo le qta capi dalle richieste rimaste e aggiorno il fabbisogno
						fabb.aggiorna_teorico(param = {'collezione': testata['collezione'],
														'id_col': testata['id_col'],
														'anno_stagione': testata['anno_stagione'],
		                                               'as_orig': riga['as_orig'],
		                                               'societa_orig': riga['societa'],
		                                               'modello_orig': riga['modello_orig'],
		                                               'variante_orig': art['variante'],
		                                               'variante_dt': art['variante']
						                               })
						fabb.salva()
				else:
					fabb_diba = Fabbisogno.get_fabbisogni_diba(art.get('id_art_padre', ''))
					for fabb in fabb_diba:
						fabb.elimina()



	@db_provided()
	def archivia_fabbisogno(self, id_art_list):
		self._archivia_fabbisogno(id_art_list)


	def _archivia_fabbisogno(self, id_art_list):
		for id_art in id_art_list:
			fabb_list = Fabbisogno.get_fabbisogni_diba(id_art)
			for fabb in fabb_list:
				fabb.archivia()
				fabb.salva()


	@db_provided()
	def aggiorna_fabbisogno_prelevato(self, rif_intr, ti_rowid, qta_prelevata, id_picking, conn=None):
		facade_giacenza = GiacenzaMPFacade(Request().get_pard())
		notifica_modelli = []
		impegni_da_notificare = []
		storno = False
		if float(qta_prelevata) < 0:
			storno = True
		qta = abs(round(float(qta_prelevata)/100, 2)) #nel BP e' tutto gestito con interi (*100)
		qta_da_scalare = qta
		num_impegni = 0
		try:
			dati_bp = prelievoDB.get_testata_bp(rif_intr)[0]
		except IndexError:
			raise ForwardException('Nessun BP trovato %s'%rif_intr)
		fabb_list = Fabbisogno.get_fabbisogni_by_prelievo(ti_rowid)
		if fabb_list:
			for fabb in fabb_list:
				num_impegni += len(fabb.list_impegni)
				for imp in fabb.list_impegni:
					if round(qta_da_scalare, 2) >0:
						if not storno and imp.da_prelevare > 0.00:
							if imp.da_prelevare >= float(qta_da_scalare):
								imp.qta_prelevata += float(qta_da_scalare)
								qta_da_scalare = 0
							else:
								imp.qta_prelevata += imp.da_prelevare
								qta_da_scalare -= imp.da_prelevare
						elif storno and imp.qta_prelevata > 0.00:
							if imp.qta_prelevata >= float(qta_da_scalare):
								imp.qta_prelevata -= float(qta_da_scalare)
								qta_da_scalare = 0
							else:
								qta_da_scalare -= imp.qta_prelevata
								imp.qta_prelevata = 0

			# se con gli impegni fatti non ho esaurito la qta considerata nel picking
			# cerco prima in giacenza e poi negli altri impegni
			if round(qta_da_scalare, 2) > 0:
				if not storno:
					# divido la qta del picking in piu' per gli impegni trovati
					if num_impegni >0:
						qta_da_scalare_parz = round(float(qta_da_scalare)/num_impegni,2)
					else:
						qta_da_scalare_parz = qta_da_scalare
					for fabb in fabb_list:
						for imp in fabb.list_impegni:
							imp.qta_prelevata += qta_da_scalare_parz
							# notifica = 'Qta impegno superata %(modello_originale)s-%(nome_originale)s<br>' % dati_bp
							# impegni_da_notificare.append({'destinatario': imp.utente,
							#                               'id_impegno': imp.id,
							#                               'id_pick': id_picking,
							#                               'msg': notifica})
							# notifica_modelli.append(notifica)

					# prima verifico se c'e' giacenza libera, se si non scalo altri impegni
					res_giac = facade_giacenza._get_giacenza_articolo(fabb_list[0].to_dictionary()['diba'])
					if res_giac and float(res_giac[0]['FREE_DT']) > 0.00:
						if float(res_giac[0]['FREE_DT']) >= float(qta_da_scalare):
							qta_da_scalare = 0
						else:
							qta_da_scalare -= float(res_giac[0]['FREE_DT'])

					#se anche la giacenza libera non e' sufficiente
					if round(qta_da_scalare, 2) >0:
						res = fabbisogni_dao.get_altri_impegni_giacenza_by_articolo(fabb_list[0].to_dictionary()['diba'])
						# scalare la qta impegnata sugli altri
						for r in res:
							if round(qta_da_scalare, 2) > 0:
								imp_altro = Impegno.get_by_key(r['id_impegno'])
								if imp_altro.da_prelevare >= float(qta_da_scalare):
									imp_altro.qta -= float(qta_da_scalare)
									qta_da_scalare = 0
								else:
									imp_altro.qta = imp_altro.qta_prelevata
									qta_da_scalare -= float(imp_altro.da_prelevare)
								imp_altro.salva(r['id_fabb'])
								notifica = 'Qta impegno non sufficiente per %(modello_originale)s-%(nome_originale)s<br>' % dati_bp + \
								           'altri modelli coinvolti: -> %(modello_orig)s - %(nome_modello_orig)s' % r
								impegni_da_notificare.append({'destinatario': imp_altro.utente,
								                              'id_impegno': imp_altro.id,
								                              'id_pick': id_picking,
								                              'msg': notifica})
								notifica_modelli.append(notifica)
				# else:
				# 	if fabb_list:
					# 	notifica_modelli.append(
					# 		'Nessun impegno da stornare trovato per %(modello_originale)s-%(nome_originale)s.' % dati_bp)
			#dopo le modifiche agli oggetti li salvo
			for fabb in fabb_list:
				fabb.salva(FLAG_CHECK=Request().get_val('flag_check', True))
			# e salvo la tabella di notifica
			for diz in impegni_da_notificare:
				fabbisogni_dao.insert_impegni_notifica(diz)

		return notifica_modelli


	@db_provided()
	def aggiorna_fabbisogno_manca(self, rif_bp, old_id_faccom, new_id_faccom, diff, conn=None):
		q_faccom_tot = 0
		fabb_list = Fabbisogno.get_fabbisogni_by_prelievo(old_id_faccom)
		for fabb in fabb_list:
			for imp in fabb.list_impegni:
				if str(imp.id_faccom) == str(old_id_faccom) and imp.stato == 'valido':
					q_faccom_tot += float(imp.qta_faccom)
		# if len(fabb_list) > 1:
		# 	raise ForwardException('Impossibile esistano piu fabbisogni per stesso BP. %s'%(str([f.to_dictionary() for f in fabb_list])))
		for fabb in fabb_list:
			capi = 0
			manca = 0
			impegnato_diff = 0
			for imp in fabb.list_impegni:
				if str(imp.id_faccom) == str(old_id_faccom) and imp.stato == 'valido':
					manca = round(float(imp.qta_faccom) * float(diff) / q_faccom_tot, 2)
					imp.qta_faccom -= manca
					if imp.qta > imp.qta_faccom:
						impegnato_diff += imp.qta - imp.qta_faccom
						imp.qta = imp.qta_faccom
					capi = imp.capi_faccom
			imp = {}
			imp['qta'] = round(impegnato_diff,2)
			imp['qta_faccom'] = manca
			imp['capi_faccom'] = capi
			imp['id_faccom'] = new_id_faccom
			imp['tipo'] = 'DT'
			imp['utente'] = fabb.list_impegni[0].utente
			res = prelievoDB.get_testata_bp(rif_bp)
			if res:
				imp['utente'] = res[0]['utente_nas']
			fabb.list_impegni.append(Impegno(imp))
			fabb.salva()


	@db_provided()
	def get_sblocca_fabbisogni(self, params):
		ord_facade = OrdineMateriaPrimaFacade(Request().get_pard())
		fabb_list = []
		res = Fabbisogno.get_fabbisogni_by_articolo(params)
		for r in res:
			if r.id not in params['id_fabbs'] and r.list_impegni:
				presenza_bp = r.check_presenza_bp()
				diz = r.to_dictionary()
				diz['id_faccom'] = ''
				diz['qta_faccom'] = 0
				diz['capi_faccom'] = 0
				diz['rif_bp'] = ''
				diz['id_ord'] = ''
				diz['id_fac_d'] = ''
				diz['id_preord'] = ''
				diz['id_fac_preord_d'] = ''
				list_impegni = sorted([imp for imp in diz['list_impegni']], key = lambda k:(k['id_faccom']))
				if list_impegni:
					# imp_trovato = False
					for k, g in itertools.groupby(list_impegni, key = lambda k:(k['id_faccom'])):
						diz_imp = {}
						diz_imp['da_prelevare'] = 0
						diz_imp['qta_prelevata'] = 0
						diz_imp['fabbisogno'] = 0
						diz_imp.update(diz.copy())
						diz_imp['list_impegni'] = list(g)
						if k and str(k) != '0':
							diz_imp['tot_capi'] = sum(int(g['capi_faccom']) for g in diz_imp['list_impegni'] if g['capi_faccom'] and g['tipo'] == 'DT')
							diz_imp['fabbisogno'] = sum(float(g['qta_faccom']) for g in diz_imp['list_impegni'] if g['qta_faccom'] and g['tipo'] == 'DT')
							diz_imp['id_faccom'] = k
							diz_imp['capi_faccom'] = sum(int(g['capi_faccom']) for g in diz_imp['list_impegni'] if g['capi_faccom'] and g['tipo'] == 'DT')
							diz_imp['qta_faccom'] = sum(float(g['qta_faccom']) for g in diz_imp['list_impegni'] if g['qta_faccom'] and g['tipo'] == 'DT')
							diz_imp['da_prelevare'] = sum(float(g['da_prelevare']) for g in diz_imp['list_impegni'] if g['da_prelevare'] and g['tipo'] == 'DT')
							diz_imp['qta_prelevata'] = sum(float(g['qta_prelevata']) for g in diz_imp['list_impegni'] if g['qta_prelevata'] and g['tipo'] == 'DT')
							rif_bp = fabbisogni_dao.get_qta_bp_by_impegno({'id_faccom': k,
							                                               'qta_capi': diz_imp['tot_capi'],
							                                               'variante_dt': diz_imp['diba']['variante_dt'],
							                                               'qta': diz_imp['fabbisogno']})
							if rif_bp:
								diz_imp['rif_bp'] = rif_bp['rif_intr']
								diz_imp['tot_capi'] = rif_bp['tot_lanciati']
								diz_imp['capi_faccom'] = rif_bp['tot_lanciati']
								diz_imp['fabbisogno'] = rif_bp['qta_reale']
								diz_imp['qta_faccom'] = rif_bp['qta_reale']
						elif presenza_bp:
							trovato = False
							for imp in diz_imp['list_impegni']:
								if imp['qta'] > 0.00:
									trovato = True
									diz_imp['da_prelevare'] = imp['da_prelevare']
									diz_imp['qta_prelevata'] = imp['qta_prelevata']
							if not trovato:
								continue
						diz_imp['da_soddisfare'] = diz_imp['fabbisogno']
						for g in diz_imp['list_impegni']:
							if g['stato'] == 'valido':
								diz_imp['da_soddisfare'] -= g['qta']
							if g['tipo'] == 'ORD':
								if g['rif_ord'] != '0':
									data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), g['rif_ord'])
									if data_utils:
										diz_imp['id_ord'] = data_utils['id_ordine']
										diz_imp['id_fac_d'] = data_utils['rif_intr']
							elif g['tipo'] == 'SOC':
								if g['rif_ord'] != '0':
									data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), g['rif_ord'])
									if data_utils:
										diz_imp['id_preord'] = data_utils['id_ordine']
										diz_imp['id_fac_preord_d'] = data_utils['rif_intr']
						if float(diz_imp['da_prelevare']) > 0 or float(diz_imp['fabbisogno']) > float(diz_imp['qta_prelevata']):
							fabb_list.append(diz_imp)
							# imp_trovato = True
					# if not imp_trovato:
					# 	fabb_list.append(diz.copy())
				else:
					fabb_list.append(diz.copy())
			# for fabb_obj in fabb_list:
			# 	diz = fabb_obj.to_dictionary()
			# 	diz['id_faccom'] = ''
			# 	diz['rif_bp'] = ''
			# 	diz['id_ord'] = ''
			# 	diz['id_fac_d'] = ''
			# 	diz['id_preord'] = ''
			# 	diz['id_fac_preord_d'] = ''
			# 	prelevato = False
			# 	lista_impegni = [i.to_dictionary() for i in r.list_impegni]
			# 	lista_impegni = sorted(lista_impegni, key = lambda k: (k['id_faccom']))
			# 	for k,g in itertools.groupby(lista_impegni, key = lambda k: (k['id_faccom'])):
			# 		lista_imp = list(g)
			# 		for imp in lista_imp:
			# 			if imp['tipo'] == 'DT':
			# 				if imp['qta'] > 0.00 and imp['da_prelevare'] <= 0.0:
			# 					prelevato = True
			# 				# trovato = True
			# 				diz['id_faccom'] = imp['id_faccom']
			# 				if imp['id_faccom']:
			# 					rif_bp = fabbisogni_dao.get_bp_by_impegno(imp['id_faccom'])
			# 					if rif_bp:
			# 						diz['rif_bp'] = rif_bp[0]['rif_intr']
			# 				# lista_fabb.append(diz.copy())
			# 			elif imp['tipo'] == 'ORD':
			# 				# trovato = True
			# 				if imp['qta'] > 0 and imp['rif_ord'] != '0':
			# 					data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), imp['rif_ord'])
			# 					if data_utils:
			# 						diz['id_ord'] = data_utils['id_ordine']
			# 						diz['id_fac_d'] = data_utils['rif_intr']
			# 						#diz['data_cons_ord'] = data_utils['data_cons_conc']
			# 			elif imp['tipo'] == 'SOC':
			# 				# trovato = True
			# 				if imp['qta'] > 0 and imp['rif_ord'] != '0':
			# 					data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), imp['rif_ord'])
			# 					if data_utils:
			# 						diz['id_preord'] = data_utils['id_ordine']
			# 						diz['id_fac_preord_d'] = data_utils['rif_intr']
			# 						#diz['data_cons_preord'] = data_utils['data_cons_conc']
			# 		if not prelevato:
			# 			fabb_list.append(diz.copy())
		scorta_sede_list = ord_facade.get_scorta_sede_by_articolo(params)
		for s in scorta_sede_list:
			if s['id_facord_dett'] not in params['id_facord_detts'] and s['list_impegni']:
				fabb_list.append(s)

		return fabb_list


	@db_provided()
	def get_fabbisogni_da_gestire(self, id_dibas):
		dati_out = []
		lista_fabb = []
		lista_diba = []
		for id in id_dibas:
			fabb_list = Fabbisogno.get_by_id_diba(id)
			for fabb_obj in fabb_list:
				presenza_bp = fabb_obj.check_presenza_bp()
				diz = fabb_obj.to_dictionary()
				diz['id_faccom'] = ''
				diz['qta_faccom'] = 0
				diz['capi_faccom'] = 0
				diz['rif_bp'] = ''
				diz['id_ord'] = ''
				diz['id_fac_d'] = ''
				diz['id_preord'] = ''
				diz['id_fac_preord_d'] = ''
				list_impegni = sorted([imp for imp in diz['list_impegni']], key = lambda k:(k['id_faccom']))
				if list_impegni:
					imp_trovato = False
					for k, g in itertools.groupby(list_impegni, key = lambda k:(k['id_faccom'])):
						diz_imp = {}
						diz_imp['da_prelevare'] = 0
						diz_imp['qta_prelevata'] = 0
						diz_imp['fabbisogno'] = 0
						diz_imp.update(diz.copy())
						diz_imp['list_impegni'] = list(g)
						if k and str(k) != '0':
							diz_imp['tot_capi'] = sum(int(g['capi_faccom']) for g in diz_imp['list_impegni'] if g['capi_faccom'] and g['tipo'] == 'DT')
							diz_imp['fabbisogno'] = sum(float(g['qta_faccom']) for g in diz_imp['list_impegni'] if g['qta_faccom'] and g['tipo'] == 'DT')
							diz_imp['id_faccom'] = k
							diz_imp['capi_faccom'] = sum(int(g['capi_faccom']) for g in diz_imp['list_impegni'] if g['capi_faccom'] and g['tipo'] == 'DT')
							diz_imp['qta_faccom'] = sum(float(g['qta_faccom']) for g in diz_imp['list_impegni'] if g['qta_faccom'] and g['tipo'] == 'DT')
							diz_imp['da_prelevare'] = sum(float(g['da_prelevare']) for g in diz_imp['list_impegni'] if g['da_prelevare'] and g['tipo'] == 'DT')
							diz_imp['qta_prelevata'] = sum(float(g['qta_prelevata']) for g in diz_imp['list_impegni'] if g['qta_prelevata'] and g['tipo'] == 'DT')
							rif_bp = fabbisogni_dao.get_qta_bp_by_impegno({'id_faccom': k,
							                                               'qta_capi': diz_imp['tot_capi'],
							                                               'variante_dt': diz_imp['diba']['variante_dt'],
							                                               'qta': diz_imp['fabbisogno']})
							if rif_bp:
								diz_imp['rif_bp'] = rif_bp['rif_intr']
								diz_imp['tot_capi'] = rif_bp['tot_lanciati']
								diz_imp['capi_faccom'] = rif_bp['tot_lanciati']
								diz_imp['fabbisogno'] = rif_bp['qta_reale']
								diz_imp['qta_faccom'] = rif_bp['qta_reale']
						elif presenza_bp:
							trovato = False
							for imp in diz_imp['list_impegni']:
								if imp['qta'] > 0.00:
									trovato = True
									diz_imp['da_prelevare'] = imp['da_prelevare']
									diz_imp['qta_prelevata'] = imp['qta_prelevata']
							if not trovato:
								continue
						diz_imp['da_soddisfare'] = diz_imp['fabbisogno']
						for g in diz_imp['list_impegni']:
							if g['stato'] == 'valido':
								diz_imp['da_soddisfare'] -= g['qta']
							if g['tipo'] == 'ORD':
								if g['rif_ord'] != '0':
									data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), g['rif_ord'])
									if data_utils:
										diz_imp['id_ord'] = data_utils['id_ordine']
										diz_imp['id_fac_d'] = data_utils['rif_intr']
							elif g['tipo'] == 'SOC':
								if g['rif_ord'] != '0':
									data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), g['rif_ord'])
									if data_utils:
										diz_imp['id_preord'] = data_utils['id_ordine']
										diz_imp['id_fac_preord_d'] = data_utils['rif_intr']
						if float(diz_imp['da_prelevare']) > 0 or float(diz_imp['fabbisogno']) > float(diz_imp['qta_prelevata']):
							lista_fabb.append(diz_imp)
							imp_trovato = True
					if not imp_trovato:
						lista_diba.append(diz.copy())
				else:
					lista_fabb.append(diz.copy())
		lista_fabb = sorted(lista_fabb, key=lambda k:(k['diba']['societa'], k['diba']['anno_stagione'], k['diba']['gm'],
			                                            k['diba']['articolo'], k['diba']['misura'], k['diba']['colore']))
		if lista_fabb:
			for k,g in itertools.groupby(lista_fabb, key=lambda k:(k['diba']['societa'], k['diba']['anno_stagione'], k['diba']['gm'],
			                                                     k['diba']['articolo'], k['diba']['misura'], k['diba']['colore'])):
				gruppo_fabb = list(g)
				diz = {}
				diz['diba'] = gruppo_fabb[0]['diba'].copy()
				diz['fabb_list'] = gruppo_fabb
				dati_out.append(diz)
		# se non ci sono fabbisogni da gestire, mando fuori solo la DiBa e le sue giacenze
		# (es. quando tutte la quantita' sono state prelevate dai BP)
		elif lista_diba:
			for k, g in itertools.groupby(lista_diba, key=lambda k: (k['diba']['societa'], k['diba']['anno_stagione'],
			                                                         k['diba']['gm'],k['diba']['articolo'], k['diba']['misura'], k['diba']['colore'])):
				gruppo_fabb = list(g)
				diz = {}
				diz['diba'] = gruppo_fabb[0]['diba'].copy()
				diz['fabb_list'] = []
				dati_out.append(diz)
		return dati_out


	@db_provided()
	def get_studio_diba(self, id_righe_r_comm):
		dati_out = []
		for id in id_righe_r_comm:
			fabb_diba = Fabbisogno.get_fabbisogni_diba(id)
			dati_out.extend([fab.to_dictionary() for fab in fabb_diba])
		for f in dati_out:
			f.update(f['diba'])
			f.pop('diba')
		return dati_out


	@db_provided(transactional=True)
	def salva_impegno(self, fabb_data):
		from facon.magmp.facade.bp_facade import BPFacade
		bp_facade = BPFacade(Request().get_pard())

		facade_giacenza = GiacenzaMPFacade(Request().get_pard())
		out_list = []
		id_list_salvati = []
		for elem in fabb_data:
			check_cambio_diba = True
			presenza_bp_orig = False
			if elem.get('id', '') and elem['id']:
				fabb = Fabbisogno.get_by_key(elem['id'], id_faccom_list=elem.get('id_faccom', ''))
				fabb.set_testata(elem)
				id_list_salvati.append(str(fabb.id))
			else:
				fabb_orig = None
				if elem['isDuplicato'] and elem['id_fabb_orig'] and str(elem['id_fabb_orig']) not in id_list_salvati:
					qta_duplica = float(elem['fabbisogno'])
					fabb_orig = Fabbisogno.get_by_key(elem['id_fabb_orig'],id_faccom_list=elem.get('id_faccom', ''))
					fabb_orig.aggiorna_fabbisogno_orig(elem)
					# verifico che il fabbisogno sia stato sostituito,
					# se non e' cambiato non devo creare un nuovo fabb
					if fabb_orig.diba.capocmat == elem['diba']['capocmat']:
						check_cambio_diba = False
					# aggiorno la riga del buono di prelievo se associato (qta)
					for imp_orig in fabb_orig.list_impegni:
						if imp_orig.id_faccom and str(imp_orig.id_faccom) != '0' and str(elem['id_faccom'])!='0' \
						and str(imp_orig.id_faccom) == str(elem['id_faccom']) and imp_orig.stato == 'valido':
							imp_orig.qta_faccom = imp_orig.qta_faccom - qta_duplica
							presenza_bp_orig = True
					if presenza_bp_orig:
						diz = fabb_orig.to_dictionary()
						diz['id_faccom'] = str(elem['id_faccom'])
						id_faccom = bp_facade.salva_riga_bp_by_fabb(diz, qta_da_scalare=qta_duplica, flag_da_scalare=presenza_bp_orig)
						elem['qta_faccom'] = qta_duplica
						fabb_orig.scala_impegni_prev_e_teor()
					fabb_orig.salva()
				if check_cambio_diba:
					# se e' nuovo o sostituito
					fabb = Fabbisogno(elem)
				else:
					fabb = fabb_orig

			fabb.aggiorna_impegno(elem)
			facade_giacenza._check_minimo_giacenza(fabb.to_dictionary())

			# aggiorno la riga del buono di prelievo se associato
			# in caso di riga duplicata, duplico la riga del bp
			# in caso di stessa riga aggiorno capocmat e qta
			presenza_bp = False
			new_id_faccom = ''
			if elem.get('id_faccom', '') and str(elem['id_faccom']) != '0':
				presenza_bp = True
				new_id_faccom = bp_facade.salva_riga_bp_by_fabb(elem, elem.get('isDuplicato', ''), qta_da_scalare=elem['qta_faccom'])
				for imp in fabb.list_impegni:
					if imp.stato == 'valido':
						# rimosso 15-01-2018: per non modificare la qta nel bp, se riga gia' esistente
						# imp.qta_faccom = float(elem.get('DT', 0)) + float(elem.get('ORD', 0)) + float(elem.get('SOC', 0))
						if check_cambio_diba:
							imp.id_faccom = new_id_faccom

			if new_id_faccom:
				elem['id_faccom'] = new_id_faccom
			fabb.aggiorna_impegno(elem)
			facade_giacenza._check_minimo_giacenza(fabb.to_dictionary())
			if presenza_bp:
				fabb.scala_impegni_prev_e_teor()
			fabb.salva()
			out_list.append(fabb.to_dictionary())
		return out_list


	# @db_provided(transactional=True)
	# def check_minimo_giacenza(self, fabb_data, giac_data):
	# 	for imp in fabb_data.list_impegni:
	# 		# tools.dump(imp)
	# 		if(imp.tipo == 'SOC' and imp.qta > giac_data['GIAC_SOC']):
	# 			raise ForwardException("Errore: giacenza insufficente a soddisfare il presente impegno: %(tipo)s: %(qta)s" %imp.to_dictionary())


	@db_provided(transactional=True)
	def arretra_diba(self, fabb_data):
		out_list = []
		for elem in fabb_data:
			if elem.get('id_fabb', '') and elem['id_fabb']:
				fabb = Fabbisogno.get_by_key(elem['id_fabb'])
				fabb.arretra_avanza_diba('bozza')
				fabb.salva()
				out_list.append(fabb.to_dictionary())

		return out_list


	@db_provided(transactional=True)
	def avanza_diba(self, fabb_data):
		out_list = []
		for elem in fabb_data:
			if elem.get('id_fabb', '') and elem['id_fabb']:
				fabb = Fabbisogno.get_by_key(elem['id_fabb'])
				fabb.arretra_avanza_diba('valutazione')
				fabb.salva()
				out_list.append(fabb.to_dictionary())

		return out_list

	@db_provided(transactional=True)
	def salva_diba(self, fabb_data):
		out_list = []
		for elem in fabb_data:
			if elem.get('id_fabb', '') and elem['id_fabb']:
				fabb = Fabbisogno.get_by_key(elem['id_fabb'])
				fabb.set_attributi_diba(elem)
				fabb.salva()

				fabb_to_send = fabb.to_dictionary()
				diba = fabb_to_send.pop('diba')
				fabb_to_send.update(diba)

				out_list.append(fabb_to_send)
		return out_list


	@db_provided(transactional=True)
	def impegna_by_tessuto(self, tessuto, riga):
		self._impegna_by_tessuto(tessuto, riga)


	def _impegna_by_tessuto(self, tessuto, riga):
		fabb = Fabbisogno.get_fabbisogni_by_tessuto(riga['list_art'][0]['id_art'], tessuto)
		fabb.impegna_tessuto(riga['id_facord_dett'])
		#tools.dump(fabb.to_dictionary())
		fabb.salva()


	@db_provided(transactional=True)
	def impegna_by_fabb(self, dati_impegni):
		self._impegna_by_fabb(dati_impegni)


	def _impegna_by_fabb(self, impegni_diz):
		"""	input:
			diz_in['rif_ord'] = ord_obj.rif_intr
			diz_in['impegni_ord'] = [(r.id_facord_dett, '', r.id_fabbisogno) for r in ord_obj.dettagli]
			diz_in['impegni_soc'] = [(preord_obj.id_facord_dett, preord_obj.id_preord, preord_obj.id_fabbisogno)]
		"""
		for (tipo, dati_impegni) in impegni_diz.items():
			for (rif_ord, rif_preord, id_fabb, id_faccom) in dati_impegni:
				fabb = Fabbisogno.get_by_key(id_fabb, id_faccom_list=id_faccom)
				fabb.impegna_ordine(tipo, rif_ord, rif_preord)
				fabb.salva()


	@db_provided(transactional=True)
	def salva_articolo_duplicato(self, dict_data):
		fabb = Fabbisogno(dict_data)
		fabb.salva()
		return fabb.to_dictionary()


	@db_provided()
	def get_ordini_da_fabbisogni(self, list_data):
		fabb = []
		for l in list_data:
			diba = l.pop('diba')
			imp = l.pop('list_impegni')
			for i in imp:
				if ((i['tipo'] == 'ORD' and str(i['rif_ord']) in ('','0')) or (i['tipo']== 'SOC' and str(i['rif_preord']) in ('','0'))) \
					and i['stato'] == 'valido':
					diz = {}
					diz.update(l)
					diz['id_fabb'] = diz['id']
					diz.update(diba)
					diz.update(i)
					rich = richiesteDB.get_dati_richiesta_by_id_art(l['id_art_rich_comm'])
					if rich:
						diz.update(rich[0])
					else:
						diz['id_coll'] = ''
						diz['collezione_dt'] = ''
						diz['anno_stagione_dt'] = ''
						diz['societa_orig'] = ''
						diz['collezione_orig'] = ''
						diz['as_orig'] = ''
					if diz['qta'] > 0:
						fabb.append(diz)
		#tools.dump(fabb)
		lista_ordini = []
		fabb = sorted(fabb, key=lambda k: (k['tipo'], k['societa_orig'], k['anno_stagione_dt'], k['as_orig'], k['id_coll'], k['collezione_dt'],
		                                   k['societa'], k['anno_stagione'], k['gm'], k['articolo'], k['id_articolo'],
		                                   k['id_fabb'], k['id_faccom'], k['misura'], k['colore'], k['modello_orig'], k['collezione_orig'], k['fornitore']))

		for k, g in itertools.groupby(fabb, key=lambda k: (k['tipo'],k['societa_orig'], k['anno_stagione_dt'], k['as_orig'],
		                                                   k['id_coll'], k['collezione_dt'], k['societa'], k['anno_stagione'],
		                                                   k['gm'], k['articolo'], k['id_articolo'], k['fornitore'])):
			gruppo = list(g)
			ordine = {}
			ordine['tipo'] = k[0]
			ordine['societa'] = k[1]
			ordine['anno_stagione'] = k[2]
			ordine['anno_stagione_orig'] = k[3]
			ordine['id_coll'] = k[4]
			ordine['collezione_dt'] = k[5]
			try:
				collNumber = int(ordine['collezione_dt'])
				collIsNumber = True
			except:
				collIsNumber = False
			if collIsNumber:
				cmatsetCollDt = ordiniDB.get_collezione(Request().get_pard(), ml=ordine['collezione_dt'])
				if cmatsetCollDt:
					ordine['collezione_dt'] = cmatsetCollDt[0]['cmatset']
					ordine['id_coll'] = cmatsetCollDt[0]['id_col']
			ordine['num_ordine'] = ''
			ordine['data_ordine'] = time.strftime('%d/%m/%Y', time.strptime(tools.get_date(), '%Y-%m-%d'))
			ordine['stato_ordine'] = 'BOZZA'
			ordine['tipo_ordine'] = ''
			ordine['fam_gm'] = ''
			fam_gm = ordiniDB.get_famgm2('%s%s' % (k[6], k[8]))
			if fam_gm:
				ordine['fam_gm'] = fam_gm[0]['fam_gm']
			if ordine['fam_gm'] in ('10'):
				ordine['tipologia'] = 'T'
				ordine['gestione'] = 'P'
			elif ordine['fam_gm'] in ('15', '16', '17'):
				ordine['tipologia'] = 'A'
				ordine['gestione'] = 'P'
			else:
				ordine['tipologia'] = 'A'
				ordine['gestione'] = 'C'
			ordine['id_articolo'] = k[10]
			ordine['soc_art_orig'] = k[6]
			ordine['art_orig'] = k[9]
			ordine['art_desc_orig'] = gruppo[0]['descrizione']
			ordine['capocmat'] = gruppo[0]['capocmat'][:9]
			dati_capocmat = {
					'societa': k[6],
					'gm': k[8],
					'articolo': k[9],
					'anno_stagione': k[7]
			}
			dati_fornitore = {
					'societa': k[6],
					'gm': k[8],
					'id_articolo': k[10],
					'articolo': k[9],
					'anno_stagione': k[7],
					'fornitore_col_forn': k[11]
			}
			ordine['fornitore'] = ''
			ordine['art_forn'] = ''
			ordine['ragione_sociale'] = ''
			ordine['id_articolo_fornitore'] = 0
			ordine['id_faccom_list'] = list(set([g['id_faccom'] for g in gruppo if g.get('id_faccom','') and g['id_faccom']])) #and str(g['id_faccom']) != '0'
			ordine['fornitori_list'] = []
			if k[0] == 'SOC':
				#se stock societa prendo il materiale dalla societa in base al capocmat
				ordine['societa'] = k[6]
				ordine['tipo_ordine'] = 'Stock_societa'
				if ordine['societa'] == 'MM':
					ordine['fornitore'] = '20000009'
				elif ordine['societa'] == 'MN':
					ordine['fornitore'] = '20000010'
				elif ordine['societa'] == 'MA':
					ordine['fornitore'] = '20000008'

				forn = diba_dao.get_dati_articolo_societa(ordine)
				if forn:
					ordine['id_articolo_fornitore'] = forn[0]['id_articolo_fornitore']
					ordine['art_forn'] = forn[0]['articolo_fornitore']
					ordine['ragione_sociale'] = forn[0]['fornitore']
			elif k[0] == 'ORD':
				ordine['tipo_ordine'] = 'Standard'
				if k[11]:
					forn_princ = ordiniDB.ricerca_colore_fornitore_desc(Request().get_pard(), dati_fornitore)
					if forn_princ:
						ordine['fornitore'] = forn_princ[0]['conto_fornitore_col_forn']
						ordine['art_forn'] = forn_princ[0]['articolo_fornitore_col_forn']
						ordine['ragione_sociale'] = forn_princ[0]['ragione_sociale_col_forn']
						ordine['id_articolo_fornitore'] = forn_princ[0]['id_articolo_fornitore_col_forn']
				else:
					forn_princ = diba_dao.get_dati_articolo_orig(dati_capocmat)
					if forn_princ:
						ordine['fornitore'] = forn_princ[0]['conto']
						ordine['art_forn'] = forn_princ[0]['articolo_fornitore']
						ordine['ragione_sociale'] = forn_princ[0]['fornitore']
						ordine['id_articolo_fornitore'] = forn_princ[0]['id_articolo_fornitore']
				forn_list = diba_dao.get_articoli_fornitore(dati_capocmat)
				ordine['fornitori_list'] = forn_list

			ordine['dettaglio'] = []
			ordine['qta_tot'] = 0
			for kk, gg in itertools.groupby(gruppo, key=lambda kk: (kk['id_fabb'], kk['id_faccom'], kk['misura'], kk['colore'], kk['modello_orig'], kk['collezione_orig'], kk['variante_dt'])):
				dett = list(gg)
				riga = dett[0].copy()
				riga['qta'] = 0
				for d in dett:
					riga['qta'] += float(d.get('qta', 0))
				ordine['dettaglio'].append(riga)
				ordine['qta_tot'] += riga['qta']
			if ordine['qta_tot'] > 0:
				lista_ordini.append(ordine)

		return lista_ordini


	@db_provided()
	def get_disimpegno_ordini_da_gestione_impegni(self, params):
		#ord_facade = OrdineMateriaPrimaFacade(Request().get_pard())

		fabb_list = []
		for f in params:
			if f.get('id', '') and f['id']:
				fabb = Fabbisogno.get_impegni_by_fabbisogno(f)
				if fabb.list_impegni:
					for l in fabb.list_impegni:
						if 'ORD' in l.tipo and l.rif_ord not in ['0',''] and l.qta > 0:
							dati_ordine = ordiniDB.get_ordine_by_id_facord_dett(l.rif_ord)
							for d in dati_ordine:
								d['diba'] = {}
								d['visible'] = 0
								if str(d['id_facord_dett']) == l.rif_ord:
									d['visible'] = 1
									d.update(fabb.to_dictionary())
								else:
									d['diba'].update(fabb.diba.to_dictionary())
								fabb_list.append(d)
						if 'SOC' in l.tipo and l.rif_preord not in ['0',''] and l.qta > 0:
							dati_preordine = ordiniDB.get_preordine_by_id_facord_dett(l.rif_ord)
							for d in dati_preordine:
								d['diba'] = {}
								d['visible'] = 0
								if str(d['id_facord_dett']) == l.rif_ord:
									d['visible'] = 1
									d.update(fabb.to_dictionary())
								else:
									d['diba'].update(fabb.diba.to_dictionary())
								fabb_list.append(d)
			elif f.get('id_facord_dett', '') and f['id_facord_dett']:
				for l in f['list_impegni']:
					if 'ORD' in l['tipo'] and l['rif_ord'] not in ['0', ''] and l['qta'] > 0:
						dati_ordine = ordiniDB.get_ordine_by_id_facord_dett(l['rif_ord'])
						for d in dati_ordine:
							d['diba'] = {}
							d['visible'] = 0
							if str(d['id_facord_dett']) == l['rif_ord']:
								d['visible'] = 1
								d.update(f.copy())
							else:
								d['diba'].update(f['diba'].copy())
							d['nome_modello'] = d.get('nome_modello','Scorta sede')
							fabb_list.append(d)
					if 'SOC' in l['tipo'] and l['rif_preord'] not in ['0', ''] and l['qta'] > 0:
						dati_preordine = ordiniDB.get_preordine_by_id_facord_dett(l['rif_ord'])
						for d in dati_preordine:
							d['diba'] = {}
							d['visible'] = 0
							if str(d['id_facord_dett']) == l['rif_ord']:
								d['visible'] = 1
								d.update(f.copy())
							else:
								d['diba'].update(f['diba'].copy())
							d['nome_modello'] = d.get('nome_modello', 'Scorta sede')
							fabb_list.append(d)

		ord_list = []
		fabb_list = sorted(fabb_list, key = lambda k:(k['tipo'], k['data_ordine'], k['num_ordine'], k['id_facord_dett']))
		for k, g in itertools.groupby(fabb_list, key = lambda k:(k['tipo'], k['data_ordine'], k['num_ordine'])):
			gruppo = list(g)
			ordine = gruppo[0].copy()
			ordine['dettaglio'] = []
			ordine['id_righe_orig_list'] = []
			ordine.pop('visible')
			ordine['qta'] = 0
			gruppo = sorted(gruppo, key=lambda kk: (kk['id_facord_dett'], kk['visible']), reverse=True)
			for kk, gg in itertools.groupby(gruppo, key=lambda kk: (kk['id_facord_dett'])):
				dett = list(gg)
				diz = dett[0]
				diz['id_faccom_list'] = list(set([d['id_faccom'] for d in dett if d['id_faccom'] and str(d['id_faccom']) != '0']))
				diz['id_fabbisogno'] = list(set([d['id'] for d in dett if d['id']]))
				if len(diz['id_fabbisogno']) > 1:
					raise ForwardException("Errore: trovati piu' fabbisogni per stesso ordine.")
				diz['id_fabbisogno'] = diz['id_fabbisogno'][0] if diz['id_fabbisogno'] else ''
				if float(diz['qta_arr']) > 0.00:
					diz['visible'] = 0
				diz['qta'] = float(diz['qta_ord'])
				ordine['qta'] += float(diz['qta_ord'])
				if diz['id_facord_dett'] and diz['id_facord_dett'] not in ordine['id_righe_orig_list']:
					ordine['id_righe_orig_list'].append(diz['id_facord_dett'])
				ordine['dettaglio'].append(diz)
			ord_list.append(ordine)

		return ord_list


	@db_provided(transactional=True)
	def crea_ordini_preordine_by_fabb(self, lista_ord):
		ord_facade = OrdineMateriaPrimaFacade(Request().get_pard())
		new_ord = []

		for ord in lista_ord:
			ord.pop('subgrigliaOrdini')
			ord_diz = ord_facade._crea_ordine_preordine_by_fabb(ord)
			imp_diz = ord_facade.crea_impegni_by_ordine(ord_diz)
			self._impegna_by_fabb(imp_diz)
			new_ord.append(ord_diz['rif_intr'])

		return new_ord


	@db_provided(transactional=True)
	def modifica_ordini_preordini_by_fabb(self, lista_ord):
		ord_facade = OrdineMateriaPrimaFacade(Request().get_pard())
		ordini_list = []
		new_ord = []

		for ord in lista_ord:
			ord_diz = ord_facade._modifica_ordine_preordine_by_fabb(ord, ord['id_righe_orig_list'])
			ordini_list.append(ord_diz)
			new_ord.append(ord_diz['rif_intr'])

		imp_diz = ord_facade.get_impegni_by_ordine(ordini_list)
		self._impegna_by_ordini(imp_diz)

		return new_ord


	def _impegna_by_ordini(self, impegni_diz):
		"""	input:
			diz_in['SOC'] = [(r.id_facord_dett, '', r.id_fabbisogno)]
			diz_in['ORD'] = [(preord_obj.id_facord_dett, preord_obj.id_preord, preord_obj.id_fabbisogno)]
		"""
		for (tipo, dati_impegni) in impegni_diz.items():
			for ((id_fabb, id_faccom), imp_list) in dati_impegni.items():
				if imp_list:
					if id_fabb:
						if len(imp_list) > 1:
							raise ForwardException(
								"Impossibile impegnare il fabbisogno %s su piu' ordini. %s" % (id_fabb, imp_list))
						imp = imp_list[0]
						fabb_orig = Fabbisogno.get_fabbisogno_by_ordine(imp['rif_ord'])
						if fabb_orig:
							fabb_orig.disimpegna_ordine(tipo, imp['rif_ord'])
							fabb_orig.salva()

						fabb = Fabbisogno.get_by_key(id_fabb)
						fabb.aggiorna_impegno_ordine(tipo, imp['rif_ord'], imp['rif_preord'], imp['qta'], id_faccom)
						fabb.salva()
					else:
						for imp in imp_list:
							fabb = Fabbisogno.get_fabbisogno_by_ordine(imp['rif_ord'])
							if fabb:
								fabb.disimpegna_ordine(tipo, imp['rif_ord'])
								fabb.salva()



	@db_provided(transactional=True)
	def aggiorna_impegni_by_ordine(self, ord, id_righe_eliminate):
		""" input:
			diz_in['SOC'] = {(r.id_facord_dett, '', r.id_fabbisogno) : qta}
			diz_in['ORD'] = {(preord_obj.id_facord_dett, preord_obj.id_preord, preord_obj.id_fabbisogno)} : qta}
		"""
		for riga in ord['dettagli']:
			fabb = Fabbisogno.get_fabbisogno_by_ordine(riga['id_facord_dett'])
			if fabb:
				if ord['tipo_ordine'] != 'Stock_societa':
					fabb.aggiorna_impegno_ordine('ORD', riga['id_facord_dett'], '', float(riga['qta_ord']))
				else:
					id_preord = ordiniDB.get_preordine_by_id_facord_dett(riga['id_facord_dett'])
					fabb.aggiorna_impegno_ordine('SOC', riga['id_facord_dett'], id_preord[0]['id'] if id_preord else '', float(riga['qta_ord']))
				fabb.salva()

		for id_el in id_righe_eliminate:
			fabb_el = Fabbisogno.get_fabbisogno_by_ordine(id_el)
			if fabb_el:
				for imp in fabb_el.list_impegni:
					if str(imp.rif_ord) == str(id_el):
						imp.elimina()


	@db_provided()
	def get_dati_studio_diba_pdf(self, dict_data):
		filteredResult = []
		rcomm = diba_dao.get_dati_studio_diba_pdf(dict_data)
		rcomm_sorted = sorted(rcomm, key=lambda k: (k['societa_orig'], k['modello'], k['variante'], k['descr_col_variante']))
		for k, g in itertools.groupby(rcomm_sorted, key=lambda k: (k['societa_orig'], k['modello'], k['variante'], k['descr_col_variante'])):
			group = list(g)
			for gp in group:
				#gp['posizione'] = gp['posizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
				gp['posizione'] = gp['posizione'].decode('latin1').encode('utf8')
				#gp['descrizione'] = gp['descrizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
				gp['descrizione'] = gp['descrizione'].decode('latin1').encode('utf8')
			sum_capi_gp = int(group[0]['tot_capi'])
			filteredResult.append({'descr_col_variante': k[3], 'variante': k[2], 'societa': k[0], 'modello': k[1], 'nome_modello': group[0]['nome_modello'], 'somma_capi':sum_capi_gp, 'variante_group':group})

		return filteredResult


	@db_provided()
	def notifica_impegni_incompleti(self, imp_esistenti):
		da_notificare = []
		for i, group in imp_esistenti.items():
			num_impegni_incompleti = 0
			if len(group['elenco_impegni']) > 0:
				for i, d in enumerate(group['elenco_impegni']):
					if float(d.get('qta_mancanti', '0')) > 0 and str(d['rif_bp']) not in ('', '0'):
						num_impegni_incompleti += 1
			if num_impegni_incompleti > 0:
				da_notificare.append(group)

		if da_notificare:
			EmailHelper.invia_mail_notifica_impegni_incompleti(list_data=da_notificare, lista_destinatari=[])


	@db_provided()
	def carica_articoli_impegnati(self, dict_data, conn=None):
		from facon.magmp.facade.bp_facade import BPFacade
		bp_facade = BPFacade(Request().get_pard())

		articoli_carico = []
		id_facord_diz = {}
		for d in dict_data:
			for p in d['dettaglio']:
				if not d.has_key('tipo_ordine'):
					ordine = ordiniDB.get_ordine_by_id_facord_dett(p['ID_FACORD_DETT'])
					d['tipo_ordine'] = ''
					if ordine:
						d['tipo_ordine'] = ordine[0]['tipo_ordine']
				if p['ID_FACORD_DETT'] not in id_facord_diz:
					if p.has_key('NR_CONF'):
						id_facord_diz[p['ID_FACORD_DETT']] = {'qta': float(p['Q_RS'])/100, 'c_mat': d['C_MAT'], 'tipo': d['tipo_ordine']}
					else:
						id_facord_diz[p['ID_FACORD_DETT']] = {'qta': float(p['L_RS'])/100, 'c_mat': d['C_MAT'], 'tipo': d['tipo_ordine']}
				else:
					if p.has_key('NR_CONF'):
						id_facord_diz[p['ID_FACORD_DETT']]['qta'] += float(p['Q_RS'])/100
					else:
						id_facord_diz[p['ID_FACORD_DETT']]['qta'] += float(p['L_RS'])/100

			#qta_tot_arrivato = float(d['Q'])/100
			#art = []
			#res = diba_dao.get_articoli_carico_pdf(d)
			#for k,g in itertools.groupby(res, key=lambda  k: (k['societa_orig'], k['modello_orig'], k['variante'])):
			#	group = list(g)
			#	r = group[0].copy()
			#	r['imp_soc'] = sum(float(g['imp_soc']) for g in group)
			#	r['imp_ord'] = sum(float(g['imp_ord']) for g in group)
			#	art.append(r)
			#
			#for i in art:
			#	if d['tipo_ordine'] == 'Standard':
			#		i['qta_mancanti'] = float(i['imp_ord'])
			#		if float(i['imp_ord']) > 0 and qta_tot_arrivato > 0:
			#			if float(i['imp_ord']) >= qta_tot_arrivato:
			#				i['qta_mancanti'] = float(i['imp_ord']) - qta_tot_arrivato
			#				qta_tot_arrivato = 0
			#			else:
			#				i['qta_mancanti'] = 0
			#				qta_tot_arrivato -= float(i['imp_ord'])
			#	else:
			#		i['qta_mancanti'] = float(i['imp_soc'])
			#		if float(i['imp_soc']) > 0 and qta_tot_arrivato > 0:
			#			if float(i['imp_soc']) >= qta_tot_arrivato:
			#				i['qta_mancanti'] = float(i['imp_soc']) - qta_tot_arrivato
			#				qta_tot_arrivato = 0
			#			else:
			#				i['qta_mancanti'] = 0
			#				qta_tot_arrivato -= float(i['imp_soc'])
			#
			#articoli_carico.append({'collezione_dt': d['collezione_dt'], 'qta_arrivato':float(d['Q'])/100, 'c_mat':d['C_MAT'], 'elenco_impegni': art})

		for (id_facord_dett, diz_carico) in id_facord_diz.items():
			fabb = Fabbisogno.get_fabbisogno_by_ordine(id_facord_dett)
			if fabb:
				qta_da_scalare = float(diz_carico['qta'])
				for imp in fabb.list_impegni:
					qta_scalata = 0
					if round(qta_da_scalare,2) > 0:
						if diz_carico['tipo'] != 'Stock_societa':
							if imp.tipo == 'ORD' and imp.rif_ord == id_facord_dett:
								if imp.qta >= qta_da_scalare:
									imp.qta -= qta_da_scalare
									qta_scalata = qta_da_scalare
									qta_da_scalare = 0
								else:
									qta_scalata = imp.qta
									qta_da_scalare -= imp.qta
									imp.qta = 0
						else:
							if imp.tipo == 'SOC' and imp.rif_ord == id_facord_dett:
								if imp.qta >= qta_da_scalare:
									imp.qta -= qta_da_scalare
									qta_scalata = qta_da_scalare
									qta_da_scalare = 0
								else:
									qta_scalata = imp.qta
									qta_da_scalare -= imp.qta
									imp.qta = 0
					if qta_scalata > 0:
						id_faccom_da_caricare = 0
						trovato = False
						for fabb_imp in fabb.list_impegni:
							if fabb_imp.tipo == 'DT' and str(fabb_imp.id_faccom) == str(imp.id_faccom) and \
							float(fabb_imp.qta) < float(fabb_imp.qta_faccom):
								trovato = True
								id_faccom_da_caricare = fabb_imp.id_faccom
						if not trovato:
							for fabb_imp in fabb.list_impegni:
								if fabb_imp.tipo == 'DT' and str(fabb_imp.id_faccom) != '0' and \
								float(fabb_imp.qta) < float(fabb_imp.qta_faccom):
									id_faccom_da_caricare = fabb_imp.id_faccom
						
						fabb.aggiungi_qta_impegno({'tipo': 'DT', 'qta': qta_scalata, 'id_faccom': id_faccom_da_caricare})
						if str(id_faccom_da_caricare) != '0':
							id_faccom = bp_facade.set_riga_bp_prelevabile(id_faccom_da_caricare)
				fabb.salva()

		tot_impegni = []
		for id, val in id_facord_diz.items():
			lista_ordini = diba_dao.get_ordini_impegni(id, val)
			for o in lista_ordini:
				o['caricato'] = True if str(o['id_facord_dett']) in id_facord_diz.keys() else False
			val['elenco_impegni'] = lista_ordini
			tot_impegni.extend(lista_ordini)


		# tools.dump(articoli_carico)
		return id_facord_diz, tot_impegni


	@db_provided(transactional=True)
	def notifica_impegni_picking(self):
		import notifica_mail
		res = fabbisogni_dao.get_impegni_da_notificare()
		res = sorted(res, key = lambda k:(k['utente']))
		for k, g in itertools.groupby(res, key = lambda k:(k['utente'])):
			notifiche_x_utente = list(g)

			DESTINATARI_MAIL = notifica_mail.get_mail_from_user(Request().get_pard(), [k])
			if Request().get_val('AMBIENTE', '') in ('ot', 'svil'):
				DESTINATARI_MAIL = ['dtsupport@otconsulting.com']

			subject = "Impegni modificati da picking (%s)<br><br> " %'-'.join(list(set([n['id_pick'] for n in notifiche_x_utente])))

			body = "Sono stati modificati i seguenti impegni tramite prelievo da magazzino: <br><br> "
			for n in notifiche_x_utente:
				body += "%s - %s <br/>" %(n['msg'] , n['capocmat'])

			for n in notifiche_x_utente:
				n['data_invio'] = tools.get_date_sql()
				fabbisogni_dao.aggiorna_impegni_notifica(n)

			DESTINATARI_MAIL = [d for d in DESTINATARI_MAIL if d]
			if DESTINATARI_MAIL:
				notifica_mail.invia('fabbisogni@diffusionetessile.it', DESTINATARI_MAIL,
				                    subject=subject, corpo=body, rispondi_a=[], tipo='html', pre=False)
			else:
				raise ForwardException('Impossibile inviare mail al seguente utente %s' %k)


	@db_provided(transactional=True)
	def check_giacenza_diba(self, capocmat=''):
		capocmat_list = fabbisogni_dao.get_capocmat_diba(capocmat)
		facade_giacenza = GiacenzaMPFacade(Request().get_pard())
		final_diz_giacenze_neg = {'final_list_giacenze':{ 'DT' : [],
								                          'SOC' :[],
								                          'ORD':[]},
		                          'final_counter':0}
		for capocmat in capocmat_list:
			giacenza = facade_giacenza.get_giacenza_articolo(capocmat)
			if giacenza and (float(giacenza[0].get('FREE_DT')) < 0 or float(giacenza[0].get('FREE_ORD')) < 0 or float(giacenza[0].get('FREE_SOC')) < 0):
				final_diz_giacenze_neg['final_counter'] += 1
				diz = {'capocmat': capocmat.get('capocmat')}
				diz['FREE_DT'] = giacenza[0].get('FREE_DT')
				diz['FREE_SOC'] = giacenza[0].get('FREE_SOC')
				diz['FREE_ORD'] = giacenza[0].get('FREE_ORD')
				if diz['FREE_ORD'] < 0:
					final_diz_giacenze_neg['final_list_giacenze']['ORD'].append(diz)
				if diz['FREE_SOC'] < 0:
					final_diz_giacenze_neg['final_list_giacenze']['SOC'].append(diz)
				if diz['FREE_DT'] < 0:
					final_diz_giacenze_neg['final_list_giacenze']['DT'].append(diz)
		return final_diz_giacenze_neg

