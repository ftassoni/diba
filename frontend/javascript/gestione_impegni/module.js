(function ()
{
	'use strict';

	angular.module('app', [
		'base_page',
		'ui.bootstrap',
		'ngTouch',
		'ui.grid',
		'ui.grid.edit',
		'ui.grid.expandable',
		'ui.grid.autoResize',
		'ui.grid.selection',
		'ui.grid.resizeColumns',
		'ui.grid.exporter',
		'ngMaterial',
	]);
})();
