(function()
{
	'use strict';

	RicercaService.$inject = ['DTHttp', 'LoggerFactory', '$q'];

	var factoryId = "RicercaService";

	angular.module('app').factory(factoryId, RicercaService);

	function RicercaService(DTHttp, LoggerFactory, $q)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {
			ricerca: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.studio_diba_ctrl', 'ricerca', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_giacenze: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_giacenza_articolo', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			estraiGenerica: function(params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.studio_diba_ctrl', 'estrai_diba', payload)
					.then(
					function successCallback(response) {

						var dati = {};
						try{
							dati = response.data.ResultSet.Result;
						}
						catch(error){
							logger.error("Il formato della risposta non è corretto.", response.data);
							if (errorHandler != null)
								errorHandler();
							return;
						}
						if (successHandler != null)
							successHandler(dati);
					},

					function errorCallback(response) {
						logger.error(response.data.message);
						if (errorHandler != null)
							errorHandler();
					}
				);
			},
			stampaDistintaBase: function(params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.studio_diba_ctrl', 'stampa_diba', payload)
					.then(
					function successCallback(response) {

						var dati = {};
						try{
							dati = response.data.ResultSet.Result;
						}
						catch(error){
							logger.error("Il formato della risposta non è corretto.", response.data);
							if (errorHandler != null)
								errorHandler();
							return;
						}
						if (successHandler != null)
							successHandler(dati);
					},

					function errorCallback(response) {
						logger.error(response.data.message);
						if (errorHandler != null)
							errorHandler();
					}
				);
			}
		};
	}
})();