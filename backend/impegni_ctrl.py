import traceback

from Common import decorators
from facon.ordpf.facade.pool_manager import Request
from facon.diba.view import gestione_impegni as views_gest_impegni
from facon.diba.facade.fabbisogni_facade import FabbFacade

import tools, cjson


def vista_gestione_impegni(pard):
    """
    controller della pagina principale dei listini
    :param pard:
    :return:
    """
    try:
        pard['html'] = views_gest_impegni.gestione_impegni(pard)
    except Exception, e:
        pard['html'] = tools.format_errore(traceback.format_exc())
        return pard
    return pard