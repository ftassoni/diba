# -*- coding: UTF-8 -*-
import operator

import tools

try:
    import xlwt
except Exception, e:
    import tablib.packages.xlwt as xlwt
import datetime


def render_xls(pard, xls):
    j = open(xls['path'], 'r')
    contenuto = j.read()
    j.close()
    tipo = xls['path'].split('.')
    pard['header'] = 'Content-type: application/vnd.ms-excel\n'
    pard['header'] = pard['header'] + 'Content-Disposition: attachment; filename=' + xls['name'] + '\n\n'
    pard['html'] = contenuto
    return pard


def title_style(style, align='CENTER'):
    font = xlwt.Font()  # Create Font
    font.bold = True  # Set font to Bold

    borders = xlwt.Borders()  # Create Borders
    borders.left = xlwt.Borders.THIN  # May be: NO_LINE, THIN, MEDIUM, DASHED, DOTTED, THICK, DOUBLE, HAIR, MEDIUM_DASHED,
    # THIN_DASH_DOTTED, MEDIUM_DASH_DOTTED, THIN_DASH_DOT_DOTTED, MEDIUM_DASH_DOT_DOTTED,
    # SLANTED_MEDIUM_DASH_DOTTED, or 0x00 through 0x0D.
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.THIN
    borders.bottom = xlwt.Borders.THIN

    alignment = xlwt.Alignment()  # Create Alignment
    if align == 'CENTER':
        alignment.horz = xlwt.Alignment.HORZ_CENTER
    elif align == 'LEFT':
        alignment.horz = xlwt.Alignment.HORZ_LEFT
    elif align == 'RIGHT':
        alignment.horz = xlwt.Alignment.HORZ_RIGHT
    alignment.vert = xlwt.Alignment.VERT_CENTER

    style_columns = style  # Create Style
    style_columns.font = font  # Add Bold Font to Style
    style_columns.borders = borders
    style_columns.alignment = alignment  # Add Alignment to Style

    return style_columns


def title_style_colour(colour=1, underlined=True, Bord=True, height=0x00FF, align='CENTER'):
    font_tytle = xlwt.Font()  # Create Font
    font_tytle.bold = True  # Set font to Bold
    font_tytle.underline = underlined
    font_tytle.height = height  # C8 in Hex (in decimal) = 10 points in height.

    pattern = xlwt.Pattern()  # Create the Pattern
    pattern.pattern = xlwt.Pattern.SOLID_PATTERN  # May be: NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
    pattern.pattern_fore_colour = colour  # May be: 8 through 63. 0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta,
    # 7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue, 19 = Dark Yellow , almost brown),
    # 20 = Dark Magenta, 21 = Teal, 22 = Light Gray, 23 = Dark Gray, the list goes on...
    tytle_columns = xlwt.XFStyle()  # Create Style
    tytle_columns.pattern = pattern  # Add Pattern to Style
    tytle_columns.font = font_tytle  # Add Bold Font to Style

    alignment = xlwt.Alignment()  # Create Alignment
    if align == 'CENTER':
        alignment.horz = xlwt.Alignment.HORZ_CENTER
    elif align == 'LEFT':
        alignment.horz = xlwt.Alignment.HORZ_LEFT
    elif align == 'RIGHT':
        alignment.horz = xlwt.Alignment.HORZ_RIGHT
    alignment.vert = xlwt.Alignment.VERT_CENTER
    tytle_columns.alignment = alignment

    if Bord:
        borders = xlwt.Borders()  # Create Borders
        borders.left = xlwt.Borders.THIN  # May be: NO_LINE, THIN, MEDIUM, DASHED, DOTTED, THICK, DOUBLE, HAIR, MEDIUM_DASHED,
        # THIN_DASH_DOTTED, MEDIUM_DASH_DOTTED, THIN_DASH_DOT_DOTTED, MEDIUM_DASH_DOT_DOTTED,
        # SLANTED_MEDIUM_DASH_DOTTED, or 0x00 through 0x0D.
        borders.right = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        borders.bottom = xlwt.Borders.THIN
        tytle_columns.borders = borders

    return tytle_columns


def sum_counter_style(style, Bord, Pattern=True):
    font = xlwt.Font()  # Create Font
    font.bold = True  # Set font to Bold

    style_columns = style  # Create Style
    style_columns.font = font  # Add Bold Font to Style
    if Pattern:
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = 22
        style_columns.pattern = pattern

    if Bord:
        borders = xlwt.Borders()  # Create Borders
        borders.left = xlwt.Borders.THIN  # May be: NO_LINE, THIN, MEDIUM, DASHED, DOTTED, THICK, DOUBLE, HAIR, MEDIUM_DASHED,
        # THIN_DASH_DOTTED, MEDIUM_DASH_DOTTED, THIN_DASH_DOT_DOTTED, MEDIUM_DASH_DOT_DOTTED,
        # SLANTED_MEDIUM_DASH_DOTTED, or 0x00 through 0x0D.
        borders.right = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        borders.bottom = xlwt.Borders.THIN
        style_columns.borders = borders

    return style_columns


def crea_xls_estrazione_diba(pard, data):
    now_str = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    name = 'EstrazioneStudioDiba-' + now_str + '.xls'
    fullpath = pard['TMP_DIR'] + "/diba/" + name

    list_colonne = [
        {'societa': "Societa"},
        {'gm': "Gm"},
        {'anno_stagione': "AS"},
        {'articolo': "Articolo"},
        {'misura': "Misura"},
        {'colore': "Colore"},
        {'colore_descrizione': "Col. Desc."},
        {'macrocolore': "Macrocol."},
        {'descrizione': "Descr."},
        {'modello': "Modello"},
        {'variante': "Variante"},
        {'nome_modello': "Nome"},
        {'consumo': "Cons."},
        {'posizione': "Descr/Posiz."},
        {'tot_capi': "Capi"},
        {'fornitore': "Forn. Cons."},
        {'articolo_fornitore': "Art Forn"},
        {'fabbisogno': "FABB"},
        {'impegni': "IMP"},
        {'stato': "Stato"},
        {'nota': "Note"},
        {'id_fac_d': "Rif. Ord."},
        {'fornitore_ord': "Fornitore"},

    ]

    workbook = xlwt.Workbook(encoding='latin-1', style_compression=2)
    # workbook = xlwt.Workbook(encoding='utf-8', style_compression=2)

    style = xlwt.XFStyle()
    tit_style = title_style(xlwt.XFStyle())
    t_style  = sum_counter_style(xlwt.XFStyle(), True, False)
    sum_style = sum_counter_style(xlwt.XFStyle(), True)
    for (ind, tab) in data.items():
        worksheet = workbook.add_sheet(ind)
        column_counter = 0
        for label in list_colonne:
            worksheet.write(1, column_counter, str(label.values()[0]), tit_style)
            column_counter += 1
        row_counter = 2

        for item in tab:
            for art in item['dettaglio_art']:
                for colore in art['dettaglio_col']:
                    column_counter = 0
                    for label in list_colonne:
                        if label.keys()[0] in colore:
                            # val = iitem[label.keys()[0]]
                            try:
                                if label.keys()[0] in ['tot_capi']:
                                    val = int(colore[label.keys()[0]])
                                else:
                                    if label.keys()[0] in ['consumo', 'fabbisogno', 'impegni']:
                                        val = round(float(colore[label.keys()[0]]), 2)
                                    else:
                                        decoded_str = colore[label.keys()[0]].decode("windows-1252")
                                        val = decoded_str.encode("utf8")
                            except (TypeError, AttributeError):
                                val = colore[label.keys()[0]]
                            worksheet.write(row_counter, column_counter, val, style)
                        # else:
                        #     worksheet.write(row_counter, column_counter, '', style)
                        column_counter += 1
                    row_counter += 1

                column_counter = 0
                for label in list_colonne:
                    if label.keys()[0] in art:
                        # val = iitem[label.keys()[0]]
                        try:
                            if label.keys()[0] in ['tot_capi']:
                                val = int(art[label.keys()[0]])
                            else:
                                if label.keys()[0] in ['consumo', 'fabbisogno', 'impegni']:
                                    val = round(float(art[label.keys()[0]]), 2)
                                else:
                                    decoded_str = art[label.keys()[0]].decode("windows-1252")
                                    val = decoded_str.encode("utf8")
                        except (TypeError, AttributeError):
                            val = art[label.keys()[0]]
                        worksheet.write(row_counter, column_counter, val, t_style)
                    column_counter += 1
                row_counter += 1

            column_counter = 0
            for label in list_colonne:
                if label.keys()[0] in item:
                    # val = iitem[label.keys()[0]]
                    try:
                        if label.keys()[0] in ['tot_capi']:
                            val = int(item[label.keys()[0]])
                        else:
                            if label.keys()[0] in ['consumo', 'fabbisogno', 'impegni']:
                                val = round(float(item[label.keys()[0]]), 2)
                            else:
                                decoded_str = item[label.keys()[0]].decode("windows-1252")
                                val = decoded_str.encode("utf8")
                    except (TypeError, AttributeError):
                        val = item[label.keys()[0]]
                    worksheet.write(row_counter, column_counter, val, sum_style)
                column_counter += 1
            row_counter += 1
    workbook.save(fullpath)
    return {'name': name, 'path': fullpath}


