(function ()
{
	'use strict';

	var controllerId = "DialogCreaOrdiniCtrl";

	DialogCreaOrdiniCtrl.$inject = ['$rootScope', '$scope', '$uibModalInstance', 'LoggerFactory', '$timeout', '$filter', 'entity',
	        'SOCIETA', 'UI_ANNI_STAGIONE', '$uibModal', 'RicercaService', 'SalvaService', 'AutoCompleteGFService',
	        'ServiceDatiVistaGiacenza', 'ServiceDatiPopover', '$location', 'uiGridConstants'];

	angular.module('app').controller(controllerId, DialogCreaOrdiniCtrl);

	function DialogCreaOrdiniCtrl($rootScope, $scope, $uibModalInstance, LoggerFactory, $timeout, $filter, entity,
	        SOCIETA, UI_ANNI_STAGIONE, $uibModal, RicercaService, SalvaService, AutoCompleteGFService,
	        ServiceDatiVistaGiacenza, ServiceDatiPopover, $location, uiGridConstants)
	{
		var vm = this;
		var logger = LoggerFactory.getLogger(controllerId);

        vm.getDatiCreaOrdini = function(entity) {
            if (entity.righe_selezionate != undefined && entity.righe_selezionate.length > 0){
                var listaAllArticoliSelezionati = [];
                angular.forEach(entity.righe_selezionate, function(item, key) {
                    if (!$.isEmptyObject(item))
                        listaAllArticoliSelezionati.push(item);
                });
                // Init con dati caricati da server
                RicercaService.get_ordini_da_gestione_impegni(listaAllArticoliSelezionati, function(data){
                    $scope.grigliaOrdini.data = data;
                    for(var i = 0; i < data.length; i++){
                        data[i].subgrigliaOrdini = {
                            enableRowSelection : true,
                            enableColumnMenus: false,
                            enableSelectAll: true,
                            columnDefs: COLONNE_SOTTOTAB,
                            data: data[i].dettaglio,
                            disableRowExpandable : data[i].dettaglio === 0,
                            enableCellEditOnFocus : true
                        }
                        data[i]['unique_datepicker_code'] = vm.randomCodeGen();
                    }
                    $scope.grigliaOrdini.data = data;

                }, function(){
                    //$scope.gridOptions.data = [];
                });
            }else
                logger.error('Nessun articolo selezionato')
        };

        vm.randomCodeGen = function(){
		    var final_code = '';
		    for(var i = 0; i < 5; i++){
		        final_code += String(Math.floor(Math.random() * 111) + 1);
		    }
		    return final_code.substr(0, 6);
		}

        function getUrlIdFabbs() {
            vm.url_params = $location.search()||"Unknown";
            var id_richiesta = "";
            if (vm.url_params.hasOwnProperty('id_dibas'))
                id_richiesta = vm.url_params['id_dibas'].split(',');
            return id_richiesta;
        }

        $scope.errorHandler = function(){
            logger.error('Errore Inatteso, contattare il supporto tecnico');
		};

		$scope.closeModal = function ()
		{
			$uibModalInstance.dismiss('cancel');
		};

		$scope.creaOrdini = function (){
		    var allSelectedRows = jQuery.extend(true, [], $scope.gridApi.selection.getSelectedRows().slice());
		    if (allSelectedRows.length > 0){
		        $(allSelectedRows).each(function(index, item){
		            if (item.dettaglio !== undefined && item.dettaglio.length > 0){
		                $(item.dettaglio).each(function(iindex, iitem){
		                    if (iitem.subgrigliaTessuti !== undefined){
                                delete iitem.subgrigliaTessuti;
                            }
                            //Controllo per sicurezza sul formato della data
                            iitem.data_consegna = $filter('date')(iitem.data_consegna, "yyyy-MM-dd");
                        });
		            }
		            //Controllo per sicurezza sul formato della data
		            item.data_consegna = $filter('date')(item.data_consegna, "yyyy-MM-dd");
                });
                SalvaService.creaOrdini(allSelectedRows, function(data){
                    if(entity.hasOwnProperty('from_table_info')){
                        $rootScope.$emit("popup_submit_refresh:grids_refresh_panel", entity.from_table_info);
                    }else{
                        $rootScope.$emit("popup_submit_refresh:grids_refresh_all");
                    }
                    $scope.closeModal();
                });//, $scope.errorHandler());
		    }else {
		        logger.error('Nessun ordine selezionato')
		    }
		};

		function row_class(grid, row, col, rowRenderIndex, colRenderIndex) {
           var classe = '';

          return classe;
        }

        $scope.getRowFornitore = function(row, value){
			return AutoCompleteGFService.getFornitore({'query' : value, });
		};

		$scope.onSelectRowFornitore = function(row, item, value, label){
		    row.fornitore = item.fornitore;
			row.ragione_sociale = item.rag_soc_forn;
		};

        $scope.getRowArticolo = function(row, value){
			return AutoCompleteGFService.getArticoloFornitore({'query' : value, });
		};

		$scope.onSelectRowArticolo = function(row, item, value, label){
		    row.art_forn = item.articolo_fornitore;
		};

        $scope.getDateVal = function(data){
            var response_data = null;
            if (['', null, 'null'].indexOf(data) >= 0 && angular.isDate(response_data))
		        response_data = '';
            else
                response_data = $filter('date')(data, "dd/MM/yyyy");
            return response_data;
		};

		$scope.grigliaOrdini = {
            showGridFooter: true,
			enableColumnMenus: false,
			enableSelectAll: true,
			enableExpandAll: false,
			enableRowHeaderSelection: true,
			enableRowSelection : true,
			enableExpandableRowHeader: false,
			expandableRowTemplate: '<div ui-grid="row.entity.subgrigliaOrdini" ng-style="grid.appScope.getSubTableHeight(row)" ui-grid-auto-resize ui-grid-resize-columns ui-grid-edit></div>',
			headerHeight: 35,
			rowHeight: 35,
			onRegisterApi: function (gridApi)
			{
				$scope.gridApi = gridApi;
                //custom Template per bottone espandi sottogriglia
                var cellTemplate = "<div style= \"height:32px \" "+
                "class=\"ui-grid-row-header-cell ui-grid-expandable-buttons-cell\">"+
                "<div class=\"ui-grid-cell-contents\">"+
                "<i ng-class=\"{ 'ui-grid-icon-plus-squared' : !row.isExpanded && row.entity.dettaglio.length !== 0,"+
                "'ui-grid-icon-minus-squared' : row.isExpanded, 'ng-grid-cell': row.entity.subgrigliaOrdini.data.length == 0 }\" "+
                "ng-click=\"grid.api.expandable.toggleRowExpansion(row.entity)\"></i></div></div>"
                //aggiungo colonna custom Template alle colonne
                $scope.gridApi.core.addRowHeaderColumn( { name: 'rowHeaderCol',
                                                        displayName: '',
                                                        width: 35,
                                                        cellTemplate: cellTemplate} );
			},
			expandableRowScope: {
                getDateVal: function(data) {
                    return $scope.getDateVal(data);
                }
            }
		};

        $scope.scegliFornitore = function($event, row){
            ServiceDatiVistaGiacenza.tableRowID = row.entity.$$hashKey;
            ServiceDatiVistaGiacenza.tableID = row.grid.options.tableID;
            ServiceDatiVistaGiacenza.isFornitore = true;
            ServiceDatiPopover.clickedButton = $event.target;
            //riga con scelta fornitore libera autocomplete
            var diz_scelta_fornitore = {fornitore: '', articolo_fornitore: '', is_empty: true}
            var row_fornitori_list = angular.copy(row.entity.fornitori_list.slice());
            //Controllo di non avere un fornitore con conto nullo
            if (row_fornitori_list.length > 0){
                row_fornitori_list.forEach( function( item, index, object ) {
                    if (item.conto !== undefined && item.conto === ''){
                        object.splice(index, 1);
                    }
                });
            }
            //Aggiungo a lista data tabella
            $scope.grigliaPopoverFornitori.data = row_fornitori_list.length > 0 ? (row_fornitori_list).concat(diz_scelta_fornitore) : [diz_scelta_fornitore];
        }


		$scope.dynamicPopover = {
            content: '',
            templateUrl: 'myPopoverTemplate.html',
            title: 'Scegli Fornitore',
            placement: 'auto',
        };

		$scope.grigliaPopoverFornitori = {
            showGridFooter: false,
			enableColumnMenus: false,
			enableRowHeaderSelection: false,
//			columnDefs: COL_LIST_FORNITORI,
			data: [],
		};

        var COL_LIST_FORNITORI = [
			{displayName: 'Fornitori', field: 'conto', category:'generic', enableCellEdit: false, width: '20%',
                cellTemplate:   '<div class="ui-grid-cell-contents" ng-model="row.entity.conto" ng-if="!row.entity.is_empty"">{{row.entity.conto}}</div>'+
                                '<div class="ui-grid-cell-contents" ng-if="row.entity.is_empty == 1">'+
                                    '<script type="text/ng-template" id="customTemplateFornitore.html">'+
                                        '<a>'+
                                            '<span ng-bind-html="match.model.fornitore | uibTypeaheadHighlight:query"></span>'+ ' - ' +
                                            '<span ng-bind-html="match.model.rag_soc_forn"></span>'+
                                        '</a>'+
                                    '</script>'+
                                    '<input type="text"  ng-model="row.entity.conto" id="row_fornitore" name="row_fornitore" '+
                                           'uib-typeahead="row_fornitore as row_fornitore.conto for row_fornitore in grid.appScope.getRowFornitore(row.entity, $viewValue)" '+
                                           'typeahead-loading="loadingFornitoreRow" typeahead-min-length="1" typeahead-wait-ms="300" typeahead-editable="false" '+
                                           'typeahead-on-select="grid.appScope.onSelectRowFornitore(row.entity, $item, $model, $label)" '+
                                           'typeahead-template-url="customTemplateFornitore.html" />'+
                                   '<i ng-show="loadingFornitoreRow" class="fa fa-refresh"></i> '+
                               '</div>'

			},
			{displayName: 'Rag. Soc.', field: 'fornitore', category:'generic', enableCellEdit: false, width: '35%'},

			{displayName: 'Art.Forn', category:'generic',
                width: '35%',
                field: 'articolo_fornitore',
                enableCellEdit: false,

                cellTemplate:   '<div class="ui-grid-cell-contents" ng-model="row.entity.articolo_fornitore" ng-if="!row.entity.is_empty"">{{row.entity.articolo_fornitore}}</div>'+
                                '<div class="ui-grid-cell-contents" ng-if="row.entity.is_empty == 1">'+
                                    '<script type="text/ng-template" id="customTemplateArticolo.html">'+
                                        '<a>'+
                                            '<span ng-bind-html="match.model.articolo_fornitore | uibTypeaheadHighlight:query"></span>'+
                                        '</a>'+
                                    '</script>'+
                                        '<input type="text" ng-model="row.entity.articolo_fornitore" id="row_articolo_fornitore" name="row_articolo_fornitore" '+
                                               'uib-typeahead="row_articolo as row_articolo.fornitore for row_articolo in grid.appScope.getRowArticolo(row.entity, $viewValue)" '+
                                               'typeahead-loading="loadingArticoloRow" typeahead-min-length="1" typeahead-wait-ms="300" typeahead-editable="false" '+
                                               'typeahead-on-select="grid.appScope.onSelectRowArticolo(row.entity, $item, $model, $label)" '+
                                               'typeahead-template-url="customTemplateArticolo.html" />'+
                                       '<i ng-show="loadingArticoloRow" class="fa fa-refresh"></i> '+
                                '</div>',
            },
			{displayName: 'Azioni', field: 'azioni',
			    category:'azioni',
                enableHiding: false,
                enableCellEdit: false,
                enableSorting: false,
                width: '10%',
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Cambia fornitore" tooltip-placement="left" ' +
                        'ng-disabled="grid.appScope.disable_condition_cambia_riga(row)" '+
                        'ng-hide="row.entity.orig === \'SOC\'" ' +
                        'ng-click="grid.appScope.cambiaFornitore(row)"'+
                        'class="btn btn-xs btn-primary stretto">'+
                        '<i class="fa fa-sign-out"></i>'+
                    '</button>'+
                '</div>'
            }
		];

		var COLONNE_CUSTOM = [
		    {   field : 'tipo',
		        displayName : 'Tipo',
		        enableCellEdit: false,
		        cellClass: row_class
            },
			{   field : 'data_ordine',
			    displayName : 'Data Ordine',
			    enableCellEdit: false,
			    type: 'date',
			    cellFilter: "date:'dd/MM/yyyy'",
			    cellClass: row_class
            },
	        {   field : 'societa',
	            displayName : 'Società',
	            enableCellEdit: false,
	            'type' : 'string',
	            cellClass: row_class
            },
		    {   field : 'anno_stagione',
		        displayName : 'Anno Stagione DT',
		        enableCellEdit: false,
		        cellClass: row_class
            },
		    {   field : 'collezione_dt',
		        displayName : 'Collezione DT',
		        enableCellEdit: false,
		        cellClass: row_class
            },
		    {   field : 'capocmat',
		        displayName : 'Capocmat',
		        enableCellEdit: false,
		        cellClass: row_class
            },
		    {   field : 'fornitore',
                displayName : 'Fornitore',
                enableCellEdit: false,
                cellClass: row_class,
                cellTemplate:
                '<div class="ui-grid-cell-contents">'+
                    '<p uib-tooltip="{{row.entity.ragione_sociale}}" tooltip-placement="left">{{row.entity.fornitore}}</p>'+
                '</div>'

            },
		    {   field : 'art_forn',
		        displayName : 'Articolo Fornitore',
		        enableCellEdit: false,
		        cellClass: row_class,
		        cellTemplate:
                '<div class="ui-grid-cell-contents">'+
                    '<p uib-tooltip="{{row.entity.art_forn}}" tooltip-placement="left">{{row.entity.art_forn}}</p>'+
                '</div>'
            },
            {   displayName: '',
                field: 'fornitore_popover',
                width: '5%',
                category:'generic',
                enableCellEdit: false,
                cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<script type="text/ng-template" id="myPopoverTemplate.html">'+
                        '<div class="form-group" ng-controller="PopupFornitoriCtrl as popUpCtrl">'+
                        '<button class="btn btn-xs btn-danger button-popover-close" ng-click="closePopover($event)">X</button>'+
//                                  '<label>Popup Title:</label>'+
                          '<div id="gridPopoverFornitori" ui-grid="grid.appScope.grigliaPopoverFornitori" style="height:260px;"'+
                          'ui-grid-auto-resize ui-grid-resize-columns ui-grid-cellnav'+
                          '></div>'+
                        '</div>'+
                    '</script>'+
                    '<button id="scegliFornitore" uib-tooltip="Scegli fornitore" tooltip-placement="left" ng-disabled="'+false+'" '+
                        'ng-hide="row.entity.tipo === \'SOC\'" ' +
                        'ng-click="grid.appScope.scegliFornitore($event, row)" style="text-overflow: ellipsis;"'+
                        'popover-class="popover-fornitore" popover-placement="{{grid.appScope.dynamicPopover.placement}}" uib-popover-template="grid.appScope.dynamicPopover.templateUrl"'+
                        'popover-title="{{grid.appScope.dynamicPopover.title}}" popover-append-to-body='+true+' type="button" class="btn btn-xs btn-default">'+
                        '<i class="fa fa-arrow-circle-down"></i>'+
                    '</button>'+
                '</div>'

            },
		    {   field : 'tipo_ordine',
		        displayName : 'Tipo Ordine',
		        enableCellEdit: false,
		        cellClass: row_class
            },
		    {   field : 'stato_ordine',
		        displayName : 'Stato Ordine',
		        enableCellEdit: false,
		        cellClass: row_class
            },
		    {displayName: 'Data Consegna',
                field: 'data_consegna',
                width: '10%',
                enableCellEdit: true,
//                cellFilter: 'textDate:"dd/MM/yyyy"',
//                cellClass: 'edit-class-grid',
//                cellTemplate:'<div class="ui-grid-cell-contents" style="padding: 4px">' +
//                                '{{grid.appScope.getDateVal(row.entity.data_consegna)}}' +
//                            '</div>',
                cellTemplate: '<div><form name="inputForm"><div ui-grid-edit-datepicker datepicker-options="datepickerOptions" ng-class="\'colt\' + col.uid"></div></form></div>'
            },
		    {   field : 'qta_tot',
		        enableCellEdit: false,
                displayName : 'Qta Totale',
                cellClass: row_class
            }
		]

		var COLONNE_SOTTOTAB = [
			{  field : 'modello_orig', displayName : 'Modello', enableHiding: false, cellClass: row_class, enableCellEdit: false},
        	{  field : 'nome_modello_orig', displayName : 'Nome', cellClass: row_class, enableCellEdit: false},
	        {  field : 'variante_dt', displayName : 'Variante', cellClass: row_class, enableCellEdit: false},
		    {  field : 'misura', displayName : 'Misura', cellClass: row_class, enableCellEdit: false},
		    {  field : 'colore', displayName : 'Colore', cellClass: row_class, enableCellEdit: false},
		    {  field : 'colore_descr', displayName : 'Descr. Colore', cellClass: row_class, enableCellEdit: false},
		    {displayName: 'Data Consegna',
                field: 'data_consegna',
                enableCellEdit: true,
                cellTemplate: '<div><form name="inputForm'+'{{row.grid.parentRow.entity.unique_datepicker_code}}'+'"><div ui-grid-edit-datepicker datepicker-options="datepickerOptions" ng-class="\'colt\' + col.uid"></div></form></div>'
            },
		    {  field : 'qta', displayName : 'Qta', cellClass: row_class, enableCellEdit: false},
		]

		$scope.getTableHeight = function() {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           return {
              height: ($scope.grigliaOrdini.data.length * rowHeight + 2 * headerHeight) + "px",
              'min-height': 400 +'px',
              'max-height': 600 +'px'
           };
        };

        vm.CambiaRiga = function(event, dati){
            var newTableData = $scope.grigliaOrdini.data;
            angular.forEach(newTableData, function(item, key) {
                //Se trovo riga di uguale id sostituisco
                if (item.$$hashKey === dati.tableRowID){
                    var giaPresente = false;
                    if(item.fornitori_list.length > 0){
                        var filteredFornList =  $.grep( angular.copy(item.fornitori_list), function( f, i ) {
                                                    return f.orig != 'SOC';
                                                });
                        angular.forEach(filteredFornList, function(iitem, kkey) {
                            if (dati.riga.hasOwnProperty('is_empty') && dati.riga.is_empty &&
                            $.trim(iitem.conto.replace(/ /g,'')) == $.trim(dati.riga.conto.replace(/ /g,'')) &&
                            $.trim(iitem.articolo_fornitore.replace(/ /g,'')) == $.trim(dati.riga.articolo_fornitore.replace(/ /g,''))){
                                giaPresente = true;
                            }
                        });
                    }
                    //Se cambio fornitore sostituisco solamente chivi fornitore
                    if (!giaPresente){
                        item.ragione_sociale = dati.riga.fornitore;
                        item.art_forn = dati.riga.articolo_fornitore;
                        item.fornitore = dati.riga.conto;
                        item.id_articolo_fornitore = dati.riga.id_articolo_fornitore || 0;
                        $scope.grigliaOrdini.data = newTableData;
                        setTimeout(function(){
                            $(dati.clickButton).trigger('click');
                        }, 100);
                    }else{
                        logger.error('Selezionato un fornitore già presente in lista fornitori!');
                    }

                }
            });
        };

        function getExpandableRowHeight(expRowHeight){
            if(expRowHeight < 50)
                return 50;
            else if(expRowHeight > 50 && expRowHeight < 200)
                return expRowHeight;
            else if(expRowHeight > 200)
                return 200;
        };

        $scope.getSubTableHeight = function(row) {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           var heightSubTable = (row.entity.subgrigliaOrdini.data.length * rowHeight + 2 * headerHeight)
           row.grid.options.expandableRowHeight = getExpandableRowHeight(heightSubTable);
           row.expandedRowHeight = getExpandableRowHeight(heightSubTable);
           return {
              height: heightSubTable + "px",
              'min-height': 50 +'px',
              'max-height': 200 +'px'
           };
        };

		$scope.grigliaOrdini.columnDefs = COLONNE_CUSTOM;
		$scope.grigliaOrdini.enableCellEditOnFocus = true;
		$scope.grigliaOrdini.data = [];

		$scope.grigliaPopoverFornitori.columnDefs = COL_LIST_FORNITORI;
		$scope.grigliaPopoverFornitori.data = [];

        console.log(entity);
        vm.getDatiCreaOrdini(entity);

        $rootScope.$on("vistaGiac:cambia_riga", vm.CambiaRiga);
	}
})();