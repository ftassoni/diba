(function ()
{
	'use strict';


    var GET_URL_ID = function() {
        url_params = $location.search()||"Unknown";
        return vm.url_params.hasOwnProperty('id') ? url_params['id'] : "";
    }

	var SOCIETA = [
		{name: "", id: ""},
		{name: "Max Mara", id: "MM"},
		{name: "Marella", id: "MA"},
		{name: "Manifatture del Nord", id: "MN"}
	];

    var UI_ANNI_STAGIONE = [];
    var ui_anni_stagione_n = [];

    for (var i=2012; i <= new Date().getFullYear()+1; i++ ) {

        var ai_item = {
            id: i + " A/I",
            name: i + " A/I"
        };

        var pe_item = {
            id: i + " P/E",
            name: i + " P/E"
        };

        ui_anni_stagione_n.push(ai_item);
        ui_anni_stagione_n.push(pe_item);
        UI_ANNI_STAGIONE.push(ai_item);
        UI_ANNI_STAGIONE.push(pe_item);

    }

    var UI_TIPI_TESSUTO = [
                        //{id: 'E', name: 'E'},
                       {id: 'T', name: 'T'}];

	angular.module('app')
		.constant('SOCIETA', SOCIETA)
		.constant('GET_URL_ID', GET_URL_ID)
		.constant('UI_ANNI_STAGIONE', UI_ANNI_STAGIONE)
		.constant('UI_TIPI_TESSUTO', UI_TIPI_TESSUTO)
	;
})();