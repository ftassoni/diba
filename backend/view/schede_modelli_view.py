import tools
import time
import pprint
import menu
import get_static
import itertools

from Common import utilities

from facon.diba.diba_common import *
from facon.diba.dao import diba_dao
from facon import ws_client_mmfg


def render_page_base(pard):
	# jquery-ui-1.8.12.custom.js
	# jquery.ui.core.js
	# jq.js
	# jquery-1.4.4.min.js
	
    html = """
    <?xml version="1.0" encoding="UTF-8"?>
    <HTML>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <HEAD>
        		<SCRIPT type="text/javascript" src="javascripts/jq/jq.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-ui-1.8.10.custom.min.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery.ui.core.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery.json-2.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/ui.datepicker-it.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-1.8.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery-ui-1.9.2.js"></SCRIPT>
				<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
                %%(page_scripts)s
                %%(page_css)s
                <title>%%(page_title)s</title>
        </HEAD>
        <BODY>
                %%(body_page)s
        </BODY>
    </HTML>""" %pard
    
    return html

def pagina_base(pard):
	"""Definisco il campo head della pagina, generalmente piuttosto statico"""
	page_head = {}
	
	js1 = get_static.get_link(pard, "ordini-common.js", "facon.pao")
	js2 = get_static.get_link(pard, "modelli_schede.js", "facon.diba")
	
	page_head['page_scripts']="""
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="tablesorter/jquery.tablesorter.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.columnFilter.js"></SCRIPT>
						<SCRIPT type='text/javascript'>
							  jQuery.noConflict();	
							  jQuery(document).ready(function() {init();});
						</SCRIPT>
						""" %(js1, js2)
	
	css1 = get_static.get_link(pard, "diba.css", "facon.diba")				
	page_head['page_css']="""
						<link rel="stylesheet" type="text/css" href="web.css">
						<link rel="stylesheet" type="text/css" href="%s">
						<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.9.2.custom.min.css">
						<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
						<link rel="stylesheet" type="text/css" href="tablesorter/themes/blue/style_DT.css">
						"""%(css1)
		
	page_head['page_title']="""
							Schede Modelli Originali
							"""
	"""Chiave dell'head che serve solo perche il % ammette solo due occorrenze consecutive e non tre"""
	page_head['body_page']="""
							%(body_page)s
							"""
						
	"""Recupero alcuni parametri di utility dal pard"""
	pard['menu'] = menu.menu_bar(pard)
	pard['TABLE_WIDTH'] = '98%'
	pard['separator'] = "<hr width='%(TABLE_WIDTH)s' noshade>" % pard
	
	
	"""Costruisco la pagina base  e poi innesto tutti i vari elementi in modo incrementale"""
	html = render_page_base(pard)
	html = html % page_head
	
	common_body = _make_common_body(pard)
	
	"""Costruisco la tabella e richiamo un metodo di utility per generare le varie righe"""

	page_elements = {}
	page_elements['panel_ricerca'] = render_panel_ricerca(pard, {})
	page_elements['render_wait_div'] = render_wait_div(pard)

	
	page_body={}
	
	page_body['body_page']= common_body %page_elements  %pard
	
	html = html % page_body
	return html



def pagina_base_scheda(pard, dati={}, dati_orig={}):
	"""Definisco il campo head della pagina, generalmente piuttosto statico"""
	page_head = {}
	
	js1 = get_static.get_link(pard, "ordini-common.js", "facon.pao")
	js2 = get_static.get_link(pard, "modelli_schede.js", "facon.diba")
	
	page_head['page_scripts']="""
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="tablesorter/jquery.tablesorter.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.columnFilter.js"></SCRIPT>
						<SCRIPT type='text/javascript'>
							  jQuery.noConflict();	
							  jQuery(document).ready(function() {init();});
						</SCRIPT>
						""" %(js1, js2)
	
	css1 = get_static.get_link(pard, "diba.css", "facon.diba")				
	page_head['page_css']="""
						<link rel="stylesheet" type="text/css" href="web.css">
						<link rel="stylesheet" type="text/css" href="%s">
						<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.9.2.custom.min.css">
						<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
						<link rel="stylesheet" type="text/css" href="tablesorter/themes/blue/style_DT.css">
						"""%(css1)
		
	page_head['page_title']="""
							Scheda Modello %s
							"""%dati['modello']
	"""Chiave dell'head che serve solo perche il % ammette solo due occorrenze consecutive e non tre"""
	page_head['body_page']="""
							%(body_page)s
							"""
						
	"""Recupero alcuni parametri di utility dal pard"""
	pard['menu'] = menu.menu_bar(pard)
	pard['WIDTH_TABLE'] = '55%'
	pard['sep_width'] = '70%'
	pard['separator'] = "<hr width='%(sep_width)s' noshade>" % pard
	
	
	"""Costruisco la pagina base  e poi innesto tutti i vari elementi in modo incrementale"""
	html = render_page_base(pard)
	html = html % page_head
	
	common_body = _make_common_body_scheda(pard)
	
	"""Costruisco la tabella e richiamo un metodo di utility per generare le varie righe"""

	page_elements = {}
	page_elements['panel_scheda'] = render_panel_scheda(pard, dati, dati_orig)
	page_elements['render_wait_div'] = render_wait_div(pard)

	
	page_body={}
	page_body['body_page']= common_body %page_elements  %pard
	
	html = html % page_body
	return html


def render_wait_div(pard):
	''' stringa di costruzione del pannello di attesa durante la ricerca ''' 
	html= """
		<div id="overall_container" >
			<div id="loading_box">
				<center>
					<h1>Elaborazione in corso</h1>
					<img src="img/loading/ajax_loader_bar.gif"/>
				</center>
			</div>	
		</div>
		"""
	return html



def _make_common_body(pard):
	common_body = """
				<input type="hidden" id="sid" value="%%(sid)s">
				<input type="hidden" id="app_server" value="%%(APPSERVER)s">
				<input type="hidden" id="menu_id" value="%%(menu_id)s">
				<input type="hidden" id="module" value="%%(module)s">
				
				<div id='overlay'> </div>
						
				<div class="pagina_dynamic">
					%%(menu)s
					<form autocomplete='off'> 
					<br>
					<div id='errors'>
					 </div>
					 <div id='message'>
					 </div>
					 %(render_wait_div)s
					 <div id = 'ricerca_div'>
						 %(panel_ricerca)s
						 <br>
						 %%(separator)s		
						 <br>
						 <div id='risultati_ricerca'>
						</div>
					</div>
					<div id = 'dettaglio_div'>
					</div>
					</form>
				</div> 
			"""
	return common_body



def _make_common_body_scheda(pard):
	common_body = """
				<input type="hidden" id="sid" value="%%(sid)s">
				<input type="hidden" id="app_server" value="%%(APPSERVER)s">
				<input type="hidden" id="menu_id" value="%%(menu_id)s">
				<input type="hidden" id="module" value="%%(module)s">
				
				<div id='overlay'> </div>
						
				<div class="pagina_dynamic">
					%%(menu)s
					<form autocomplete='off'> 
					<br>
					<div id='errors'>
					 </div>
					 <div id='message'>
					 </div>
					 %(render_wait_div)s
					 <div id = 'scheda_div'>
					 %(panel_scheda)s
					</div>
					</form>
				</div> 
			"""
	return common_body


def render_panel_ricerca(pard, valori_parametri):
	
	valori_parametri.setdefault('gm','')
	valori_parametri.setdefault('articolo','')
	valori_parametri.setdefault('anno_stagione','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('societa','')
	valori_parametri.setdefault('fornitore','')
	valori_parametri.setdefault('modello_orig','')
	valori_parametri.setdefault('modello_nome_orig','')
	valori_parametri.setdefault('modello','')
	valori_parametri.setdefault('modello_nome','')
	valori_parametri.setdefault('collezione','')
	valori_parametri.setdefault('descrizione','')
	valori_parametri.setdefault('gm_tessuto_orig','')
	valori_parametri.setdefault('art_tessuto_orig','')
	valori_parametri.setdefault('tessuto_orig_descr','')
	valori_parametri.setdefault('gm_ns_tessuto','')
	valori_parametri.setdefault('art_ns_tessuto','')
	valori_parametri.setdefault('ns_tessuto_descr','')
	
	parametri_details = {}
	parametri_details['html_societa_options'] = utilities.build_options__(SOCIETA_LIST, selected=valori_parametri['societa'], tutti='Tutti')
	parametri_details['html_as_options'] = utilities.build_options__(AS_LIST, selected=valori_parametri['anno_stagione'], tutti='Tutti')
	#parametri_details['html_classe_options'] = utilities.build_options__(CLASSE_LIST, tutti='Tutti')
	
	panel_ricerca = """ <div>
				<table align=CENTER id='table_ricerca'>
					<tr align=CENTER>
							<td colspan='3' class="lbl">Societa'</td>
							<td width= "50px"></td>
							<td class="lbl">Modello</td>
							<td width= "10px"></td>
							<td class="lbl">Nome</td>
							<td width= "50px"></td>
							<td class="lbl">Modello Orig.</td>
							<td width= "10px"></td>
							<td class="lbl">Nome Orig.</td>
							<td width= "50px"></td>
							<td class="lbl">Collezione</td>
					</tr>
					<tr>
						 <td colspan='3' class="lbl">
						  <select id="param_societa">
	                           %(html_societa_options)s
	                      </select>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="10" maxlength='8' name="modello" id="param_modello" value='%%(modello)s'>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" name="nome" id="param_nome" value='%%(modello_nome)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="10" maxlength='8' name="modello_orig" id="param_modello_orig" value='%%(modello_orig)s'>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" name="nome" id="param_nome_orig" value='%%(modello_nome_orig)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="35" name="collezione" id="param_collezione" value='%%(collezione)s'>
						</td>
					</tr>
					<tr height='10'></tr>
					<tr>
						<td class="lbl">Anno Stagione</td>
						<td width= "10px"></td>
						<td class="lbl">Classe</td>
						<td width= "50px"></td>
						<td class="lbl">Art. Ns. Tessuto</td>
						<td width= "10px"></td>
						<td class="lbl">Art. Ns. Tessuto Descr.</td>
						<td width= "50px"></td>
						<td class="lbl">Art. Tessuto Orig.</td>
						<td width= "10px"></td>
						<td class="lbl">Art. Tessuto Orig. Descr.</td>
						<td width= "50px"></td>
						<td></td>
					</tr>
					<tr>
						<td class="lbl">
						  <select id="param_as">
	                           %(html_as_options)s
	                      </select>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="10" name="classe" id="param_classe" value='%%(classe)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="3" name="gm_ns_tessuto" id="param_gm_ns_tessuto" value='%%(gm_ns_tessuto)s'>
							-
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="5" name="art_ns_tessuto" id="param_art_ns_tessuto" value='%%(art_ns_tessuto)s'>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" name="ns_tessuto_descr" id="param_ns_tessuto_descr" value='%%(ns_tessuto_descr)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="3" name="gm_tessuto_orig" id="param_gm_tessuto_orig" value='%%(gm_tessuto_orig)s'>
							-
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="5" name="art_tessuto_orig" id="param_art_tessuto_orig" value='%%(art_tessuto_orig)s'>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
							<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" name="tessuto_orig_descr" id="param_tessuto_orig_descr" value='%%(tessuto_orig_descr)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl" style='text-align: right'>
						 	 <input class='btn_ricerca' type="button" size="15" name="btn_ricerca" id="btn_ricerca" value='   Ricerca' title='Ricerca'>
						</td>
					</tr>
				</table> 
			</div>
			<br>
			""" %parametri_details %valori_parametri 
#			<td colspan='3' class="lbl">Fornitore Princ.</td>
#			<td width= "50px"></td>
#
#			<td colspan='3' class="lbl">
#			  <input type="text" class="ui-autocomplete-input ui-corner-all" size="30" name="fornitore" id="param_fornitore" value='%%(fornitore)s'>
#			</td>
#			<td width= "10px"></td>
	
	return panel_ricerca



def render_panel_scheda(pard, dati, dati_orig):
	valori_parametri = {}
	valori_parametri.setdefault('id_scheda', dati.get('id_scheda',''))
	valori_parametri.setdefault('id_scheda_old', dati.get('id_scheda_old',''))
	valori_parametri.setdefault('societa', dati.get('societa',''))
	valori_parametri.setdefault('modello', dati.get('modello',''))
	valori_parametri.setdefault('modello_orig', dati.get('modello_orig',''))
	valori_parametri.setdefault('nome', dati.get('nome',''))
	valori_parametri.setdefault('nome_orig', dati.get('nome_orig',''))
	valori_parametri.setdefault('faconista', dati.get('faconista',''))
	valori_parametri.setdefault('ragsoc', dati.get('ragsoc',''))
	valori_parametri.setdefault('tessuto', dati.get('tessuto', []))
	valori_parametri.setdefault('nota', dati.get('nota',''))
	valori_parametri.setdefault('costo_ind', dati.get('costo_ind',''))
	valori_parametri.setdefault('anno', dati.get('anno',''))
	valori_parametri.setdefault('stag', dati.get('stag',''))
	valori_parametri.setdefault('stagione', dati.get('stagione',''))
	valori_parametri.setdefault('etichetta', dati.get('etichetta', ''))
	if valori_parametri['stagione']=='1':
		valori_parametri['stag'] = 'P/E'
	elif valori_parametri['stagione']=='2':
		valori_parametri['stag'] = 'A/I'
	valori_parametri.setdefault('desc_classe','')
	valori_parametri.setdefault('classe','')
	if dati.get('tessuto_orig', ''):
		valori_parametri.setdefault('tessuto_orig',  dati.get('tessuto_orig', ''))
	else:
		valori_parametri.setdefault('tessuto_orig', dati_orig.get('gruppo_merceologico','').zfill(2)+dati_orig.get('progr_articolo','').zfill(3))
	valori_parametri.setdefault('descrizione_orig', dati.get('tessuto_descr_orig', dati_orig.get('desc_articolo','')).decode('latin-1').encode('utf8'))
	valori_parametri.setdefault('varianti',dati.get('varianti',[]))
	valori_parametri.setdefault('prezzo_m1_orig', dati['prezzi_orig'].get('prezzo_m1', ''))
	valori_parametri.setdefault('prezzo_m2_orig', dati['prezzi_orig'].get('prezzo_m2', ''))
	valori_parametri.setdefault('prezzo_m1', dati['prezzi'].get('prezzo_m1', ''))
	valori_parametri.setdefault('prezzo_m2', dati['prezzi'].get('prezzo_m2', ''))
	valori_parametri.setdefault('compos', '')
	valori_parametri.setdefault('componente', '')
	valori_parametri.setdefault('pz', '')
	composizioni = dati.get('composizioni', [])
	if composizioni:
		valori_parametri['compos'] = composizioni[0].get('compos', '')
		valori_parametri['componente'] = composizioni[0].get('componente', '')
		valori_parametri['pz'] = composizioni[0].get('pz', '')
	
	valori_parametri['classe'] = ''
	if len(valori_parametri['modello_orig']) == 12:
		valori_parametri['classe'] = " readonly class='il_readonly' "
	else:
		valori_parametri['classe'] = " class='il_err' "
	valori_parametri['classe_facon'] = ''
	if valori_parametri['faconista']:
		valori_parametri['classe_facon'] = " readonly class='il_readonly' "
	else:
		valori_parametri['classe_facon'] = " class='il_err' "
	
# 	if valori_parametri['societa'] == 'MN':
# 		pard['url_img'] = ws_client_mmfg.get_img_modello(pard, 'model',valori_parametri['societa'], str(valori_parametri['modello_orig'][:6]+'1'+valori_parametri['modello_orig'][6]+valori_parametri['modello_orig'])[:11])
# 	else:
	pard['url_img'] = ws_client_mmfg.get_img_modello(pard, 'model',valori_parametri['societa'], valori_parametri['modello_orig'][:11])

	valori_parametri['html_etichetta'] = render_etichetta_lavaggio(pard, valori_parametri['etichetta'])

	html = """%(intestazione)s 
			   <br>
			   %%(separator)s
			   <br>
			   %(modello)s
			"""
	html_details = {}
	html_details['intestazione'] = """<table id="result_table" align='center' class="" width="%%(WIDTH_TABLE)s" style="">
										<tr>
											<td width="250px"></td>
											<td width="400px"></td>
											<td width="400px"></td>
											<td width="100px"></td>
										</tr>
										<tr>
											<td>
												<img class="click_load_schizzo" height="200px" title="Modello %(modello_orig)s"  alt="Inline image" src="%%(url_img)s">
											</td>
											<td align='left' style='padding-left: 0px;' colspan='2'>
												<table>
													<tr>
														<td class='lbl'>Cod. Mod. Ricodificato</td>
														<td width='10px'></td>
														<td class='lbl'>Nome Mod. Ricodificato</td>
													</tr>
													<tr>
														<td >
															<input type='text' class='il_readonly' readonly id='param_modello' size='60' value='%(modello)s' />
															<input type='hidden' id='param_id_scheda' size='10' value='%(id_scheda)s' />
														</td>
														<td width='10px'></td>
														<td ><input type='text' class='il_readonly' readonly id='param_nome' size='70' value='%(nome)s' /></td>
													</tr>
													<tr>
														<td class='lbl'>Cod. Mod. Originale</td>
														<td width='10px'></td>
														<td class='lbl'>Nome Mod. Originale</td>
													</tr>
													<tr>
														<td ><input type='text' %(classe)s id='param_modello_orig' size='60' value='%(modello_orig)s' /></td>
														<td width='10px'></td>
														<td ><input type='text' class='il_readonly' readonly id='param_nome_orig' size='70' value='%(nome_orig)s' /></td>
													</tr>
													<tr>
														<td class='lbl'>Costo Industriale</td>
														<td width='10px'></td>
														<td class='lbl'>Sell-Out Originale</td>
													</tr>
													<tr>
														<td ><input class='il_readonly' readonly id='param_costo' size='60' value='%(costo_ind)s' /></td>
														<td width='10px'></td>
														<td >
															<span class='lbl' >
																M1: <input type='text' class='il_readonly' readonly id='param_sell_out_orig_m1' size='29' value='%(prezzo_m1_orig)s' />
															</span>
															<span class='lbl' style='text-align:right'>
																M2: <input type='text' class='il_readonly' readonly id='param_sell_out_orig_m2' size='29' value='%(prezzo_m2_orig)s' />
															</span>
														</td>
													</tr>
													<tr>
														<td class='lbl'></td>
														<td width='10px'></td>
														<td class='lbl'>Sell-Out Ricodificato</td>
													</tr>
													<tr>
														<td ></td>
														<td width='10px'></td>
														<td>
															<span class='lbl' >
																M1: <input type='text' class='il_readonly' readonly id='param_sell_out_m1' size='29' value='%(prezzo_m1)s' />
															</span>
															<span class='lbl' align='right' style='text-align:right'>
																M2: <input type='text' class='il_readonly' readonly id='param_sell_out_m2' size='29' value='%(prezzo_m2)s' />
															</span>
														</td>
													</tr>
													<tr>
														<td class='lbl'>Art. Tessuto Mod. Originale</td>
														<td width='10px'></td>
														<td class='lbl'>Descrizione Tessuto Mod. Originale</td>
													</tr>
													<tr>
														<td ><input type='text' class='il' id='param_tessuto' size='60' value='%(tessuto_orig)s' /></td>
														<td width='10px'></td>
														<td ><input type='text' class='il' id='param_tessuto_descr' size='70' value='%(descrizione_orig)s' /></td>
													</tr>
													<tr>
														<td class='lbl'>Faconista</td>
														<td width='10px'></td>
														<td class='lbl'>Composizione Modello</td>
													</tr>
													<tr>
														<td >
															<input type='text'  %(classe_facon)s  id='param_conto' size='10' value='%(faconista)s' />
															<input type='text' class='il_readonly' readonly id='param_faconista' size='46' value='%(ragsoc)s' />
														</td>
														<td width='10px'></td>
														<td class='lbl'>
															Pz: <input type='text' class='il_readonly' readonly id='param_pezzo' size='2' value='%(pz)s' />
															Comp: <input type='text' class='il_readonly' readonly id='param_componente' size='20' value='%(componente)s' />
															Composiz: <input type='text' class='il_readonly' readonly id='param_compos' size='20' value='%(compos)s' />
														</td>
													</tr>"""%valori_parametri
	for c in composizioni[1:]:
		html_details['intestazione'] +=	"""<tr>
											<td></td>
											<td width='10px'></td>
											<td class='lbl'>
												Pz: <input type='text' class='il_readonly' readonly id='param_pezzo' size='2' value='%(pz)s' />
												Comp: <input type='text' class='il_readonly' readonly id='param_componente' size='20' value='%(componente)s' />
												Composiz: <input type='text' class='il_readonly' readonly id='param_compos' size='20' value='%(compos)s' />
											</td>
										</tr>"""%c
		
													
	html_details['intestazione'] +=	"""
										</table>
									</td>
									<td></td>
									</tr>
									<tr>
										<td class='lbl' colspan='2'>Etichetta Lavaggio</td>
										<td class='lbl'>Note</td>
										<td></td>
									</tr>
									<tr>
										<td colspan='2'>%(html_etichetta)s</td>
										<td style="vertical-align: top;">
											<textarea class='il' style='height: 50px; width: 450px;' id='param_nota' >%(nota)s</textarea>
										</td>
										<td style='vertical-align: bottom; text-align: right;'>
											<input type='button' id="btn_pdf" class="btn_stampa" value="    Stampa PDF" onclick='getSchedaPdf("%(modello)s", "%(id_scheda)s")'>
											<br>
											<input type='button' id="btn_save" class="btn_salva" value="    Salva" onclick='salva("%(id_scheda_old)s")'>
											<br>
											<input type='button' id="btn_close" class="btn_close" value="Chiudi" onclick='chiudi()'>
										</td>
									</tr>
								</table>
					"""%valori_parametri


	html_details['modello'] = """<table id="table_articolo" align='center' class="" width="%(WIDTH_TABLE)s" style="border-collapse:collapse;">
									<tr>
										<td></td>
										<td colspan='3' class='lbl'>Ns Art. Tessuto</td>
										<td colspan='' class='lbl'>Descr. Tessuto</td>
										<td colspan='2' class='lbl'>CAPOCMAT</td>
										<td></td>
									</tr>"""
	for t in valori_parametri['tessuto']:
		html_details['modello'] +="""
									<tr>
										<td></td>
										<td colspan='3'><input class='il' id='param_ns_tessuto' size='30' value='%(ns_tessuto)s' /></td>
										<td colspan=''><input class='il' id='param_ns_tessuto_descr' size='50' value='%(ns_tessuto_descr)s' /></td>
										<td colspan='2'><input class='il' id='param_capocmat' size='20' value='%(capocmat)s' /></td>
										<td></td>
									</tr>
								"""	%t

	html_details['modello'] += """<tr height='50px' ></tr>""" 
	#html_details['varianti'] = '''<table id ='table_varianti' align='center' class="" width="%(WIDTH_TABLE)s" style="border-collapse:collapse;" >'''
	for variante in valori_parametri['varianti']:
		variante.setdefault('variante_modello', variante.get('variante_modello',''))
		variante.setdefault('variante_descr', variante.get('variante_descr',''))
		variante.setdefault('variante_orig', variante.get('variante_orig',''))
		variante.setdefault('tale_quale', variante.get('tale_quale',''))
		variante['check_tale_quale'] = ''
		if variante['tale_quale'] == 'S':
			variante['check_tale_quale'] = " checked = 'checked' "
		variante.setdefault('cmat', variante.get('cmat',''))
		variante.setdefault('variante', variante.get('variante',''))
		variante.setdefault('qta', variante.get('qta',''))
		variante['variante_3'] = str(variante['variante']).zfill(3)
		if variante['variante_orig']:
			variante['variante_orig'] = str(variante['variante_orig']).zfill(3)
		if variante['variante_modello']:
			variante['variante_modello'] = str(variante['variante_modello']).zfill(3)
		
		
		html_details['modello'] += """<tr>
											<td width='100px' rowspan='2' style='text-align: center;'>
												<input type='button' class='btn_delete' id='btn_delete_%(variante)s' onclick='deleteVar(\"%(variante)s\")' title='Cancella Variante'  />
											</td>
											<td class='lbl'>Variante Modello</td>
											<td width='80px' class='lbl'>Variante Orig.</td>
											<td style='text-align: center' class='lbl'>Tale Quale</td>
											<td class='lbl'>Descrizione Colore</td>
											<td class='lbl'>CMAT</td>
											<td class='lbl'>Variante Dis.</td>
											<td class='lbl'>Tot. Capi Lanciati</td>
										</tr>
										<tr height='60px'>
											<td><input class='il' id='param_variante_modello' size='15' value='%(variante_modello)s' /></td>
											<td><input class='il' id='param_var_orig' size='15' value='%(variante_orig)s' /></td>
											<td style='text-align: center'><input type='checkbox' %(check_tale_quale)s id='param_tale_quale' /></td>
											<td><input class='il' id='param_colore_descr' size='50' value="%(variante_descr)s" /></td>
											<td><input class='il' id='param_cmat' size='15' value='%(cmat)s' /></td>
											<td><input class='il' id='param_variante' size='15' value='%(variante_3)s' /></td>
											<td><input class='il' id='param_capi' size='20' value='%(qta)s' /></td>
										</tr> 
									"""%variante
	
	html_details['modello'] += """<tr>
									<td width='100px' rowspan='2' style='text-align: center;'>
										<input type='button' id='btn_add' class='btn_add' onclick='addVar()' title='Aggiungi Variante'  />
									</td>
									<td class='lbl'>Variante Modello</td>
									<td width='80px' class='lbl'>Variante Orig. </td>
									<td style='text-align: center' class='lbl'>Tale Quale</td>
									<td class='lbl'>Descrizione Colore</td>
									<td class='lbl'>CMAT</td>
									<td class='lbl'>Variante Dis.</td>
									<td class='lbl'>Tot. Capi Lanciati</td>
								</tr>
								<tr height='60px'>
									<td><input class='il' id='param_variante_modello' size='15' value='' /></td>
									<td><input class='il' id='param_var_orig' size='15' value='' /></td>
									<td style='text-align: center'><input type='checkbox' id='param_tale_quale' /></td>
									<td><input class='il' id='param_colore_descr' size='50' value='' /></td>
									<td><input class='il' id='param_cmat' size='15' value='' /></td>
									<td><input class='il' id='param_variante' size='15' value='' /></td>
									<td><input class='il' id='param_capi' size='20' value='' /></td>
								</tr> 
							</table> 
							"""
																							
								
	html = html %html_details
	return html



def get_db_modello(pard, valori_parametri, db):
	
	valori_parametri.setdefault('modello_base','')
	valori_parametri.setdefault('anno','')
	valori_parametri.setdefault('stag','')
	valori_parametri.setdefault('stagione','')
	if valori_parametri['stagione']=='1':
		valori_parametri['stag'] = 'P/E'
	elif valori_parametri['stagione']=='2':
		valori_parametri['stag'] = 'A/I'
	valori_parametri.setdefault('uscita_collezione','')
	valori_parametri.setdefault('desc_uscita_collezione','')
	valori_parametri.setdefault('desc_classe','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('gruppo_merceologico','')
	valori_parametri.setdefault('progr_articolo','')
	valori_parametri.setdefault('desc_articolo','')
	
	html = """ %(btn_indietro)s
				<br>
				%(intestazione)s 
			   <br> 
			   %(db)s
			   <br>
			   """
	
	html_details = {}
	html_details['btn_indietro'] = """ <div id="div_back" style="width:63%; margin:auto;">
											<input type='button' id="btn_back" class="btn_back" value="    Indietro" onclick='indietro()'>
										</div>
									"""
	html_details['intestazione'] = """<fieldset  style="width:60%%%%; margin:auto;">
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										
										<tr>
											<td width="80px">
												<img class="click_load_schizzo" height="70px" title="Visualizza Schizzo" style="cursor:pointer;" alt="Inline image" src="">
											</td>
											<td align='center'>
												<b><span style="font-size:12;color:#444444;">Modello: </span>
													<span style="font-size:15;color:red;">%(modello_base)s000</span>
													</b>
												<br>
												<b>
												<span style="font-size:11;color:#444444;">%(classe)s - %(desc_classe)s (%(uscita_collezione)s - %(desc_uscita_collezione)s %(anno)s %(stag)s )</span>
												</b>
													<br>
													Articolo principale:
													<b>%(gruppo_merceologico)s - %(progr_articolo)s</b>
													(%(desc_articolo)s)
											</td>
											<td width="80px">
											</td>
										</tr>
									
									</table>
								</fieldset>
								"""%valori_parametri
	
	
	html_details['db'] = """<fieldset  style="width:80%; margin:auto;">
										<legend><b>DISTINTA BASE</b></legend>
										<br>
										<table id="result_table" align='center' cellpadding='3px' class="" width="1100px" style="border-collapse:collapse;">
										<thead>
											<th class='hc'>Pezzo</th>
											<th class='hc'>Colonna</th>
											<th class='hc'>Gm</th>
											<th class='hc'>AS</th>
											<th class='hc'>Art</th>
											<th class='hc'>Desc. Art.</th>
											<th class='hc'>Compos</th>
											<th class='hc'>Fornitore Princ.</th>
											<th class='hc'>Misura Base</th>
											<th class='hc'>Style</th>
											<th class='hc'>Tipo Segnata</th>
											<th class='hc'>Ricerca Colore</th>
											<th class='hc'>Consumo Base</th>
										</thead> """	
	chiave = db.keys()[0]
	#tools.dump(db[chiave])
	for db_art in db[chiave]:
		tr_style = ''
		if int(db_art['id_articolo']) == int(valori_parametri['articolo']):
			tr_style = """ style="background-color: #FFFFCC;" """
		html_details['db'] += """
										<tr %%s>
											<td class='dl'>%(pezzo)s</td>
											<td class='dl'>%(colonna)s</td>
											<td class='dl'>%(gm)s</td>
											<td class='dl'>%(anno_stagione)s</td>
											<td class='dl'>%(articolo)s</td>
											<td class='dl'>%(descrizione)s</td>
											<td class='dl'>%(composizione)s</td>
											<td class='dl'>%(fornitore_principale)s - %(ragione_sociale)s</td>
											<td class='dl'>%(misura_base)s</td>
											<td class='dl'>%(style)s</td>
											<td class='dl'>%(tipo_segnata)s</td>
											<td class='dl'>%(ricerca_colore)s</td>
											<td class='dl'>%(consumo_base)s</td>
										</tr> 
										""" %db_art %tr_style
	
	html_details['db'] += """								
									</table>
								</fieldset>
								"""
															
								
	html = html %html_details
	html = html.replace('%%','%')
	return html



def render_etichetta_lavaggio(pard, etichetta):
	html = """<table width='95%%' >"""
	for pezzo in etichetta:
		col1 = ''
		col2 = ''
		#pezzo['trasc'] = ''
		dati = diba_dao.get_img_etichetta_lavaggio_completa(pard, pezzo['etichetta'])
		for k,g in itertools.groupby(dati, key = lambda k : (k['tipo'])):
			d = {'tipo': k,
			     'dati' : list(g)}
			if d['tipo'] == 'std':
				#pezzo['trasc'] = '%s/%s' %(d['dati'][0]['trasc'][:2], d['dati'][0]['trasc'][2:])
				col1 += """ <table><tr> """
				for dd in d['dati']:
					col1 += """ <td><img src='img/care_symbol/%(img)s' title='%(desc)s' /></td> """%dd
				if len(d['dati']) < 5:
					for i in range(len(d['dati']), 5):
						col1 += "<td></td>"
				col1 += """ </tr>
							</table>"""
			else:
				for dd in d['dati']:
					dd['desc'] = dd['desc'].replace('%','%%')
					col2 += """ <tr>
									<td class ='lbl'>%(tipo)s</td>
									<td >%(desc)s</td>
								</tr> """%dd
		html += """	<tr>
						<td colspan='2' style='background-color: lightgray !important;' class ='lbl'> Pezzo %(PROGR_PEZZO)s
					</tr>
					<tr>
						<td>%%s</td>
						<td><table>%%s</table></td>
					</tr>"""%pezzo %(col1, col2)
					#- CODICE %(trasc)s</td>
	html += """</table>"""

	return html