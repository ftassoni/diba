(function()
{
	'use strict';

	AutoCompleteGFService.$inject = ['DTHttp', 'LoggerFactory', '$q'];

	var factoryId = "AutoCompleteGFService";

	angular.module('app').factory(factoryId, AutoCompleteGFService);

	function AutoCompleteGFService(DTHttp, LoggerFactory, $q)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {
			getCollezioni: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/ordpf.richieste_pf_ctrl', 'autocomplete_collezione', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			},
			getCollezioniOrig: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/ordpf.richieste_pf_ctrl', 'autocomplete_collezione_orig', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			},
			getColori: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/diba.fabbisogni_ctrl', 'autocomplete_colore', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			},
			getMisure: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/diba.fabbisogni_ctrl', 'autocomplete_misura', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			},
			getFornitore: function(param){
			    var deferred = $q.defer();
			    var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/ordpf.richieste_pf_ctrl', 'autocomplete_fornitore', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
                        console.log(response);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;

			},
			getArticoloFornitore: function(param){
			    var deferred = $q.defer();
			    var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/ordpf.richieste_pf_ctrl', 'autocomplete_art_fornitore', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
                        console.log(response);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;

			}
		};
	}
})();