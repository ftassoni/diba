(function()
{
	'use strict';

	SalvaService.$inject = ['DTHttp', 'LoggerFactory'];

	var factoryId = "SalvaService";

	angular.module('app').factory(factoryId, SalvaService);

	function SalvaService(DTHttp, LoggerFactory)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {

			salvaDiba: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.studio_diba_ctrl', 'salva_diba', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Distinta base salvata con successo");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			avanzaDiba: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.studio_diba_ctrl', 'avanza_diba', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Stato aggiornato correttamente in: valutazione");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			arretraDiba: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.studio_diba_ctrl', 'arretra_diba', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Stato aggiornato correttamente in: bozza");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			}
		};
	}
})();