(function(){
    'use strict';

    var controllerId = "StudioDibaResearchCtrl";

    ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', '$window', '$location', 'RicercaService',
                            'SOCIETA', 'CLASSI', 'AVANZAMENTO', 'GetDatiStudioService'];

    angular.module('app').controller(controllerId, ControllerFn)
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    angular.module('app').config(function($locationProvider) {
      $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
      });
    });

    function ControllerFn($scope, $rootScope, LoggerFactory, $window, $location, RicercaService,
                           SOCIETA, CLASSI, AVANZAMENTO, GetDatiStudioService) {
        var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);

        /**
         * Metodo di inizializzazione del controller
         */
        function init(){
            vm._init_fields();
        };

        function reset(){
            vm._init_fields();
            $rootScope.$emit("studiodiba:reset_data_tabelle");
        };

        function getArticoliList (articoli){
            var finalArtiList = []
            angular.forEach(articoli, function(art){
                if (finalArtiList.indexOf(art['text']) === -1){
                    finalArtiList.push(art['text']);
                }
            });
            return finalArtiList.toString();
        };

        function getFilter(){
            return {
                classe: vm.classe_orig,
                modello: vm.modello,
                nome_modello: vm.nome_modello,
                variante: vm.variante,
                societa: vm.societa,
                fam_gm: vm.fam_gm,
                gm: vm.gm,
                articolo: vm.articolo,
                collezione: vm.collezione,
                colore: vm.colore,
                art_princ: getArticoliList(vm.art_princ),
                stato: vm.stato,
                avanzamento: vm.avanzamento
            }
        };

        function _init_fields(){

            //init valori input di ricerca
            vm.classe_orig = [];
            vm.modello = $scope.getUrlCodModelli().split(",").length === 1 ? $scope.getUrlCodModelli() : '';
            vm.nome_modello = "";
            vm.variante = "";
            vm.societa = "";
            vm.fam_gm = "";
            vm.gm = "";
            vm.articolo = "";
            vm.collezione = "";
            vm.colore = "";
            vm.art_princ = "";
            vm.stato = "";
            vm.avanzamento = "";

            $scope.classeOptions = CLASSI;

            //campi
            vm.lista_societa = SOCIETA;
            vm.lista_avanzamenti = AVANZAMENTO;
            vm.lista_stati = [
                {label: 'Tutti', value: ''},
                {label: 'Bozza', value: 'bozza'},
                {label: 'Valutato', value: 'valutazione'}
            ];
        };

        function cerca() {
            var params = getFilter();
            params['id_richiesta_studio'] = getUrlId();
            params['tipo_studio'] = getUrlTipoStudio(),
            params['cod_modelli'] = $scope.getUrlCodModelli();
            $rootScope.$emit("studiodiba:ricerca", params);
        };

        function getUrlId() {
            vm.url_params = $location.search()||"Unknown";
            return vm.url_params.hasOwnProperty('id_rich_studio') ? vm.url_params['id_rich_studio'] : "";
        }

        function getUrlTipoStudio() {
            vm.url_params = $location.search()||"Unknown";
            return vm.url_params.hasOwnProperty('tipo_studio') ? vm.url_params['tipo_studio'] : "";
        }

        $scope.getCollezioneOrig = function(value){
			return GetDatiStudioService.getCollezioniOrig({'query' : value,
			                                                    'societa' : vm.societa,
			                                                    'anno_stagione': vm.anno_stagione,
			                                                    });
		};

		$scope.onSelectCollezioneOrig = function(item, value, label){
			vm.collezione = item.descrizione;
        };

        $scope.getUrlCodModelli = function() {
            vm.url_params = $location.search()||"Unknown";
            return vm.url_params.hasOwnProperty('cod_modelli') ? vm.url_params['cod_modelli'] : "";
        }

        function salva_diba(){
		    $rootScope.$emit("studiodiba:salvaMultiploDiba");
        }

        function arretra_diba(){
		    $rootScope.$emit("studiodiba:arretraMultiploDiba");
        }

        function avanza_diba(){
		    $rootScope.$emit("studiodiba:avanzaMultiploDiba");
        }

        function estrai_diba() {
            var params = getFilter();
            params['id_richiesta_studio'] = getUrlId();
            params['tipo_studio'] = getUrlTipoStudio(),
            params['cod_modelli'] = $scope.getUrlCodModelli();
            $rootScope.$emit("studiodiba:estrazione", params);
        }

        function stampa_diba() {
            var params = getFilter();
            var modelli = '';
            if (vm.modello != ''){
                modelli = vm.modello;
            }else if ($scope.getUrlCodModelli() != ''){
                modelli = $scope.getUrlCodModelli();
            }else {
                modelli = '';
            }
            params['id_richiesta_studio'] = getUrlId();
            params['tipo_studio'] = getUrlTipoStudio();
            params['cod_modelli'] = modelli;
            $rootScope.$emit("studiodiba:stampaDiba", params);
        }

        vm.init = init;
        vm.cercaClick = cerca;
        vm.salvaDiba = salva_diba;
        vm.arretraDiba = arretra_diba;
        vm.avanzaDiba = avanza_diba;
        vm.estraiDiba = estrai_diba;
        vm.stampaDiba = stampa_diba;
        vm._init_fields = _init_fields;
        vm._reset_fields = reset;
        vm.init();
        vm.cercaClick();

    }
})();