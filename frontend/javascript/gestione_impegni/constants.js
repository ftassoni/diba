(function(){

    'use strict';


	var SOCIETA = [
		{name: "", id: ""},
		{name: "Max Mara", id: "MM"},
		{name: "Marella", id: "MA"},
		{name: "Diffusione Tessile", id: "DT"},
		{name: "Manifatture del Nord", id: "MN"}
	];

	var SOCIETA_ORIG = [
		{name: "", id: ""},
		{name: "Max Mara", id: "MM", id_orig: "OM"},
		{name: "Marella", id: "MA", id_orig: "OA"},
		{name: "Manifatture del Nord", id: "MN", id_orig: "ON"}
	];

	var MACROCOLORE = [
	    {name: "Tutti", id: ""},
		{name: "ARANCIO", id: "1"},
		{name: "ARGENTO", id: "2"},
		{name: "BEIGE/CAMMELLO", id: "4"},
		{name: "BIANCO", id: "5"},
		{name: "GIALLO", id: "6"},
		{name: "GRIGIO", id: "7"},
		{name: "MARRONE", id: "8"},
		{name: "NERO", id: "9"},
		{name: "ORO", id: "10"},
		{name: "ROSA", id: "11"},
		{name: "ROSSO", id: "12"},
		{name: "VERDE", id: "13"},
		{name: "VIOLA", id: "14"},
		{name: "AZZURRO/BLU", id: "3"},
	];

    var MODELLO = {
		'name': 'Per Modello',
		'value': 'modello'
	};

	var ARTICOLO = {
		'name': 'Per Articolo',
		'value': 'articolo'
	};

    var TIPO_VIS = [
	    MODELLO,
		ARTICOLO
	];


	var UI_ANNI_STAGIONE_ID = [{id:"", name:"Tutti"}, {id:"PP", name:"permanente"}];
    var ui_anni_stagione_n_id = [{id:"", name:"Tutti"}, {id:"PP", name:"permanente"}];

    for (var i=2012; i <= new Date().getFullYear()+1; i++ ) {

        var ai_item = {
            id: String(i)[3] + "2",
            name: i + " A/I"
        };

        var pe_item = {
            id: String(i)[3] + "1",
            name: i + " P/E"
        };

        ui_anni_stagione_n_id.push(ai_item);
        ui_anni_stagione_n_id.push(pe_item);
        UI_ANNI_STAGIONE_ID.push(ai_item);
        UI_ANNI_STAGIONE_ID.push(pe_item);

    }


	var UI_ANNI_STAGIONE = [{id:"", name:"Tutti"}, {id:"permanente", name:"Permanente"}];
    var ui_anni_stagione_n = [{id:"", name:"Tutti"}, {id:"permanente", name:"Permanente"}];

    for (var i=2012; i <= new Date().getFullYear()+1; i++ ) {

        var ai_item = {
            id: i + " A/I",
            name: i + " A/I"
        };

        var pe_item = {
            id: i + " P/E",
            name: i + " P/E"
        };

        ui_anni_stagione_n.push(ai_item);
        ui_anni_stagione_n.push(pe_item);
        UI_ANNI_STAGIONE.push(ai_item);
        UI_ANNI_STAGIONE.push(pe_item);

    }

    var CLASSI = [];
    for (var i=1; i <= 99; i++ ) {
        if (i>0 && i<10)
            CLASSI.push('0'+i)
        else
            CLASSI.push(i.toString())
    }

    var STATUS = [
        {'name':'Tutti', 'value':'tutti'},
        {'name':'Da soddisfare', 'value':'incompleti'},
        {'name':'Completi', 'value':'completi'}
    ];

	angular.module('app')
		.constant('SOCIETA', SOCIETA)
		.constant('SOCIETA_ORIG', SOCIETA_ORIG)
		.constant('MODELLO', MODELLO)
		.constant('ARTICOLO', ARTICOLO)
		.constant('TIPO_VIS', TIPO_VIS)
		.constant('UI_ANNI_STAGIONE', UI_ANNI_STAGIONE)
		.constant('UI_ANNI_STAGIONE_ID', UI_ANNI_STAGIONE_ID)
		.constant('STATUS', STATUS)
		.constant('CLASSI', CLASSI)
		.constant('MACROCOLORE', MACROCOLORE)
	;


})();