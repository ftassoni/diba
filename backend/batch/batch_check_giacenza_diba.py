import sys
# sys.path.insert(0, '/Users/marcogualdi/dt-home/re/mp/bin')
# sys.path.insert(1, '/Users/marcogualdi/dt-home/re/mp/bin/workflow_mmfg/')
# sys.path.insert(0, '/Users/elisafusari/dt-home/re/mp/bin')
# sys.path.insert(1, '/Users/elisafusari/dt-home/re/mp/bin/workflow_mmfg/')
sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/re/mp/bin/workflow_mmfg/')

import tools, traceback, json, notifica_mail
from facon.diba.facade import fabbisogni_facade

DESTINATARI_MAIL = ['dtsupport@otconsulting.com']

def main(pard, capocmat=''):
	name_file = 'giacenze_capocmat.log'
	filepath_giac = pard['LOG_DIR'] + '/' + name_file
	file = open(filepath_giac, 'a')
	try:
		tools.batch_logging('Avvio esecuzione...', file)
		fabfac = fabbisogni_facade.FabbFacade(pard)
		res_giac = fabfac.check_giacenza_diba(capocmat)
		if capocmat:
			# std output
			file = ''
		if res_giac['final_list_giacenze']:
			tools.batch_logging('Inizio Scrittura File giacenze_capocmat.log...', file)
			for (key, lista) in res_giac['final_list_giacenze'].items():
				tools.batch_logging(key + '\n', file)
				for l in lista:
					string_diz = json.dumps(l)
					tools.batch_logging(string_diz + '\n', file)
				tools.batch_logging('Totali %s negative trovate: '%key + str(len(lista)) + '\n', file)
			tools.batch_logging('Totali giacenze negative trovate: ' + str(res_giac['final_counter']) + '\n', file)
			tools.batch_logging('Fine Scrittura File giacenze_capocmat.log...', file)
	except Exception, e:
		tools.batch_logging(traceback.format_exc(), file)
		if pard['AMBIENTE'] not in ['svil','ot']:
			name_file = 'giacenze_capocmat.log'
			filepath_giac = pard['LOG_DIR'] + '/' + name_file
			body = "Messaggio dal server: %(IP_ADDRESS)s <br><br> " % pard
			body += "Errore nel batch di notifica impegni (batch_check_giacenza_diba.py) <br><br> "
			body += traceback.format_exc() + "<br><br>"
			body += "Consultare il log in %s" % filepath_giac
			subject = "[BATCH] Errore: Notifica impegni"
			notifica_mail.invia('debug@diffusionetessile.it', DESTINATARI_MAIL,
								subject=subject, corpo=body, rispondi_a=[], tipo='html', pre=False)
	finally:
		tools.batch_logging('Fine esecuzione...', file)





if __name__ == "__main__" :
	import config
	pard = config.configura_batch_crontab(user='CRON')
	if len(sys.argv) == 1: #and '-fix' not in sys.argv[1]:
		main(pard)
	elif len(sys.argv) > 1:
		capocmat = sys.argv[1]
		main(pard, capocmat)