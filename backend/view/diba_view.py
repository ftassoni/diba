import tools
import time
import pprint
import menu
import cjson
import get_static
import caio_print as cp

from Common import utilities

from facon.diba.diba_common import *


def render_page_base(pard):
	# jquery-ui-1.8.12.custom.js
	# jquery.ui.core.js
	# jq.js
	# jquery-1.4.4.min.js
	
    html = """
    <?xml version="1.0" encoding="UTF-8"?>
    <HTML>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <HEAD>
        		<SCRIPT type="text/javascript" src="javascripts/jq/jq.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-ui-1.8.10.custom.min.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery.ui.core.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery.json-2.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/ui.datepicker-it.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-1.8.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery-ui-1.9.2.js"></SCRIPT>
				<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
                %%(page_scripts)s
                %%(page_css)s
                <title>%%(page_title)s</title>
        </HEAD>
        <BODY>
                %%(body_page)s
        </BODY>
    </HTML>""" %pard
    
    return html

def pagina_base_db(pard, dati = {}):
	"""Definisco il campo head della pagina, generalmente piuttosto statico"""
	page_head = {}
	
	js1 = get_static.get_link(pard, "ordini-common.js", "facon.pao")
	js2 = get_static.get_link(pard, "diba.js", "facon.diba")
	
	page_head['page_scripts']="""
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="vertical_tabs/vertical-tabs.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.columnFilter.js"></SCRIPT>
						<SCRIPT type='text/javascript'>
							  jQuery.noConflict();	
							  jQuery(document).ready(function() {init();});
						</SCRIPT>
						""" %(js1, js2)
	
	css1 = get_static.get_link(pard, "diba.css", "facon.diba")				
	page_head['page_css']="""
						<link rel="stylesheet" type="text/css" href="web.css">
						<link rel="stylesheet" type="text/css" href="%s">
						<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.9.2.custom.min.css">
						<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
						<link rel="stylesheet" type="text/css" href="vertical_tabs/vertical-tabs.css">
						"""%(css1)
		
	page_head['page_title']="""
							Distinta Base
							"""
	"""Chiave dell'head che serve solo perche il % ammette solo due occorrenze consecutive e non tre"""
	page_head['body_page']="""
							%(body_page)s
							"""
						
	"""Recupero alcuni parametri di utility dal pard"""
	pard['menu'] = menu.menu_bar(pard)
	pard['TABLE_WIDTH'] = '98%'
	pard['separator'] = "<hr width='%(TABLE_WIDTH)s' noshade>" % pard
	
	
	"""Costruisco la pagina base  e poi innesto tutti i vari elementi in modo incrementale"""
	html = render_page_base(pard)
	html = html % page_head
	
	common_body = _make_common_body(pard)
	
	"""Costruisco la tabella e richiamo un metodo di utility per generare le varie righe"""

	page_elements = {}
	page_elements['intestazione_div'] = render_intestazione_diba(pard, dati['dati_mod'])
	page_elements['dati_modello_div'] = render_dati_modello_dt(pard)
	page_elements['dati_diba_div'] = render_dati_diba(pard, dati['dati_mod'], dati['db'])
	page_elements['render_wait_div'] = render_wait_div(pard)

	
	page_body={}
	
	page_body['body_page']= common_body %page_elements  %pard
	
	html = html % page_body
	return html



def render_wait_div(pard):
	''' stringa di costruzione del pannello di attesa durante la ricerca ''' 
	html= """
		<div id="overall_container" >
			<div id="loading_box">
				<center>
					<h1>Elaborazione in corso</h1>
					<img src="img/loading/ajax_loader_bar.gif"/>
				</center>
			</div>	
		</div>
		"""
	return html


def _make_common_body(pard):
	common_body = """
				<input type="hidden" id="sid" value="%%(sid)s">
				<input type="hidden" id="app_server" value="%%(APPSERVER)s">
				<input type="hidden" id="menu_id" value="%%(menu_id)s">
				<input type="hidden" id="module" value="%%(module)s">
				
				<div id='overlay'> </div>
						
				<div class="pagina_dynamic">
					%%(menu)s
					<form autocomplete='off'> 
					<br>
					<div id='errors'>
					 </div>
					 <div id='message'>
					 </div>
					 %(render_wait_div)s
					 <div id = 'intestazione_div'>
					 %(intestazione_div)s
					 </div>
					 <br>
					<div id="global_tabs">
					 	<ul>
					 		<li><a href="#tab_1">
					 			<input id='btn_add_1' type='button' class='btn_add' title='Copia scheda modello' onclick='add_tab(1)'/>
					 			<span></span>
					 		</a></li>
					 	</ul> 
						<div id="tab_1" align='center'>
							%(dati_modello_div)s
							<br>
							<br>
							%(dati_diba_div)s
					 	</div>
					</div>
					</form>
				</div> 
			"""
	return common_body


def render_intestazione_diba(pard, valori_parametri={}):
	valori_parametri.setdefault('modello_base','')
	valori_parametri.setdefault('anno','')
	valori_parametri.setdefault('stag','')
	valori_parametri.setdefault('stagione','')
	if valori_parametri['stagione']=='1':
		valori_parametri['stag'] = 'P/E'
	elif valori_parametri['stagione']=='2':
		valori_parametri['stag'] = 'A/I'
	valori_parametri.setdefault('uscita_collezione','')
	valori_parametri.setdefault('desc_uscita_collezione','')
	valori_parametri.setdefault('desc_classe','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('gruppo_merceologico','')
	valori_parametri.setdefault('progr_articolo','')
	valori_parametri.setdefault('desc_articolo','')
	
	
	html = """
					<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
					
					<tr>
						<td width="80px">
							<img class="click_load_schizzo" height="70px" title="Visualizza Schizzo" style="cursor:pointer;" alt="Inline image" src="">
						</td>
						<td align='center'>
							<b><span style="font-size:12;color:#444444;">Modello: </span>
								<span style="font-size:15;color:red;">%(modello_base)s000 - %(nome)s</span>
								</b>
							<br>
							<b>
							<span style="font-size:11;color:#444444;">%(classe)s - %(desc_classe)s (%(uscita_collezione)s - %(desc_uscita_collezione)s %(anno)s %(stag)s )</span>
							</b>
								<br>
								Articolo principale:
								<b>%(gruppo_merceologico)s - %(progr_articolo)s</b>
								(%(desc_articolo)s)
						</td>
						<td width="80px">
							<input type='button' id='btn_salva' value='Salva' title='Salva distinte base importate' />
							<input type='button' id='btn_annulla' value='Annulla' title='Annulla' />
						</td>
					</tr>			
				</table>
			
		"""%valori_parametri
	return html

def render_dati_modello_dt(pard, valori_parametri={}):
	html = """ 
				<table align='center' width='800px'>
					<tr> 
						<td class='lbl'>Modello DT</td>
						<td class='lbl'>Descrizione</td>
						<td class='lbl'>Collezione DT</td>
						<td class='lbl'>Anno Stag. DT</td>
					</tr>
					<tr> 
						<td class='lbl'><input type='text' size='15' class='il' id='param_modello_dt' onchange='set_modello()' value='' /></td>
						<td class='lbl'><input type='text' size='15' class='il' id='param_modello_descr' value='' /></td>
						<td class='lbl'><input type='text' size='15' class='il' id='param_collezione_dt' value='' /></td>
						<td class='lbl'><input type='text' size='15' class='il' id='param_as_dt' value='' /></td>
					</tr>
				</table>
				
	"""
	return html

def render_dati_diba(pard, valori_parametri={}, db={}):
	html = ''
	html = """<table id="result_table" align='center' class="" cellpadding="3px" style="border-collapse:collapse;">
					<thead>
						<tr>
							<th class='hc' rowspan='2'>Azioni</th>
							<th class='hc' colspan='5'>DiBa Originale</th>
							<th></th>
							<th class='hc_dt' colspan='6'>DiBa DT</th>
							<th></th>
							<th class='hc' rowspan='2'>Consumo Base</th>
						</tr>
						<tr>
							<th class='hc'>Gm</th>
							<th class='hc'>AS</th>
							<th class='hc'>Art</th>
							<th class='hc'>Desc. Art.</th>
							<th class='hc'>Compos</th>
							<th class=''>&nbsp;</th>
							<th class='hc_dt'>CAPOCMAT</th>
							<th class='hc_dt' width='300px'>Descr.</th>
							<th class='hc_dt'>Qta Ord.</th>
							<th class='hc_dt'>MAG 9</th>
							<th class='hc_dt'>MAG 801</th>
							<th class='hc_dt'>in Transito</th>
							<th class=''>&nbsp;</th>
						</tr>
					</thead> """	
										
	chiave = db.keys()[0]
	for db_art in db[chiave]:
		tr_style = ''
		if int(db_art['id_articolo']) == int(valori_parametri['articolo']):
			tr_style = """ style="background-color: #FFFFCC" """
		html += """
										<tr %%s>
											<td class='dl'>
												<input id='btn_del_diba' type='button' class='btn_delete'  onclick=''/>
											</td>
											<td class='dl'>%(gm)s</td>
											<td class='dl'>%(anno_stagione)s</td>
											<td class='dl'>%(articolo)s</td>
											<td class='dl'>%(descrizione)s</td>
											<td class='dl'>%(composizione)s</td>
											<td class=''>&nbsp;</td>
											<td class='dl'><input type='text' class='il' size='20' /></td>
											<td class='dl'></td>
											<td class='dl'></td>
											<td class='dl'></td>
											<td class='dl'></td>
											<td class='dl'></td>
											<td class=''>&nbsp;</td>
											<td class='dl' style='text-align:right;'>%(consumo_base)s</td>
										</tr> 
										""" %db_art %tr_style
	
	html += """	<tr>
					<td class='dl'>
						<input id='btn_add_row' type='button' class='btn_add'  onclick=''/>
					</td>
					<td class='dl'><input type='text' class='il' size='4' /></td>
					<td class='dl'><input type='text' class='il' size='10' /></td>
					<td class='dl'><input type='text' class='il' size='10' /></td>
					<td class='dl'></td>
					<td class='dl'></td>
					<td class=''>&nbsp;</td>
					<td class='dl'><input type='text' class='il' size='20' /></td>
					<td class='dl'></td>
					<td class='dl'></td>
					<td class='dl'></td>
					<td class='dl'></td>
					<td class='dl'></td>
					<td class=''>&nbsp;</td>
					<td class='dl' style='text-align:right;'><input type='text' class='il' size='10' /></td>
				</tr> 							
			</table>
			"""
																						
								
	return html




