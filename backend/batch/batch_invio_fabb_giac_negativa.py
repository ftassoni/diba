import sys

sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/re/mp/bin/workflow_mmfg/')
# sys.path.insert(0, '/Users/marcogualdi/dt-home/re/mp/bin')
# sys.path.insert(1, '/Users/marcogualdi/dt-home/re/mp/bin/workflow_mmfg/')
import tools, traceback, time, notifica_mail, os

DESTINATARI_MAIL = ['dtsupport@otconsulting.com']

def main(pard, name_file):
	try:
		tools.batch_logging('Inizio esecuzione...')
		time.sleep(5)
		if pard['AMBIENTE'] not in ['ot']:
			tools.batch_logging('Invio email giacenza negativa...')
			body = "GIACENZA NEGATIVA APPENA SALVATA DAL SERVER: %(IP_ADDRESS)s <br><br> " % pard
			body += "Consultare il log in %s" % name_file
			subject = "SALVATA GIACENZA NEGATIVA"
			notifica_mail.invia('debug@diffusionetessile.it',DESTINATARI_MAIL,
						subject=subject,corpo=body,files = [str(name_file)], rispondi_a=[],tipo='html',pre=False)
			tools.batch_logging('Email inviata')
			tools.batch_logging('Elimino file su disco...')
			time.sleep(2)
			os.remove(name_file)
			tools.batch_logging('File eliminato')
	except Exception, e:
		tools.batch_logging(traceback.format_exc())
		if pard['AMBIENTE'] not in ['ot']:
			body = "Messaggio dal server: %(IP_ADDRESS)s <br><br> " % pard
			body += "Errore nel batch di notifica impegni (batch_invio_fabb_giac_negativa.py) <br><br> "
			body += traceback.format_exc() + "<br><br>"
			subject = "[BATCH] Errore: Giacenza Negativa"
			notifica_mail.invia('debug@diffusionetessile.it', DESTINATARI_MAIL,
								subject=subject, corpo=body, rispondi_a=[], tipo='html', pre=False)

	finally:
		tools.batch_logging('Fine esecuzione...')

if __name__ == "__main__" :
	import config
	import cjson
	pard = config.global_parameters({})
	pard['menu_selected'] = ''
	pard['sid'] = ''
	pard['menu_id'] = ''
	pard['module'] = 'batch_invio_fabb_giac_negativa'
	pard['program'] = 'main'
	name_file = sys.argv[1]
	main(pard, name_file)
