from Common.errors import DAOException
from facon.ordpf.dao.decorators import query_with_result, query_without_result, insert_query_with_id_as_result
from facon.diba.dao import diba_dao
from facon.ordpf.facade.pool_manager import ConnPool, Request
from facon.ordpf.logger import logger
import db_access
import tools
import itertools
import time
import anno_stagione_lib
from facon.diba.diba_common import SOCIETA_LIST




def applica_filtri_ricerca(params):
	filtri = []

	q_id_rich = ''
	if params.get('id_richiesta', ''):
		q_id_rich = " and (R.ID = '%(id_richiesta)s' or R_figli.ID = '%(id_richiesta)s' ) " % params
		filtri.append(q_id_rich)

	q_societa = ''
	if params.get('societa', ''):
		q_societa = " and D.SOCIETA_ORIG = '%(societa)s' " % params
		filtri.append(q_societa)

	q_classe = ''
	if params.get('classe_orig', '') and params['classe_orig']:
		q_classe = " and substring(D.MODELLO_ORIG,2,2) IN (%s) " % ','.join(params['classe_orig'])
		filtri.append(q_classe)

	q_modello = ''
	if params.get('modello', ''):
		q_modello = " and D.MODELLO_ORIG = '%(modello)s' " % params
		filtri.append(q_modello)

	q_variante = ''
	if params.get('variante', ''):
		q_variante = " and D.VARIANTE_DT = '%(variante)s' " % params
		filtri.append(q_variante)

	q_nome_modello = ''
	if params.get('nome_modello', ''):
		q_nome_modello = " and D.NOME_MODELLO_ORIG like '%(nome_modello)s%%' " % params
		filtri.append(q_nome_modello)

	q_capocmat_societa = ''
	if params.get('capocmat_societa', ''):
		q_capocmat_societa = " and D.SOCIETA = '%(capocmat_societa)s' " % params
		filtri.append(q_capocmat_societa)

	q_capocmat_gm = ''
	if params.get('capocmat_gm', ''):
		q_capocmat_gm = " and D.GM = '%(capocmat_gm)s' " % params
		filtri.append(q_capocmat_gm)

	q_capocmat_as = ''
	if params.get('capocmat_as', ''):
		params['capocmat_as'] = anno_stagione_lib.decode_as(params['capocmat_as'])
		q_capocmat_as = " and D.ANNO_STAGIONE = '%(capocmat_as)s' " % params
		filtri.append(q_capocmat_as)

	q_capocmat_articolo = ''
	if params.get('capocmat_articolo', ''):
		q_capocmat_articolo = " and D.ARTICOLO = '%(capocmat_articolo)s' " % params
		filtri.append(q_capocmat_articolo)

	q_capocmat_misura = ''
	if params.get('capocmat_misura', ''):
		q_capocmat_misura = " and D.MISURA = '%(capocmat_misura)s' " % params
		filtri.append(q_capocmat_misura)

	q_capocmat_colore = ''
	if params.get('capocmat_colore', ''):
		q_capocmat_colore = " and D.COLORE = '%(capocmat_colore)s' " % params
		filtri.append(q_capocmat_colore)

	q_anno_stagione_dt = ''
	if params.get('anno_stagione', '') and params['anno_stagione'] != 'tutti':
		q_anno_stagione_dt = " and R.ANNO_STAGIONE = '%(anno_stagione)s' " % params
		filtri.append(q_anno_stagione_dt)

	q_anno_stagione_orig = ''
	if params.get('anno_stagione_orig', '') and params['anno_stagione_orig'] != 'tutti':
		q_anno_stagione_orig = " and RR.AS_ORIG = '%(anno_stagione_orig)s' " % params
		filtri.append(q_anno_stagione_orig)

	q_coll_dt = ''
	if params.get('collezione', ''):
		q_coll_dt = " and R.COLLEZIONE = '%(collezione)s' " % params
		filtri.append(q_coll_dt)

	q_coll_orig = ''
	if params.get('collezione_orig', ''):
		q_coll_orig = " and RR.COLLEZIONE_ORIG = '%(collezione_orig)s' " % params
		filtri.append(q_coll_orig)

	q_stato_diba = ''
	if params.get('stato_diba', '') and params['stato_diba'] != 'tutti':
		q_stato_diba = " and F.STATO = '%(stato_diba)s' " % params
		filtri.append(q_stato_diba)

	q_tipo_fabb = ''
	if params.get('tipo_fabb', '') and params['tipo_fabb'] != 'tutti':
		if params['tipo_fabb'] == 'previsionale':
			q_tipo_fabb = " and F.QTA_FABB_PREV > 0 "
		elif params['tipo_fabb'] == 'teorico':
			q_tipo_fabb = " and F.QTA_FABB_TEOR > 0 "
		elif params['tipo_fabb'] == 'reale':
			q_tipo_fabb = " and F.QTA_FABB_REALE > 0 "
		filtri.append(q_tipo_fabb)

	q_stato = ''
	if params.get('stato', '') and params['stato'] != 'tutti':
		if params['stato'] == 'da_soddisfare':
			q_stato = " and F.COMPLETATO != 'S' " % params
		elif params['stato'] == 'completato':
			q_stato = " and F.COMPLETATO = 'S' " % params
		filtri.append(q_stato)

	q_fam_gm = ''
	if params.get('fam_gm', ''):
		q_fam_gm = " and FGM.FAM_GM = '%(fam_gm)s'" % params
		filtri.append(q_fam_gm)


	q_art_tessuto = ''
	if params.get('articolo', ''):
		q_art_tessuto = " and RR.ART_PRINC IN (%(articolo)s)" % params
		filtri.append(q_art_tessuto)

	q_tessuto_descr = ''
	if params.get('articolo_descr', ''):
		q_tessuto_descr = " and RR.DESCR_ART_PRINC like '%%%(articolo_descr)s%%'" % params
		filtri.append(q_tessuto_descr)

	filtri_bp, join_clause = _applica_filtri_ricerca_bp(params)
	filtri.extend(filtri_bp)

	return ' '.join(filtri), ' '.join(join_clause)


def _applica_filtri_ricerca_bp(params):
	filtri = []
	join_clause = []
	q_data_da_bp = ''
	if params.get('data_da', ''):
		q_data_da_bp = " and B.RIF_INTR >= %s000000 " % time.strftime('%y%m%d',
		                                                              time.strptime(params['data_da'], '%Y-%m-%d'))
		filtri.append(q_data_da_bp)

	q_data_a_bp = ''
	if params.get('data_a', ''):
		q_data_a_bp = " and B.RIF_INTR <= %s999999  " % time.strftime('%y%m%d',
		                                                              time.strptime(params['data_a'], '%Y-%m-%d'))
		filtri.append(q_data_a_bp)

	q_num_bp = ''
	if params.get('num_bp', ''):
		q_num_bp = " and B.RIF_INTR like '%%%s' " % str(params['num_bp'])
		filtri.append(q_num_bp)
	if filtri:
		join_clause.append(""" left join ORDINI_PF_RIGHE ORI ON (ORI.ID_RIGA_RCOMM_PF = IFNULL(RR_figli.ID, RR.ID) and ORI.VARIANTE = D.VARIANTE_DT)
				        left join FACBP_ORDPF_ASSO FOA ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
				        left join FACBP B ON (B.RIF_INTR = FOA.RIF_BP)
				        left join FACBP_EST BE ON (BE.RIF_INTR = B.RIF_INTR AND BE.FLAG_SALDATO != 'S') """)
	return filtri, join_clause


def applica_filtri_ricerca_bp(params):
	filtri, join_clause = _applica_filtri_ricerca_bp(params)
	return ' '.join(filtri), ' '.join(join_clause)


@query_with_result
def ricerca_fabbisogni(dict_params={}):
	filtri, join_clause = applica_filtri_ricerca(dict_params)
	query = """ select  distinct R.ID as id_richiesta,
						AR.ID as id_art_comm,
						F.ID as id_fabb,
						D.ID as id_diba,
						R.TIPO_RICHIESTA as tipo_richiesta,
					    D.SOCIETA_ORIG as societa_orig,
                        D.MODELLO_ORIG as modello_orig,
                        if(D.VARIANTE_DT ='', D.VARIANTE_ORIG, D.VARIANTE_DT) as variante,
                        ifnull(D.NOME_MODELLO_ORIG, RR.NOME_MODELLO_ORIG) as nome_modello,
                        R.ANNO_STAGIONE as anno_stagione_dt,
                        R.COLLEZIONE as collezione_dt,
                        IF(RR.NOME_VARIANTE_ORIG = '', AR.DESC_COL_ORIG, RR.NOME_VARIANTE_ORIG) as colore,
                        IFNULL(RR.U_NAS,'') as utente,
						F.QTA_FABB_PREV as qta_prev,
						F.CAPI_FABB_PREV as capi_prev,
						F.QTA_FABB_TEOR as qta_teor,
						F.CAPI_FABB_TEOR as capi_teor,
						F.CAPI_LANCIATI as capi_lanciati,
						F.QTA_FABB_REALE as qta_reale,
						D.SOCIETA as societa,
						D.GM as gm,
						D.ARTICOLO as articolo,
						D.ANNO_STAGIONE as anno_stagione,
						substring(D.CAPOCMAT, 1, 9) as capocmat,
                        D.COLORE as colore,
                        D.MISURA as misura,
                        D.DESCRIZIONE as descrizione
				from FABBISOGNI F
	            JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
	            join FACFAMGM_ASSO FGM ON (D.SOCIETA = FGM.SOC AND D.GM = FGM.SOC_GM)
	            JOIN RICHIESTE_COMM_PF_ART AR on (AR.ID = F.ID_ART_COMM)
	            JOIN RICHIESTE_COMM_PF_RIGHE RR on (RR.ID = AR.ID_RIGA)
	            join RICHIESTE_COMM_PF R on (R.ID = RR.ID_RICHIESTA)
	            left JOIN RICHIESTE_COMM_PF_ART AR_figli on (AR.ID = AR_figli.ID_ART_PADRE)
		        left JOIN RICHIESTE_COMM_PF_RIGHE RR_figli on (AR_figli.ID_RIGA = RR_figli.ID)
		        left JOIN RICHIESTE_COMM_PF R_figli on (R_figli.ID = RR_figli.ID_RICHIESTA)
		        %s
	            WHERE 1=1
	            and R.STATO != 'chiusa'
	            %s
	            """%(join_clause, filtri)
	#tools.dump(query)
	return query



@query_with_result
def get_dettaglio_x_modello(dati):
	query = """  select  group_concat(distinct F.ID) as id_fabbs_dett,
							group_concat(distinct D.ID) as id_dibas_dett,
							substring(D.CAPOCMAT, 1, 9) as capocmat,
	                        D.COLORE as colore,
	                        D.DESCRIZIONE as descrizione,
	                        D.MISURA as misura,
							D.MODELLO_ORIG as modello,
							SUM(F.QTA_FABB_PREV) as qta_prev,
							F.CAPI_FABB_PREV as capi_prev,
							SUM(F.QTA_FABB_TEOR) as qta_teor,
							F.CAPI_FABB_TEOR as capi_teor,
							SUM(F.QTA_FABB_REALE) as qta_reale,
							F.CAPI_LANCIATI as capi_lanciati,
							group_concat(distinct RR.ID) as id_rcomm_list,
							concat_ws(',',group_concat(distinct AR.ID), ifnull(group_concat(distinct AR_figli.ID),'')) as id_artcomm_list,
							group_concat(distinct D.VARIANTE_DT) as var_dt_list,
							ifnull(if(group_concat(distinct concat(R_figli.DATA_RICHIESTA, ' - ',R_figli.ID)) is not null,group_concat(distinct concat(R_figli.DATA_RICHIESTA, ' - ',R_figli.ID)), group_concat(distinct concat(R.DATA_RICHIESTA, ' - ',R.ID) ) ), '') as rif_rich,
							if(SUM(F.QTA_FABB_REALE)>0,SUM(F.QTA_FABB_REALE),if(SUM(F.QTA_FABB_TEOR)>0, SUM(F.QTA_FABB_TEOR), SUM(F.QTA_FABB_PREV))) as fabbisogno
					from FABBISOGNI F
		            JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
		            JOIN RICHIESTE_COMM_PF_ART AR on (AR.ID = F.ID_ART_COMM)
		            JOIN RICHIESTE_COMM_PF_RIGHE RR on (RR.ID = AR.ID_RIGA)
		            join RICHIESTE_COMM_PF R on (R.ID = RR.ID_RICHIESTA)
			        left JOIN RICHIESTE_COMM_PF_ART AR_figli on (AR.ID = AR_figli.ID_ART_PADRE)
			        left JOIN RICHIESTE_COMM_PF_RIGHE RR_figli on (AR_figli.ID_RIGA = RR_figli.ID)
			        left JOIN RICHIESTE_COMM_PF R_figli on (R_figli.ID = RR_figli.ID_RICHIESTA)
		            WHERE F.ID in (%(id_fabbs)s)
		            group by D.CAPOCMAT
		            """ % dati
	#tools.dump(query)
	return query


# @query_with_result
# def get_bp_by_id_artcomm_righe(id_artcomm_list, var_dt_list):
# 	query = """ select ifnull(group_concat(distinct FOA.RIF_BP), '') as rif_bp
# 				from ORDINI_PF_RIGHE ORI
# 			    join FACBP_ORDPF_ASSO FOA ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
# 			    join FACBP_EST BE ON (BE.RIF_INTR = FOA.RIF_BP AND BE.FLAG_SALDATO!='S')
# 				where ORI.ID_ART_RCOMM_PF in (%s)
# 				and ORI.VARIANTE in (%s)
# 			"""%(','.join([i for i in id_artcomm_list.split(',') if i]), var_dt_list)
# 	#tools.dump(query)
# 	return query

@query_with_result
def get_dettaglio_x_articolo(dati):
	query = """  select  group_concat(distinct F.ID) as id_fabbs_dett,
						group_concat(distinct D.ID) as id_dibas_dett,
						substring(D.CAPOCMAT, 1, 9) as capocmat,
                        D.COLORE as colore,
                        D.MISURA as misura,
                        if(D.VARIANTE_DT ='', D.VARIANTE_ORIG, D.VARIANTE_DT) as variante,
                        D.NOME_MODELLO_ORIG as nome_modello,
                        D.MODELLO_ORIG as modello_orig,
                        D.SOCIETA_ORIG as societa_orig,
						SUM(F.QTA_FABB_PREV) as qta_prev,
						SUM(F.QTA_FABB_TEOR) as qta_teor,
						SUM(F.QTA_FABB_REALE) as qta_reale,
						if(F.CAPI_LANCIATI>0,SUM(F.CAPI_LANCIATI),if(F.CAPI_FABB_TEOR>0, F.CAPI_FABB_TEOR, F.CAPI_FABB_PREV)) as tot_capi,
						group_concat(distinct RR.ID) as id_rcomm_list,
						concat_ws(',',group_concat(distinct AR.ID), ifnull(group_concat(distinct AR_figli.ID),'')) as id_artcomm_list,
						group_concat(distinct D.VARIANTE_DT) as var_dt_list,
						if(R.TIPO_RICHIESTA ='standard', group_concat(distinct concat(R.DATA_RICHIESTA, ' - ',R.ID)), group_concat(distinct concat(R_figli.DATA_RICHIESTA, ' - ',R_figli.ID))) as rif_rich,
						if(R.TIPO_RICHIESTA ='studio', group_concat(distinct concat(R.DATA_RICHIESTA, ' - ',R.ID)), '') as rif_studio,
						if(SUM(F.QTA_FABB_REALE)>0,SUM(F.QTA_FABB_REALE),if(SUM(F.QTA_FABB_TEOR)>0, SUM(F.QTA_FABB_TEOR), SUM(F.QTA_FABB_PREV))) as fabbisogno
				from FABBISOGNI F
	            JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
	            JOIN RICHIESTE_COMM_PF_ART AR on (AR.ID = F.ID_ART_COMM)
	            JOIN RICHIESTE_COMM_PF_RIGHE RR on (AR.ID_RIGA = RR.ID)
			    join RICHIESTE_COMM_PF R on (R.ID = RR.ID_RICHIESTA)
		        left JOIN RICHIESTE_COMM_PF_ART AR_figli on (AR.ID = AR_figli.ID_ART_PADRE)
		        left JOIN RICHIESTE_COMM_PF_RIGHE RR_figli on (AR_figli.ID_RIGA = RR_figli.ID)
		        left JOIN RICHIESTE_COMM_PF R_figli on (R_figli.ID = RR_figli.ID_RICHIESTA)
		        left JOIN IMPEGNI I ON (F.ID = I.ID_FABBISOGNO AND I.TIPO='DT' AND I.STATO ='valido' )
		        LEFT JOIN FACCOM FC ON (I.ID_FACCOM = FC.TI_ROWID)
	            WHERE F.ID in (%(id_fabbs)s)
	            group by F.ID_ART_COMM, D.CAPOCMAT, D.MODELLO_ORIG, if(D.VARIANTE_DT ='', D.VARIANTE_ORIG, D.VARIANTE_DT)
	            """ %dati
	#tools.dump(query)
	return query


def get_qta_capi(params, res):
	diz_out = {'capi_prev' : 0, 'capi_teor' : 0, 'capi_lanciati': 0 , 'fabbisogno': 0,
	           'qta_prev': 0, 'qta_teor': 0, 'qta_reale': 0 , 'tot_capi' : 0}
	res = sorted(res, key = lambda k: (k['societa_orig'], k['modello_orig'], k['variante']))
	for k,g in itertools.groupby(res, key = lambda k: (k['societa_orig'], k['modello_orig'], k['variante'])):
		mod = list(g)
		diz_out['capi_prev'] += int(mod[0]['capi_prev']) if mod[0]['capi_prev'] else 0
		diz_out['capi_teor'] += int(mod[0]['capi_teor']) if mod[0]['capi_teor'] else 0
		tot_lanciati = get_capi_lanciati(params, k[0], k[1], k[2])
		if tot_lanciati:
			diz_out['capi_lanciati'] += int(tot_lanciati[0]['capi_lanciati'])
		for m in mod:
			#tools.dump(m)
			diz_out['qta_prev'] += float(m['qta_prev']) if m['qta_prev'] else 0.00
			diz_out['qta_teor'] += float(m['qta_teor']) if m['qta_teor'] else 0.00
			diz_out['qta_reale'] += float(m['qta_reale']) if m['qta_reale'] else 0.00
	if diz_out['qta_reale']>0:
		diz_out['fabbisogno'] = diz_out['qta_reale']
		diz_out['tot_capi'] = diz_out['capi_lanciati']
	elif diz_out['qta_teor']>0:
		diz_out['fabbisogno'] = diz_out['qta_teor']
		diz_out['tot_capi'] = diz_out['capi_teor']
	else:
		diz_out['fabbisogno'] = diz_out['qta_prev']
		diz_out['tot_capi'] = diz_out['capi_prev']
	return diz_out


@query_with_result
def get_capi_lanciati(params, societa_orig, modello_orig, variante):
	filtri, join_clause = applica_filtri_ricerca_bp(params)
	societa_orig_codice = ''
	for s in SOCIETA_LIST:
		if s['id'] == societa_orig:
			societa_orig_codice = s['cod'].zfill(4)
	query = """ select sum(C.QT1+C.QT2+C.QT3+C.QT4+C.QT5+C.QT6+C.QT7+C.QT8+C.QTC1+C.QTC2+C.QTC3+C.QTC4+C.QTC5+C.QTC6+C.QTC7+C.QTC8) as capi_lanciati
				from FACCAP C
				join FACBP B ON (C.rif_intr = B.rif_intr)
				join FACBP_EST BE on (BE.rif_intr = B.rif_intr and BE.FLAG_SALDATO !='S')
				WHERE B.C_MAT_A = %s%s000
				and C.VAR_DIS = '%s'
				%%s
				group by B.C_MAT_A, C.VAR_DIS
			"""%(modello_orig[:8], societa_orig_codice, variante) %filtri

	return query


def get_fabbisogno_diba(id_art_rich):
	fabb = get_fabbisogno_by_id_art_rich_comm(id_art_rich)
	for f in fabb:
		#tools.dump(f)
		f['diba'] = {}
		diba = diba_dao.get_diba_esplosa_by_key(f['id_diba'])
		if diba:
			f['diba'] = diba[0]
	return fabb


def get_fabbisogno_tessuto(id_art_rich, tessuto):
	fabb = get_fabbisogno_by_id_art_rich_comm(id_art_rich)

	for f in fabb:
		#tools.dump(f)
		f['diba'] = {}
		diba = diba_dao.get_tessuto_diba_esplosa(f['id_diba'], tessuto)
		#tools.dump(diba)
		if diba:
			f['diba'] = diba[0]
			return f
	return {}


@query_with_result
def get_fabbisogno_by_id_art_rich_comm(id_art_rich):
	query = """ select      F.ID as id,
							ID_DIBA as id_diba,
							ID_ART_COMM as id_art_rich_comm,
						  QTA_FABB_PREV as qta_prev,
						  CAPI_FABB_PREV as capi_prev,
						  QTA_FABB_TEOR as qta_teor,
						  CAPI_FABB_TEOR as capi_teor,
						  QTA_FABB_REALE as qta_reale,
						  CAPI_LANCIATI as capi_lanciati,
						  STATO as stato,
						  NOTA as nota,
						  COMPLETATO as completato,
						  FORNITORE as fornitore,
						  DESC_COL_FORNITORE as desc_col_fornitore,
						  AR.CONSUMO as consumo
					from FABBISOGNI F
					left join RICHIESTE_COMM_PF_ART AR on (F.ID_ART_COMM = AR.ID)
					where F.ID_ART_COMM in (%s)
					and F.stato != 'annullato'
				""" % id_art_rich
	return query


@query_with_result
def get_fabbisogno_by_key(id):
	query = """ select ID as id,
						ID_DIBA as id_diba,
						ID_ART_COMM as id_art_rich_comm,
					  QTA_FABB_PREV as qta_prev,
					  CAPI_FABB_PREV as capi_prev,
					  QTA_FABB_TEOR as qta_teor,
					  CAPI_FABB_TEOR as capi_teor,
					  QTA_FABB_REALE as qta_reale,
					  CAPI_LANCIATI as capi_lanciati,
					  STATO as stato,
					  NOTA as nota,
					  COMPLETATO as completato,
					  FORNITORE as fornitore,
					  DESC_COL_FORNITORE as desc_col_fornitore
				from FABBISOGNI
				where ID = %s
			""" % id
	return query


@query_with_result
def get_fabbisogno_by_id_diba(id_diba):
	query = """ select ID as id,
							ID_DIBA as id_diba,
							ID_ART_COMM as id_art_rich_comm,
						  QTA_FABB_PREV as qta_prev,
						  CAPI_FABB_PREV as capi_prev,
						  QTA_FABB_TEOR as qta_teor,
						  CAPI_FABB_TEOR as capi_teor,
						  QTA_FABB_REALE as qta_reale,
						  CAPI_LANCIATI as capi_lanciati,
						  STATO as stato,
						  NOTA as nota,
						  COMPLETATO as completato,
						  FORNITORE as fornitore,
						  DESC_COL_FORNITORE as desc_col_fornitore
					from FABBISOGNI
					where ID_DIBA = %s
				""" % id_diba
	return query




@query_with_result
def get_fabbisogni_by_articolo_impegno(dati):
	query = """ select
					F.ID as id,
					  ID_DIBA as id_diba,
					  ID_ART_COMM as id_art_rich_comm,
					  QTA_FABB_PREV as qta_prev,
					  CAPI_FABB_PREV as capi_prev,
					  QTA_FABB_TEOR as qta_teor,
					  CAPI_FABB_TEOR as capi_teor,
					  QTA_FABB_REALE as qta_reale,
					  CAPI_LANCIATI as capi_lanciati,
					  STATO as stato,
					  NOTA as nota,
					  COMPLETATO as completato,
					  FORNITORE as fornitore,
					  DESC_COL_FORNITORE as desc_col_fornitore
					from FABBISOGNI F
					 JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					where  SOCIETA  = '%(societa)s'
					AND GM  = '%(gm)s'
					AND ANNO_STAGIONE  = '%(anno_stagione)s'
					AND ARTICOLO  = '%(articolo)s'
					AND MISURA  = '%(misura)s'
					AND	COLORE  = '%(colore)s'
					AND STATO != 'chiuso'
				""" % dati
	return query


@query_with_result
def get_fabbisogno_by_id_facord_dett(id_facord_dett):
	query = """ select distinct
						F.ID as id,
						  ID_DIBA as id_diba,
						  ID_ART_COMM as id_art_rich_comm,
						  QTA_FABB_PREV as qta_prev,
						  CAPI_FABB_PREV as capi_prev,
						  QTA_FABB_TEOR as qta_teor,
						  CAPI_FABB_TEOR as capi_teor,
						  QTA_FABB_REALE as qta_reale,
						  CAPI_LANCIATI as capi_lanciati,
						  F.STATO as stato,
						  NOTA as nota,
						  COMPLETATO as completato,
						  FORNITORE as fornitore,
						  DESC_COL_FORNITORE as desc_col_fornitore
						from FABBISOGNI F
						 JOIN IMPEGNI I on (F.ID = I.ID_FABBISOGNO)
						where I.RIF_ORD = %s
						AND F.STATO != 'chiuso'
						AND I.STATO='valido'
					""" % id_facord_dett
	return query


@query_with_result
def get_qta_fabbisogno_by_articolo(dati):
	query = """ select
					ifnull(if(SUM(F.QTA_FABB_REALE)>0,SUM(F.QTA_FABB_REALE),if(SUM(F.QTA_FABB_TEOR)>0, SUM(F.QTA_FABB_TEOR), SUM(F.QTA_FABB_PREV))), 0) as qta
					from FABBISOGNI F
					 JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					where  SOCIETA  = '%(societa)s'
					AND GM  = '%(gm)s'
					AND ANNO_STAGIONE  = '%(anno_stagione)s'
					AND ARTICOLO  = '%(articolo)s'
					AND MISURA  = '%(misura)s'
					AND	COLORE  = '%(colore)s'
					AND STATO != 'chiuso'
				""" % dati
	return query


@query_with_result
def get_fabbisogni_by_id_art_rcomm(dati):
	query = ''' select F.ID as id,
					  ID_DIBA as id_diba,
					  ID_ART_COMM as id_art_rich_comm,
					  QTA_FABB_PREV as qta_prev,
					  CAPI_FABB_PREV as capi_prev,
					  QTA_FABB_TEOR as qta_teor,
					  CAPI_FABB_TEOR as capi_teor,
					  QTA_FABB_REALE as qta_reale,
					  CAPI_LANCIATI as capi_lanciati,
					  STATO as stato,
					  NOTA as nota,
					  COMPLETATO as completato
					from FABBISOGNI F
					JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					JOIN RICHIESTE_COMM_PF_ART AR on (AR.ID = F.ID_ART_COMM)
					left JOIN RICHIESTE_COMM_PF_ART AR_figli on (AR.ID = AR_figli.ID_ART_PADRE)
					where F.STATO != 'chiuso'
					AND (AR.ID in (%(id_art_rcomm_list)s) or AR_figli.ID in (%(id_art_rcomm_list)s) )
			''' %dati
	return query


@query_with_result
def get_tot_capi_teor(dati):
	dati['modello_8'] = str(dati['modello_orig'])[:8] if dati['modello_orig'] else ''
	query = '''SELECT ifnull(sum(AR.Q_CAPI), 0) as qta
	           FROM RICHIESTE_COMM_PF_ART AR
	           join RICHIESTE_COMM_PF_RIGHE RR on (RR.ID = AR.ID_RIGA)
	           join RICHIESTE_COMM_PF R on (R.ID = RR.ID_RICHIESTA)
	           WHERE RR.SOCIETA_ORIG= '%(societa_orig)s'
	           and RR.AS_ORIG = '%(as_orig)s'
	           and substring(RR.MODELLO_ORIG, 1, 8) = '%(modello_8)s'
	           and AR.VARIANTE = '%(variante_orig)s'
	           and AR.FLAG_ORIGINALE = '1'
	           AND R.TIPO_RICHIESTA = 'Standard'
	           and R.collezione = '%(collezione)s'
	           and R.id_col = '%(id_col)s'
	           and R.anno_stagione = '%(anno_stagione)s'
	           and R.stato != 'chiusa'
	           group by RR.SOCIETA_ORIG, RR.AS_ORIG, RR.MODELLO_ORIG, RR.VARIANTE_ORIG
		'''%dati
	return query


@query_with_result
def get_tot_capi_prev(dati):
	dati['modello_8'] = str(dati['modello_orig'])[:8] if dati['modello_orig'] else ''
	query = '''SELECT ifnull(sum(F.CAPI_FABB_PREV), 0) as qta
	           FROM FABBISOGNI F
	           JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
	           join RICHIESTE_COMM_PF_ART AR on (F.ID_ART_COMM = AR.ID)
	           join RICHIESTE_COMM_PF_RIGHE RR on (RR.ID = AR.ID_RIGA)
	           join RICHIESTE_COMM_PF R on (R.ID = RR.ID_RICHIESTA)
	           WHERE D.SOCIETA_ORIG= '%(societa_orig)s'
	           and RR.AS_ORIG = '%(as_orig)s'
	           and substring(D.MODELLO_ORIG, 1, 8) = '%(modello_8)s'
	           and D.VARIANTE_ORIG = '%(variante_orig)s'
	           AND R.TIPO_RICHIESTA = 'Studio'
	           and R.collezione = '%(collezione)s'
	           and R.id_col = '%(id_col)s'
	           and R.anno_stagione = '%(anno_stagione)s'
	           AND D.CAPOCMAT = '%(capocmat)s'
	           group by D.SOCIETA_ORIG,  RR.AS_ORIG, D.MODELLO_ORIG, D.VARIANTE_ORIG, D.CAPOCMAT
		'''%dati
	return query


@query_without_result
def update_fabbisogno(dati):
	query = {}
	query['query'] = ''' update FABBISOGNI set
					  QTA_FABB_PREV = %(qta_prev)s,
					  CAPI_FABB_PREV = %(capi_prev)s,
					  QTA_FABB_TEOR = %(qta_teor)s,
					  CAPI_FABB_TEOR = %(capi_teor)s,
					  QTA_FABB_REALE = %(qta_reale)s,
					  CAPI_LANCIATI = %(capi_lanciati)s,
					  NOTA = '%(nota)s',
					  STATO = '%(stato)s',
					  COMPLETATO = '%(completato)s',
					  FORNITORE = '%(fornitore)s',
					  DESC_COL_FORNITORE = '%(desc_col_fornitore)s'
					  where ID = %(id)s
					''' % dati
	return query


@insert_query_with_id_as_result
def insert_fabbisogno(dati):
	query = {}
	query['query'] = ''' insert into FABBISOGNI set
						   ID_DIBA = %(id_diba)s,
						   ID_ART_COMM = "%(id_art_rich_comm)s",
						  QTA_FABB_PREV = %(qta_prev)s,
						  CAPI_FABB_PREV = %(capi_prev)s,
						  QTA_FABB_TEOR = %(qta_teor)s,
						  CAPI_FABB_TEOR = %(capi_teor)s,
						  QTA_FABB_REALE = %(qta_reale)s,
						  CAPI_LANCIATI = %(capi_lanciati)s,
						  NOTA = '%(nota)s',
						  STATO = '%(stato)s',
						  COMPLETATO = '%(completato)s',
						  FORNITORE = '%(fornitore)s',
					  	  DESC_COL_FORNITORE = '%(desc_col_fornitore)s'
						''' % dati
	#tools.dump(query['query'])
	return query


@query_without_result
def delete_fabbisogno(id):
	query = {}
	query['query'] = ''' delete from FABBISOGNI
					  where ID = %s
					''' % id
	#tools.dump(query)
	return query


@query_with_result
def get_impegno_by_key(id):
	query = ''' select ID as id,
						ID_FABBISOGNO as id_fabb,
						TIPO as tipo,
						QTA as qta,
						QTA_ORIG as qta_orig,
						RIF_ORD as rif_ord,
						RIF_PREORD as  rif_preord,
						ID_FACCOM as id_faccom,
						CAPI_FACCOM as capi_faccom,
						QTA_FACCOM as qta_faccom,
						QTA_PRELEVATA as qta_prelevata,
						STATO as stato,
						UTENTE as utente,
						UTENTE_MOD as utente_mod
				from IMPEGNI
				where ID = %s
				and STATO = 'valido'
				'''%id
	return query


@query_with_result
def get_impegni_by_fabb(id_fabb, id_faccom_list=''):
	where_clause = ''
	if id_faccom_list and str(id_faccom_list) != '0':
		where_clause += " and ID_FACCOM in (%s) "%id_faccom_list
	query = ''' select ID as id,
						ID_FABBISOGNO as id_fabb,
						TIPO as tipo,
						QTA as qta,
						QTA_ORIG as qta_orig,
						RIF_ORD as rif_ord,
						RIF_PREORD as  rif_preord,
						ID_FACCOM as id_faccom,
						CAPI_FACCOM as capi_faccom,
						QTA_FACCOM as qta_faccom,
						QTA_PRELEVATA as qta_prelevata,
						STATO as stato,
						UTENTE as utente,
						UTENTE_MOD as utente_mod
				from IMPEGNI
				where ID_FABBISOGNO = %s
				and STATO = 'valido'
				%%s
				''' %id_fabb %where_clause
	return query


@query_with_result
def get_qta_impegno_by_id_fabb(params, id_fabbs):
	filtri, join_clause = applica_filtri_ricerca_bp(params)
	query = ''' select ifnull(sum(I.QTA), 0) as qta_imp
				from IMPEGNI I
				left join FACCOM C ON (I.ID_FACCOM = C.TI_ROWID)
				left join FACBP B ON (B.RIF_INTR = C.RIF_INTR)
				where I.ID_FABBISOGNO in (%s)
				and I.STATO = 'valido'
				%%s
				''' %id_fabbs %filtri
	return query


def get_qta_reale_by_id_fabb(params, id_fabbs):
	diz = {'fabbisogno' : 0,
			'qta_reale' : 0,
			'capi_lanciati' : 0,
			'tot_capi' : 0,
			'rif_bp' : [],
			'utente' : ''}
	res = _get_qta_reale_by_id_fabb(params, id_fabbs)
	for r in res:
		diz['fabbisogno'] += float(r['fabbisogno'])
		diz['qta_reale'] += float(r['qta_reale'])
		diz['capi_lanciati'] += float(r['capi_lanciati'])
		diz['tot_capi'] += float(r['tot_capi'])
		if r['rif_bp']:
			diz['rif_bp'].append(r['rif_bp'])
			diz['utente'] = r['utente']
	diz['rif_bp'] = ','.join(diz['rif_bp'])
	return diz


@query_with_result
def _get_qta_reale_by_id_fabb(params, id_fabbs):
	filtri, join_clause = applica_filtri_ricerca_bp(params)
	query = ''' select IFNULL(I.QTA_FACCOM, 0) as fabbisogno,
						IFNULL(I.QTA_FACCOM, 0) as qta_reale,
						IFNULL(I.CAPI_FACCOM, 0) as capi_lanciati,
						IFNULL(I.CAPI_FACCOM, 0) as tot_capi,
						ifnull(B.RIF_INTR, '') as rif_bp,
						ifnull(B.UTENTE_NA,'') as utente
					from IMPEGNI I
					join FABBISOGNI F ON (I.ID_FABBISOGNO = F.ID)
					left join FACCOM C ON (I.ID_FACCOM = C.TI_ROWID)
					left join FACBP B ON (B.RIF_INTR = C.RIF_INTR)
					where I.ID_FABBISOGNO in (%s)
					and I.STATO = 'valido'
					and I.ID_FACCOM != 0
					AND F.CAPI_LANCIATI > 0
					%%s
				group by I.ID_FACCOM
			''' % id_fabbs % filtri
	#tools.dump(query)
	return query

@query_with_result
def get_impegno_by_articolo(dati, tipo=''):
	where_clause = ''
	if tipo:
		where_clause = ' and TIPO = "%s" ' %tipo

	query = ''' select
					ifnull(sum(if(I.QTA-I.QTA_PRELEVATA<0, 0, I.QTA-I.QTA_PRELEVATA)), 0) as qta,
					ifnull(group_concat(distinct I.id), '') as id_impegni
					 from IMPEGNI I
					 join FABBISOGNI F on (F.ID = I.ID_FABBISOGNO)
					 JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					where I.STATO = 'valido'
					and D.SOCIETA  = '%(societa)s'
					AND D.GM  = '%(gm)s'
					AND D.ANNO_STAGIONE  = '%(anno_stagione)s'
					AND D.ARTICOLO  = '%(articolo)s'
					AND D.MISURA  = '%(misura)s'
					AND	D.COLORE  = '%(colore)s'
					AND F.STATO != 'chiuso'
					%%s
					''' %dati %where_clause
	return query


@query_with_result
def get_altri_impegni_by_articolo(dati, tipo):
	where_clause = ''
	if tipo:
		where_clause = ' and TIPO = "%s" ' %tipo

	query = ''' select
					ifnull(SUM(I.QTA-I.QTA_PRELEVATA), 0) as qta,
					ifnull(group_concat(distinct I.id), '') as id_impegni
					 from IMPEGNI I
					 join FABBISOGNI F on (F.ID = I.ID_FABBISOGNO)
					 JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					where I.STATO = 'valido'
					and D.SOCIETA  = '%(societa)s'
					AND D.GM  = '%(gm)s'
					AND D.ANNO_STAGIONE  = '%(anno_stagione)s'
					AND D.ARTICOLO  = '%(articolo)s'
					AND D.MISURA  = '%(misura)s'
					AND	D.COLORE  = '%(colore)s'
					AND F.STATO != 'chiuso'
					AND F.ID != '%(id_fabbisogno)s'
					%%s
					''' %dati %where_clause
	return query




@insert_query_with_id_as_result
def insert_impegno(dati):
	query = {}
	query['query'] = '''insert into IMPEGNI set
						  ID_FABBISOGNO = '%(id_fabb)s',
						  TIPO = '%(tipo)s',
						  QTA = '%(qta)s',
						  RIF_ORD = '%(rif_ord)s',
						  RIF_PREORD = '%(rif_preord)s',
						  ID_FACCOM = '%(id_faccom)s',
						  CAPI_FACCOM = '%(capi_faccom)s',
						  QTA_FACCOM = '%(qta_faccom)s',
						  QTA_ORIG = '%(qta_orig)s',
						  QTA_PRELEVATA = '%(qta_prelevata)s',
						  STATO = '%(stato)s',
						  UTENTE = '%(utente)s',
						  UTENTE_MOD = '%(utente)s'
					''' % dati
	#tools.dump(query['query'])
	return query


@query_without_result
def update_impegno(dati):
	query = {}
	query['query'] = '''update IMPEGNI set
						  ID_FABBISOGNO = '%(id_fabb)s',
						  TIPO = '%(tipo)s',
						  QTA = '%(qta)s',
						  RIF_ORD = '%(rif_ord)s',
						  RIF_PREORD = '%(rif_preord)s',
						  ID_FACCOM = '%(id_faccom)s',
						  CAPI_FACCOM = '%(capi_faccom)s',
						  QTA_FACCOM = '%(qta_faccom)s',
						  QTA_ORIG = '%(qta_orig)s',
						  QTA_PRELEVATA = '%(qta_prelevata)s',
						  STATO = '%(stato)s',
						  UTENTE_MOD = '%(utente_mod)s'
					  where ID = '%(id)s'
					''' % dati
	#tools.dump(query['query'])
	return query


@query_without_result
def delete_impegno(imp):
	query = {}
	query['query'] = ''' UPDATE IMPEGNI
						SET STATO = '%(stato)s'
					  where ID = %(id)s
					''' % imp
	#tools.dump(query)
	return query


@query_with_result
def get_misura_capocmat(params):
	query = """SELECT distinct MIS as mis
		   FROM MP_CAPOCMAT
		   where MIS like '%(query)s%%'
		   and SUBSTR(CAPOCMAT, 1, 9) = '%(capocmat)s'
		   and LENGTH(CAPOCMAT) = 15
		   and CARICABILE = 1
		""" %params

	return query


@query_with_result
def get_colore_capocmat(params):
	q_macrogruppo = ""
	if params.get('macrocolore', '') and params['macrocolore']:
		q_macrogruppo = " AND i.id_raggr_colore = '%s' " % params['macrocolore']

	query = """SELECT distinct COL as col_capocmat, i.descrizione as col_descrizione
			   FROM MP_CAPOCMAT CC
			   join impo_societa isoc on CC.SOC=isoc.SOC_DT
			   join impo_colori i on (lpad(CC.COL,4,'0') = lpad(i.colore, 4,'0') and i.societa = isoc.SOC_ORIG)
			   where COL like '%%%(query)s%%'
			   and SOC = '%(societa)s'
			   and gm ='%(gm)s'
			   and art='%(articolo)s'
			   and a_s = '%(a_s)s'
			   and CARICABILE = 1
			   and length(capocmat) = 15
			""" %params + q_macrogruppo

	return query


@query_with_result
def get_id_art_rich_by_bp(param):
	query = '''SELECT group_concat(AR.ID) as id_art_rcomm_list
	           FROM FACBP_ORDPF_ASSO FOA
	           JOIN ORDINI_PF_RIGHE ORI ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
	           JOIN RICHIESTE_COMM_PF_ART AR ON (ORI.ID_ART_RCOMM_PF = AR.ID)
	           join RICHIESTE_COMM_PF_RIGHE RR ON (AR.ID_RIGA = RR.ID)
	           join RICHIESTE_COMM_PF R ON (RR.ID_RICHIESTA = R.ID)
	           WHERE FOA.RIF_BP = '%(rif_intr)s'
	           and R.STATO != 'chiusa'
	           group by FOA.RIF_BP
	    '''%param
	return query


@query_with_result
def get_id_art_rich_by_variante_bp(rif_bp, variante):
	query = '''SELECT group_concat(ORI.ID_ART_RCOMM_PF) as id_art_rcomm,
					ORI.VARIANTE as variante,
					ORI.VARIANTE_ORIG as variante_orig,
					sum(ORI.Q_TOT) as capi_ordpf
	           FROM FACBP_ORDPF_ASSO FOA
	           JOIN ORDINI_PF_RIGHE ORI ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
	           JOIN FACBP_EST BE ON (FOA.RIF_BP =BE.RIF_INTR AND BE.FLAG_SALDATO!='S')
	           WHERE FOA.RIF_BP = '%s'
	           and LPAD(ORI.VARIANTE,3,'0') = '%s'
	           group by ORI.VARIANTE
	    '''%(rif_bp, variante.zfill(3))
	#tools.dump(query)
	return query


@query_with_result
def get_id_art_rich_std_by_variante_bp(rif_bp, variante):
	query = '''SELECT AR.ID as id_art_rcomm
	           FROM FACBP_ORDPF_ASSO FOA
	           JOIN ORDINI_PF_RIGHE ORI ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
	           JOIN FACBP_EST BE ON (FOA.RIF_BP =BE.RIF_INTR AND BE.FLAG_SALDATO!='S')
	           JOIN RICHIESTE_COMM_PF_ART AR ON (ORI.ID_ART_RCOMM_PF = AR.ID)
		    WHERE FOA.RIF_BP = '%s'
	        and LPAD(ORI.VARIANTE,3,'0') = '%s'
		    group by ORI.VARIANTE'''% (rif_bp, variante.zfill(3)) + \
		'''
		union
		    SELECT	AR2.ID as id_art_rcomm
	           FROM FACBP_ORDPF_ASSO FOA
	           JOIN ORDINI_PF_RIGHE ORI ON (FOA.ID_RIGA_ORDINE_PF = ORI.ID_RIGA_ORDINE_PF)
	           JOIN FACBP_EST BE ON (FOA.RIF_BP =BE.RIF_INTR AND BE.FLAG_SALDATO!='S')
	           JOIN RICHIESTE_COMM_PF_ART AR ON (ORI.ID_ART_RCOMM_PF = AR.ID)
	           JOIN RICHIESTE_COMM_PF_ART AR2 ON (AR.ID_ART_PADRE = AR2.ID)
		    WHERE FOA.RIF_BP = '%s'
	        and LPAD(ORI.VARIANTE,3,'0') = '%s'
		    group by ORI.VARIANTE
		    ''' % (rif_bp, variante.zfill(3))
	# tools.dump(query)
	return query


@query_with_result
def get_varianti_fabb_by_bp(dati_bp):
	dati_bp['modello_8'] = dati_bp['modello_originale'][:8] if dati_bp['modello_originale'] else ''
	query = '''SELECT D.VARIANTE_DT as variante_dt,
					D.MODELLO_ORIG as modello_originale,
					D.SOCIETA_ORIG as societa_originale,
					D.VARIANTE_ORIG as variante_orig,
					C.CMATSET as collez,
					substring(R.ANNO_STAGIONE,4,1) as anno,
					if(substring(R.ANNO_STAGIONE,-3,3)='P/E',2,4) as stag,
					group_concat(distinct F.ID_ART_COMM) as id_art_rcomm
	           FROM DIBA_ESPLOSA D
	           JOIN FABBISOGNI F ON (D.ID = F.ID_DIBA)
	           JOIN RICHIESTE_COMM_PF_ART AR ON (AR.ID = F.ID_ART_COMM)
	           JOIN RICHIESTE_COMM_PF_RIGHE RR ON (AR.ID_RIGA = RR.ID)
	           JOIN RICHIESTE_COMM_PF R ON (RR.ID_RICHIESTA = R.ID)
			   join FACCOL C ON (R.COLLEZIONE = C.ML or R.COLLEZIONE=C.CMATSET)
	           WHERE substring(D.MODELLO_ORIG,1,8) = '%(modello_8)s'
	           AND C.CMATSET = '%(collez)s'
	           AND R.ANNO_STAGIONE = '%(anno_stagione)s'
	           AND R.STATO !='chiusa'
	           AND F.STATO != 'chiuso'
	           group by D.VARIANTE_DT
	        '''%dati_bp
	#tools.dump(query)
	return query


@query_with_result
def get_qta_reale(id_fabb):
	query = """ select ifnull(sum(Q.qta),0) as qta_reale
				from ( select id_faccom, QTA_FACCOM as qta
						from IMPEGNI
						WHERE ID_FABBISOGNO = %s
						and id_faccom != 0
						and stato != 'annullato'
						group by ID_FACCOM ) as Q
			""" %id_fabb
	return query


def calcola_lanciato(var_diz,capocmat):
	diz_out = { 'qta_reale': 0, 'id_art_rcomm': [], 'tot_lanciati': 0}
	res = get_righe_articolo_lanciate(var_diz,capocmat)
	for r in res:
		variante_unica = True
		rr = get_numero_varianti_bp(r['rif_intr'])
		if int(rr[0]['numero_varianti']) > 1:
			variante_unica = False
		res_id = get_id_art_rich_by_variante_bp(r['rif_intr'], r['variante'])
		if res_id:
			diz_out['id_art_rcomm'].extend(res_id[0]['id_art_rcomm'].split(','))
		diz_out['tot_lanciati'] += int(r['tot_lanciati'])
		if r['variante'] != '' and r['variante'] != '0':
			# proporziono la qta in base alle varianti sulla riga
			if str(var_diz['variante_dt']).zfill(3) == str(r['variante']).zfill(3):
				if r['tipo_riga'] in ('T', 'E'):
					if r['qta_capi'] and float(r['qta_capi']) > 0:
						diz_out['qta_reale'] += float(r['qta'])
				else:
					if r['qta_capi'] and float(r['qta_capi']) > 0:
						diz_out['qta_reale'] += float(r['qta']) * float(r['tot_lanciati']) / float(r['qta_capi'])
		elif variante_unica:
			if r['qta_capi'] and float(r['qta_capi']) > 0:
				diz_out['qta_reale'] += float(r['qta'])
		elif 'Variante' in r['note']:
			if str(var_diz['variante_dt']).zfill(3) in r['note'].replace('Variante:', '').split(','):
				if r['qta_capi'] and float(r['qta_capi']) > 0:
					diz_out['qta_reale'] += float(r['qta']) * float(r['tot_lanciati']) / float(r['qta_capi'])
		else:
			if r['qta_capi'] and float(r['qta_capi']) > 0:
				diz_out['qta_reale'] += float(r['qta']) * float(r['tot_lanciati']) / float(r['qta_capi'])

	if diz_out['qta_reale'] > 0 and diz_out['qta_reale'] < 1:
		diz_out['qta_reale'] = 1

	diz_out['qta_reale'] = round(diz_out['qta_reale'], 2)
	diz_out['id_art_rcomm'] = ','.join(list(set(diz_out['id_art_rcomm'])))
	return diz_out


@query_with_result
def get_righe_articolo_lanciate(var_diz, capocmat):
	var_diz['societa_orig_codice'] = ''
	for s in SOCIETA_LIST:
		if s['id'] == var_diz['societa_originale']:
			var_diz['societa_orig_codice'] = s['cod'].zfill(4)

	query = ''' select B.RIF_INTR as rif_intr, sum(CC.QT1) as qta_capi,sum(CC.Q/100) as qta, CC.VAR_DIS as variante,
						CC.NOTE as note, CC.T_MAT as tipo_riga,
					(C.QT1+C.QT2+C.QT3+C.QT4+C.QT5+C.QT6+C.QT7+C.QT8+C.QTC1+C.QTC2+C.QTC3+C.QTC4+C.QTC5+C.QTC6+C.QTC7+C.QTC8) AS tot_lanciati
				FROM FACBP B
				JOIN FACBP_EST BE ON (B.RIF_INTR = BE.RIF_INTR AND BE.FLAG_SALDATO != 'S')
				JOIN FACCAP C ON (B.RIF_INTR = C.RIF_INTR)
				JOIN FACCOM CC ON (CC.RIF_INTR = C.RIF_INTR)
				join FACCOM_EST CE ON (CC.TI_ROWID = CE.ID_FACCOM)
				WHERE B.C_MAT_A = '%(modello_originale)s%(societa_orig_codice)s000'
				and (CC.VAR_DIS = '%(variante_dt)s' or CC.VAR_DIS = '0')
				and C.VAR_DIS = '%(variante_dt)s'
				AND B.CMATSET = '%(collez)s'
	            AND B.ANNO = '%(anno)s'
	            and B.STAG = '%(stag)s'
				and (CC.CAPOCMAT = '%%s' or CE.CAPOCMAT_CMAT = '%%s')
				and ((CC.C_MAT!=0 and CC.CAPOCMAT='') or (CC.C_MAT=0 and CC.CAPOCMAT!='') or (CC.C_MAT=0 and CE.CAPOCMAT_CMAT!=''))
				 group by B.RIF_INTR, C.VAR_DIS,CC.CAPOCMAT,CE.CAPOCMAT_CMAT
				 '''%var_diz %(capocmat, capocmat)
	#tools.dump(query)
	return query


@query_with_result
def get_righe_articolo_lanciate_by_id_faccom(id_faccom, variante):
	query = ''' select B.RIF_INTR as rif_intr, sum(CC.QT1) as qta_capi,sum(CC.Q/100) as qta, CC.VAR_DIS as variante,
					CC.NOTE as note, CC.T_MAT as tipo_riga,
					(C.QT1+C.QT2+C.QT3+C.QT4+C.QT5+C.QT6+C.QT7+C.QT8+C.QTC1+C.QTC2+C.QTC3+C.QTC4+C.QTC5+C.QTC6+C.QTC7+C.QTC8) AS tot_lanciati
				FROM FACBP B
				JOIN FACBP_EST BE ON (B.RIF_INTR=BE.RIF_INTR AND BE.FLAG_SALDATO!='S')
				JOIN FACCAP C ON (B.RIF_INTR =C.RIF_INTR)
				JOIN FACCOM CC ON (CC.RIF_INTR = C.RIF_INTR)
				join FACCOM_EST CE ON (CC.TI_ROWID = CE.ID_FACCOM)
				WHERE CC.TI_ROWID in (%s)
				and C.VAR_DIS = '%s'
				and ((CC.C_MAT!=0 and CC.CAPOCMAT='') or (CC.C_MAT=0 and CC.CAPOCMAT!='') or (CC.C_MAT=0 and CE.CAPOCMAT_CMAT!=''))
				group by B.RIF_INTR, C.VAR_DIS,CC.CAPOCMAT,CE.CAPOCMAT_CMAT
				 ''' %(id_faccom, variante)
	#tools.dump(query)
	return query

@query_with_result
def get_numero_varianti_bp(rif_bp):
	query =""" select ifnull(count(distinct VAR_DIS),0) as numero_varianti
				from FACCAP
				WHERE RIF_INTR = '%s'
			"""%rif_bp
	return query



@query_with_result
def get_bp_by_impegno(id_faccom):
	query = ''' select RIF_INTR as rif_intr
				from FACCOM
				where TI_ROWID = %s
			'''%id_faccom
	return query


def get_qta_bp_by_impegno(dati):
	diz_out = {'rif_intr': '', 'qta_reale': dati['qta'], 'tot_lanciati': dati['qta_capi']}
	res = get_righe_articolo_lanciate_by_id_faccom(dati['id_faccom'], dati['variante_dt'])
	if res:
		variante_unica = True
		r = res[0]
		rr = get_numero_varianti_bp(r['rif_intr'])
		if int(rr[0]['numero_varianti']) > 1:
			variante_unica = False
		diz_out['rif_intr'] = r['rif_intr']
		diz_out['tot_lanciati'] = int(r['tot_lanciati'])
		if r['variante'] != '' and r['variante'] != '0':
			# proporziono la qta in base alle varianti sulla riga
			if str(dati['variante_dt']).zfill(3) == str(r['variante']).zfill(3):
				if r['qta_capi'] and float(r['qta_capi']) > 0:
					diz_out['qta_reale'] = float(r['qta'])
					diz_out['tot_lanciati'] = float(r['qta_capi'])
		elif variante_unica:
			if r['qta_capi'] and float(r['qta_capi']) > 0:
				diz_out['qta_reale'] = float(r['qta'])
				diz_out['tot_lanciati'] = float(r['qta_capi'])
		elif 'Variante' in r['note']:
			if str(dati['variante_dt']).zfill(3) in r['note'].replace('Variante:', '').split(','):
				if r['qta_capi'] and float(r['qta_capi']) > 0:
					diz_out['qta_reale'] = float(r['qta']) * float(r['tot_lanciati']) / float(r['qta_capi'])
		else:
			if r['qta_capi'] and float(r['qta_capi']) > 1:
				diz_out['qta_reale'] = float(r['qta']) * float(r['tot_lanciati']) / float(r['qta_capi'])
			elif r['qta_capi'] and float(r['qta_capi']) == 1:
				diz_out['qta_reale'] = float(r['qta'])
				diz_out['tot_lanciati'] = float(r['qta_capi'])

		if diz_out['qta_reale'] > 0 and diz_out['qta_reale'] < 1:
			diz_out['qta_reale'] = 1

		diz_out['qta_reale'] = round(diz_out['qta_reale'], 2)
	return diz_out


def get_fabbisogno_by_prelievo(id_faccom):
	fabb = get_fabbisogno_by_id_faccom(id_faccom)
	for f in fabb:
		#tools.dump(f)
		f['diba'] = {}
		diba = diba_dao.get_diba_esplosa_by_key(f['id_diba'])
		if diba:
			f['diba'] = diba[0]
	return fabb



@query_with_result
def get_fabbisogno_by_id_faccom(id_faccom):
	query = """
			select  distinct F.ID as id,
					  ID_DIBA as id_diba,
					  ID_ART_COMM as id_art_rich_comm,
					  QTA_FABB_PREV as qta_prev,
					  CAPI_FABB_PREV as capi_prev,
					  QTA_FABB_TEOR as qta_teor,
					  CAPI_FABB_TEOR as capi_teor,
					  QTA_FABB_REALE as qta_reale,
					  CAPI_LANCIATI as capi_lanciati,
					  F.STATO as stato,
					  NOTA as nota,
					  COMPLETATO as completato,
					  FORNITORE as fornitore,
					  DESC_COL_FORNITORE as desc_col_fornitore
			from FABBISOGNI F
			join IMPEGNI I on (F.ID = I.ID_FABBISOGNO)
			where I.ID_FACCOM = %s
		""" % (id_faccom)
	#tools.dump(query)
	return query



def get_impegni_pianificabili_by_id_faccom_picking(pard, id_faccom_list):
	pard['sql'] = """
		select I.*
		from IMPEGNI I
		join FACCOM F ON (F.ti_rowid=I.id_faccom)
		where I.ID_FACCOM in (%s)
		and I.STATO = 'valido'
		and I.TIPO = 'DT'
		and I.QTA > 0
		and F.rif_bll_f = 0
	""" %(id_faccom_list)
	#tools.dump(query)
	res = db_access.mysql_dict(pard)
	if res:
		return True
	return False



@query_with_result
def get_impegni_pianificabili_by_id_faccom(id_faccom_list):
	query = """
		select I.*
		from IMPEGNI I
		join FACCOM F ON (F.ti_rowid=I.id_faccom)
		where I.ID_FACCOM in (%s)
		and I.STATO = 'valido'
		and I.TIPO = 'DT'
		and I.QTA > 0
		and F.rif_bll_f = 0
	""" %(id_faccom_list)
	#tools.dump(query)
	return query

@query_with_result
def get_impegni_spedibili_by_id_faccom(id_faccom_list):
	query = """
		select I.*
		from IMPEGNI I
		join FACCOM F ON (F.ti_rowid=I.id_faccom)
		where I.ID_FACCOM in (%s)
		and I.STATO = 'valido'
		and I.QTA_PRELEVATA >= I.QTA
		and I.TIPO = 'DT'
		and I.QTA > 0
		and F.rif_bll_f = 0
	""" %(id_faccom_list)
	#tools.dump(query)
	return query


@query_with_result
def get_altri_impegni_giacenza_by_articolo(dati):
	query = """ select
					distinct F.ID as id_fabb,
					 I.ID as id_impegno,
					  ID_DIBA as id_diba,
					  ID_ART_COMM as id_art_rich_comm,
					  QTA_FABB_PREV as qta_prev,
					  CAPI_FABB_PREV as capi_prev,
					  QTA_FABB_TEOR as qta_teor,
					  CAPI_FABB_TEOR as capi_teor,
					  QTA_FABB_REALE as qta_reale,
					  CAPI_LANCIATI as capi_lanciati,
					  F.STATO as stato,
					  NOTA as nota,
					  COMPLETATO as completato,
					  D.MODELLO_ORIG modello_orig,
					  D.NOME_MODELLO_ORIG nome_modello_orig,
					  I.QTA as qta,
					  I.QTA_PRELEVATA as qta_prelevata,
					  I.QTA - I.QTA_PRELEVATA as qta_da_prelevare
					from FABBISOGNI F
					 JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					 join IMPEGNI I on (F.ID = I.ID_FABBISOGNO)
					where SOCIETA = '%(societa)s'
					AND GM = '%(gm)s'
					AND ANNO_STAGIONE = '%(anno_stagione)s'
					AND ARTICOLO = '%(articolo)s'
					AND MISURA = '%(misura)s'
					AND	COLORE = '%(colore)s'
					and ( MODELLO_ORIG <> '%(modello_orig)s' or SOCIETA_ORIG <> '%(societa_orig)s' )
					AND F.STATO <> 'chiuso'
					AND F.COMPLETATO <> 'S'
					AND I.STATO = 'valido'
					AND I.TIPO = 'DT'
					AND I.QTA - I.QTA_PRELEVATA > 0
				""" %dati
	#tools.dump(query)
	return query


@query_with_result
def get_totale_altri_impegni_giacenza_by_articolo(dati):
	query = """ select
					ifnull(group_concat(distinct F.ID), '') as id_fabbisogni_list,
					ifnull(sum(I.QTA), 0) as qta
					from FABBISOGNI F
					JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					join IMPEGNI I on (F.ID = I.ID_FABBISOGNO)
					where SOCIETA = '%(societa)s'
					AND GM = '%(gm)s'
					AND ANNO_STAGIONE = '%(anno_stagione)s'
					AND ARTICOLO = '%(articolo)s'
					AND MISURA = '%(misura)s'
					AND	COLORE = '%(colore)s'
					AND F.STATO <> 'chiuso'
					AND I.STATO = 'valido'
					AND I.TIPO = 'DT'
					AND I.QTA > 0
					AND I.ID_FACCOM = 0
				""" %dati
	#tools.dump(query)
	return query


@query_with_result
def check_ordine_preordine_in_impegni(impegno):
	query = """ select
					F.ID as id_fabb,
					I.ID as id_impegno,
					F.ID_DIBA as id_diba,
					F.ID_ART_COMM as id_art_rich_comm,
					I.QTA as qta
				from FABBISOGNI F
				join IMPEGNI I on (F.ID = I.ID_FABBISOGNO)
				where I.RIF_ORD = %(rif_ord)s
				and F.ID != %(id_fabb)s
				and I.STATO ='valido'
			"""%impegno
	#tools.dump(query)
	return query


@query_with_result
def check_esiste_riga_bp(id_faccom):
	query = """ select C.TI_ROWID as ti_rowid,
					C.NR_R as nr_r,
					C.C_MAT as c_mat,
					C.RIF_INTR as rif_intr,
					C.T_MAT as tipo_riga,
					C.D_MAT as d_mat,
					C.COMP_F as d_mat_col,
					C.VAR_DIS as variante,
					C.Q/100 as qta,
					C.Q_C/100 as qta_prelevata,
					C.QT1 as qta_capi,
					C.PS_M/100 as consumo,
					C.TIPO_REC as da_ordinare,
					C.RIF_BLL_F as rif_bll_f,
					C.CTO_A,
					C.DT_NA,
					C.DT_UV,
					C.VAL_REC,
					CASE WHEN C.CAPOCMAT=''
						THEN CE.CAPOCMAT_CMAT
						ELSE C.CAPOCMAT
						END as capocmat,
					C.NOTE as note,
					ifnull(CE.LAVAGGIO, '') as lavaggio,
					ifnull(CE.CONTO_SPED, '') as conto_spedizione
				from FACCOM C
				left join FACCOM_EST CE on (C.TI_ROWID = CE.ID_FACCOM)
				where C.TI_ROWID = %s
			"""%id_faccom
	#tools.dump(query)
	return query



@query_without_result
def insert_impegni_notifica(dati):
	dati['utente'] = Request().get_val('sid_id_utente', '')
	query = """ insert into IMPEGNI_NOTIFICA
				set ID_PICK = %(id_pick)s,
					ID_IMPEGNO = %(id_impegno)s,
					TESTO_MSG = '%(msg)s',
					DESTINATARI_MSG = '%(destinatario)s',
					UPD_USER = '%(utente)s',
					UPD_TS = CURRENT_TIMESTAMP
			"""%dati
	return {'query' : query}


@query_with_result
def get_impegni_da_notificare():
	query = """ select NI.ID as id,
					ID_PICK as id_pick,
					ID_IMPEGNO as id_impegno,
					TESTO_MSG as msg,
					DESTINATARI_MSG as utente,
					D.CAPOCMAT AS capocmat
				from IMPEGNI_NOTIFICA NI join IMPEGNI I on (I.id=NI.ID_IMPEGNO)
				JOIN FABBISOGNI F ON (I.ID_FABBISOGNO= F.ID)
				JOIN DIBA_ESPLOSA D ON (D.ID=F.ID_DIBA)
				where DATA_INVIO is NULL
			"""
	return query


@query_with_result
def get_capocmat_diba(capocmat=''):
	where_clause = ''
	if capocmat:
		where_clause += ' and D.CAPOCMAT = "%s" '%capocmat
	query = """ select DISTINCT D.CAPOCMAT as capocmat,
					D.SOCIETA as societa,
					D.GM as gm,
					D.ANNO_STAGIONE as anno_stagione,
					D.ARTICOLO as articolo,
					D.MISURA as misura,
					D.COLORE as colore
				from DIBA_ESPLOSA D
				where 1=1
				%s
			"""%where_clause
	return query


@query_without_result
def aggiorna_impegni_notifica(dati):
	query = """ update IMPEGNI_NOTIFICA
				set DATA_INVIO = '%(data_invio)s'
				where ID = '%(id)s'
			"""%dati
	return {'query' : query}



def get_fabbisogni_by_modello(pard, societa, modello):
	pard['sql'] = """ select *
					from FABBISOGNI F
					JOIN DIBA_ESPLOSA D ON (F.ID_DIBA = D.ID)
					WHERE MODELLO_ORIG like '%s%%'
					and D.SOCIETA = '%s'
					and STATO != 'chiuso'
				"""%(modello, societa)
	resd = db_access.mysql_dict(pard)
	return resd


def check_ordine_annullato(pard, id_facord_dett):
	pard['sql'] = """ select STATO
					FROM FACORD F
					JOIN FACORD_DETT FD on (F.ID = FD.ID_FACORD)
					where FD.TI_ROWID = '%s'
				""" %id_facord_dett
	resd = db_access.mysql_dict(pard)
	if (resd and resd[0]['STATO'] == 'ANNULLATO') or not resd:
		return True
	else:
		return False
