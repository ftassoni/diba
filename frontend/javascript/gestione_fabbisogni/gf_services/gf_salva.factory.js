(function()
{
	'use strict';

	SalvaService.$inject = ['DTHttp', 'LoggerFactory'];

	var factoryId = "SalvaService";

	angular.module('app').factory(factoryId, SalvaService);

	function SalvaService(DTHttp, LoggerFactory)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {

			impegna_articolo: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'salva_impegno', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
								logger.success("Impegni salvati con successo.");
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
            salva_articolo_duplicato: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'salva_articolo_duplicato', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
								logger.success("Articolo duplicato con successo.");
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			creaOrdini: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'crea_ordini_da_fabb', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Ordini creati con successo.");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			modificaOrdiniDaFabb: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'modifica_ordini_da_fabb', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Ordini modificati con successo.");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			avanzaDiba: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'avanza_diba', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Fabbisogni spostati in stato valutazione");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			arretraDiba: function (params, successHandler, errorHandler) {
			    var payload = {
					dati: JSON.stringify(params)
				};

				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'arretra_diba', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    logger.success("Fabbisogni spostati in stato valutazione");
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			}
		};
	}
})();