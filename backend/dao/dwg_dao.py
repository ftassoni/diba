import db_access
import tools
import itertools
import simplejson as json
import time
from Common import errors
from commons import dwg_logger
import math
import mmfg_ws_config
import mmfg_ws_client
import anno_stagione_lib
import ws_db_gruppo
import config
import traceback
from facon.ordpf.dao.decorators import query_with_result, query_without_result, insert_query_with_id_as_result
from facon.ordpf.facade.pool_manager import ConnPool, Request

DEBUG = True

WS_MODULE = 'ws_interface'
WS_PROGRAM = 'exec_query_dt'

LABEL_CONFIG = 'db_gruppo' 
WS_RET_OK = 'Successo'
WS_RET_ERR = 'Fallimento'

WS_DIBA_PRODOTTO = 'get_distinta_base_prodotto'
WS_DIBA_ESPLOSA_PRODOTTO = 'get_distinta_base_esplosa'



def local_wrap(fn):
	def wrapped(program, diz_dati, societa, debug=False):
		pard = config.global_parameters({})
		if pard.get('AMBIENTE','') != 'ot':
			return fn(program, diz_dati, societa, debug)
		else:
			try:
				b = True
				pard['sql'] = diz_dati[0]
				res = db_access.mysql_dict(pard, alt_db='DBGRUPPO')
				r = [{
					'result': WS_RET_OK,
					'data': res
				}]
			except Exception, e:
				mess = traceback.format_exc()
				r = [{
					'result': WS_RET_ERR,
					'data': mess
				}]
				b = False
			finally:
				return (r,b)

	return wrapped


@local_wrap
def chiama_ws(program, diz_dati, societa, debug=False):

	ws_conf = mmfg_ws_config.get_config(LABEL_CONFIG)
	param_d = {
		'module': WS_MODULE,
		'program': program,
		'societa': societa,
	}
	ws_conf['url'] = 'http://dwh-gruppo.mmfg.it/bin/driver_ws'
	param_d['data'] = json.dumps(diz_dati)
	dwg_logger.debug(ws_conf)
	dwg_logger.debug(param_d)
	r = mmfg_ws_client.send_request(param_d=param_d, ws_conf=ws_conf, debug=False)
	dwg_logger.debug(r)
	try:
		r = json.loads(r)
		if r[0]['result'] == WS_RET_ERR:
			b = False
		else:
			b = True
	except:
		mess = str(r)
		r = [{
			'result': WS_RET_ERR,
			'data': mess
		}]
		b = False

	return (r,b)


def get_modello_originale(pard, societa, modello):	
	if societa == 'MN': 
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '7':
			modello = 'V'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello = modello[:8]
		
	query = ["""select * from mod_modello m
			join mod_articolo ma on (m.societa = ma.societa and m.articolo = ma.articolo)
			 where m.societa='%s' 
			 and substring(m.modello,1,8) like '%%%s%%' 
			 and m.versione_modello != 'C' 
			 and m.validita != 'A' """%(societa, modello)]
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(query)
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		if resd[0]['data']:
			diz = resd[0]['data'][0]
			for k,v in diz.items():
				if v:
					diz[k] = v.encode('utf8')
			return diz
		else:
			raise errors.DAOException('Nessun modello trovato %s. (Traceback: %s)' %(modello, resd))


def get_lista_modelli_originali(pard, societa='', a_s='', modello=''):
	if societa == 'MN':
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '7':
			modello = 'V'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello = modello[:8]
		
	q_as = ''
	if a_s:	
		anno, stagione = anno_stagione_lib.decodifica_as(a_s, tipo='tupla_anno_stagione_aaaa_s')
		q_as = """ and m.anno='%s' and m.stagione='%s' """ %(anno, stagione)
	q_mod = ''
	if modello:
		q_mod = """ and substring(m.modello,1,8)='%s'  """ %modello
		
	query = ["""select * from mod_modello m
			join mod_articolo ma on (m.societa = ma.societa and m.articolo = ma.articolo)
			join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
			join mod_modello_produzione mmp on (m.societa = mmp.societa and m.modello = mmp.modello)
			 where m.societa='%s' 
			 %%(q_as)s
			 %%(q_mod)s
			 """%societa %vars()]
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		if resd[0]['data']:
			return resd[0]['data']
		else:
			raise errors.DAOException('Nessun modello trovato %s. (Traceback: %s)' %(modello, resd))


def get_modello_variante_originale(pard, societa, modello_in, variante=''):
	modello = modello_in[:]
	q_variante = ''
	if variante:
		if societa == 'MN':
			q_variante = """ and mv.variante = '%s' """ %(int(variante) if variante != '' else '')
		else:
			q_variante = """ and mvp.variante_produzione = '%s' """ %variante
	if societa == 'MN':
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '7':
			modello = 'V'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello = modello[:8]

	if societa == 'MN':
		q_variante += " and m.tipo_produzione != 'I' "

	query = ["""select
					m.societa,
					m.modello,
					m.anno,
					m.stagione,
					m.classe,
					m.nome,
					m.articolo,
					m.uscita_collezione,
					m.tipo_taglia,
					m.modalita_produzione,
					ma.*,
					mv.*,
					uc.*,
					mp.*
			from mod_modello m
			join mod_modello_produzione mp on (m.societa = mp.societa and m.modello = mp.modello)
			join mod_articolo ma on (m.societa = ma.societa and m.articolo = ma.articolo)
			left join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
			left join mod_variante_produzione mvp on (mv.societa = mvp.societa and mvp.modello = mv.modello and mv.variante=mvp.variante)
			join mod_uscita_collezione uc
					on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione
						and m.anno=uc.anno and m.stagione=uc.stagione)
			 where (mv.validita != 'A' or mv.validita is NULL)
			 and m.societa = '%s'
			 and substring(m.modello,1,8) = '%s'
			 and m.versione_modello != 'C'
			 %%s  """%(societa, modello) %q_variante]
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(query[0])
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(resd)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		# Se sto cercando con variante e non ho trovato risultato,
		# restituisco i dati del solo modello
		if q_variante and not resd[0]['data']:
			return get_modello_variante_originale(pard, societa, modello_in, variante='')
		elif resd[0]['data']:
			trovato = False
			for r in resd[0]['data']:
				# controllo che sia un modello permanente
				if r['modello_permanente']:
					trovato = True
					diz = r.copy()

			if not trovato:
				for rr in sorted(resd[0]['data'], key=lambda k:(k['modello_produzione']) ,reverse=True):
					# controllo che sia un modello permanente
					if rr['made_in']:
						trovato = True
						diz = rr.copy()
				if not trovato:
					diz = resd[0]['data'][0]
	#			for k,v in diz.items():
	#				if v:
	#					diz[k] = v.encode('latin1')

			return diz
		else:
			raise errors.DAOException('Nessun modello trovato %s. (Traceback: %s)' %(modello, resd))


def get_modelli_variante_prod(pard, societa, modello_in, variante=''):
	modello = modello_in[:]
	q_variante = ''
	if variante:
		if societa == 'MN':
			q_variante = """ and mv.variante = '%s' """ %(int(variante) if variante != '' else '')
		else:
			q_variante = """ and mvp.variante_produzione = '%s' """ %variante

	if societa == 'MN':
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '7':
			modello = 'V'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello = modello[:8]


	query = ["""select 
					m.societa,
					m.modello,
					m.anno,
					m.stagione,
					m.classe,
					m.nome,
					m.articolo,
					m.uscita_collezione,
					m.tipo_taglia,
					m.tipo_produzione,
					m.versione_modello,
					ma.*,
					mv.*,
					uc.*,
					mp.*			
			from mod_modello m
			join mod_modello_produzione mp on (m.societa = mp.societa and m.modello = mp.modello)
			join mod_articolo ma on (m.societa = ma.societa and m.articolo = ma.articolo)
			left join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
			left join mod_variante_produzione mvp on (mv.societa = mvp.societa and mvp.modello = mv.modello and mv.variante=mvp.variante)
			join mod_uscita_collezione uc 
					on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione 
						and m.anno=uc.anno and m.stagione=uc.stagione)
			 where (mv.validita != 'A' or mv.validita is NULL)
			 and m.societa = '%s' 
			 and substring(m.modello,1,8) = '%s'
			 and m.versione_modello != 'C'
			 %%s
			 """%(societa, modello) %q_variante]
	# if pard['sid_id_utente'] == 'elifus':
	# tools.dump(query[0])
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
#  	if pard['sid_id_utente'] == 'elifus':
#  		tools.dump(resd)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		# Se sto cercando con variante e non ho trovato risultato, 
		# restituisco i dati del solo modello
		if q_variante and not resd[0]['data']:
			return get_modelli_variante_prod(pard, societa, modello_in, variante='')
		elif resd[0]['data']:

			return resd[0]['data']
		else:
			raise errors.DAOException('Nessun modello produttivo trovato %s. (Traceback: %s)' %(modello, resd))
	
	
def get_modello_originale_by_articolo(pard, societa, art, gm, descr):
	if not societa:
		return []
	where_clause = ''
	if gm:
		where_clause += " and ma.gruppo_merceologico = '%s' "%gm
	if art:
		where_clause += "and ma.progr_articolo = '%s' "%art
	if descr:
		where_clause += " and ma.desc_articolo like '%%%%s%%' " %descr
	query = ["""select modello 
				from mod_modello m 
				join mod_articolo ma on (m.societa = ma.societa and m.articolo = ma.articolo) 
				where m.societa = '%s'
				%s
			"""%(societa, where_clause)]
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(query)
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		if resd[0]['data']:
			return [r['modello'] for r in resd[0]['data']]
		else:
			return []	
		
	
def get_scheda_modello_base(pard, societa, modello):
	ret = {}
	
	if societa == 'MN':
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello = modello[:8]
	
	query = ["""select m.classe as classe,
					m.*, c.*, uc.*, a.*, tg.*, p.*, i.*
				from mod_modello m 
				left join mod_uscita_collezione uc 
					on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione 
						and m.anno=uc.anno and m.stagione=uc.stagione)
				left join mod_articolo a 
					on (m.societa=a.societa and m.articolo=a.articolo)
				left join mod_classe c
					on (m.classe = c.classe)
				left join mod_tipo_taglia tg
					on (m.tipo_taglia=tg.tipo_taglia and m.societa=tg.societa) 
				left join mod_tipo_produzione p
					on (m.tipo_produzione=p.tipo_produzione)
				left join mod_tipo_imballo i
					on (m.tipo_imballo=i.tipo_imballo)
				where m.societa='%s' and SUBSTRING(m.modello_base, 1, 8)='%s' """%(societa, modello)]
	resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	
	if not resd[0]['data']:
		raise errors.DAOException('Nessun modello trovato %s, %s.' %(societa, modello))
	ret = resd[0]['data'][0].copy()
	ret['prima_tg'] = ''
	ret['ultima_tg'] = ''
	ret['desc_articolo'] = ret['desc_articolo'].encode('latin-1')
		
	ret['modelli_commerciali'] = []
	for mod_comm in resd[0]['data']:
		
		query = ["""select tg.* 
				from mod_taglia_nazione tg 
				where tg.nazione='IT' and tg.societa='%s' and tg.tipo_taglia='%s' """%(societa, mod_comm['tipo_taglia'])]
		resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
		if not ok_status:
			raise errors.DAOException(resd[0]['data'])
		for tg in resd[0]['data']:
			if str(tg['indice_taglia']).strip() == str(mod_comm['indice_prima_tg']).strip():
				ret['prima_tg'] = tg['taglia']
			elif str(tg['indice_taglia']).strip() == str(mod_comm['indice_ultima_tg']).strip():
				ret['ultima_tg'] = tg['taglia']
		
		query = ["""select m.* 
				from mod_modello_produzione m 
				where m.societa='%s' and m.modello='%s' """%(societa, mod_comm['modello'])]
		resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
		if not ok_status:
			raise errors.DAOException(resd[0]['data'])
		mod_comm['modelli_produttivi'] = []
		for mod_prod in resd[0]['data']:
			mod_comm['modelli_produttivi'].append(mod_prod)
		
		query = ["""select m.*, p.*
				from mod_modello_pezzo m
					join mod_pezzo p on m.codice_pezzo=p.codice_pezzo and m.societa=p.societa 
				where m.societa='%s' and m.modello='%s' """%(societa, mod_comm['modello'])]
		resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
		if not ok_status:
			raise errors.DAOException(resd[0]['data'])
		mod_comm['pezzi'] = []
		for pezzo in resd[0]['data']:
			pezzo['composizioni'] = []
			
			query = ["""select m.*, t.*
				from mod_composizione m
					left join mod_tipo_materiale t on m.tipo_materiale=t.tipo_materiale and m.societa=t.societa 
				where m.societa='%s' and m.modello='%s' and m.progr_pezzo='%s'
				order by m.progr_composizione 
				"""%(societa, mod_comm['modello'], pezzo['progr_pezzo'])]
			resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
			if not ok_status:
				raise errors.DAOException(resd[0]['data'])
			for compos in resd[0]['data']:
				pezzo['composizioni'].append(compos)
			
#			pezzo['etichette'] = []
#			
#			query = ["""select eti.*
#				from mod_etichetta_modello eti
#				where eti.societa='%s' and eti.modello='%s' and eti.progr_pezzo='%s'
#				order by eti.tipo_etichetta, eti.progr_etichetta
#				"""%(societa, mod_comm['modello'], pezzo['progr_pezzo'])]
#			resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
#			if not ok_status:
#				raise errors.DAOException(resd[0]['data'])
#			for eti in resd[0]['data']:
#				pezzo['etichette'].append(eti)
			
			mod_comm['pezzi'].append(pezzo)
		
		query = ["""select m.*, c.*, t.*
				from mod_variante m
					left join mod_colore c on m.colore=c.colore and m.societa=c.societa
					left join mod_tema t on m.societa=t.societa and m.anno=t.anno 
						and m.stagione=t.stagione and m.uscita_collezione=t.uscita_collezione
						and m.tema=t.tema
				where m.societa='%s' and m.modello='%s' 
				order by m.variante """%(societa, mod_comm['modello'])]
		resd, ok_status = chiama_ws(WS_PROGRAM , query, societa, debug=False)
		if not ok_status:
			raise errors.DAOException(resd[0]['data'])	
		mod_comm['varianti'] = []

		for variante in resd[0]['data']:
			
			variante['html_pantone'] = ''
			variante['colore_variante'] = ''
			variante['desc_colore_variante'] = ''
			if societa in ('MM','MA'):
				var_dict = get_impo_pantone(pard, societa, ret.copy(), variante['variante'])
				variante['html_pantone'] = var_dict.get('html_pantone','')
				variante['colore_variante'] = var_dict.get('colore_variante','')
				variante['desc_colore_variante'] = var_dict.get('desc_colore_variante','')
			
			mod_comm['varianti'].append(variante)
		
		ret['modelli_commerciali'].append(mod_comm)
	
	return ret
	
	
def ricerca_modello_originale(pard, dict_params):
	
	q_societa = ''
	if dict_params.get('societa',''):
		q_societa = " and m.societa='%(societa)s' " %dict_params
	
	q_as = ''
	if dict_params.get('as','') and dict_params['as']!='Tutti':
		anno = dict_params['as'][:4]
		if dict_params['as'][5:] == 'P/E':
			stag = '1'
		else:
			stag = '2'
		q_as = "and (m.anno = '%s' and m.stagione = '%s') " %(anno, stag)
		
	q_gm = ''
	if dict_params.get('gm',''):
		q_gm = " and a.gruppo_merceologico = '%(gm)s' " %dict_params
		
	q_art = ''
	if dict_params.get('articolo',''):
		q_art = " and a.progr_articolo = '%(articolo)s' " %dict_params
		
	q_classe = ''
	if dict_params.get('classe',''):
		q_classe = " and ( m.classe = '%(classe)s' or  c.desc_classe like '%%%(classe)s%%' ) " %dict_params
	
	q_collezione = ''
	if dict_params.get('collezione',''):
		q_collezione = " and (uc.uscita_collezione like '%%%(collezione)s%%' or uc.desc_uscita_collezione like '%%%(collezione)s%%') " %dict_params
	
	q_modello = ''
	if dict_params.get('modello',''):
		q_modello = " and ( substring(m.modello,1,8) like '%%%(modello)s%%' and m.modello_base like '%%%(modello)s%%' ) " %dict_params
		
	q_nome = ''
	if dict_params.get('nome_modello',''):
		q_nome = " and m.nome like '%%%(nome_modello)s%%' " %dict_params
		
#	q_fornitore = ''
#	if dict_params.get('fornitore',''):
#		q_fornitore = " and m.articolo = '%(fornitore)s' " %dict_params
	
	query = ["""select distinct m.modello_base,
						m.nome,
						m.societa,
						m.anno,
						m.stagione,
						m.classe,
						c.desc_classe,
						a.progr_articolo,
						a.desc_articolo,
						a.gruppo_merceologico,
						uc.uscita_collezione,
						uc.desc_uscita_collezione
				from mod_modello m 
				left join mod_uscita_collezione uc 
					on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione and m.anno=uc.anno and m.stagione=uc.stagione)
				left join mod_articolo a 
					on (m.societa=a.societa and m.articolo=a.articolo)
				left join mod_classe c
					on (m.classe = c.classe)
				where 1=1
				%(q_societa)s %(q_as)s
				%(q_gm)s %(q_art)s
				%(q_collezione)s %(q_classe)s
				%(q_modello)s %(q_nome)s
				 """ %vars()]
	resd, ok_status = chiama_ws(WS_PROGRAM , query, dict_params['societa'], debug=False)
	#tools.dump(query)
	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		# Per mostrare solo i modelli per cui abbiamo una copia della DiBa
		# DA RIMUOVERE
		rlist = []
		for r in resd[0]['data']:
#			pard['sql'] = """ select * from impo_db_esplosa where societa='MM' and modello like '%s%%' """ %r['modello_base']
#			ret = db_access.mysql_dict(pard)
#			if ret:
			rlist.append(r)
		return rlist


def get_impo_pantone(pard, societa, modello_dict, variante):
	pard['sql'] = """ select ifnull(P.html,'') as html_pantone,
							AV.colore as colore_variante,
							AV.descrizione as desc_colore_variante
					  from impo_articoli_varianti AV
					  	left join impo_colori F 
							on (F.colore = AV.colore and F.societa = AV.societa)
						left join impo_pantone P 
							on (P.id = F.id_pantone and P.societa = F.societa) 
					  where AV.ml='%(uscita_collezione)s'
					  		and AV.id_articolo = %(articolo)s
					  		and AV.stato in ('Valido','Consolidato','Annullo Segr')
					  		and AV.variante = '%%s'
					  		and AV.societa = '%%%%s' 
					 """ %modello_dict %variante %societa
	resd = db_access.mysql_dict(pard)
	if resd:
		return resd[0]
	else:
		return {}


def get_diba_esplosa_originale(societa, modello, variante, debug=False):
	pard = config.global_parameters({})
	if pard.get('AMBIENTE', '') == 'ot':
		query = ["""select m.nome, diba.*, de.*,
						substring(de.prodotto_componente,6,2) as gruppo_merceologico,
						if(substring(de.prodotto_componente,8,1)!='0', concat('20',substring(de.prodotto_componente,3,2)), '0') as anno,
						substring(de.prodotto_componente,9,1) as stagione,
						substring(de.prodotto_componente,10,3) as progr_articolo
						from pro_distinta_base_prodotto diba
						join mod_modello m on (m.modello = diba.modello and m.societa=diba.societa)
						left join pro_distinta_base_esplosa de on (diba.societa = de.societa and diba.prodotto=de.prodotto
																and diba.numero_riga = de.numero_riga)
						where 1=1
						and diba.societa = '%s'
						and diba.prodotto = '%s'
						and (de.variante_prodotto = %s or de.variante_prodotto is null)
						and (de.misura_prodotto in ('42', 'SM', 'M') or de.misura_prodotto is null)
						 """ % (societa, modello, variante)]
		r, ok_status = chiama_ws(WS_PROGRAM, query,societa, debug=False)
		#tools.dump(query[0])
		if not ok_status:
			raise errors.DAOException(r[0]['data'])
	else:
		ws_conf = mmfg_ws_config.get_config(ws_db_gruppo.LABEL_CONFIG)
		param_d = {}
		param_d['module'] = ws_db_gruppo.WS_MODULE
		param_d['program'] = WS_DIBA_ESPLOSA_PRODOTTO
		param_d['societa'] = societa
		dati = {
			'societa': societa,
			'prodotto': modello,
			'variante_prodotto': variante
		}
		param_d['data'] = json.dumps(dati)
		dwg_logger.debug(param_d)
		r = mmfg_ws_client.send_request(param_d=param_d, ws_conf=ws_conf, debug=debug)
		dwg_logger.debug(r)
		try:
			r = json.loads(r)
		except:
			try:
				r = eval(r)
			except:
				mess = str(r)
				r = [{
					'result': WS_RET_ERR,
					'data': mess
				}]

	return r


def get_diba_prodotto_originale(societa, modello, debug=False):
	pard = config.global_parameters({})
	if pard.get('AMBIENTE', '') == 'ot':
		query = ["""select *
					from pro_distinta_base_prodotto diba
					where 1=1
					and diba.societa = '%s'
					and diba.prodotto = %s
					 """ % (societa, modello)]
		r, ok_status = chiama_ws(WS_PROGRAM, query, societa, debug=False)
		# tools.dump(query)
		if not ok_status:
			raise errors.DAOException(r[0]['data'])
	else:
		ws_conf = mmfg_ws_config.get_config(ws_db_gruppo.LABEL_CONFIG)
		param_d = {}
		param_d['module'] = ws_db_gruppo.WS_MODULE
		param_d['program'] = WS_DIBA_PRODOTTO
		param_d['societa'] = societa
		dati = {
			'societa': societa,
			'prodotto': modello
		}
		param_d['data'] = json.dumps(dati)
		r = mmfg_ws_client.send_request(param_d=param_d, ws_conf=ws_conf, debug=debug)

		try:
	# 		r = json.loads(r)
			r = eval(r)
		except:
			mess = str(r)
			r = [{
				'result': WS_RET_ERR,
				'data': mess
			}]

	return r


def inserisci_diba_esplosa(pard, conn, diba):
	if diba['descrizione']:
		diba['descrizione'] = conn.escape_string(diba['descrizione'])
	if diba['posizione']:
		diba['posizione'] = conn.escape_string(diba['posizione'])
	pard['sql'] = """ insert into impo_diba_esplosa
						set
					  societa = '%(societa)s',
					  prodotto = '%(prodotto)s',
					  variante_prodotto = '%(variante_prodotto)s',
					  progr_pezzo = '%(progr_pezzo)s',
					  numero_riga = '%(numero_riga)s',
					  misura_prodotto = '%(misura_prodotto)s',
					  materiale = '%(materiale)s',
					  materiale_componente = '%(materiale_componente)s',
					  misura_prodotto_componente = '%(misura_prodotto_componente)s',
					  prodotto_componente = '%(prodotto_componente)s',
					  variante_prodotto_componente = '%(variante_prodotto_componente)s',
					  gruppo_merceologico = '%(gruppo_merceologico)s',
					  anno = '%(anno)s',
					  stagione = '%(stagione)s',
					  progr_articolo = '%(progr_articolo)s',
					  descrizione = "%(descrizione)s",
					  consumo_unitario_netto = '%(consumo_unitario_netto)s',
					  consumo_unitario_lordo = '%(consumo_unitario_lordo)s',
					  colonna = '%(colonna)s',
					  posizione = '%(posizione)s'
				""" %diba
	db_access.mysql_send_conn(pard, conn)


def elimina_diba_esplosa(pard, conn, societa, modello, variante):
	pard['sql'] = """ delete 
						from impo_diba_esplosa
					  where
					  	societa = '%s'
					  	and prodotto = '%s'
					  	and variante_prodotto = '%s'
				""" %(societa.strip(), modello.strip(), variante.strip())
	db_access.mysql_send_conn(pard, conn)


def ricerca_modelli_originali_by_tessuto(dict_params):
	q_societa = ''
	if dict_params.get('societa_orig', ''):
		q_societa = " and m.societa='%(societa_orig)s' " % dict_params
	#
	q_as = ''
	if dict_params.get('as_orig', ''):
		as_orig = anno_stagione_lib.decodifica_as(dict_params['as_orig'], tipo='tupla_anno_stagione_aaaa_s')
		if as_orig:
			anno = as_orig[0]
			stag = as_orig[1]
			q_as = "and (a.anno = '%s' and a.stagione = '%s') " % (anno, stag)
	#
	q_gm = ''
	if dict_params.get('gm', ''):
		q_gm = " and a.gruppo_merceologico = '%(gm)s' " % dict_params
	#
	q_art = ''
	if dict_params.get('articolo', ''):
		q_art = " and a.progr_articolo = '%(articolo)s' " % dict_params

	q_desc_art = ''
	if dict_params.get('articolo_descrizione', ''):
		q_desc_art = """ and a.desc_articolo like "%%%(articolo_descrizione)s%%" """ % dict_params
	#
	q_classe = ''
	if dict_params.get('classe', ''):
		q_classe = " and ( m.classe = '%(classe)s' or  c.desc_classe like '%%%(classe)s%%' ) " % dict_params
	#
	q_collezione = ''
	if dict_params.get('collezione_orig', ''):
		if '-' in dict_params['collezione_orig']:
			ml = dict_params['collezione_orig'].split('-')[0]
			collezione = dict_params['collezione_orig'].split('-')[1]
		else:
			ml = dict_params['collezione_orig']
			collezione = dict_params['collezione_orig']
		q_collezione = ' and (uc.uscita_collezione like "%s%%" or uc.desc_uscita_collezione like "%%%s%%")' %(ml, collezione)
	#
	q_modello = ''
	if dict_params.get('modello_orig', ''):
		q_modello = " and substring(m.modello,1,8) = '%(modello_orig)s' " % dict_params
	#
	q_nome = ''
	if dict_params.get('nome_modello_orig', ''):
		q_nome = " and m.nome like '%%%(nome_modello_orig)s%%' " % dict_params
	#
	q_variante = ''
	if dict_params.get('variante', ''):
		q_variante = " and mv.variante = '%(variante)s' " % dict_params

	q_colore = ''
	if dict_params.get('colore', '') and dict_params['colore']:
		q_colore = " and lpad(diba.variante_prodotto_componente, 4, '0') = '%s' " % str(dict_params['colore']).zfill(4)

	q_tipo_tessuto = ''
	if dict_params.get('tipo_tessuto','') == 'T':
		q_tipo_tessuto = " and a.gruppo_merceologico='%(gm)s' and diba.numero_riga in (1110, 100) "  % dict_params
	elif dict_params.get('tipo_tessuto','') == 'E':
		q_tipo_tessuto = " and a.gruppo_merceologico='%(gm)s'and diba.numero_riga not in (1110, 100) " % dict_params
	#
	#
	query = ["""select distinct m.modello_base as modello_orig,
						m.nome as nome_modello_orig,
						m.modello as modello_prodotto,
						m.societa as societa_orig,
						m.anno,
						m.stagione,
						concat(m.anno, ' ', if(m.stagione=1, 'P/E', 'A/I')) as as_orig,
						m.classe,
						c.desc_classe,
						a.progr_articolo as articolo,
						a.desc_articolo as articolo_descrizione,
						a.progr_articolo as art_princ,
						a.desc_articolo as descr_art_princ,
						a.societa as societa,
						a.gruppo_merceologico as gm,
						concat(a.anno, ' ', if(a.stagione=1, 'P/E', 'A/I')) as anno_stagione,
						ifnull(if((diba.numero_riga=1110 and m.societa in ('MM','MA')) or (diba.numero_riga=100 and m.societa ='MN'), 'T', 'E'), '') as tipo_tessuto,
						uc.uscita_collezione as ml,
						uc.desc_uscita_collezione as desc_collezione_orig,
						mv.variante as variante_orig,
						mv.desc_variante as colore_descrizione,
						diba.materiale_componente as id_articolo_orig,
						diba.variante_prodotto_componente as colore,
						diba.consumo_unitario_lordo as consumo
				from mod_modello m
				join mod_uscita_collezione uc
					on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione and m.anno=uc.anno and m.stagione=uc.stagione)
				join mod_classe c
					on (m.classe = c.classe)
				join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
				join mod_variante_produzione mvp on (mv.societa = mvp.societa and mvp.modello = mv.modello and mv.variante=mvp.variante)
				left join pro_distinta_base_esplosa diba  on (diba.societa=m.societa and diba.materiale = mvp.modello
								and diba.variante_prodotto = mvp.variante and substring(diba.prodotto,-2,2) = '00')
				left join art_articolo a
					on (diba.societa=a.societa and diba.materiale_componente=a.articolo)
				where  (mv.validita != 'A' or mv.validita is NULL)
			    and m.versione_modello != 'C'
			    and ( diba.misura_prodotto in ('42', 'SM', 'M') or diba.misura_prodotto is null)
				%(q_societa)s %(q_as)s
				%(q_gm)s %(q_art)s %(q_desc_art)s %(q_colore)s
				%(q_collezione)s %(q_classe)s %(q_tipo_tessuto)s
				%(q_modello)s %(q_nome)s
				%(q_variante)s
				limit 1000
				 """ % vars()]
	#tools.dump(query[0])
	resd, ok_status = chiama_ws(WS_PROGRAM, query, dict_params.get('societa_orig', ''), debug=False)

	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		return resd[0]['data']

# al momento non utilizzato
def ricerca_esplosi_by_modello(dict_params):
	query = ["""select distinct	a.anno,
							a.stagione,
							concat(a.anno, ' ', if(a.stagione=1, 'P/E', 'A/I')) as anno_stagione_orig,
							a.progr_articolo as articolo,
							a.desc_articolo as articolo_descrizione,
							a.gruppo_merceologico as gm,
							diba.prodotto_componente as articolo_tessuto,
							diba.variante_prodotto_componente as colore,
							diba.consumo_unitario_lordo as consumo
					from  pro_distinta_base_esplosa diba
					left join mod_articolo a
						on (diba.societa=a.societa and diba.materiale_componente=a.articolo)
					where  diba.societa = '%(societa_orig)s'
					and diba.prodotto = '%(modello_prodotto)s'
				    and diba.variante_prodotto = '%(variante)s'
				    and diba.numero_riga!=1110
				    and a.gruppo_merceologico = %(gruppo_merceologico)s
				    and diba.misura_prodotto in ('42', 'SM', 'M')
					limit 100
					 """ %dict_params]
	#tools.dump(query[0])
	resd, ok_status = chiama_ws(WS_PROGRAM, query, dict_params['societa_orig'], debug=False)

	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		return resd[0]['data']



def get_varianti_orig(societa_orig, as_orig, modello_trascod):
	query = ["""select distinct	mvp.variante as variante,
								ifnull(mv.desc_variante, '') as descrizione_variante
						from mod_modello m
						left join mod_uscita_collezione uc
							on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione and m.anno=uc.anno and m.stagione=uc.stagione)
						left join mod_articolo a
							on (m.societa=a.societa and m.articolo=a.articolo)
						left join mod_classe c
							on (m.classe = c.classe)
						left join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
						left join mod_variante_produzione mvp on (mv.societa = mvp.societa and mvp.modello = mv.modello and mv.variante=mvp.variante)
						where m.societa='%s'
						and concat(m.anno, ' ', if(m.stagione=1, 'P/E', 'A/I')) = '%s'
						and substring(m.modello, 1,8) = '%s'
					    and  (mv.validita != 'A' or mv.validita is NULL)
			            and m.versione_modello != 'C'
						 """ % (societa_orig, as_orig, modello_trascod[:8])]

	resd, ok_status = chiama_ws(WS_PROGRAM, query, societa_orig, debug=False)

	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		return resd[0]['data']


def get_varianti_orig_by_colore(societa_orig, as_orig, modello, colore_orig):
	if societa_orig == 'MN':
		if str(modello[0]) == '9' and str(modello[7]) == '6':
			modello_orig = '8'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '6':
			modello_orig = 'U'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '4' and str(modello[7]) == '5':
			modello_orig = 'T'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '8' and str(modello[7]) == '5':
			modello_orig = 'N'+str(modello)[1:6]+'1'+str(modello)[6]
		elif str(modello[0]) == '2' and str(modello[7]) == '8':
			modello_orig = 'K'+str(modello)[1:6]+'1'+str(modello)[6]
		else:
			modello_orig = str(modello)[:6]+'1'+str(modello)[6]
	else:
		modello_orig = modello[:8]
	query = ["""select distinct	mvp.variante as variante,
								ifnull(mv.desc_variante, '') as descrizione_variante,
								diba.prodotto_componente as articolo, diba.variante_prodotto_componente as colore, diba.prodotto as modello
						from mod_modello m
						left join mod_uscita_collezione uc
							on (m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione and m.anno=uc.anno and m.stagione=uc.stagione)
						left join mod_articolo a
							on (m.societa=a.societa and m.articolo=a.articolo)
						left join mod_classe c
							on (m.classe = c.classe)
						left join mod_variante mv on (m.societa = mv.societa and m.modello = mv.modello)
						left join mod_variante_produzione mvp on (mv.societa = mvp.societa and mvp.modello = mv.modello and mv.variante=mvp.variante)
						LEFT JOIN pro_distinta_base_esplosa diba on (m.societa = diba.societa and m.modello = diba.prodotto and mvp.variante = diba.`variante_prodotto` )
						where m.societa='%s'
						and concat(m.anno, ' ', if(m.stagione=1, 'P/E', 'A/I')) = '%s'
						and substring(m.modello, 1,8) = '%s'
					    and  (mv.validita != 'A' or mv.validita is NULL)
			            and m.versione_modello != 'C'
			            and diba.misura_prodotto in ('42', 'SM', 'M')
			            and diba.numero_riga in (1110, 100)
			            and diba.variante_prodotto_componente like '%s'
						 """ % (societa_orig, as_orig, modello_orig[:8], colore_orig)]

	resd, ok_status = chiama_ws(WS_PROGRAM, query, societa_orig, debug=False)

	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		return resd[0]['data']


def get_composizione_modello(modello, societa):
	query = ["""
				select progr_pezzo, progr_composizione, desc_tipo_materiale, composizione
					from mod_composizione mc
					join mod_tipo_materiale mt on (mc.tipo_materiale = mt.tipo_materiale)
					where substring(mc.modello,1,8) = substring('%s',1,8)
					and substring(mc.modello,10,11) = '00'
					and mc.societa = '%s'
					order by progr_pezzo, progr_composizione
						 """ % (modello, societa)]
	# tools.dump(query[0])
	resd, ok_status = chiama_ws(WS_PROGRAM, query, societa, debug=False)

	if not ok_status:
		raise errors.DAOException(resd[0]['data'])
	else:
		return resd[0]['data']

# @query_with_result
# def get_diba_test(dict_params):
# 	q_societa = ''
# 	if dict_params.get('societa_orig', ''):
# 		q_societa = " and societa='%(societa_orig)s' " % dict_params
#
# 	q_as = ''
# 	if dict_params.get('anno_stagione_orig', ''):
# 		as_orig = anno_stagione_lib.decodifica_as(dict_params['anno_stagione_orig'], tipo='tupla_anno_stagione_aaaa_s')
# 		if as_orig:
# 			anno = as_orig[0]
# 			stag = as_orig[1]
# 			q_as = " and (anno = '%s' and stagione = '%s') " % (anno, stag)
#
# 	q_gm = ''
# 	if dict_params.get('gm', ''):
# 		q_gm = " and gm = '%(gm)s' " % dict_params
#
# 	q_art = ''
# 	if dict_params.get('articolo', ''):
# 		q_art = " and articolo = '%(articolo)s' " % dict_params
#
# 	q_classe = ''
# 	if dict_params.get('classe', ''):
# 		q_classe = " and ( classe = '%(classe)s' or  desc_classe like '%%%(classe)s%%' ) " % dict_params
#
# 	q_collezione = ''
# 	if dict_params.get('collezione', ''):
# 		q_collezione = " and (uscita_collezione like '%%%(collezione)s%%' or desc_uscita_collezione like '%%%(collezione)s%%') " % dict_params
#
# 	q_modello = ''
# 	if dict_params.get('modello_orig', ''):
# 		q_modello = " and ( substring(modello_orig,1,8) like '%%%(modello_orig)s%%' and modello_orig like '%%%(modello_orig)s%%' ) " % dict_params
#
# 	q_nome = ''
# 	if dict_params.get('nome_modello', ''):
# 		q_nome = " and nome like '%%%(nome_modello)s%%' " % dict_params
#
# 	q_variante = ''
# 	if dict_params.get('variante', ''):
# 		q_variante = " and variante like '%%%(variante)s%%' " % dict_params
#
# 	query = """ select *
# 				from DIBA_TEST
# 				where 1=1
# 				%(q_societa)s %(q_as)s
# 	 			%(q_gm)s %(q_art)s
# 				%(q_collezione)s %(q_classe)s
# 	 			%(q_modello)s %(q_nome)s
# 	 			%(q_variante)s
# 			"""%vars()
#
# 	return query