(function()
{
	'use strict';

	RicercaService.$inject = ['DTHttp', 'LoggerFactory'];

	var factoryId = "RicercaService";

	angular.module('app').factory(factoryId, RicercaService);

	function RicercaService(DTHttp, LoggerFactory)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {
			ricerca_fabbisogni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'ricerca_fabbisogni', payload)
					.then(
						function successCallback(response) {
							if (successHandler != null)
							    var response_list_impegni = response.data.ResultSet.Result
							    if (response_list_impegni != undefined){

                                    $(response_list_impegni).each(function(index,item){

                                        if( item.dettaglio != undefined &&
                                            item.dettaglio.length > 0)
                                        {
                                            $(item.dettaglio).each(function(iindex,iitem){
                                                iitem['isSelected'] = false;
                                            });
                                        }
                                    });
                                }
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_impegni_da_fabbisogni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_fabbisogni', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
							    var response_fabbisogni = response.data.ResultSet.Result;
							    if (response_fabbisogni != undefined){

                                    $(response_fabbisogni).each(function(index,item){

                                        if( item.fabb_list != undefined &&
                                            item.fabb_list.length > 0)
                                        {
                                            $(item.fabb_list).each(function(iindex,iitem){
                                                iitem['DT'] = 0;
                                                iitem['SOC'] = 0;
                                                iitem['ORD'] = 0;
                                            });
                                        }
                                    });
                                }
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_impegni_da_fabbisogni_refresh: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_fabbisogni_refresh', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
							    var response_fabbisogni = response.data.ResultSet.Result;
							    if (response_fabbisogni != undefined){

                                    $(response_fabbisogni).each(function(index,item){

                                        if( item.fabb_list != undefined &&
                                            item.fabb_list.length > 0)
                                        {
                                            $(item.fabb_list).each(function(iindex,iitem){
                                                iitem['DT'] = 0;
                                                iitem['SOC'] = 0;
                                                iitem['ORD'] = 0;
                                            });
                                        }
                                    });
                                }
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_giacenze: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'ricerca_giacenza', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_giacenza_da_impegni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'ricerca_giacenza_da_impegni', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			get_giacenze: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_giacenza_complessiva', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			get_giacenza_complessiva_refresh: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'get_giacenza_complessiva_refresh', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			ricerca_giacenza_articolo: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_giacenza_articolo', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			get_ordini_da_gestione_impegni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'get_ordini_da_gestione_impegni', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			get_disimpegno_ordini_da_gestione_impegni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.post('facon/diba.fabbisogni_ctrl', 'get_disimpegno_ordini_da_gestione_impegni', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			getClassi: function (successHandler, errorHandler) {
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'autocomplete_classi')
					.then(
						function successCallback(response) {
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			getSbloccaImpegni: function (params, successHandler, errorHandler) {
				var payload = {
					dati: JSON.stringify(params)
				};
				DTHttp
					.get('facon/diba.fabbisogni_ctrl', 'get_sblocca_impegni', payload)
					.then(
						function successCallback(response) {
                            logger.debug(response);
							if (successHandler != null){
							    var response_sblocca_impegni = response.data.ResultSet.Result;
							    if (response_sblocca_impegni != undefined){
                                    $(response_sblocca_impegni).each(function(index,item){
                                        item['DT'] = 0;
                                        item['SOC'] = 0;
                                        item['ORD'] = 0;
                                    });
                                }

								successHandler(response.data.ResultSet.Result);
                            }
						},

						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			stampaDistintaBase: function(entity, successHandler, errorHandler)
			{
				var payload = {dati: JSON.stringify(entity)
                                };
				var program = 'stampa_distinta_base';
				DTHttp
					.post('facon/diba.fabbisogni_ctrl', program, payload)
					.then(

					function successCallback(response){
						var dati = null;
						try
						{
							dati = response.data.message;
						}
						catch (error)
						{
							logger.error("Errore nel formato della risposta.", response.data);
							if (errorHandler != null)
								errorHandler();
							return;
						}
						if (successHandler != null)
							successHandler(dati);
					},

					function errorCallback(response){
						logger.error(response.data.message);
						if (errorHandler != null)
							errorHandler();
					}
				);
			}
		};
	}
})();