(function(){
    'use strict';

    var controllerId = "GestFabbVistaModelloController";

    ControllerFn.$inject = ['$scope', '$rootScope', 'RicercaService', 'LoggerFactory', 'TIPO_VIS', 'DTHttp', '$window', 'i18nService'];

    angular.module("app").controller(controllerId, ControllerFn)
    .directive('ngActiveScroll', function () {
        return {
            link: function($scope, element, attr) {
              $scope.$watch('isDisplayed', function(newValue, oldValue) {
                var viewport = element.find('.ui-grid-render-container');

                ['touchstart', 'touchmove', 'touchend','keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
                  viewport.unbind(eventName);
                });
              });
            },
        };
    });

    function ControllerFn($scope, $rootScope, RicercaService, LoggerFactory, TIPO_VIS, DTHttp, $window, i18nService) {

        var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);

        //variabile che setta la lingua delle descrizioni in tabella
        vm.lang = 'it';

        vm.ui_tipo_vis= TIPO_VIS;

        $scope.grigliaModelli = {
            showGridFooter: true,
			enableColumnMenus: false,
			enableExpandableRowHeader: false,
			headerHeight: 35,
			rowHeight: 35,
            expandableRowTemplate: '<div ui-grid="row.entity.subgrigliaModelli" ng-style="grid.appScope.getSubTableHeight(row)" ng-active-scroll ui-grid-selection ui-grid-auto-resize ui-grid-resize-columns></div>',
			expandableRowScope: {
                apriGestioneImpegni : function(id_dibas_dett){
                    var sid = jQuery('#sid').val();
                    var app_server = jQuery('#app_server').val();
                    var menu_id = jQuery('#menu_id').val();
                    var module = 'facon/diba.impegni_ctrl';
                    var program = 'vista_gestione_impegni';
                    var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_dibas=" + id_dibas_dett + "&sid=" + sid;
                    window.open(url);
                },
                getQtaRichValDettaglio: function(riga){
                    var richVal = riga.qta_teor > 0 ? riga.qta_teor : riga.qta_prev;
                    return richVal;
                }

            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                });

                //custom Template per bottone espandi sottogriglia
                var cellTemplate = "<div style= \"height:32px \" "+
                "class=\"ui-grid-row-header-cell ui-grid-expandable-buttons-cell\">"+
                "<div class=\"ui-grid-cell-contents\">"+
                "<i ng-class=\"{ 'ui-grid-icon-plus-squared' : !row.isExpanded && row.entity.dettaglio.length !== 0,"+
                "'ui-grid-icon-minus-squared' : row.isExpanded, 'ng-grid-cell': row.entity.subgrigliaModelli.data.length == 0 }\" "+
                "ng-click=\"grid.api.expandable.toggleRowExpansion(row.entity)\"></i></div></div>"
                //aggiungo colonna custom Template alle colonne
                $scope.gridApi.core.addRowHeaderColumn( { name: 'rowHeaderCol',
                                                        displayName: '',
                                                        width: 35,
                                                        cellTemplate: cellTemplate} );

			}
		};

        function row_class(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.collezione_facon === 'OUTLET PROG') {
            return 'giallo_outlet_prog';
          }
          return '';
        }

        $scope.setTypeProgressBar = function(fabbisogno_perc) {
            if (fabbisogno_perc >= '0' && fabbisogno_perc < '30')
                return 'warning';
            else if (fabbisogno_perc >= '30' && fabbisogno_perc < '60')
                return 'info';
            else if (fabbisogno_perc >= '60' && fabbisogno_perc < '100')
                return 'primary';
            else if (fabbisogno_perc ==='100')
                return 'success';
        }

        $scope.getQtaRichVal = function(riga){
            var richVal = riga.capi_teor > 0 ? riga.capi_teor : riga.capi_prev;
            return richVal;
        };

        var columnDefs_innerTable = [
			{displayName: 'Capocmat', field: 'capocmat'},
			{displayName: 'Mis', field: 'misura'},
			{displayName: 'Col', field: 'colore'},
			{displayName: 'Descrizione', field: 'descrizione'},
			{displayName: 'Rich. Comm.', field: 'rif_rich'},
			{displayName: 'Qta Rich.', field: 'qta_teor',
                cellTemplate:
                '<div class="ui-grid-cell-contents">'+
                        '{{grid.appScope.getQtaRichValDettaglio(row.entity)}}'+
                '</div>'
			},
			{displayName: 'BP', field: 'rif_bp', width: '160', cellTemplate:'<div ng-repeat="item in row.entity[col.field]">{{item}}</div>'},
			{displayName: 'Qta BP', field: 'qta_reale'},
			{displayName: 'Qta FABB', field: 'fabbisogno'},
			{displayName: 'Imp', field: 'impegno'},
			{displayName: 'Da Sodd.', field: 'da_soddisfare'},
			{displayName: 'Azioni', field: 'nota_lotto', width: '60', cellStyle: {color: 'red', backgroundColor: 'green'},
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Gestione Impegni" tooltip-placement="left" ng-click="grid.appScope.apriGestioneImpegni(row.entity.id_dibas_dett)"'+
                        'class="btn btn-xs btn-success stretto">'+
                        '<i class="glyphicon glyphicon-log-out"></i>'+
                    '</button>'+
                '</div>'}
		];

        var COLONNE_CUSTOM = [
            {displayName: 'ID', field: 'id_richiesta', cellClass: row_class},
			{displayName: 'Tipo', field: 'tipo_richiesta', cellClass: row_class},
			{displayName: 'AS DT.', field: 'anno_stagione_dt', cellClass: row_class},
			{displayName: 'Collezione DT', field: 'collezione_dt', cellClass: row_class},
			{displayName: 'Modello', field: 'modello_orig', cellClass: row_class},
			{displayName: 'Nome', field: 'nome_modello', cellClass: row_class},
			{displayName: 'Variante', field: 'variante', cellClass: row_class},
			{displayName: 'Colore', field: 'colore', cellClass: row_class},
			{displayName: 'Qta Capi Studio', field: 'capi_prev', cellClass: row_class},
			{displayName: 'Qta Capi Rich', field: 'capi_teor', cellClass: row_class,
                cellTemplate:
                '<div class="ui-grid-cell-contents">'+
                        '{{grid.appScope.getQtaRichVal(row.entity)}}'+
                '</div>'
			},
			{displayName: 'Qta Capi BP', field: 'capi_lanciati', cellClass: row_class},
			{displayName: 'FABB %', field: 'fabbisogno_perc',
			                cellClass: row_class,
			                cellTemplate: "<uib-progressbar value=\"row.entity.fabbisogno_perc\"  type ='{{grid.appScope.setTypeProgressBar(row.entity.fabbisogno_perc)}}' animate='true'><b ng-class='{\"progress-low-values-class\": row.entity.fabbisogno_perc >= "+0+" && row.entity.fabbisogno_perc <= "+10+"}'>{{row.entity.fabbisogno_perc.toFixed(0)}}</b></uib-progressbar>",
                            enableSorting: false
            },
			{displayName: 'Operatore', field: 'utente', cellClass: row_class},
			{displayName: 'Azioni', name: 'azioni',
                enableHiding: false, enableCellEdit: false,
                cellClass: row_class,
                cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Studio diba" tooltip-placement="left" ng-click="grid.appScope.apriStudioDiba(row.entity)"'+
                        'class="btn btn-xs btn-warning stretto">'+
                        '<i class="glyphicon glyphicon-log-out"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Stampa" tooltip-placement="left" ng-click="grid.appScope.stampaDistintaBase(row.entity)"'+
                        'class="btn btn-xs btn-default stretto">'+
                        '<i class="glyphicon glyphicon-print"></i>'+
                    '</button>'+
                '</div>'
            }
		]

		$scope.grigliaModelli.columnDefs = COLONNE_CUSTOM;
		$scope.grigliaModelli.data = [];

        vm.aggiorna_dati_modello = function(dati_modello){
            for(var i = 0; i < dati_modello.length; i++){
                dati_modello[i].subgrigliaModelli = {
                    enableRowSelection : true,
                    enableColumnMenus: false,
                    enableSelectAll: true,
                    columnDefs: columnDefs_innerTable,
                    data: dati_modello[i].dettaglio,
                    disableRowExpandable : dati_modello[i].dettaglio === 0,
//                    onRegisterApi: subGridApiRegister
                }
            }
            $scope.grigliaModelli.data = dati_modello;
        };

        vm.RisultatoRicerca = function(event, dati){
            RicercaService.ricerca_fabbisogni(dati, function(data){
                if (data.length > 0){
                    $('.modello-view .fixed-table-container').css({'max-height': '600px'});
                }else{
                    $('.modello-view .fixed-table-container').css({'max-height': 'auto'});
                }
                vm.aggiorna_dati_modello(data);
            }, function(){
                $scope.grigliaModelli.data = [];
            });
        };

        var getIdsList = function(selRowsList){
            var resultIdsList = [];
            if (selRowsList != undefined && selRowsList.length > 0){
                angular.forEach(selRowsList, function(item){
                    if(item.hasOwnProperty('id_richiesta') && resultIdsList.indexOf(item.id_richiesta) === -1){
                        resultIdsList.push(item.id_richiesta);
                    }
                });
            }
            return resultIdsList;
        }

        var getModelsList = function(selRowsList){
            var resultIdsList = [];
            if (selRowsList != undefined && selRowsList.length > 0){
                angular.forEach(selRowsList, function(item){
                    if(item.hasOwnProperty('modello_orig') && resultIdsList.indexOf(item.modello_orig) === -1){
                        resultIdsList.push(item.modello_orig);
                    }
                });
            }
            return resultIdsList;
        }

        $scope.apriStudioDiba = function(riga){
            var id_richiesta = riga.id_richiesta;
            if (riga != undefined && riga.name === 'gestFabb:apriDiba'){
                var id_richiesta = getIdsList(angular.copy($scope.selectedRows));
                var modelli = getModelsList(angular.copy($scope.selectedRows));
            }else if (riga != undefined && $.isNumeric(riga.id_richiesta)){
                var id_richiesta = [id_richiesta];
                var modelli = riga.modello_orig
            }
            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/diba.studio_diba_ctrl';
            var program = 'vista_studio_diba';
            var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_rich_studio=" + id_richiesta + "&cod_modelli=" + modelli + "&tipo_studio=" + "diba" + "&sid=" + sid;
            if (id_richiesta != undefined && id_richiesta.length > 0){
                window.open(url);
            }else{
                logger.error('Nessun elemento selezionato per Studio Diba');
            }

        }

        $scope.apriStudioPermanenti = function(riga){
            var id_richiesta = riga.id_richiesta;
            if (riga != undefined && riga.name === 'gestFabb:apriPermanenti'){
                var id_richiesta = getIdsList(angular.copy($scope.selectedRows));
                var modelli = getModelsList(angular.copy($scope.selectedRows));
            }else if (riga != undefined && $.isNumeric(riga.id_richiesta)){
                var id_richiesta = [id_richiesta];
                var modelli = riga.modello_orig
            }

            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/diba.studio_diba_ctrl';
            var program = 'vista_studio_diba';
            var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_rich_studio=" + id_richiesta + "&cod_modelli=" + modelli + "&tipo_studio=" + "permanenti" + "&sid=" + sid;
            if (id_richiesta != undefined && id_richiesta.length > 0){
                window.open(url);
            }else{
                logger.error('Nessun elemento selezionato per Studio Permanenti');
            }

        }

        function getExpandableRowHeight(expRowHeight){
            if(expRowHeight < 50)
                return 50;
            else if(expRowHeight > 50 && expRowHeight < 500)
                return expRowHeight;
            else if(expRowHeight > 500)
                return 500;
        };

        $scope.getSubTableHeight = function(row) {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           var heightSubTable = (row.entity.subgrigliaModelli.data.length * rowHeight + 2 * headerHeight)
           row.grid.options.expandableRowHeight = heightSubTable;//getExpandableRowHeight(heightSubTable);
           row.expandedRowHeight = heightSubTable;//getExpandableRowHeight(heightSubTable);
           return {
              height: heightSubTable + "px",
//              'min-height': 50 +'px',
//              'max-height': 500 +'px'
           };
        };

        vm.anteprimaDocumentoPDFSuccessHandler = function(data){
            console.log(data)
			var program = 'render_doc';
			var url = DTHttp.urlBuilder('facon/diba.fabbisogni_ctrl', program, {doc:JSON.stringify(data)});
			$window.open(url, '_blank');
		};

		vm.anteprimaDocumentoPDFErrorHandler = function(){
		};

		$scope.stampaDistintaBase = function(entity)
		{
		    var objToSend = [{'id': entity.id_richiesta, 'societa': entity.societa, 'modello_orig': entity.modello_orig, 'nome_modello': entity.nome_modello}]
			RicercaService.stampaDistintaBase(objToSend, vm.anteprimaDocumentoPDFSuccessHandler, vm.anteprimaDocumentoPDFErrorHandler)
		};

        $scope.stampaDistintaBaseMultiplo = function(entity= null)
		{
            var objToSend = [];
		    angular.forEach($scope.selectedRows, function(item){
		        var newSmartItem = {'id': item.id_richiesta, 'societa': item.societa, 'modello_orig': item.modello_orig, 'nome_modello': item.nome_modello}
		        objToSend.push(newSmartItem);
		    });

		    if ($scope.selectedRows != undefined && $scope.selectedRows.length > 0){
		        RicercaService.stampaDistintaBase(objToSend, vm.anteprimaDocumentoPDFSuccessHandler, vm.anteprimaDocumentoPDFErrorHandler)
		    }else{
		        logger.error('Nessun elemento selezionato');
		    }
        };

        $rootScope.$on("gestFabb:ricerca_modello", vm.RisultatoRicerca);
        $rootScope.$on("gestFabb:stampaDiba", $scope.stampaDistintaBaseMultiplo);
        $rootScope.$on("gestFabb:apriDiba", $scope.apriStudioDiba);
        $rootScope.$on("gestFabb:apriPermanenti", $scope.apriStudioPermanenti);
        $rootScope.$on("gestFabb:reset_modelli_data", function(){$scope.grigliaModelli.data = []});
    }


})();