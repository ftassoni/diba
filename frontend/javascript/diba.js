function init(){
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	jQuery("#overlay").hide();
	jQuery("#overall_container").hide();
	
	jQuery("#global_tabs").tabs({ orientation: "vertical" });
	
	jQuery('#btn_annulla').unbind('click');
	jQuery('#btn_annulla').click(chiudi);
	

}

function chiudi() {
	window.close();
}


function add_tab(id) {
	var tab = jQuery(jQuery('#btn_add_'+id).parent().attr('href'));
	var link =  jQuery('#btn_add_'+id).parent().parent();
	var modello = jQuery('span', link).html();
	
	if (modello==''){
		alert('Definire prima il modello.')
		return;
	}
	

    var num_tabs = jQuery("div#global_tabs ul li").length + 1;
    
    var content = jQuery("#tab_"+id).html();
    jQuery(link).parent().append("<li><a id='' href='#tab_" + num_tabs + "'>" +
    									"<input id='btn_add_"+num_tabs+"' type='button' onclick='add_tab("+num_tabs+")' class='btn_add' title='Copia scheda modello' />" +
    									"<input id='btn_delete_"+num_tabs+"' type='button' class='btn_delete' title='Elimina scheda modello' onclick='del_tab("+num_tabs+")'/>" +
    									"<span></span>" +
    								"</a></li>");
    jQuery(tab).parent().append("<div align='center' id='tab_" + num_tabs + "'>"+content+"</div>");
    
    
    jQuery("#global_tabs").tabs("refresh");
    var i = jQuery("div#global_tabs ul li:last").index();
    //console.log(i);
	jQuery('#global_tabs').tabs('option', 'active', i);  
	

}


function del_tab(num_tabs) {
	var btn = jQuery("#btn_delete_"+num_tabs);
	var tab = jQuery(btn).parent().attr('href');
	
	jQuery(tab).remove();
	jQuery(btn).parent().parent().remove();
	
	jQuery("#global_tabs").tabs("refresh");
	var i = jQuery("div#global_tabs ul li:last").index();
	jQuery('#global_tabs').tabs('option', 'active', i);
	
}


function set_modello() {
	var modello = jQuery('[id="param_modello_dt"]:visible').val();
	//jQuery('[id="param_modello_dt"]:visible').attr('value', modello);
	
	if (!check_modello(modello)) {
		jQuery('[id="param_modello_dt"]:visible').val('');
		alert("Modello gia' utilizzato.")
		return;
	}
	
	var tab = jQuery(jQuery('[id="param_modello_dt"]:visible').parents('div')[0]);
	var link = jQuery('a[href="#'+tab.attr('id')+'"]');
	
	//jQuery(tab).attr('id', 'tab_'+modello);
	//jQuery(link).attr('href', '#tab_'+modello);
	jQuery('span', link).html(modello);
	
}


function check_modello(modello) {
	var trovato = false;
	jQuery("div#global_tabs ul li span").each(function() {
		var nome = jQuery(this).html();
		if (modello==nome) {
			trovato = true;
		}
	});
	
	if (!trovato)
		return true;
	else
		return false;
}









