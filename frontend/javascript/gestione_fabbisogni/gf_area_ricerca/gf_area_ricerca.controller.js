(function(){
    'use strict';

    var controllerId = "GestFabbAreaRicercaController";

    ControllerFn.$inject = ['$scope', '$rootScope', '$filter', 'RicercaService', 'AutoCompleteGFService',
                            'LoggerFactory', 'SOCIETA', 'STATUS', 'STATO_DIBA', 'TIPO_FABB', 'CLASSI', 'TIPO_VIS', 'UI_ANNI_STAGIONE'];

    angular.module("app").controller(controllerId, ControllerFn)
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    function ControllerFn($scope, $rootScope, $filter, RicercaService, AutoCompleteGFService, LoggerFactory,
                            SOCIETA, STATUS, STATO_DIBA, TIPO_FABB, CLASSI, TIPO_VIS, UI_ANNI_STAGIONE) {

        var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);

        vm.initRicercaFields = init;
        vm.cercaClick = cerca;
        vm.CLASSI = [];
        vm.initRicercaFields();
        vm.areaRicercaPanelIsopen = true;

        vm.resetRicercaFields = function ()
		{
			vm.initRicercaFields();
			$rootScope.$emit("gestFabb:reset_modelli_data");
			$rootScope.$emit("gestFabb:reset_articoli_data");
			$rootScope.$emit("gestFabb:cambio_tipo_vista_event", vm.tipo_vista);

		};

        vm.tipoVistaChange = function ()
		{
			$rootScope.$emit("gestFabb:cambio_tipo_vista_event", vm.tipo_vista);
		};



		$scope.open1 = function ()
		{
			$scope.popup1.opened = true;
		};

		$scope.popup1 = {
			opened: false
		};

		$scope.open2 = function ()
		{
			$scope.popup2.opened = true;
		};

		$scope.popup2 = {
			opened: false
		};

        $scope.getCollezione = function(value){
			return AutoCompleteGFService.getCollezioni(value);
		};

		$scope.onSelectCollezione = function(item, value, label){
			vm.collezione = item.cmatset;
			vm.id_col = item.id_col;
		};

        $scope.getCollezioneOrig = function(value){
			return AutoCompleteGFService.getCollezioniOrig({'query' : value,
			                                                    'societa' : "",
			                                                    'anno_stagione': "",
			                                                    'id_articolo' : "",});
		};
		$scope.onSelectCollezioneOrig = function(item, value, label){
			vm.collezione_orig = item.descrizione;
		};

        $scope.apriStampaDiba = function(entity= ''){
		    $rootScope.$emit("gestFabb:stampaDiba");
        }

		$scope.apriStudioDiba = function(entity= ''){
		    $rootScope.$emit("gestFabb:apriDiba");
        }

        $scope.apriStudioPermanenti = function(entity= ''){
		    $rootScope.$emit("gestFabb:apriPermanenti");
        }

        function getArticoliList (articoli){
            var finalArtiList = []
            angular.forEach(articoli, function(art){
                if (finalArtiList.indexOf(art['text']) === -1){
                    finalArtiList.push(art['text']);
                }
            });
            return finalArtiList.toString();
        };

        function getFilter(){

            var ret = {
                id_richiesta: vm.id_richiesta,
                data_da: $filter('date')(vm.data_da, "yyyy-MM-dd"),
                data_a: $filter('date')(vm.data_a, "yyyy-MM-dd"),
                num_bp: vm.num_bp,
                anno_stagione: vm.anno_stagione,
                collezione_orig: vm.collezione_orig,
                collezione: vm.collezione,
                stato_diba: vm.stato_diba,
                stato: vm.stato,
                tipo_fabb: vm.tipo_fabb,
                societa: vm.societa,
                classe_orig: vm.classe_orig,
                modello: vm.modello,
                nome_modello: vm.nome_modello,
                variante: vm.variante,
                articolo: getArticoliList(vm.articolo),
                articolo_descr: vm.articolo_descr,
                capocmat_societa: vm.capocmat_societa,
                capocmat_gm: vm.capocmat_gm,
                capocmat_as: vm.capocmat_as,
                capocmat_articolo: vm.capocmat_articolo,
                capocmat_misura: vm.capocmat_misura,
                capocmat_colore: vm.capocmat_colore,
                fam_gm: vm.fam_gm,
                tipo_vista: vm.tipo_vista
            };
            return ret;

        }

        function cerca() {
            var params = getFilter();
            $rootScope.$emit("gestFabb:ricerca_"+vm.tipo_vista, params);
        }

        function init (){
            vm.ui_tipo_vis= TIPO_VIS;
            vm.tipo_vista = vm.ui_tipo_vis[0].value;
            vm.ui_societa = SOCIETA;
            vm.ui_stato_diba = STATO_DIBA;
            vm.ui_stato = STATUS;
            vm.ui_tipo_fabb = TIPO_FABB;
            vm.ui_anni_stagione = UI_ANNI_STAGIONE;
            vm.classe_orig = [];

            //Parametri di ricerca
            vm.id_richiesta = '';
            vm.data_da = '';
            vm.data_a = '';
            vm.num_bp = '';
            vm.anno_stagione = 'tutti';
            vm.collezione_orig = '';
            vm.collezione = '';
            vm.stato_diba = 'valutazione';
            vm.stato = 'tutti';
            vm.tipo_fabb = 'tutti';
            vm.societa = '';
            vm.modello = '';
            vm.nome_modello = '';
            vm.variante = '';
            vm.articolo = '';
            vm.articolo_descr = '';
            vm.capocmat_societa = '';
            vm.capocmat_gm = '';
            vm.capocmat_as = '';
            vm.capocmat_articolo = '';
            vm.capocmat_misura = '';
            vm.capocmat_colore = '';
            vm.fam_gm = '';
            $scope.classeOptions = CLASSI;

//            $scope.classeOptions = ServiceDatiClassi.datiClassi;
//            setTimeout(function(){ $scope.classeOptions = ServiceDatiClassi.datiClassi; console.log($scope.classeOptions) }, 1000);
        }

        $rootScope.$on("gestFabb:send_receiveClassi", vm.getClassi);
    }


})();