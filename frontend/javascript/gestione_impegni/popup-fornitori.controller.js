(function ()
{
	'use strict';

	var controllerId = "PopupFornitoriCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', '$uibModal',
	                        'AutoCompleteGFService', 'ServiceDatiVistaGiacenza',
	                        'ServiceDatiPopover', 'MACROCOLORE', 'uiGridConstants'];

	angular.module('app').controller(controllerId, ControllerFn);

	function ControllerFn($scope, $rootScope, LoggerFactory, $uibModal,
	                        AutoCompleteGFService, ServiceDatiVistaGiacenza,
	                        ServiceDatiPopover, MACROCOLORE, uiGridConstants)
	{

		var vm = this;

		$scope.closePopover = function(ev){
		    var targetButton = ServiceDatiPopover.clickedButton;
		    setTimeout(function(){
                $(targetButton).trigger('click');
            }, 100);
		};

        $scope.cambiaFornitore = function(row){
            $rootScope.$emit("vistaGiac:cambia_riga", {
                riga: row.entity,
                tableRowID: ServiceDatiVistaGiacenza.tableRowID,
                tableID: ServiceDatiVistaGiacenza.tableID,
                isFornitore: ServiceDatiVistaGiacenza.isFornitore,
                clickButton: ServiceDatiPopover.clickedButton,
            });
        };

        $scope.disable_condition_cambia_riga = function (row){
            if ([undefined,null,''].indexOf(row.entity.conto) >= 0  || [undefined,null,''].indexOf(row.entity.fornitore) >= 0  || [undefined,null,''].indexOf(row.entity.articolo_fornitore) >= 0){
                return true;
            }else{
                return false;
            }
        };

        $scope.getRowFornitore = function(row, value){
			return AutoCompleteGFService.getFornitore({'query' : value, });
		};

		$scope.onSelectRowFornitore = function(row, item, value, label){
		    row['conto'] = item.fornitore;
		    row['capoconto'] = item.capoconto;
			row['fornitore'] = item.rag_soc_forn;
		};

		$scope.getRowArticolo = function(row, value){
			return AutoCompleteGFService.getArticoloFornitore({'query' : value, });
		};

		$scope.onSelectRowArticolo = function(row, item, value, label){
		    row.articolo_fornitore = item.articolo_fornitore;
		};

    }
})();