# -*- coding: utf-8 -*-
import tools
import time
import copy
import cjson
import caio_print as cp
import os
import traceback
import itertools
import simplejson as json

from facon.diba.view import anagrafica_modelli_view, diba_view
from facon.diba.dao import dwg_dao
from facon.diba.dao import diba_dao
from facon.diba.facade import diba_facade

from Common import errors
from Common import decorators
import ws_client_mmfg

def main(pard):
	'''
	Funzione base che si occupa di disegnare la pagina di base
	'''
	try:
		pard['html'] = anagrafica_modelli_view.pagina_base(pard)
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard


@decorators.buildJsonResultSet	
def ricerca_modelli(pard):
#	res = [{'a.articolo': '347',
#             'a.dataora_aggiornamento': '2012-04-20 14:29:59',
#             'a.dataora_inserimento': '2009-05-19 11:29:48',
#             'a.dataora_modifica': '2012-04-19 14:40:07',
#             'a.societa': 'MM',
#             'a.utente_aggiornamento': 'dwh_mm',
#             'a.utente_inserimento': 'dwh_mm',
#             'a.utente_modifica': 'dwh_mm',
#             'anno': '2008',
#             'articolo': '347',
#             'classe': '01',
#             'collezione': 'MM',
#             'collezione_brand': None,
#             'dataora_aggiornamento': '2010-05-08 14:41:25',
#             'dataora_inserimento': '2010-04-24 15:08:44',
#             'dataora_modifica': '2013-04-10 11:45:39',
#             'desc_articolo': '',
#             'desc_editoriale': None,
#             'desc_tecnica': None,
#             'desc_tecnica_estesa': None,
#             'desc_uscita_collezione': 'MAXMARA 2^ USCITA',
#             'f_promozionale': '0',
#             'genere': None,
#             'gruppo_imposta': '01',
#             'gruppo_merceologico': '03',
#             'indice_prima_tg': '1',
#             'indice_ultima_tg': '8',
#             'modalita_produzione': 'C',
#             'modello': '10100081000',
#             'modello_base': '10100081',
#             'modello_origine': None,
#             'modulo': None,
#             'motivo_ricodifica': None,
#             'nome': '*A20001',
#             'nome_file_schizzo': '',
#             'num_comp_pacco': '1',
#             'num_pezzi': '1',
#             'peso': '0',
#             'peso_netto': '0',
#             'progr_articolo': '111',
#             'societa': 'MM',
#             'stagione': '1',
#             'tipo_imballo': 'AL',
#             'tipo_produzione': 'N',
#             'tipo_raggr_classe': '0',
#             'tipo_taglia': 'SPACE',
#             'uc.anno': '2008',
#             'uc.dataora_aggiornamento': '2012-11-06 14:50:35',
#             'uc.dataora_inserimento': '0000-00-00 00:00:00',
#             'uc.dataora_modifica': '2012-10-08 17:50:17',
#             'uc.societa': 'MM',
#             'uc.stagione': '1',
#             'uc.uscita_collezione': '11',
#             'uc.utente_aggiornamento': 'dwh_mm',
#             'uc.utente_inserimento': None,
#             'uc.utente_modifica': 'dwh_mm',
#             'unita_vendibile_origine': None,
#             'uscita_collezione': '11',
#             'utente_aggiornamento': 'dwh_mm',
#             'utente_inserimento': 'dwh_mm',
#             'utente_modifica': 'dwh_mm',
#             'validita': 'A',
#             'versione_modello': None}]
	
	dict_params = cjson.decode(pard.get('params_ricerca', {}))
	
	res = dwg_dao.ricerca_modello_originale(pard, dict_params)
	
	for r in res:
		r['collezione_descr'] = r['uscita_collezione']+ ' - '+r['desc_uscita_collezione']
		r['anno_stagione'] = r['anno']
		if r['stagione'] == '1':
			r['anno_stagione'] += ' P/E'
		else:
			r['anno_stagione'] += ' A/I'
		r['materiale'] = r['gruppo_merceologico']+'.'+r['progr_articolo']
	
	return res


@decorators.buildJsonResultSet	
def ricerca_gm(pard):
	soc = pard.get('societa', '')
	if soc:
		return diba_dao.get_gm_autocomplete(pard, pard.get('query',''), soc)
	else:
		return []


@decorators.buildJsonResultSet	
def ricerca_articolo(pard):
	soc = pard.get('societa', '')
	if soc:
		return diba_dao.get_articolo_autocomplete(pard, pard.get('query',''), soc)
	else:
		return []


@decorators.buildJsonResultSet	
def ricerca_collezione(pard):
	soc = pard.get('societa', '')
	anno_stagione = pard.get('anno_stagione', '')
	if soc:
		return diba_dao.get_collezione_autocomplete(pard, pard.get('query',''), soc, anno_stagione)
	else:
		return []
	
	
	
def render_scheda_modello(pard):
	try:
		dati_mod = dwg_dao.get_scheda_modello_base(pard, pard.get('societa', ''), pard.get('modello', ''))
		list_db = []
		for k in dati_mod.get('modelli_commerciali', []):
			for v in k.get('varianti', []):
				list_db.append({'societa': dati_mod['societa'], 'modello': k['modello'], 'variante': v['variante']})
		
		for l in list_db:
			db = diba_dao.get_db_modello_originale_ana(pard, l.get('societa', ''), l.get('modello', ''), l.get('variante', ''))
			l['db'] = db
		
		pard['html'] = anagrafica_modelli_view.get_scheda_modello(pard, dati_mod, db=list_db[0])
	except errors.DAOException, de:
		pard['html'] = tools.format_errore(de.value)
		return pard
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard


@decorators.buildSimpleJson
def get_link_img_modello(pard):
	result = []
	try:
		link_url = ws_client_mmfg.get_img_modello(pard, 'model', pard.get('societa',''), pard.get('modello',''))
		return {'result':'OK', 'message': link_url}
	except Exception, e:
		return {'result':'KO', 'message': traceback.format_exc()}


def render_importa_diba(pard):
	try:
		dati = {}
		diba_fac = diba_facade.DiBaFacade(pard)
		resd = diba_fac.get_modello_originale(pard.get('societa', ''), pard.get('modello', ''))
		if resd['errors']:
			raise Exception(resd['errors'])
		dati['dati_mod'] = resd['result']
		dati['db'] = diba_dao.get_db_modello_originale_ana(pard, pard.get('societa', ''), pard.get('modello', ''))
		if dati['dati_mod'] and dati['db']:
			pard['html'] = diba_view.pagina_base_db(pard, dati)
		else:
			pard['html'] = diba_view.pagina_base_db(pard)
	except errors.DAOException, de:
		pard['html'] = tools.format_errore(de.value)
		return pard
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard



#def render_db_modello_originale(pard):
#	try:
#		scheda_tecnica = dwg_dao.get_scheda_modello_base(pard, pard.get('societa', ''), pard.get('modello', ''))
#		res = diba_dao.get_db_modello_originale(pard, pard.get('societa', ''), pard.get('modello', ''))
#		if res:
#			pard['html'] = anagrafica_modelli_view.get_db_modello(pard, scheda_tecnica, res)
#	except Exception, e:
#		pard['html'] = tools.format_errore(traceback.format_exc())
#		return pard
#	return pard
