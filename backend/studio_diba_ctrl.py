import traceback

from Common import decorators
from facon.diba.doc import diba_doc
from facon.diba.doc import diba_doc_xls
from facon.ordpf.facade.pool_manager import Request
from facon.diba.view import studio_diba as views_studio_diba
from facon.diba.facade.diba_facade import DiBaFacade
from facon.diba.facade.fabbisogni_facade import FabbFacade

import tools, cjson

def vista_studio_diba(pard):
    """
    controller della pagina principale dei listini
    :param pard:
    :return:
    """
    try:
        pard['html'] = views_studio_diba.studio_diba(pard)
    except Exception, e:
        pard['html'] = tools.format_errore(traceback.format_exc())
        return pard
    return pard


@decorators.buildJsonResultSet
def ricerca(pard):
    facade = DiBaFacade(pard)
    dict_params = cjson.decode(Request().get_val('dati', "{}"))
    result = facade.ricerca_studio_diba(dict_params)
    return result


@decorators.buildJsonResultSet
def arretra_diba(pard):
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.arretra_diba(params)
	return result

@decorators.buildJsonResultSet
def avanza_diba(pard):
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.avanza_diba(params)
	return result

@decorators.buildJsonResultSet
def salva_diba(pard):
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.salva_diba(params)
	return result

@decorators.buildJsonResultSet
def estrai_diba(pard):
    facade = DiBaFacade(pard)
    dict_params = cjson.decode(Request().get_val('dati', "{}"))
    result = facade.ricerca_studio_diba_xls(dict_params)
    return diba_doc_xls.crea_xls_estrazione_diba(pard, result)

@decorators.buildJsonResultSet
def stampa_diba(pard):
    facade_diba = DiBaFacade(pard)
    facade_fabb = FabbFacade(pard)
    dict_params = cjson.decode(Request().get_val('dati', "{}"))
    dati = facade_diba.ricerca_stampa_studio_diba(dict_params)
    result = []
    for d in dati:
        query_modello = facade_fabb.get_dati_studio_diba_pdf(d)
        result.append(query_modello)
    return diba_doc.build_studio_diba_doc(Request().get_pard(), result)

def render_doc_xls(pard):
    try:
        doc = cjson.decode(pard.get('doc', {}))
        diz = {'path': doc['path'], 'name': doc['name']}
        diba_doc_xls.render_xls(pard, diz)
    except:
        pard['html'] = tools.format_errore(traceback.format_exc())
    finally:
        return pard