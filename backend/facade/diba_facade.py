import traceback
import itertools

import cjson

import db_access
import anno_stagione_lib
import pprint
from Common import business_object, errors
from facon.diba.dao import dwg_dao, diba_dao
from facon import ws_client_mmfg
from facon.ordpf.errors import ForwardException
from facon.ordpf.facade.pool_manager import Request, db_provided, ConnPool, CONN_KEY
import tools

# ---------------------------------------------------- INTERFACCIA ----------------------------------------------------
from facon.pao.dao import ordiniDB


class DiBaFacade(business_object.FacadeObject):
	
	def get_modello_originale(self, modello, societa):
		"""
		Restituisce i dati di massimo dettaglio del modello originale reperendoli dal DBG.

		:param societa: Societa del modello
		:param modello: Modello commerciale base (a 8)
		:return: Dizionario con i dati di massimo dettaglio del modello originale reperito dal DBG
		:rtype: {'result': dict, 'errors': ''}
		"""
		resd = {'result': '', 'errors': ''}
		try:
			ret_dict = dwg_dao.get_scheda_modello_base(self._pard, societa, modello)
			resd['result'] = ret_dict
		except errors.DAOException, e:
			resd['errors'] = e.value
			return resd
		except errors.BOException, e:
			resd['errors'] = e.value
			return resd
		except Exception, e:
			resd['errors'] = traceback.format_exc()
			return resd
		else:
			return resd

	def get_modello_originale_fast(self, modello, societa, variante=''):
		"""
		Restituisce i dati principali del modello originale reperendoli dal DBG.

		:param societa: Societa del modello
		:param modello: Modello commerciale base (a 8)
		:param variante: Variante del modello (non obbligatoria)
		:return: Dizionario con i dati principali del modello originale reperito dal DBG
		:rtype: {'result': dict, 'errors': ''}
		"""
		resd = {'result': '', 'errors': ''}
		try:
			ret_dict = dwg_dao.get_modello_variante_originale(self._pard, societa, modello, variante)
			if variante:
				art_dict = diba_dao.get_articolo_variante_by_id(self._pard, societa, ret_dict['articolo'], ret_dict['uscita_collezione'], ret_dict['colore'], variante)
				if art_dict:
					ret_dict.update(art_dict)
					resd['result'] = ret_dict
				else:
					resd['result'] = ret_dict
			else:
				resd['result'] = ret_dict
		except errors.DAOException, e:
			resd['errors'] = e.value
			return resd
		except errors.BOException, e:
			resd['errors'] = e.value
			return resd
		except Exception, e:
			resd['errors'] = traceback.format_exc()
			return resd
		else:
			return resd

	@db_provided(transactional=True)
	def get_modello_variante_fast(self, anno_stagione, societa, gm, articolo, variante = ''):
		resd = {'result': '', 'errors': ''}
		try:
			ret_dict = diba_dao.get_modello_variante(anno_stagione, societa, gm, articolo, variante)
			resd['result'] = ret_dict[0] if ret_dict else ''

		except errors.DAOException, e:
			resd['errors'] = e.value
			return resd
		except errors.BOException, e:
			resd['errors'] = e.value
			return resd
		except Exception, e:
			resd['errors'] = traceback.format_exc()
			return resd
		else:
			return resd

	def importa_diba_originali(self, dati={}):
		"""
		Importa i dati delle distinte base dei modelli presenti nelle richieste commerciali dal DBG
		nella tabella gestionale "impo_diba_esplosa".

		:param: dati: dict, default empty
		:return: OK/KO
		:rtype: {'result': 'OK/KO', 'errors': ''}
		"""
		resd = {'result': '', 'errors': []}
		self._conn = db_access.mysql_connect(self._pard)
		modelli_list = diba_dao.get_modelli_schede(self._pard, self._conn, dati)
		for mod in modelli_list:
			try:
				print 'Importazione: ', mod
				lista_mod = dwg_dao.get_lista_modelli_originali(self._pard, societa=mod['societa'], a_s=mod['anno_stagione'], modello=mod['modello'])
				lista_mod = sorted(lista_mod, key=lambda k: (k['societa'], k['modello_produzione']))
				for k, g in itertools.groupby(lista_mod, key=lambda k: (k['societa'], k['modello_produzione'])):
					varianti = list(g)
					print 'Get DIBA Base'
					diba_base = dwg_dao.get_diba_prodotto_originale(k[0], k[1])
					if diba_base[0]['data']:
						print 'OK'
					else:
						print 'KO'
					for v in varianti:
						print 'Get DIBA esplosa: %s - %s - %s' %(k[0], k[1], v['variante'])
						dwg_dao.elimina_diba_esplosa(self._pard, self._conn, k[0], k[1], v['variante'])
						diba_esplosa = dwg_dao.get_diba_esplosa_originale(k[0], k[1], v['variante'])
						if diba_esplosa[0]['data']:
							print 'OK'
						else:
							print 'KO'
						for diba in diba_esplosa[0]['data']:
							trovato = False
							for descr in diba_base[0]['data']:
								if diba['numero_riga'] == descr['numero_riga'] and descr['descrizione']:
									trovato = True
									diba['descrizione'] = descr['descrizione']
									diba['colonna'] = descr['progr_componente_coloritura']
									dwg_dao.inserisci_diba_esplosa(self._pard, self._conn, diba)
							if not trovato:
								diba['descrizione'] = diba_dao.get_articolo_descr(self._pard, self._conn, diba)
								diba['colonna'] = 0
								dwg_dao.inserisci_diba_esplosa(self._pard, self._conn, diba)
				diba_dao.update_import_modelli_schede(self._pard, self._conn, mod)
			except errors.DAOException, e:
				print 'Errore nel modello %s' %mod
				self._conn.rollback()
				resd['errors'].append(e.value)
			except Exception, e:
				print 'Errore nel modello %s' %mod
				self._conn.rollback()
				resd['errors'].append(traceback.format_exc())
				return resd
			else:
				self._conn.commit()

		self._conn.close()
		return resd
		
	
	def get_diba_originali(self, societa, modello, variante):
		"""
		Restituisce i dati della distinta base esplosa del modello variante fornito in ingresso
		reperendo i dati dal DBG.

		:param societa: Societa del modello
		:param modello: Modello commerciale base (a 8)
		:param variante: Variante del modello
		:return: Lista con i dati della distinta base esplosa (in tutte le taglie)
		:rtype: {'result': list, 'errors': ''}

		Example:

		Input:
					societa: 'MM'
					modello: '91060353'
					variante: '003'

		Output:		[
					{'anno': '0',
					 'consumo_unitario_lordo': '1',
					 'consumo_unitario_netto': '0',
					 'gruppo_merceologico': '51',
					 'materiale': '91060353000',
					 'materiale_componente': '460937',
					 'misura_prodotto': '50',
					 'misura_prodotto_componente': '000',
					 'numero_riga': '11259',
					 'prodotto': '91060353000',
					 'prodotto_componente': 'MM00.5100102',
					 'progr_articolo': '102',
					 'progr_pezzo': '01',
					 'societa': 'MM',
					 'stagione': '0',
					 'variante_prodotto': '003',
					 'variante_prodotto_componente': '000'},
					 ...

					]
		"""
		resd = {'result': '', 'errors': ''}
		try:
			diba_esplosa = dwg_dao.get_diba_esplosa_originale(societa, modello, variante)
			resd['result'] = diba_esplosa[0]['data']
		except errors.DAOException, e:
			resd['errors'] = e.value
			return resd
		except errors.BOException, e:
			resd['errors'] = e.value
			return resd
		except Exception, e:
			resd['errors'] = traceback.format_exc()
			return resd
		else:
			return resd
	
	def get_consumo_art_princ_originale(self, societa, modello):
		"""
		Restituisce i dati relativi al consumo diba dell'articolo principale del modello fornito in input.

		:param societa: Societa del modello
		:param modello: Modello commerciale base (a 8)
		:return: Dizionario con i dati di consumo dell'articolo principale del modello
		:rtype: {'result': dict, 'errors': ''}

		Example:

		Input:
					societa: 'MM'
					modello: '91060353'

		Output:    	{
					'societa': 'MM',
					'articolo': '12345',
					'consumo_lordo': 1.20,
					'consumo_netto': 1.00,
					'difettosita': 5.00
					}
		"""
		resd = {'result': {}, 'errors': ''}
		try:
			consumo_dict = {}
			ret_dict = dwg_dao.get_modello_variante_originale(self._pard, societa, modello)
			diba_esplosa = dwg_dao.get_diba_esplosa_originale(societa, ret_dict['modello_produzione'],
			                                                  ret_dict['variante'])
			for art_diba in diba_esplosa[0]['data']:
				if societa in ('MM', 'MA'):
					if str(art_diba['materiale_componente']) == str(ret_dict['articolo']) and str(
							art_diba['numero_riga']) == "1110":
						if str(art_diba['misura_prodotto']) in ('42', 'SM', 'M'):
							consumo_dict = {
								'societa': societa,
								'articolo': ret_dict['articolo'],
								'consumo_lordo': float(art_diba['consumo_unitario_lordo']),
								'consumo_netto': float(art_diba['consumo_unitario_lordo']),
								'difettosita': 0.00,
								'diba': diba_esplosa
							}
							if societa == 'MM':
								soc_orig = 'OM'
							elif societa == 'MA':
								soc_orig = 'OA'
							elif societa == 'MN':
								soc_orig = 'ON'
							else:
								soc_orig = societa
							art_orig_data = diba_dao.get_articolo_by_id(self._pard, ret_dict['articolo'], soc_orig)
							if art_orig_data and art_orig_data['difettosita']:
								consumo_dict['difettosita'] = float(art_orig_data['difettosita'])
								consumo_dict['consumo_netto'] = round(
									(1 - float(art_orig_data['difettosita']) / 100.00) * float(
										consumo_dict['consumo_lordo']), 2)
				elif societa == 'MN':
					if str(art_diba['materiale_componente']) == str(ret_dict['articolo']) and str(
							art_diba['numero_riga']) == "100":
						if str(art_diba['misura_prodotto']) in ('42', 'M'):
							consumo_dict = {
								'societa': societa,
								'articolo': ret_dict['articolo'],
								'consumo_lordo': float(art_diba['consumo_unitario_lordo']),
								'consumo_netto': float(art_diba['consumo_unitario_lordo']),
								'difettosita': 0.00,
								'diba': diba_esplosa
							}
			resd['result'] = consumo_dict

		except errors.DAOException, e:
			resd['errors'] = e.value
			return resd
		except errors.BOException, e:
			resd['errors'] = e.value
			return resd
		except Exception, e:
			resd['errors'] = traceback.format_exc()
			return resd
		else:
			return resd


	@db_provided(transactional=True)
	def ricerca_modelli(self, dict_data):
		rcomm = dwg_dao.ricerca_modelli_originali_by_tessuto(dict_data)
		for r in rcomm:
			r['modello_trascodifica'] = r['modello_orig']
			if r['societa_orig'] == 'MN':
				if r['modello_prodotto'][0] == '8' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '9' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'6'
				elif r['modello_prodotto'][0] == 'U' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '4' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'6'
				elif r['modello_prodotto'][0] == 'T' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '4' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'5'
				elif r['modello_prodotto'][0] == 'V' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '4' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'7'
				elif r['modello_prodotto'][0] == 'N' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '8' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'5'
				elif r['modello_prodotto'][0] == 'K' and r['modello_prodotto'][6] == '1':
					r['modello_orig'] = '2' + str(r['modello_prodotto'])[1:6] + str(r['modello_prodotto'])[7]+'8'
				else:
					last_num = ''
					res = diba_dao.get_impo_collezione_mn(r['societa_orig'], r['modello_prodotto'][0])
					if res:
						last_num = res[0]['ml'][0]
					r['modello_orig'] =  str(r['modello_prodotto'])[:6] + str(r['modello_prodotto'])[7] + last_num
			r['id_articolo'] = ''
			dati_art = diba_dao.get_dati_articolo_orig(r)
			if dati_art:
				r['id_articolo'] = dati_art[0]['id_articolo']
			r['variante_orig'] = r['variante_orig'].zfill(3)
			r['collezione_orig'] = r['ml'] + '-' + r['desc_collezione_orig']
			collezioni_orig = diba_dao.get_collezione_orig(r)
			if collezioni_orig:
				r['collezione_orig'] = collezioni_orig[0]['descrizione']
				r['ml'] = collezioni_orig[0]['ml']
			# if len(str(r['modello_orig'])) > 8 and len(r['ml']) < 3:
			# 	r['ml'] = str(r['modello_orig'][8]) + r['ml']
			# r['collezione_orig'] = r['ml'] + '-' + r['desc_collezione_orig']
			r['consumo'] = float(r['consumo']) if r['consumo'] else 0.00
			r['consumo'] = diba_dao.calcola_consumo_netto(Request().get_pard(), r['societa_orig'], r['id_articolo_orig'], r['consumo'])
			r['schizzo'] = ''
			if r['tipo_tessuto'] == 'T':
				r['schizzo'] = ws_client_mmfg.get_img_modello(Request().get_pard(), 'model', r['societa_orig'], r['modello_trascodifica'][:8], flag_schizzo=True)
		return rcomm



	@db_provided()
	def ricerca_varianti_orig_by_colore(self, params):
		params['colore'] = params.get('c_mat_col','')
		# caso degli articoli DT
		if params.get('articolo') != '' and params.get('gm') != '' and params.get('c_mat_col') != '':
			ricod_orig = diba_dao.get_ricodifiche_orig(params)
			if ricod_orig:
				params['colore'] = ricod_orig[0]['colore'][-3:]
			else:
				params['colore'] = ''
		res = dwg_dao.get_varianti_orig_by_colore(params['societa_orig'], params['as_orig'], params['modello_orig'], params['colore'])
		return res



	@db_provided()
	def ricerca_studio_diba(self, dict_data):
		filteredResult = {'tessuti':[], 'artModello': [], 'artPermanenti': []}
		matchTessuti = ["10"]
		matchArtModello = ["12","15","18","20","23","24","29","35","39","56","75"]
		matchArtPermanenti = ["16","17","25","30","33","50","51","62","84","91"]
		rcomm = diba_dao.ricerca_studio_diba(dict_data)
		for art in rcomm:
			art['id_ord'] = ''
			art['id_fac_d'] = ''
			if art.get('id_facord_dett', '') and art['id_facord_dett']:
				data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), art['id_facord_dett'])
				if data_utils:
					art['id_ord'] = data_utils['id_ordine']
					art['id_fac_d'] = data_utils['rif_intr']
					art['data_consegna'] = data_utils['data_cons_conc']
					art['fornitore_ord'] = data_utils['fornitore_ord']
			art['fornitore'] = ''
			art['articolo_fornitore'] = ''
			dati_forn = diba_dao.get_articoli_fornitore_orig(art)
			if dati_forn:
				art['fornitore'] = dati_forn[0]['fornitore']
				art['articolo_fornitore'] = dati_forn[0]['articolo_fornitore']
		rcomm_sorted = sorted(rcomm, key=lambda k: (k['societa'], k['fam_gm'], k['gm'], k['anno_stagione'], k['articolo']))
		for k, g in itertools.groupby(rcomm_sorted, key=lambda k: (k['societa'], k['fam_gm'], k['gm'], k['anno_stagione'], k['articolo'])):
			lista_gruppi = list(g)
			for gp in lista_gruppi:
				#gp['posizione'] = gp['posizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
				gp['posizione'] = gp['posizione'].decode('latin-1').encode('utf8')
				#gp['descrizione'] = gp['descrizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
				gp['descrizione'] = gp['descrizione'].decode('latin-1').encode('utf8')
			group = [{'societa': k[0], 'fam_gm': k[1], 'gm': k[2],'anno_stagione': k[3], 'articolo': k[4], 'descrizione':lista_gruppi[0]['descrizione'], '$$treeLevel': 0 }] + lista_gruppi
			if k[1] in matchTessuti and dict_data['tipo_studio'] == 'diba':
				filteredResult['tessuti'].append(group)
			elif k[1] in matchArtModello  and dict_data['tipo_studio'] == 'diba':
				filteredResult['artModello'].append(group)
			elif k[1] in matchArtPermanenti:
				filteredResult['artPermanenti'].append(group)

		return filteredResult

	@db_provided()
	def ricerca_studio_diba_xls(self, dict_data):
		filteredResult = {'tessuti':[], 'artModello': [], 'artPermanenti': []}
		matchTessuti = ["10"]
		matchArtModello = ["12","15","18","20","23","24","29","35","39","56","75"]
		matchArtPermanenti = ["16","17","25","30","33","50","51","62","84","91"]
		rcomm = diba_dao.ricerca_studio_diba(dict_data)
		for art in rcomm:
			art['id_ord'] = ''
			art['id_fac_d'] = ''
			if art.get('id_facord_dett', '') and art['id_facord_dett']:
				data_utils = ordiniDB.get_riga_ordine(Request().get_pard(), art['id_facord_dett'])
				if data_utils:
					art['id_ord'] = data_utils['id_ordine']
					art['id_fac_d'] = data_utils['rif_intr']
					art['data_consegna'] = data_utils['data_cons_conc']
					art['fornitore_ord'] = data_utils['fornitore_ord']
			art['fornitore'] = ''
			art['articolo_fornitore'] = ''
			dati_forn = diba_dao.get_articoli_fornitore_orig(art)
			if dati_forn:
				art['fornitore'] = dati_forn[0]['fornitore']
				art['articolo_fornitore'] = dati_forn[0]['articolo_fornitore']
		rcomm_sorted = sorted(rcomm, key=lambda k: (k['societa'], k['fam_gm'], k['gm'], k['anno_stagione'], k['articolo'], k['misura'], k['colore']))
		for k, g in itertools.groupby(rcomm_sorted, key=lambda k: (k['societa'], k['fam_gm'], k['gm'], k['anno_stagione'], k['articolo'])):
			lista_gruppi = list(g)
			group = {'societa': k[0], 'fam_gm': k[1], 'gm': k[2],'anno_stagione': k[3], 'articolo': k[4],
					'descrizione':lista_gruppi[0]['descrizione'], 'dettaglio_art': [], 'tot_capi': 0, 'fabbisogno': 0, 'impegni': 0}
			for kk, gg in itertools.groupby(lista_gruppi, key=lambda kk: (kk['misura'], kk['colore'])):
				lista_gruppi2 = list(gg)
				group2 = group.copy()
				group2['misura'] = kk[0]
				group2['colore'] = kk[1]
				group2['tot_capi'] = 0
				group2['fabbisogno'] = 0
				group2['impegni'] = 0
				for gp in lista_gruppi2:
					#gp['posizione'] = gp['posizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
					gp['posizione'] = gp['posizione'].decode('latin-1').encode('utf8')
					#gp['descrizione'] = gp['descrizione'].decode('latin1').replace(u'\xa0', u' ').encode('utf-8')
					gp['descrizione'] = gp['descrizione'].decode('latin-1').encode('utf8')
					group2['tot_capi'] += int(gp['tot_capi']) if gp['tot_capi'] else 0
					group2['fabbisogno'] += float(gp['fabbisogno']) if gp['fabbisogno'] else 0
					group2['impegni'] += float(gp['impegni']) if gp['impegni'] else 0
				group2['dettaglio_col'] = lista_gruppi2
				group['tot_capi'] += int(group2['tot_capi'])
				group['fabbisogno'] += float(group2['fabbisogno'])
				group['impegni'] += float(group2['impegni'])
				group['dettaglio_art'].append(group2)
			if k[1] in matchTessuti and dict_data['tipo_studio'] == 'diba':
				filteredResult['tessuti'].append(group)
			elif k[1] in matchArtModello  and dict_data['tipo_studio'] == 'diba':
				filteredResult['artModello'].append(group)
			elif k[1] in matchArtPermanenti:
				filteredResult['artPermanenti'].append(group)

		return filteredResult

	@db_provided()
	def ricerca_stampa_studio_diba(self, dict_data):
		result = []
		rcomm = diba_dao.ricerca_studio_diba(dict_data)
		rcomm_sorted = sorted(rcomm, key=lambda k: (
			k['societa'], k['modello'], k['nome_modello']))
		for k, g in itertools.groupby(rcomm_sorted, key=lambda k: (
				k['societa'], k['modello'], k['nome_modello'])):
			gruppo = list(g)
			result.append({'societa':gruppo[0]['societa'], 'modello_orig':gruppo[0]['modello'], 'nome_modello': gruppo[0]['nome_modello']})
		return result


	@db_provided()
	def get_modelli_autocomplete(self, soc='', param='', nome=''):
		res = diba_dao.get_modelli_autocomplete(soc, param, nome)
		for r in res:
			if r.get('societa', '') == '002':
				r['soc'] = 'MA'
			elif r.get('societa', '') == '006':
				r['soc'] = 'MM'
			elif r.get('societa', '') == '003':
				r['soc'] = 'MN'
			elif r.get('societa', '') == '013':
				r['soc'] = 'DT'
		return res

	@db_provided(transactional=True)
	def importa_capocmat_diba(self, dati):
		capocmat_list = diba_dao.get_articoli_diba_esplosa(dati)

		from facon.origana import importa_anagrafica
		from facon.origana.importa_anagrafica import societa_list
		pard = Request().get_pard()
		conn = ConnPool().get_connection(Request().get_val(CONN_KEY))

		for c in capocmat_list:
			c['societa_old'] = ''
			for s in societa_list:
				if s['soc_dt'] == c['societa']:
					c['societa_old'] = s['soc_orig']
			c['id_articolo_old'] = ''
			c['id_articolo_new'] = ''
			c['id_artforn_old'] = ''
			c['id_artforn_new'] = ''

			pard['batch_mode'] = True
			importa_anagrafica.importa_articolo(pard, c, conn)
		return True


	@db_provided()
	def get_diba_modello_originale_doc(self, dati):
		diba_esplosa = []
		try:
			dati_prod = dwg_dao.get_modelli_variante_prod(Request().get_pard(), dati['societa'], dati['modello'],
		                                              dati['variante'])
		except:
			raise ForwardException('Modello non trovato %(societa)s-%(modello)s-%(variante)s' % dati)
		else:
			ret = dwg_dao.get_diba_esplosa_originale(dati_prod[0]['societa'], dati_prod[0]['modello_produzione'],
			                                         dati_prod[0]['variante'], debug=False)
			res = ret[0]['data']

			for diz in res:
				if str(diz['misura_prodotto']) in ('42', 'SM', 'M', 'None'):
					diz['nome'] = dati_prod[0]['nome']
					diz['societa'] = dati['societa']
					diz['articolo'] = diz['progr_articolo']
					if diz['anno'] != '0' and diz['stagione'] != '0':
						if diz['stagione'] == '1':
							stagione = 'P/E'
						else:
							stagione = 'A/I'
						diz['anno_stagione'] = "%s %s" %(diz['anno'], stagione)
						diz['a_s'] = anno_stagione_lib.decodifica_as(diz['anno_stagione'])
					else:
						diz['anno_stagione'] = anno_stagione_lib.decode_as('PP')
						diz['a_s'] = 'PP'

					diz['gm'] = diz['gruppo_merceologico']
					diz['pezzo'] = diz['progr_pezzo']
					diz['colonna'] = diz.get('progr_componente_coloritura', '')
					diz['posizione'] = diz['posizione'] if diz['posizione'] else ''
					diz['consumo_netto'] = diz['consumo_unitario_lordo']
					if (diz['societa'] in ('MM', 'MA') and diz['numero_riga'] == '1110'):
						diz['consumo_netto'] = diba_dao.calcola_consumo_netto(Request().get_pard(), diz['societa'], diz['materiale_componente'], diz['consumo_unitario_lordo'])
					diz['id_articolo'] = ''
					diz['descrizione'] = ''
					dati_art_orig = diba_dao.get_dati_articolo_orig(diz)
					if dati_art_orig:
						diz['id_articolo'] = dati_art_orig[0]['id_articolo']
						diz['descrizione'] = dati_art_orig[0]['descrizione']

					for k, d in diz.items():
						diz[k] = (d, '')[d == None or d == 'None']
					diba_esplosa.append(diz)


		return diba_esplosa

	@db_provided()
	def get_ricodifiche_dt(self):

		resd = {'result': '', 'errors': ''}

		try:
			import anno_stagione_lib
			dati = cjson.decode(Request().get_val('dati', "{}"))
			anno_stagione_2_cifre = anno_stagione_lib.decodifica_as(dati.get('anno_stagione'),
																	tipo='due_cifre')  # 2012 A/I
			dati['anno_stagione'] = anno_stagione_2_cifre
			ricod_list = diba_dao.get_ricodifiche_dt(dati)
			finalListResult = []
			for r in ricod_list:
				finalListResult.append('%s %s %s %s' % (
				r.get('societa', ''), r.get('gm', ''), r.get('anno_stagione', ''), r.get('articolo', '')))
			resd['result'] = finalListResult
		except errors.DAOException, e:
			resd['errors'] = e.value
		except errors.BOException, e:
			resd['errors'] = e.value
		except Exception, e:
			resd['errors'] = traceback.format_exc()
		finally:
			return resd

	@db_provided()
	def get_ricodifiche_orig(self):

		resd = {'result': '', 'errors': ''}

		try:
			dati = cjson.decode(Request().get_val('dati', "{}"))
			ricod_list = diba_dao.get_ricodifiche_orig(dati)
			finalListResult = []
			for r in ricod_list:
				finalListResult.append('%s %s %s' % (
					r.get('societa', ''), r.get('gm', ''), r.get('articolo', '')))
			resd['result'] = finalListResult
		except errors.DAOException, e:
			resd['errors'] = e.value
		except errors.BOException, e:
			resd['errors'] = e.value
		except Exception, e:
			resd['errors'] = traceback.format_exc()
		finally:
			return resd



