(function(){
    'use strict';

    var controllerId = "GestFabbVistaArticoloController";

    ControllerFn.$inject = ['$scope', '$rootScope', 'RicercaService', 'LoggerFactory', '$timeout', 'TIPO_VIS', 'i18nService'];

    angular.module("app").controller(controllerId, ControllerFn)
    .directive('ngActiveScroll', function () {
        return {
            link: function($scope, element, attr) {
              $scope.$watch('isDisplayed', function(newValue, oldValue) {
                var viewport = element.find('.ui-grid-render-container');

                ['touchstart', 'touchmove', 'touchend','keydown', 'wheel', 'mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'].forEach(function (eventName) {
                  viewport.unbind(eventName);
                });
              });
            },
        };
    });

    function ControllerFn($scope, $rootScope, RicercaService, LoggerFactory, $timeout, TIPO_VIS, i18nService) {

        var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);

        //variabile che setta la lingua delle descrizioni in tabella
        vm.lang = 'it';

        vm.ui_tipo_vis= TIPO_VIS;
        $scope.mySubSelections = [];

        function subGridApiRegister(gridApi){
            // register the child API in the parent - can't tell why it's not in the core...
            var parentRow = gridApi.grid.appScope.row;
            parentRow.subGridApi = gridApi;
            $timeout(function(){
              if (angular.isUndefined(parentRow.isSelected)) return;
              angular.forEach(gridApi.grid.rows, function(row){
                // if tagged as selected, select it
                if(parentRow.isSelected[row.entity.isSelected]){
                  gridApi.selection.toggleRowSelection(row.entity);
                }
              });

            });
            // subGrid selection method
            gridApi.selection.on.rowSelectionChanged(gridApi.grid.appScope, function(row){
                $scope.mySubSelections = gridApi.selection.getSelectedRows();
                if (angular.isUndefined(parentRow.isSelected)){
                    parentRow.isSelected = {};
                }
                row.entity['isSelected'] = row.isSelected;
                parentRow.isSelected = $(row.grid.rows).filter( function(index, item){ // just use arr
                    return item['isSelected'] === true;
                }).length > 0;
            });
        }

        $scope.grigliaArticoli = {
            showGridFooter: true,
			enableColumnMenus: false,
			enableExpandableRowHeader: false,
			enableRowHeaderSelection: true,
			enableRowSelection: true,
            enableSelectAll: true,
			headerHeight: 35,
			rowHeight: 35,
            expandableRowTemplate: '<div ui-grid="row.entity.subgrigliaArticoli" ng-style="grid.appScope.getSubTableHeight(row)" ng-active-scroll ui-grid-selection ui-grid-auto-resize ui-grid-resize-columns></div>',
			expandableRowScope: {
                apriGestioneImpegni : function(row, from){
                    var id_dibas_dett = (from == 'dettaglio') ? row.entity.id_dibas_dett : getSelectedSubArticoli(row);
                    var sid = jQuery('#sid').val();
                    var app_server = jQuery('#app_server').val();
                    var menu_id = jQuery('#menu_id').val();
                    var module = 'facon/diba.impegni_ctrl';
                    var program = 'vista_gestione_impegni';
                    var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_dibas=" + id_dibas_dett + "&sid=" + sid;
                    window.open(url);
                }

            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApi = gridApi;
                //custom Template per bottone espandi sottogriglia
                var cellTemplate = "<div style= \"height:32px \" "+
                "class=\"ui-grid-row-header-cell ui-grid-expandable-buttons-cell\">"+
                "<div class=\"ui-grid-cell-contents\">"+
                "<i ng-class=\"{ 'ui-grid-icon-plus-squared' : !row.isExpanded && row.entity.dettaglio.length !== 0,"+
                "'ui-grid-icon-minus-squared' : row.isExpanded, 'ng-grid-cell': row.entity.subgrigliaArticoli.data.length == 0 }\" "+
                "ng-click=\"grid.api.expandable.toggleRowExpansion(row.entity)\"></i></div></div>"
                //aggiungo colonna custom Template alle colonne
                $scope.gridApi.core.addRowHeaderColumn( { name: 'rowHeaderCol',
                                                        displayName: '',
                                                        width: 35,
                                                        cellTemplate: cellTemplate} );


                gridApi.selection.on.rowSelectionChanged($scope,function(row){

                    var selectedState = row.isSelected;
                    // if row is expanded, toggle its children as selected
                    if (row.isExpanded){
                        // choose the right callback according to row status
                        var selectCallBack = selectedState?"selectAllRows":"clearSelectedRows";
                        // do the selection/unselection of children
                        row.subGridApi.selection[selectCallBack]();
                    }

                    angular.forEach(row.entity.subgrigliaArticoli.data, function(value){
                        // create the "isSelected" property if not exists
                        if (angular.isUndefined(row.isSelected)){
                          row.isSelected = {};
                        }
                        // keep the selected rows values in the parent row - idealy would be a unique ID coming from the server
                        value.isSelected = selectedState;
                    });
                });


			}
		};

        function row_class(grid, row, col, rowRenderIndex, colRenderIndex) {
          if (row.entity.collezione_facon === 'OUTLET PROG') {
            return 'giallo_outlet_prog';
          }
          return '';
        }

        var columnDefs_innerTable = [
			{displayName: 'Capocmat', field: 'capocmat'},
			{displayName: 'Mis', field: 'misura'},
			{displayName: 'Col', field: 'colore'},
			{displayName: 'Modello', field: 'modello_orig'},
			{displayName: 'Variante', field: 'variante'},
			{displayName: 'Nome Modello', field: 'nome_modello'},
			{displayName: 'Tot. Capi', field: 'tot_capi'},
			{displayName: 'Tot. Capi lanciati ', field: 'capi_lanciati'},
			{displayName: 'Studio', field: 'rif_studio'},
			{displayName: 'Qta Studio', field: 'qta_prev'},
			{displayName: 'Rich. Comm.', field: 'rif_rich'},
			{displayName: 'Qta Rich.', field: 'qta_teor'},
			{displayName: 'BP', field: 'rif_bp', width: '160', cellTemplate:'<div ng-repeat="item in row.entity[col.field]">{{item}}</div>'},
			{displayName: 'Qta BP', field: 'qta_reale'},
			{displayName: 'Qta FABB.', field: 'fabbisogno'},
			{displayName: 'Imp', field: 'impegno'},
			{displayName: 'Da Sodd.', field: 'da_soddisfare'},
			{displayName: 'Azioni', field: 'nota_lotto', width: '60', cellStyle: {color: 'red', backgroundColor: 'green'},
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Gestione Impegni" tooltip-placement="left" ng-click="grid.appScope.apriGestioneImpegni(row, \'dettaglio\')"'+
                        'class="btn btn-xs btn-success stretto">'+
                        '<i class="glyphicon glyphicon-log-out"></i>'+
                    '</button>'+
                '</div>'}
		];

        var COLONNE_CUSTOM = [
            {displayName: 'Capocmat', field: 'capocmat', cellClass: row_class},
			{displayName: 'Descrizione', field: 'descrizione', cellClass: row_class},
			{displayName: 'Qta Capi', field: 'tot_capi', cellClass: row_class},
			{displayName: 'Qta Capi lanciati', field: 'capi_lanciati', cellClass: row_class},
			{displayName: 'Qta Studio', field: 'qta_prev', cellClass: row_class},
			{displayName: 'Qta Rich', field: 'qta_teor', cellClass: row_class},
			{displayName: 'Qta BP', field: 'qta_reale', cellClass: row_class},
			{displayName: 'Qta FABB', field: 'fabbisogno', cellClass: row_class},
			{displayName: 'Impegno', field: 'impegno', cellClass: row_class},
			{displayName: 'Qta Da soddisfare', field: 'da_soddisfare', cellClass: row_class},
			{displayName: 'Azioni', name: 'azioni',
                enableHiding: false, enableCellEdit: false,
                cellClass: row_class,
                cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Gestione Impegni" tooltip-placement="left" ng-click="grid.appScope.apriGestioneImpegni(row, \'principale\')"'+
                        'class="btn btn-xs btn-success stretto">'+
                        '<i class="glyphicon glyphicon-log-out"></i>'+
                    '</button>'+
                '</div>'
            }
		]

		$scope.grigliaArticoli.columnDefs = COLONNE_CUSTOM;
		$scope.grigliaArticoli.data = [];

        vm.aggiorna_dati_articolo = function(dati_articolo){
            for(var i = 0; i < dati_articolo.length; i++){
                dati_articolo[i].subgrigliaArticoli = {
                    enableRowSelection : true,
                    enableColumnMenus: false,
                    enableSelectAll: true,
                    columnDefs: columnDefs_innerTable,
                    data: dati_articolo[i].dettaglio,
                    disableRowExpandable : dati_articolo[i].dettaglio === 0,
                    onRegisterApi: subGridApiRegister
                }
            }
            $scope.grigliaArticoli.data = dati_articolo;
        };

        vm.RisultatoRicerca = function(event, dati){
            RicercaService.ricerca_fabbisogni(dati, function(data){
                if (data.length > 0){
                    $('.articolo-view .fixed-table-container').css({'max-height': '600px'});
                }else{
                    $('.articolo-view .fixed-table-container').css({'max-height': 'auto'});
                }
                vm.aggiorna_dati_articolo(data);
            }, function(){
                $scope.grigliaArticoli.data = [];
            });
        };

        function getSelectedSubArticoli(row){

            var articoli_selezionati = jQuery.extend(true, [], row.entity.dettaglio.slice());
            var list_articoli_selezionati = [];
            if (articoli_selezionati !== undefined &&
                articoli_selezionati.length > 0){
                    list_articoli_selezionati = [];
                    $(articoli_selezionati).each(function(index, item){
                        if ([list_articoli_selezionati].indexOf(item) === -1){
                            if (item['isSelected'] && $scope.mySubSelections.length > 0){
                            list_articoli_selezionati.push(item.id_dibas_dett);
                            }else if([list_articoli_selezionati].indexOf(item) === -1 && $scope.mySubSelections.length === 0){
                                list_articoli_selezionati.push(item.id_dibas_dett);
                            }
                        }
                    });
            }else
                logger.error('Nessun elemento selezionato');
            return list_articoli_selezionati;
        };

        $scope.apriGestioneImpegni = function(row, from){
            var id_dibas_dett = (from == 'dettaglio') ? row.entity.id_dibas : getSelectedSubArticoli(row);
            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/diba.impegni_ctrl';
            var program = 'vista_gestione_impegni';
            var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_dibas=" + id_dibas_dett + "&sid=" + sid;
            if (id_dibas_dett.length > 0)
                window.open(url);
        }

        function getExpandableRowHeight(expRowHeight){
            if(expRowHeight < 50)
                return 50;
            else if(expRowHeight > 50 && expRowHeight < 500)
                return expRowHeight;
            else if(expRowHeight > 500)
                return 500;
        };

        $scope.getSubTableHeight = function(row) {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           var heightSubTable = (row.entity.subgrigliaArticoli.data.length * rowHeight + 2 * headerHeight)
           row.grid.options.expandableRowHeight = heightSubTable;//getExpandableRowHeight(heightSubTable);
           row.expandedRowHeight = heightSubTable;//getExpandableRowHeight(heightSubTable);
           return {
              height: heightSubTable + "px",
//              'min-height': 50 +'px',
//              'max-height': 500 +'px'
           };
        };

        $rootScope.$on("gestFabb:ricerca_articolo", vm.RisultatoRicerca);
        $rootScope.$on("gestFabb:reset_articoli_data", function(){$scope.grigliaArticoli.data = []});
    }


})();