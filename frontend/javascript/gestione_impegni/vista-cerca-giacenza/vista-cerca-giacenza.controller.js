(function ()
{
	'use strict';

	var controllerId = "DialogCercaGiacenzaCtrl";

	DialogCercaGiacenzaCtrl.$inject = ['$rootScope', '$scope', '$uibModalInstance', 'LoggerFactory', '$timeout','$filter', 'entity',
	        'SOCIETA', 'UI_ANNI_STAGIONE', 'UI_ANNI_STAGIONE_ID', 'MACROCOLORE', '$uibModal', '$location', 'uiGridConstants',
            'RicercaService', 'ServiceDatiVistaGiacenza','ServiceDatiPopoverColoreFornitore', 'AutoCompleteGFService' ];

	angular.module('app').controller(controllerId, DialogCercaGiacenzaCtrl)
	.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });
    angular.module('app').service('ServiceDatiPopoverColoreFornitore', function() {
      this.clickedButton = null ;
      this.tableRowID = null ;
      this.tableID = null ;
      this.clickedRow = {} ;
    });

	function DialogCercaGiacenzaCtrl($rootScope, $scope, $uibModalInstance, LoggerFactory, $timeout, $filter, entity,
	        SOCIETA, UI_ANNI_STAGIONE, UI_ANNI_STAGIONE_ID, MACROCOLORE, $uibModal, $location, uiGridConstants,
	        RicercaService, ServiceDatiVistaGiacenza, ServiceDatiPopoverColoreFornitore, AutoCompleteGFService)
	{
		var vm = this;
		var logger = LoggerFactory.getLogger(controllerId);
        $scope.entity = entity;

		$scope.closeModal = function ()
		{
			$uibModalInstance.dismiss('cancel');
		};

        function _init_fields(){
            //valori
            vm.societa = entity.riga.societa != undefined ? entity.riga.societa : "";
            vm.anno_stagione = entity.riga.anno_stagione != undefined ? entity.riga.anno_stagione : "tutti";
            vm.fam_gm = ServiceDatiVistaGiacenza.diba.fam_gm != undefined ? ServiceDatiVistaGiacenza.diba.fam_gm : "";
            vm.gm = entity.riga.gm != undefined ? entity.riga.gm : "";
            vm.articolo = entity.riga.articolo != undefined ? entity.riga.articolo : "";
            vm.misura = entity.riga.misura != undefined ? entity.riga.misura : "";
            vm.colore = entity.riga.colore != undefined ? entity.riga.colore : "";
            vm.macrocolore = entity.riga.macrocolore != undefined ? entity.riga.macrocolore : "";
            vm.cmat = entity.riga.CMAT != undefined ? entity.riga.CMAT : "";
            vm.articolo_fornitore = entity.riga.articolo_fornitore != undefined ? entity.riga.articolo_fornitore : "";
            vm.fornitore = "";
            vm.desc_colore_orig_col_forn = "";
            vm.flag_giacenza_dt = "";
            vm.flag_giacenza_soc = "";
            //campi
            vm.ui_societa=SOCIETA;
            vm.ui_anni_stagione = UI_ANNI_STAGIONE;
            vm.ui_macrocolore = MACROCOLORE;
        }

        function _reset_fields(){
            //valori
            vm.societa = "";
            vm.anno_stagione = "tutti";
            vm.fam_gm = ServiceDatiVistaGiacenza.diba.fam_gm;
            vm.gm = "";
            vm.articolo = "";
            vm.misura = "";
            vm.colore = "";
            vm.macrocolore = "";
            vm.cmat = "";
            vm.articolo_fornitore = "";
            vm.fornitore = "";
            vm.desc_colore_orig_col_forn = "";
            vm.flag_giacenza_dt = "";
            vm.flag_giacenza_soc = "";
            $scope.grigliaGiacenze.data = [];
        }

        function getFilter(){
            return {
                societa: vm.societa,
                anno_stagione: vm.anno_stagione,
                fam_gm: vm.fam_gm,
                gm: vm.gm,
                articolo: vm.articolo,
                misura: vm.misura,
                colore: vm.colore,
                macrocolore: vm.macrocolore,
                cmat: vm.cmat,
                articolo_fornitore: vm.articolo_fornitore,
                fornitore: vm.fornitore,
                desc_colore_orig_col_forn: vm.desc_colore_orig_col_forn,
                flag_giacenza_dt: vm.flag_giacenza_dt,
                flag_giacenza_soc: vm.flag_giacenza_soc
            }
        }

        function init(){
            vm._init_fields();
        }

        $scope.scegliArticolo = function(row){
            $rootScope.$emit("vistaGiac:scegli_articolo", {
                                riga: row,
                                tableRowID: ServiceDatiVistaGiacenza.tableRowID,
                                tableID: ServiceDatiVistaGiacenza.tableID,
                            }
            )
            $scope.closeModal();
        };

        var CUSTOM_COLUMNS = [
			{displayName: 'Società', field: 'societa',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Gm', field: 'gm',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'AS', field: 'anno_stagione',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Articolo', field: 'articolo',
                enableHiding: false,
                enableCellEdit: false,
            },
			{displayName: 'Mis.', field: 'misura',
			    enableHiding: false,
			    enableCellEdit: false,
			},
            {displayName: 'Colore', field: 'colore',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Art. Forn.', field: 'articolo_fornitore',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Col. Forn.', field: 'col_forn_desc_ragsoc',
                width: '15%',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: '', field: 'col_forn_desc_ragsoc',
                width: '3%',
                enableHiding: false,
                enableCellEdit: false,
                cellTemplate:'<div class="ui-grid-cell-contents" align="center">'+
                    '<script type="text/ng-template" id="popoverColFornitoreTemplate.html">'+
                        '<div class="form-group" ng-controller="PopoverColFornitoriCtrl as popOvColCtrl">'+
                            '<button class="btn btn-xs btn-danger button-popover-close" ng-click="closePopover($event)">X</button>'+
                            '<div id="gridPopoverFornitori" ui-grid="grid.appScope.grigliaPopoverColFornitore" style="height:260px;"'+
                                'ui-grid-auto-resize ui-grid-resize-columns ui-grid-cellnav'+
                                'class="MyGrid">'+
                            '</div>'+
                        '</div>'+
                    '</script>'+
                    '<button id="scegliColFornitore" uib-tooltip="Colore fornitore" tooltip-placement="left" ng-disabled="row.entity.list_col_forn.length <= 1" ng-click="grid.appScope.scegliColFornitore($event, row)" style="text-overflow: ellipsis;"'+
                        'popover-class="popover-fornitore" popover-placement="{{grid.appScope.popoverColFornitore.placement}}" uib-popover-template="grid.appScope.popoverColFornitore.templateUrl"'+
                        'popover-title="{{grid.appScope.popoverColFornitore.title}}" popover-append-to-body='+true+' type="button" class="btn btn-xs btn-default">'+
                        '<i class="fa fa-search-plus"></i>'+
                    '</button>'+
                '</div>'
            },
            {displayName: 'Giac DT', field: 'GIAC_DT',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Giac Società', field: 'GIAC_SOC',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Qta Ordini', field: 'GIAC_ORD',
                enableHiding: false,
                enableCellEdit: false,
            },
            {displayName: 'Qta Impegno', field: 'IMP',
                enableHiding: false,
                enableCellEdit: false,
            },
			{displayName: 'Azioni', name: 'azioni',
                enableHiding: false, enableCellEdit: false,
                cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Cambia" tooltip-placement="left" ng-click="grid.appScope.scegliArticolo(row.entity)"'+
                    'class="btn btn-xs btn-primary stretto">'+
                    '<i class="fa fa-sign-out"></i></button>'+
                '</div>'

            }
		];

		$scope.popoverColFornitore = {
            content: '',
            templateUrl: 'popoverColFornitoreTemplate.html',
            title: 'Scegli Colore Fornitore',
            placement: 'bottom auto',
        };

		$scope.scegliColFornitore = function($event, row){
            $scope.grigliaPopoverColFornitore.data = row.entity.list_col_forn;
            ServiceDatiPopoverColoreFornitore.clickedButton = $event.currentTarget;
            ServiceDatiPopoverColoreFornitore.tableRowID = row.entity.$$hashKey;
            ServiceDatiPopoverColoreFornitore.tableID = row.grid.options.tableNumID;
            ServiceDatiPopoverColoreFornitore.clickedRow = row.entity;
        };

        var COL_LIST_COL_FORNITORE = [
			{displayName: 'Fornitore', width:'18%', field: 'ragione_sociale_col_forn', enableCellEdit: false},
			{displayName: 'Art. Forn.', width:'18%', field: 'articolo_fornitore_col_forn', enableCellEdit: false},
			{displayName: 'Col. Orig.', width:'18%', field: 'colore_orig_col_forn', enableCellEdit: false},
			{displayName: 'Descr. col. orig', width:'18%', field: 'desc_colore_orig_col_forn', enableCellEdit: false},
            {displayName: 'Descr. col. forn', width:'18%', field: 'desc_colore_forn_col_forn', enableCellEdit: false},
            {displayName: 'Azioni', name: 'azioni',
                width:'10%',
                enableHiding: false, enableCellEdit: false,
                cellTemplate:'<div class="ui-grid-cell-contents" align="center">'+
                '<button uib-tooltip="Cambia Colore" tooltip-placement="left" ng-click="grid.appScope.cambiaColoreFornitore(row.entity)"'+
                    'class="btn btn-xs btn-primary stretto">'+
                    '<i class="fa fa-sign-out"></i></button>'+
                '</div>'
            }
		];

        $scope.grigliaPopoverColFornitore = {
            showGridFooter: false,
			enableColumnMenus: false,
			enableRowHeaderSelection: false,
			columnDefs: COL_LIST_COL_FORNITORE,
			data: [],
		};

        $scope.grigliaGiacenze = {
            headerHeight: 30,
			rowHeight: 30,
            enableRowHeaderSelection: false,
			enableSelectAll: false,
            enableGridMenu: false,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 2,
            showGridFooter:true,
            columnDefs: CUSTOM_COLUMNS,
			data: [],

			onRegisterApi: function (gridApi)
			{
				$scope.gridApi = gridApi;
			}
		};


        $scope.ricerca_giacenze = function(event, dati){
            var params = getFilter();
            //Check campi obbligatori
            if(params &&
            (params['societa'] != '' && params['anno_stagione'] != '' && params['gm'] != '')
            ||([params['societa'],params['articolo']].indexOf('') < 0)){
                logger.debug(params);
                RicercaService.ricerca_giacenza_da_impegni(params, function(data){
                    console.log(data);
                    $scope.grigliaGiacenze.data = data;

                }, function(){
                    $scope.grigliaGiacenze.data = [];
                });
            }else{
                logger.error('Inserire campi obbligatori: societa + anno stagione + gm oppure societa + articolo.')
            }
        };

        vm.init = init;
        vm._init_fields = _init_fields;
        vm._reset_fields = _reset_fields;
        vm.init();

        vm.cambiaColFornitore = function(event, dati){
            var newTableData = $scope.grigliaGiacenze.data;
            angular.forEach(newTableData, function(item, key) {
                //Se trovo riga di uguale id sostituisco
                if (item.$$hashKey === dati.tableRowID){
                    item['colore'] = dati.riga_col_forn.colore_orig_col_forn || '';
                    item['colore_descr'] = dati.riga_col_forn.desc_colore_orig_col_forn || '';
                    item['macrocolore'] = dati.riga_col_forn.macrocolore_col_forn || '';
                    item['capocmat'] = item['capocmat'].slice(0,9) + item['misura'] + item['colore'];
                    item['col_forn_dict'] = dati.riga_col_forn;
                    item['col_forn_desc_ragsoc'] = dati.riga_col_forn.desc_colore_orig_col_forn + ' - '+ dati.riga_col_forn.ragione_sociale_col_forn
                    console.log(item);
                }
            });
        };

        $scope.getColFornitore = function(value){
			return AutoCompleteGFService.getFornitore({'query' : value, });
		};

		$scope.onSelectColFornitore = function(row, item, value, label){
		    vm.fornitore = item.fornitore;
//			var objToSend = {'fornitore':item.fornitore, 'ragione_sociale':item.rag_soc_forn}
//			$rootScope.$emit("popoverColFornitore:autocomplete_fornitore", objToSend);
		};

        $rootScope.$on("popoverColFornitoreRicercaGiacenza:cambiaColFornitore", vm.cambiaColFornitore);
        //console.log($scope.entity);

    }
})();