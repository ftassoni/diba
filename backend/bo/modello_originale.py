import tools
import time
import pprint
import traceback
import caio_print as cp
import copy
import itertools
import os
import datetime

from Common import business_object, errors, utilities


from facon.diba.dao import dwg_dao


class ModelloOriginale(business_object.BusinessObject):
	
	def __init__(self, pard, data):
		super(ModelloOriginale, self).__init__(pard, data)
		self.societa = str(data.get('societa', ''))
		self.modello = str(data.get('modello', ''))
		self.nome = str(data.get('nome', ''))
		self.modello_base = str(data.get('modello_base', ''))
		self.anno = str(data.get('anno',''))
		self.stagione = str(data.get('stagione',''))
		self.uscita_collezione = str(data.get('uscita_collezione',''))
		self.classe = str(data.get('classe',''))
		self.articolo = str(data.get('articolo',''))
		self.num_pezzi = int(data.get('num_pezzi','0'))
		self.indice_prima_tg = int(data.get('indice_prima_tg','0'))
		self.indice_ultima_tg = int(data.get('indice_ultima_tg','0'))
		self.gruppo_imposta = str(data.get('gruppo_imposta', ''))
		self.modalita_produzione = str(data.get('modalita_produzione', ''))
		self.tipo_imballo = str(data.get('tipo_imballo', ''))
		self.validita = str(data.get('validita', ''))
	
	@staticmethod
	def get_by_key(pard, societa, modello):
		dict_modello = dwg_dao.get_modello_originale(pard, societa, modello)
		if not dict_modello:
			return None
		else:
			return ModelloOriginale(pard, dict_modello[0])		