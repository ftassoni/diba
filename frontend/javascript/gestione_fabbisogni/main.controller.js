(function ()
{
	'use strict';

	var controllerId = "GestioneFabbisogniCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', 'MODELLO', 'ARTICOLO', 'RicercaService'];

	angular.module('app').controller(controllerId, ControllerFn);
//    angular.module('app').service('ServiceDatiClassi', function() {
//      this.datiClassi = [] ;
//    });
	function ControllerFn($scope, $rootScope, LoggerFactory, MODELLO, ARTICOLO, RicercaService)
	{

		var vm = this;

        vm.TIPO_VISTA__MODELLO = MODELLO.value;
		vm.TIPO_VISTA__ARTICOLO = ARTICOLO.value;

		vm.tipoVista = vm.TIPO_VISTA__MODELLO;
		var logger = LoggerFactory.getLogger(controllerId);


		vm.cambioTipoVista = function(event, tipoVista){

			vm.tipoVista = tipoVista;

		};

//        $scope.getClassiRicerca = function(){
//            RicercaService.getClassi( function(response){
//                ServiceDatiClassi.datiClassi = response;
////                vm.classeOptions = response;
//                }, function(){
//                    logger.error(response.data.message);
//                    vm.CLASSI =[];
//                });
//
//
//        };

//        $scope.getClassiRicerca();

        vm.getClassi = function(event, receivedClassi){
            return receivedClassi;
        };



		$rootScope.$on("gestFabb:cambio_tipo_vista_event", vm.cambioTipoVista);


	}
})();