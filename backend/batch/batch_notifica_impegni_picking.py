import sys
# sys.path.insert(0, '/Users/elisafusari/dt-home/re/mp/bin')
# sys.path.insert(1, '/Users/elisafusari/dt-home/re/mp/bin/workflow_mmfg/')
sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/re/mp/bin/workflow_mmfg/')

from Common import constants, logger

import tools
import datetime
import traceback
import time
import notifica_mail
import pprint
from facon.diba.facade import fabbisogni_facade

DESTINATARI_MAIL = ['dtsupport@otconsulting.com']

def main(pard):
	try:
		tools.batch_logging('Avvio esecuzione...')
		fabfac = fabbisogni_facade.FabbFacade(pard)
		fabfac.notifica_impegni_picking()

	except Exception, e:
		tools.batch_logging(traceback.format_exc())
		if pard['AMBIENTE'] != 'svil':
			body = "Messaggio dal server: %(IP_ADDRESS)s <br><br> " % pard
			body += "Errore nel batch di notifica impegni (batch_notifica_impegni_picking.py) <br><br> "
			body += traceback.format_exc() + "<br><br>"
			body += "Consultare il log in /home/re/mp/log/batch_notifica_impegni_picking.log"
			subject = "[BATCH] Errore: Notifica impegni"
			notifica_mail.invia('debug@diffusionetessile.it', DESTINATARI_MAIL,
			                    subject=subject, corpo=body, rispondi_a=[], tipo='html', pre=False)
	finally:
		tools.batch_logging('Fine esecuzione...')





if __name__ == "__main__" :
	import config
	pard = config.configura_batch_crontab(user='CRON')
	main(pard)