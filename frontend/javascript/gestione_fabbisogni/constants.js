(function(){

    'use strict';


	var SOCIETA = [
		{name: "Tutti", id: ""},
		{name: "Max Mara", id: "MM"},
		{name: "Marella", id: "MA"},
		{name: "Manifatture del Nord", id: "MN"}
	];

    var MODELLO = {
		'name': 'Per Modello',
		'value': 'modello'
	};

	var ARTICOLO = {
		'name': 'Per Articolo',
		'value': 'articolo'
	};

    var TIPO_VIS = [
	    MODELLO,
		ARTICOLO
	];

	var UI_ANNI_STAGIONE = [{id:"tutti", name:"Tutti"}];
    var ui_anni_stagione_n = [{id:"tutti", name:"Tutti"}];

    for (var i=2012; i <= new Date().getFullYear()+1; i++ ) {

        var ai_item = {
            id: i + " A/I",
            name: i + " A/I"
        };

        var pe_item = {
            id: i + " P/E",
            name: i + " P/E"
        };

        ui_anni_stagione_n.push(ai_item);
        ui_anni_stagione_n.push(pe_item);
        UI_ANNI_STAGIONE.push(ai_item);
        UI_ANNI_STAGIONE.push(pe_item);

    }

    var CLASSI = [];
    for (var i=1; i <= 99; i++ ) {
        if (i>0 && i<10)
            CLASSI.push('0'+i)
        else
            CLASSI.push(i.toString())
    }

    var STATUS = [
        {'name':'Tutti', 'value':'tutti'},
        {'name':'Da soddisfare', 'value':'da_soddisfare'},
        {'name':'Completi', 'value':'completato'}
    ];

     var STATO_DIBA = [
        {'name':'Tutti', 'value':'tutti'},
        {'name':'Valutati', 'value':'valutazione'},
        {'name':'Bozza', 'value':'bozza'}
    ];

     var TIPO_FABB = [
        {'name':'Tutti', 'value':'tutti'},
        {'name':'Reale', 'value':'reale'},
        {'name':'Teorico', 'value':'teorico'},
        {'name':'Previsionale', 'value':'previsionale'}
    ];

	angular.module('app')
		.constant('SOCIETA', SOCIETA)
		.constant('MODELLO', MODELLO)
		.constant('ARTICOLO', ARTICOLO)
		.constant('TIPO_VIS', TIPO_VIS)
		.constant('UI_ANNI_STAGIONE', UI_ANNI_STAGIONE)
		.constant('STATUS', STATUS)
		.constant('STATO_DIBA', STATO_DIBA)
		.constant('TIPO_FABB', TIPO_FABB)
		.constant('CLASSI', CLASSI)
	;


})();