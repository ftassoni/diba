(function(){

    'use strict';


	var SOCIETA = [
		{name: "", id: ""},
		{name: "MM", id: "MM"},
		{name: "MA", id: "MA"},
		{name: "DT", id: "DT"},
		{name: "MN", id: "MN"}
	];

	var AVANZAMENTO = [
		{name: "Tutti", id: ""},
		{name: "Ordinato", id: "ordinato"},
		{name: "Da ordinare", id: "da_ordinare"},
		{name: "Impegnato", id: "impegnato"},
		{name: "Da impegnare", id: "da_impegnare"}
	];

	var MACROCOLORE = [
	    {name: "TUTTI", id: ""},
		{name: "ARANCIO", id: "1"},
		{name: "ARGENTO", id: "2"},
		{name: "BEIGE/CAMMELLO", id: "4"},
		{name: "BIANCO", id: "5"},
		{name: "GIALLO", id: "6"},
		{name: "GRIGIO", id: "7"},
		{name: "MARRONE", id: "8"},
		{name: "NERO", id: "9"},
		{name: "ORO", id: "10"},
		{name: "ROSA", id: "11"},
		{name: "ROSSO", id: "12"},
		{name: "VERDE", id: "13"},
		{name: "VIOLA", id: "14"},
		{name: "AZZURRO/BLU", id: "3"},
	];

	var CLASSI = [];
    for (var i=1; i <= 99; i++ ) {
        if (i>0 && i<10)
            CLASSI.push('0'+i)
        else
            CLASSI.push(i.toString())
    }

	angular.module('app')
		.constant('SOCIETA', SOCIETA)
		.constant('MACROCOLORE', MACROCOLORE)
        .constant('AVANZAMENTO', AVANZAMENTO)
        .constant('CLASSI', CLASSI)
	;


})();