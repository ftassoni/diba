import sys
sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/re/mp/bin/workflow_mmfg/')

import config
import unittest
import pprint
from facon.diba.facade.diba_facade import DiBaFacade


class TestDiBaFacade(unittest.TestCase):

	def setUp(self):
		self._pard = config.configura_batch_crontab(user='CRON')
		self._db_fac = DiBaFacade(self._pard)

	def test_get_modello_originale(self):
		resd = self._db_fac.get_modello_originale('91060353', 'MM')
		self.assertTrue(isinstance(resd, dict), msg='Output non in formato standard')
		self.assertEqual(resd['errors'],'', msg='Errore durante la chiamata %s' %resd['errors'])
		self.assertTrue(isinstance(resd['result'], dict), msg='Output non in formato dict')

	def test_get_modello_originale_fast(self):
		resd = self._db_fac.get_modello_originale_fast('91060353', 'MM')
		self.assertTrue(isinstance(resd, dict), msg='Output non in formato standard')
		self.assertEqual(resd['errors'],'', msg='Errore durante la chiamata %s' %resd['errors'])
		self.assertTrue(isinstance(resd['result'], dict), msg='Output non in formato dict')

	def test_get_diba_originali(self):
		EXC_KEYS = ['anno',
					'consumo_unitario_lordo',
					'consumo_unitario_netto',
					'gruppo_merceologico',
					'materiale',
					'materiale_componente',
					'misura_prodotto',
					'misura_prodotto_componente',
					'numero_riga',
					'prodotto',
					'progr_articolo',
					'progr_pezzo',
					'societa',
					'stagione',
					'variante_prodotto',
					'variante_prodotto_componente'
		]
		resd = self._db_fac.get_diba_originali('MM', '91060353', '003')
		self.assertTrue(isinstance(resd, dict), msg='Output non in formato standard')
		self.assertEqual(resd['errors'],'', msg='Errore durante la chiamata %s' %resd['errors'])
		self.assertTrue(isinstance(resd['result'], list), msg='Output non in formato list')
		if resd['result']:
			self.assertTrue(set(resd['result'][0].keys()) == set(EXC_KEYS), msg='Chiavi in output non corrette %s' %EXC_KEYS)

	def test_get_consumo_art_princ_originale(self):
		EXC_KEYS = ['societa',
					'articolo',
					'consumo_lordo',
					'consumo_netto',
					'difettosita'
		]
		resd = self._db_fac.get_consumo_art_princ_originale('MN', '70440550')
		self.assertTrue(isinstance(resd, dict), msg='Output non in formato standard')
		self.assertEqual(resd['errors'],'', msg='Errore durante la chiamata %s' %resd['errors'])
		self.assertTrue(isinstance(resd['result'], dict), msg='Output non in formato dict')
		if resd['result']:
			self.assertTrue(set(resd['result'].keys()) == set(EXC_KEYS), msg='Chiavi in output non corrette %s' %EXC_KEYS)


if __name__ == "__main__" :
	if len(sys.argv) > 1 and sys.argv[1] == 'get_modello_originale_fast':
		pard = config.configura_batch_crontab(user='fabio')
		db_fac = DiBaFacade(pard)
		if len(sys.argv) == 4:
			resd = db_fac.get_modello_originale_fast(sys.argv[2], sys.argv[3])
		else:
			resd = db_fac.get_modello_originale_fast(sys.argv[2], sys.argv[3], sys.argv[4])
		pprint.pprint(resd)
	elif len(sys.argv) > 1 and sys.argv[1] == 'get_consumo_art_princ_originale':
		pard = config.configura_batch_crontab(user='fabio')
		db_fac = DiBaFacade(pard)
		resd = db_fac.get_consumo_art_princ_originale(sys.argv[2], sys.argv[3])
		pprint.pprint(resd)
	else:
		unittest.main()