(function(){
    'use strict';

    var controllerId = "AreaRisultatiController";

    ControllerFn.$inject = ['$scope', '$rootScope', '$window', '$location', '$timeout', '$templateCache',
                            'DTHttp', 'RicercaService', 'SalvaService',
                            'LoggerFactory', 'GetDatiStudioService', 'uiGridConstants',
                            'i18nService', 'ServiceTipoStudio','uiGridGroupingConstants',
                            'uiGridTreeBaseConstants', 'MACROCOLORE'];
    angular.module("app").controller(controllerId, ControllerFn);

    function ControllerFn($scope, $rootScope, $window, $location, $timeout, $templateCache,
                            DTHttp, RicercaService, SalvaService,
                            LoggerFactory, GetDatiStudioService, uiGridConstants,
                            i18nService, ServiceTipoStudio,uiGridGroupingConstants,
                            uiGridTreeBaseConstants, MACROCOLORE, $ngMaterial) {

        var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);

        $templateCache.put('ui-grid/selectionRowHeader',
            "<div ng-if='!row.entity.hasOwnProperty(\"$$treeLevel\")'"+
                 "class=\"ui-grid-disable-selection\">"+
                 "<div class=\"ui-grid-cell-contents\">"+
                    "<ui-grid-selection-row-header-buttons></ui-grid-selection-row-header-buttons>"+
                "</div>"+
            "</div>"+
            "<div ng-if='row.entity.hasOwnProperty(\"$$treeLevel\")'"+
                 "<div class=\"ui-grid-cell-contents\">"+
                    "<i class=\"fa fa-check\" aria-hidden=\"true\""+
                        "ng-class=\"{'select-all-articles-selected': !row.spuntaSelected}\" "+
                        "ng-click=\"row.spuntaSelected = !row.spuntaSelected; grid.appScope.selectAllChildren(row)\" >"+
                    "</i>"+
                "</div>"+
            "</div>"
        );

        $scope.dataTabelle = {tessuti: [], artModello: [], artPermanenti: []};
        $scope.allSelectedRows ={};
        vm.filterValue = '';
        $scope.selectedIndex = 0;
        $scope.selectedIndexAfterActions = 0;
        $scope.tipoStudio = ServiceTipoStudio.tipoStudio;

        //variabile che setta la lingua delle descrizioni in tabella
        vm.lang = 'it';

        function row_class(grid, row, col, rowRenderIndex, colRenderIndex) {
            var classe = 'grigio_default';
            if(row.entity.$$treeLevel != undefined && row.entity.$$treeLevel === 0){
                classe = 'bianco';
            }
            return classe;
        }

        function aggregateFabb( aggregation, row ) { aggregation.rendered = aggregation.value; };

        function rowAggregation( aggregation, fieldValue, numValue, row ) {
            if ( typeof(aggregation.value) === 'undefined') {
                aggregation.value = 0;
              }
            if(row.visible){
                aggregation.value = aggregation.value + parseInt(row.entity.tot_capi);
            }

        };

        function editCondRow ($scope) {return $scope.row.entity.flag_variante === '1' || false}

        var COL_MOSTRA_GIACENZA = [
			{displayName: 'Art', field: 'articolo'},
			{displayName: 'Giac.Soc.', field: 'GIAC_SOC'},
			{displayName: 'Giac.Dt.', field: 'GIAC_DT'},
			{displayName: 'Ord.Mp.', field: 'GIAC_ORD'},
			{displayName: 'Imp', field: 'impegnati'},
			{displayName: 'Fabb', field: 'fabbisogno'}

		];

        var COLONNE_CUSTOM = [
            {displayName: 'Soc.', field: 'societa', width: '3%',
                            enableCellEdit: false,
//                            footerCellTemplate: '<div class="ui-grid-cell-contents">Totali: {{grid.rows.length}}</div>',
                            cellClass: row_class,
                            treeAggregationType: uiGridGroupingConstants.aggregation.COUNT,
//                            footerCellTemplate: '<div class="ui-grid-cell-contents">Totali: {{col.getAggregationValue()}}</div>',
                            treeAggregationLabel: 'Tot:',
                            cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.societa">{{row.entity.societa}}</div>'

            },
            {displayName: 'Anno Stagione', field: 'anno_stagione',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '4%',
//			                treeAggregationType: uiGridGroupingConstants.aggregation.MAX,
//                            cellTemplate: '<div class="ui-grid-cell-contents" > <div fakemax value="{{COL_FIELD}}" /></div>'
            },
			{displayName: 'Gm', field: 'gm',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '3%',
//			                treeAggregationType: uiGridGroupingConstants.aggregation.MAX,
//                            cellTemplate: '<div class="ui-grid-cell-contents" > <div fakemax value="{{COL_FIELD}}" /></div>'
			},
			{displayName: 'Art.', field: 'articolo',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '3%',
//			                grouping: { groupPriority: 0 },
//                            sort: { priority: 0, direction: 'desc'
//                            },
			},
			{displayName: 'Mis.', field: 'misura',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '3%',
//			                treeAggregationType: uiGridGroupingConstants.aggregation.MAX,
//                            cellTemplate: '<div class="ui-grid-cell-contents" > <div fakemax value="{{COL_FIELD}}" /></div>'
			},
			{displayName: 'Col', field: 'colore', category:'generic',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '3%',
			                cellTemplate:
			                '<div class="ui-grid-cell-contents" ng-model="row.entity.colore" ng-if="row.entity.flag_variante === \'0\'">{{row.entity.colore}}</div>'+
                            '<div class="ui-grid-cell-contents" ng-if="row.entity.flag_variante === \'1\'">'+
			                    '<script type="text/ng-template" id="customTemplateColori.html">'+
                                '<a>'+
                                    '<span ng-bind-html="match.model.col_capocmat | uibTypeaheadHighlight:query"></span> '+
                                    '<span ng-bind-html="match.model.col_descrizione"></span>'+
                                '</a>'+
                                '</script>'+
                                '<input class="form-control" type="text" ng-model="row.entity.colore"'+
                                    'uib-typeahead="colore as colore.col_capocmat for colore in grid.appScope.getColore(row, $viewValue)" '+
                                           'typeahead-loading="loadingColori" typeahead-min-length="1" typeahead-wait-ms="300" '+
                                           'typeahead-on-select="grid.appScope.onSelectColore(row, $item, $model, $label)" '+
                                           'typeahead-template-url="customTemplateColori.html" />'+
                                   '<i ng-show="loadingColori" class="fa fa-refresh"></i> '+
                            '</div>'
            },
			{displayName: 'Colore-Descr', field: 'colore_descrizione',
			                enableCellEdit: false,
			                cellClass: row_class,
			},
            {displayName: 'Macrocol', field: 'macrocolore', category:'generic',
                            cellClass: row_class,
			                editableCellTemplate: 'ui-grid/dropdownEditor',
			                editDropdownIdLabel: 'id',
                            editDropdownValueLabel: 'name',
                            editDropdownOptionsArray: MACROCOLORE,
                            cellEditableCondition: editCondRow,
                            visible: false

			},
			{displayName: 'Descr', field: 'descrizione',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '8%'
			},
			{displayName: 'Modello', field: 'modello',
			                enableCellEdit: false,
			                cellClass: row_class
			},
			{displayName: 'Modello Trasc.', field: 'modello_trascodifica',
			                enableCellEdit: false,
			                cellClass: row_class,
			                visible: false
			},
			{displayName: 'Variante', field: 'variante',
			                enableCellEdit: false,
			                cellClass: row_class,
			},
			{displayName: 'Nome', field: 'nome_modello',
			                enableCellEdit: false,
			                cellClass: row_class,
			},
			{displayName: 'Collez. Orig.', field: 'collezione_orig',
			                enableCellEdit: false,
			                cellClass: row_class,
			                visible: false
			},
			{displayName: 'Cons.', field: 'consumo',
			                enableCellEdit: false,
			                cellClass: row_class,
			                width: '3%',
			                cellTemplate:
			                '<div class="ui-grid-cell-contents" ng-model="row.entity.consumo" ng-if="row.entity.consumo_dettaglio === \'\'">{{row.entity.consumo}}</div>'+
                            '<div class="ui-grid-cell-contents" ng-if="row.entity.consumo_dettaglio !== \'\'">'+
                                 '<input class="form-control" type="text" value="{{row.entity.consumo}}" ng-model="row.entity.consumo" ng-hide="grid.appScope.checkButtonVisibility(row)">'+
                            '</div>'
			},
			{displayName: 'Descr/Posiz.', field: 'posizione',
			                enableCellEdit: false,
			                cellClass: row_class,
			},
			{displayName: 'Capi', field: 'tot_capi', name: 'tot_capi',
			                enableCellEdit: false,
			                width: '3%',
			                cellTemplate:'<div class="ui-grid-cell-contents">'+
			                '{{row.entity.$$treeLevel != undefined && row.entity.$$treeLevel == 0 ? row.entity.header_tot_capi : row.entity.tot_capi }}'+
			                '</div>',
                            cellClass: row_class,
			                treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
//			                customTreeAggregationFn : rowAggregation,
			                customTreeAggregationFinalizerFn: aggregateFabb,
//                            aggregationType: uiGridConstants.aggregationTypes.sum,
			                footerCellTemplate: '<div class="ui-grid-cell-contents">Tot:{{col.getAggregationValue()}}</div>',

			},
			{displayName: 'Fornitore Cons', field: 'fornitore', cellClass: row_class, enableCellEdit: false,
			},
			{displayName: 'Art Forn', field: 'articolo_fornitore', cellClass: row_class, enableCellEdit: false,},
			{displayName: 'FABB', field: 'fabbisogno', cellClass: row_class,
			                enableCellEdit: false,
			                width: '3%',
			                cellTemplate:'<div class="ui-grid-cell-contents">'+
			                '{{row.entity.$$treeLevel != undefined && row.entity.$$treeLevel == 0 ? row.entity.header_fabbisogno : row.entity.fabbisogno }}'+
			                '</div>',
			                customTreeAggregationFinalizerFn: aggregateFabb,
			                treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
//                            aggregationType: uiGridConstants.aggregationTypes.sum,
			                footerCellTemplate: '<div class="ui-grid-cell-contents">Tot:{{col.getAggregationValue() | number:2 }}</div>',
			},
			{displayName: 'IMP', field: 'impegni', cellClass: row_class,
			                enableCellEdit: false,
			                width: '3%',
			                cellTemplate:'<div class="ui-grid-cell-contents">'+
			                '{{row.entity.$$treeLevel != undefined && row.entity.$$treeLevel == 0 ? row.entity.header_impegni : row.entity.impegni }}'+
			                '</div>',
			                customTreeAggregationFinalizerFn: aggregateFabb,
			                treeAggregationType: uiGridGroupingConstants.aggregation.SUM,
//                            aggregationType: uiGridConstants.aggregationTypes.sum,
			                footerCellTemplate: '<div class="ui-grid-cell-contents">Tot:{{col.getAggregationValue() | number:2 }}</div>',
			},
			{displayName: 'Stato', field: 'stato',
                cellClass: row_class,
                enableCellEdit: false,
                width: '4%',

			},
			{displayName: 'Note', field: 'nota', cellClass: row_class, enableCellEdit: false,
                            cellTemplate: '<div class="ui-grid-cell-contents">'+
                                    '<input class="form-control" type="text" value="{{row.entity.nota}}" ng-model="row.entity.nota" ng-hide="grid.appScope.checkButtonVisibility(row)">'+
                                '</div>'



			},
			{displayName: 'Rif.Ord.', field: 'id_fac_d', enableHiding: false,
                width: '8%',
                category:'col_riford',
                enableCellEdit: false,
                cellClass: row_class,
                cellTemplate:
                '<div class="ui-grid-cell-contents" tooltip-placement="left" ng-attr-uib-tooltip="{{!row.entity.hasOwnProperty(\'$$treeLevel\') && row.entity.id_fac_d !== \'\' ? \'Modifica Ordine\' : \'\'}}"'+
                    'ng-click="!row.entity.hasOwnProperty(\'$$treeLevel\') && row.entity.id_fac_d !== \'\' ? grid.appScope.apriModificaOrdine(row.entity.id_ord) : \'\'"'+
                    'ng-model="row.entity.id_fac_d" ng-class="(!row.entity.hasOwnProperty(\'$$treeLevel\') && row.entity.id_fac_d !== \'\') ? \'riford-pointer\' : \'riford-normal\'">'+
                    '<a ng-show="!row.entity.hasOwnProperty(\'$$treeLevel\') && row.entity.id_fac_d !== \'\'">{{row.entity.id_fac_d | limitTo:2:4}}/{{row.entity.id_fac_d | limitTo:2:2}}/{{row.entity.id_fac_d | limitTo:2:0}} - {{row.entity.id_fac_d | limitTo:6:6}}</a>'+
                '</div>'
            },
			{displayName: 'Fornitore', field: 'fornitore_ord', cellClass: row_class, enableCellEdit: false,
            },
			{displayName: 'Azioni', field: 'actions', width: '60', cellClass: row_class, enableCellEdit: false,
				cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center">' +
			    '<script type="text/ng-template" id="myPopoverTemplate.html">'+
                    '<div class="form-group">'+
//                                  '<label>Popup Title:</label>'+
                      '<div id="gridMostraGiacenza" ui-grid="grid.appScope.grigliaMostraGiacenza" style="height:70px;"'+
                      'ui-grid-auto-resize ui-grid-resize-columns ui-grid-cellnav'+
                      'class="MyGrid"></div>'+
                    '</div>'+
                '</script>'+
                '<button id="showGiacenza" uib-tooltip="Mostra Giacenza" tooltip-placement="left" ng-hide="grid.appScope.checkButtonVisibility(row)" ng-click="grid.appScope.mostraGiacenza($event, row)"'+
                    'popover-trigger="outsideClick" popover-placement="{{grid.appScope.dynamicPopover.placement}}" uib-popover-template="grid.appScope.dynamicPopover.templateUrl"'+
                    'popover-title="{{grid.appScope.dynamicPopover.title}}" popover-append-to-body='+false+' type="button" class="btn btn-xs btn-warning">'+
                    '<i class="fa fa-arrow-circle-down"></i>'+
                '</button>'+
                '<button uib-tooltip="Salva" ng-hide="grid.appScope.checkButtonVisibility(row)" ng-click="grid.appScope.salvaRigaDiba(row.entity, grid.options.tabName)"'+
			    'tooltip-placement="left" class="btn btn-primary my-grid-btn btn-xs"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>' +
				'<button uib-tooltip="Gestione Impegni"  ng-show="grid.appScope.checkButtonVisibility(row)" tooltip-placement="left" ng-click="grid.appScope.apriGestioneImpegni(row)"'+
                    'class="btn btn-xs btn-success stretto">'+
                    '<i class="glyphicon glyphicon-log-out"></i>'+
                '</button>'+
				'</div>'


            }
		];

		$scope.checkButtonVisibility = function(row){
            var isHidden = false;
            if(row.entity.$$treeLevel != undefined && row.entity.$$treeLevel === 0){
              isHidden = true;
            }
            return isHidden;

		};

		$scope.selectAllChildren = function(row, value){
            if (row.treeNode.children != undefined && row.treeNode.children.length > 0){
                var selectionState= row.spuntaSelected;
                angular.forEach(row.treeNode.children, function(item, key) {
                    item.row.isSelected = selectionState;
                });
            }
        };

        $scope.getColore = function(row, value){
                return GetDatiStudioService.getColori({
                    'query' : value,
                    'societa' : angular.copy(row.entity.societa),
                    'articolo' : angular.copy(row.entity.articolo),
                    'gm' : angular.copy(row.entity.gm),
//                   'anno_stagione' : angular.copy(row.entity.anno_stagione),
                    'a_s' : angular.copy(row.entity.capocmat.slice(4,6)),
                    'macrocolore': angular.copy(row.entity.macrocolore),
                });
        };

        $scope.onSelectColore = function(row, item, value, label){
            row.entity['colore'] = item.col_capocmat;
            row.entity['colore_descrizione'] = item.col_descrizione;

		};

        $scope.dynamicPopover = {
            content: '',
            templateUrl: 'myPopoverTemplate.html',
//            title: 'Mostra Giacenza',
            placement: 'left',
        };

        $scope.grigliaMostraGiacenza = {
            showGridFooter: false,
			enableColumnMenus: false,
			enableRowHeaderSelection: false,
			columnDefs: COL_MOSTRA_GIACENZA,
		};

        $scope.grigliaMostraGiacenza.columnDefs = COL_MOSTRA_GIACENZA;

        $scope.grigliaTessuti = {
//            enableFiltering: true,
//            treeRowHeaderAlwaysVisible: false,
            tabName: 'grigliaTessuti',
            apiID: 'gridApiTess',
            showTreeExpandNoChildren: false,
            enableGridMenu: true,
            enableColumnMenus: false,
            enableSorting: false,
//            showGridFooter: true,
            showColumnFooter: true,
//            expandableRowTemplate: '<div ui-grid="row.entity.subgrigliatessuti" ng-style="grid.appScope.getSubTableHeight(row)" ng-show="row.entity.dettaglio.length > 0" ui-grid-selection ui-grid-edit ui-grid-auto-resize ui-grid-resize-columns></div>',
			headerHeight: 35,
			rowHeight: 35,
			enableExpandableRowHeader: false,
//			loader: true,
            isRowSelectable: function(row){
                return !row.entity.hasOwnProperty('$$treeLevel');
            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApiTess = gridApi;
                $scope.gridApiTess.grid.registerRowsProcessor( $scope.singleFilter, 200 );

                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    var selectedState = row.isSelected;
                    if (!selectedState){
                        row.treeNode.parentRow.spuntaSelected = false;
                    }
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                    if (this.grid.rows != undefined && this.grid.rows.length > 0){
                        var selectionState= $scope.gridApiTess.selection.getSelectAllState();
                        angular.forEach(this.grid.rows, function(item, key) {
                            if (item.hasOwnProperty('treeLevel')) {
                                item.spuntaSelected = !selectionState;
                            }
                        });
                    }
                });
            }
		};

		$scope.grigliaArtModello = {
//            enableFiltering: true,
//            treeRowHeaderAlwaysVisible: false,
            tabName: 'grigliaArtModello',
            apiID: 'gridApiMod',
            showTreeExpandNoChildren: false,
            enableGridMenu: true,
            enableColumnMenus: false,
            enableSorting: false,
//            showGridFooter: true,
            showColumnFooter: true,
//            expandableRowTemplate: '<div ui-grid="row.entity.subgrigliatessuti" ng-style="grid.appScope.getSubTableHeight(row)" ng-show="row.entity.dettaglio.length > 0" ui-grid-selection ui-grid-edit ui-grid-auto-resize ui-grid-resize-columns></div>',
			headerHeight: 35,
			rowHeight: 35,
			enableExpandableRowHeader: false,
//			loader: true,
			isRowSelectable: function(row){
                return !row.entity.hasOwnProperty('$$treeLevel');
            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApiMod = gridApi;
                $scope.gridApiMod.grid.registerRowsProcessor( $scope.singleFilter, 200 );

                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    var selectedState = row.isSelected;
                    if (!selectedState){
                        row.treeNode.parentRow.spuntaSelected = false;
                    }
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                    if (this.grid.rows != undefined && this.grid.rows.length > 0){
                        var selectionState= $scope.gridApiMod.selection.getSelectAllState();
                        angular.forEach(this.grid.rows, function(item, key) {
                            if (item.hasOwnProperty('treeLevel')) {
                                item.spuntaSelected = !selectionState;
                            }
                        });
                    }
                });
            }
		};

		$scope.grigliaArtPermanenti = {
//            enableFiltering: true,
//            treeRowHeaderAlwaysVisible: false,
            tabName: 'grigliaArtPermanenti',
            apiID: 'gridApiPerm',
            showTreeExpandNoChildren: false,
            enableGridMenu: true,
            enableColumnMenus: false,
            enableSorting: false,
//            showGridFooter: true,
            showColumnFooter: true,
//            expandableRowTemplate: '<div ui-grid="row.entity.subgrigliatessuti" ng-style="grid.appScope.getSubTableHeight(row)" ng-show="row.entity.dettaglio.length > 0" ui-grid-selection ui-grid-edit ui-grid-auto-resize ui-grid-resize-columns></div>',
			headerHeight: 35,
			rowHeight: 35,
			enableExpandableRowHeader: false,
//			loader: true,
			isRowSelectable: function(row){
                return !row.entity.hasOwnProperty('$$treeLevel');
            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApiPerm = gridApi;
                $scope.gridApiPerm.grid.registerRowsProcessor( $scope.singleFilter, 200 );

                gridApi.selection.on.rowSelectionChanged($scope,function(row){
                    var selectedState = row.isSelected;
                    if (!selectedState){
                        row.treeNode.parentRow.spuntaSelected = false;
                    }
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                    if (this.grid.rows != undefined && this.grid.rows.length > 0){
                        var selectionState= $scope.gridApiPerm.selection.getSelectAllState();
                        angular.forEach(this.grid.rows, function(item, key) {
                            if (item.hasOwnProperty('treeLevel')) {
                                item.spuntaSelected = !selectionState;
                            }
                        });
                    }
                });

            }
		};

		$scope.grigliaTessuti.columnDefs = COLONNE_CUSTOM;
		$scope.grigliaArtModello.columnDefs = COLONNE_CUSTOM;
        $scope.grigliaArtPermanenti.columnDefs = COLONNE_CUSTOM;

        function getUrlId() {
            vm.url_params = $location.search()||"Unknown";
            return vm.url_params.hasOwnProperty('id_rich_studio') ? vm.url_params['id_rich_studio'] : "";
        }

        var parseResult = function(res){
            var newParsedObj = {'tessuti':[], 'artModello': [], 'artPermanenti':[]}
            angular.forEach(res, function(item, key) {
                angular.forEach(item, function(iitem, kkey) {
                    newParsedObj[key] = newParsedObj[key].concat(iitem);
                });
            });
            return newParsedObj;
        };

        $scope.initGriglie = function (){
            $scope.grigliaTessuti.data = [];
            $scope.grigliaArtModello.data = [];
            $scope.grigliaArtPermanenti.data = [];
        };

        //Applica aggiornamenti sui totali
        $scope.notifyChanges = function (){
            if ($scope.gridApiTess != undefined && $scope.tipoStudio === 'diba') $scope.gridApiTess.core.notifyDataChange(uiGridConstants.dataChange.ALL);
            if ($scope.gridApiMod != undefined && $scope.tipoStudio === 'diba') $scope.gridApiMod.core.notifyDataChange(uiGridConstants.dataChange.ALL);
            if ($scope.gridApiPerm != undefined) $scope.gridApiPerm.core.notifyDataChange(uiGridConstants.dataChange.ALL);
        }

        //Espande righe tabelle
        $scope.expandTablesRows = function (){
            $timeout(function(){
                if ($scope.grigliaTessuti.data.length > 0 && $scope.gridApiTess != undefined && $scope.tipoStudio === 'diba') $scope.gridApiTess.treeBase.expandAllRows();
                if ($scope.grigliaArtModello.data.length > 0 && $scope.gridApiMod != undefined && $scope.tipoStudio === 'diba') $scope.gridApiMod.treeBase.expandAllRows();
                if ($scope.grigliaArtPermanenti.data.length > 0) $scope.gridApiPerm.treeBase.expandAllRows();
            });
        }

        vm.showLoaders = function(){
            $scope.grigliaTessuti.loader = true;
            $scope.grigliaArtModello.loader = true;
            $scope.grigliaArtPermanenti.loader = true;
        }

        vm.hideLoaders = function(){
            $scope.grigliaTessuti.loader = false;
            $scope.grigliaArtModello.loader = false;
            $scope.grigliaArtPermanenti.loader = false;
        }

        vm.ricerca = function(event, dati){
            vm.showLoaders();
            $scope.initGriglie();
            RicercaService.ricerca(dati, function(response){
                var parseNewObj = parseResult(response);
                $scope.grigliaTessuti.data = parseNewObj.tessuti;
                $scope.grigliaArtModello.data = parseNewObj.artModello;
                $scope.grigliaArtPermanenti.data = parseNewObj.artPermanenti;
//                $scope.expandTablesRows();
                $scope.notifyChanges();
                vm.hideLoaders();
            }, function(){
                vm.hideLoaders();
                logger.error(response.data.message);
            });
        };

        $scope.filterData = function() {
            if ($scope.tipoStudio === 'diba') $scope.gridApiTess.core.refresh();
            if ($scope.tipoStudio === 'diba') $scope.gridApiMod.core.refresh();
            $scope.gridApiPerm.core.refresh();
         };

        $scope.setNewTotals = function(renderableRows){
            if (renderableRows != undefined && renderableRows.length >0)
            {
                angular.forEach(renderableRows, function(item){
                    var totalCapi = 0;
                    var totalFabb = 0;
                    var totalImp = 0;
                    if (item.hasOwnProperty('treeLevel')){
                        var visibleChildren =  $.grep(item.treeNode.children, function (element, index) {
                            return element.row.entity.isVisible;
                        });
                        if(visibleChildren != undefined && visibleChildren.length > 0){
                            angular.forEach(visibleChildren, function(iitem){
                                totalCapi += parseInt(iitem.row.entity.tot_capi);
                                totalFabb += parseFloat(iitem.row.entity.fabbisogno);
                                totalImp += parseFloat(iitem.row.entity.impegni);
                            });
                            item.treeNode.row.entity.header_tot_capi = totalCapi;
                            item.treeNode.row.entity.header_fabbisogno = parseFloat(totalFabb).toFixed(2);
                            item.treeNode.row.entity.header_impegni = parseFloat(totalImp).toFixed(2);
                        }
                    }
                });
            }
        };

        $scope.singleFilter = function( renderableRows ){
            var matcher = new RegExp(String(vm.filterValue).toLowerCase());
            var fieldsList = [];
            if (COLONNE_CUSTOM != undefined && COLONNE_CUSTOM.length > 0){
                angular.forEach(COLONNE_CUSTOM, function(item){
                    if (item !== 'actions')
                        fieldsList.push(item.field);
                });
            }
            if(matcher != undefined){
                renderableRows.forEach( function( row ) {
                  var match = false;
                  fieldsList.forEach(function( field ){
                    if ( row.entity[field] != undefined && String(row.entity[field]).toLowerCase().match(matcher) && field !== undefined ){
                      match = true;
                    }
                  });
                  if ( !match ){
                    row.visible = false;
                    row.entity.isVisible = false;
//                    row.treeNode.row.visible = false
                  }else{
                    row.entity.isVisible = true;
                  }
                });
            }
            $scope.setNewTotals(renderableRows);
            return renderableRows;
        };

        $scope.mostraGiacenza = function($event, row){
            var objToSend = angular.copy(row.entity);
            RicercaService.ricerca_giacenze(objToSend, function(response){
                if(response != undefined && response != '' && response.length > 0){
                    console.log(response);
                    var dataObj = response[0];
                    dataObj['impegnati'] = parseFloat(dataObj['IMP_DT'] + dataObj['IMP_ORD'] + dataObj['IMP_SOC']).toFixed(2);
                    $scope.grigliaMostraGiacenza.data = [dataObj];
                };
            });
        }

        var getAllSelected = function (){
            var objToSend = [];
            var selectedTess = ($scope.grigliaTessuti.data.length > 0 && $scope.tipoStudio === 'diba') ? $scope.gridApiTess.selection.getSelectedRows() : [];
            var selectedMod = ($scope.grigliaArtModello.data.length > 0 && $scope.tipoStudio === 'diba') ? $scope.gridApiMod.selection.getSelectedRows() : [];
            var selectedPerm = ($scope.grigliaArtPermanenti.data.length > 0) ? $scope.gridApiPerm.selection.getSelectedRows() : [];
            objToSend = objToSend.concat(selectedTess).concat(selectedMod).concat(selectedPerm);
            objToSend = $.grep( objToSend, function( item, index ) {
                return !item.hasOwnProperty('$$treeLevel');
            });

            return objToSend;
        };

        vm.salvaMultiploDiba = function(){
            var objToSend = getAllSelected();
            if(objToSend != undefined && objToSend.length > 0){
                SalvaService.salvaDiba(objToSend, function(response){
                    console.log(response);
                    $timeout(function(){$('#cercaButton').trigger('click');}, 200)
                    $scope.filterData();
                });
            }else logger.error('Nessun elemento selezionato')
        }

        vm.arretraMultiploDiba = function(){
            var objToSend = getAllSelected();
            if(objToSend != undefined && objToSend.length > 0){
                SalvaService.arretraDiba(objToSend, function(response){
                    console.log(response);
                    $timeout(function(){$('#cercaButton').trigger('click');}, 200);
                    $scope.selectedIndex = $scope.selectedIndexAfterActions;
                    $scope.filterData();
                });
            }else logger.error('Nessun elemento selezionato')
        }

        vm.avanzaMultiploDiba = function(){
            var objToSend = getAllSelected();
            if(objToSend != undefined && objToSend.length > 0){
                SalvaService.avanzaDiba(objToSend, function(response){
                    console.log(response);
                    $timeout(function(){$('#cercaButton').trigger('click');}, 200)
                    $scope.selectedIndex = $scope.selectedIndexAfterActions;
                    $scope.filterData();
                });
            }else logger.error('Nessun elemento selezionato')
        }

        $scope.salvaRigaDiba = function(rowEntity, tableID){
            var idRiga = rowEntity['$$hashKey'];
            SalvaService.salvaDiba([rowEntity], function(response){
                if(response != undefined && response.length > 0){
                    angular.forEach($scope[tableID].data, function(item, key) {
                        if(item['$$hashKey'] === idRiga) {
                            item['nota'] = angular.copy(response[0].nota || '');
                            item['colore'] = angular.copy(response[0].colore || '');
                            item['macrocolore'] = angular.copy(response[0].macrocolore || '');
                            item['consumo'] = angular.copy(response[0].consumo || '');
                            item['fabbisogno'] = angular.copy(response[0].fabbisogno || '');
                        };
                    })
                }
                console.log(response);
                $scope.selectedIndex = $scope.selectedIndexAfterActions;
                $scope.filterData();
            });
        }

        // per modificare l'altezza della tabella dinamicamente, aggiungere ng-style="ctrl.getTableHeight()" agli
        // attributi della grid e scommentare qui sotto
        $scope.getTableHeight = function(tableData) {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           return {
              height: (tableData.length * rowHeight + headerHeight) + "px",
              'min-height': 200 +'px',
              'max-height': 500 +'px'
           };
        };

        $scope.loadMore = function(partials, totals, totalDisplayed) {
            var last = partials.length -1;
            if (partials.length < totals.length ){
              $scope.infiniteScrollDisabled = true;
              for(var i = 1; i < 2; i++) {
                partials.push(totals[last+i]);
                $scope[totalDisplayed] +=1;
              }
            }else{
            }
        };

        $scope.cambiaIndiceTab = function(currentTabIndex){
            $scope.selectedIndexAfterActions = currentTabIndex;
        };

        function getSelectedSubArticoli(row){
            var apiID = row.grid.options.apiID;
            var articoli_selezionati = $scope[apiID].treeBase.getRowChildren(row);
            var list_articoli_selezionati = [];
            if (articoli_selezionati !== undefined &&
                articoli_selezionati.length > 0){
                    list_articoli_selezionati = [];
                    $(articoli_selezionati).each(function(index, row){
                        if ([list_articoli_selezionati].indexOf(row.entity.id_diba) === -1 && row.visible){
                            list_articoli_selezionati.push(row.entity.id_diba);
                        }
                    });
            }else
                logger.error('Nessun elemento selezionato');
            console.log(list_articoli_selezionati);
            return list_articoli_selezionati;
        };

        $scope.apriGestioneImpegni = function(row){
            var id_dibas_dett = getSelectedSubArticoli(row);
            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/diba.impegni_ctrl';
            var program = 'vista_gestione_impegni';
            var url = app_server + "?module=" + module + "&program=" + program + "&menu_id=" + menu_id + "&id_dibas=" + id_dibas_dett + "&sid=" + sid;
            if (id_dibas_dett.length > 0)
                window.open(url);
        }

        $scope.apriModificaOrdine = function(id_ordine){
            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/pao.ordini';
            var program = 'visualizza_ordine';
            var action = 'modifica_ordine';
            //var id_ordine = jQuery(this).attr('id').replace('btn_apri_ordine_','');
            var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&id_ordine="+id_ordine+"&action="+action;
            window.open(url);
        }

        vm.estrazione = function(event, dati){
            RicercaService.estraiGenerica(
                    dati,
                    vm.estraiGenericaSuccessHandler,
                    vm.estraiGenericaErrorHandler
                );
        }

		vm.estraiGenericaSuccessHandler = function(data){
		console.log(data);
			var url = DTHttp.urlBuilder('facon/diba.studio_diba_ctrl', 'render_doc_xls', {doc:data});
			$window.open(url, '_blank');
		};

		vm.estraiGenericaErrorHandler = function(){

		};

		vm.reset_data_tabelle = function(){
            $scope.grigliaTessuti.data = [];
            $scope.grigliaArtModello.data = [];
            $scope.grigliaArtPermanenti.data = [];

		};

		vm.anteprimaDocumentoPDFSuccessHandler = function(data){
			var program = 'render_doc';
			var url = DTHttp.urlBuilder('facon/diba.fabbisogni_ctrl', program, {doc:JSON.stringify(data)});
			$window.open(url, '_blank');
		};

		vm.anteprimaDocumentoPDFErrorHandler = function(){
		};

        $scope.stampaDistintaBaseMultiplo = function(event, params)
		{
            var objToSend = [];
            RicercaService.stampaDistintaBase(params,
                            vm.anteprimaDocumentoPDFSuccessHandler,
                            vm.anteprimaDocumentoPDFErrorHandler)

        };


        console.log($scope);
        $rootScope.$on("studiodiba:ricerca", vm.ricerca);
        $rootScope.$on("studiodiba:estrazione", vm.estrazione);
        $rootScope.$on("studiodiba:salvaMultiploDiba", vm.salvaMultiploDiba);
        $rootScope.$on("studiodiba:arretraMultiploDiba", vm.arretraMultiploDiba);
        $rootScope.$on("studiodiba:avanzaMultiploDiba", vm.avanzaMultiploDiba);
        $rootScope.$on("studiodiba:reset_data_tabelle", vm.reset_data_tabelle);
        $rootScope.$on("studiodiba:stampaDiba", $scope.stampaDistintaBaseMultiplo);
    }
})();