(function ()
{
	'use strict';

	var controllerId = "DialogDisimpegnaArticoliCtrl";

	DialogDisimpegnaArticoliCtrl.$inject = ['$rootScope', '$scope', '$uibModalInstance', 'LoggerFactory', '$timeout', '$filter', 'entity',
	        'SOCIETA', 'UI_ANNI_STAGIONE', '$uibModal', 'RicercaService', 'AutoCompleteGFService', 'SalvaService',
	        '$location', 'uiGridConstants', '$templateCache'];

	angular.module('app').controller(controllerId, DialogDisimpegnaArticoliCtrl);

	function DialogDisimpegnaArticoliCtrl($rootScope, $scope, $uibModalInstance, LoggerFactory, $timeout, $filter, entity,
	        SOCIETA, UI_ANNI_STAGIONE, $uibModal, RicercaService, AutoCompleteGFService, SalvaService,
	        $location, uiGridConstants, $templateCache)
	{
		var vm = this;
		var logger = LoggerFactory.getLogger(controllerId);
        $scope.entity = entity;
        $scope.ordiniSelected = [];
        $templateCache.put('ui-grid/selectionRowHeader',
            "<div ng-if='row.grid.options.gridName !== \"grigliaImpegniPresenti\" || row.entity.id !== \"\" && row.entity.diba.nome_modello_orig !== \"Scorta sede\"''"+
                 "class=\"ui-grid-disable-selection\">"+
                 "<div class=\"ui-grid-cell-contents\">"+
                    "<ui-grid-selection-row-header-buttons></ui-grid-selection-row-header-buttons>"+
                "</div>"+
            "</div>"
        );
        $templateCache.put('ui-grid/uiGridViewport',
            "<div class=\"ui-grid-viewport\" ng-style=\"colContainer.getViewportStyle()\">"+
                "<div class=\"ui-grid-canvas\">"+
                    "<div ng-repeat=\"(rowRenderIndex, row) in rowContainer.renderedRows track by $index\""+
                        "ng-if=\"row.grid.options.gridName === \'subgrigliaOrdini\' && row.entity.visible == 1 || row.grid.options.gridName !== \'subgrigliaOrdini\' || row.grid.options.gridName === \'grigliaOrdini\'\""+
                        "class=\"ui-grid-row\" ng-style=\"Viewport.rowStyle(rowRenderIndex)\">"+
                        "<div ui-grid-row=\"row\" row-render-index=\"rowRenderIndex\"></div>"+
                    "</div>"+
                "</div>"+
            "</div>"
        );
        vm.getDatiDisimpegnaOrdini = function(entity) {
            if (entity.righe_selezionate != undefined && !$.isEmptyObject(entity.righe_selezionate)){
                var listaAllArticoliSelezionati = [];
                angular.forEach(entity.righe_selezionate, function(item, key) {
                    if (!$.isEmptyObject(item))
                        listaAllArticoliSelezionati.push(item);
                });
                // Init con dati caricati da server
                //TODO: Get server dati per tabella ordini
                RicercaService.get_disimpegno_ordini_da_gestione_impegni(listaAllArticoliSelezionati, function(data){

                    for(var i = 0; i < data.length; i++){
                        data[i].subgrigliaOrdini = {
                            headerHeight: 30,
			                rowHeight: 30,
                            enableRowSelection : true,
                            enableColumnMenus: false,
                            enableSelectAll: false,
                            multiSelect: false,
                            columnDefs: COLONNE_SOTTOTAB,
                            data: data[i].dettaglio,
                            disableRowExpandable : data[i].dettaglio === 0,
                            subNumId: i,
                            gridName: 'subgrigliaOrdini',
                            onRegisterApi: function (gridApi)
                            {
                                $scope['subgridApi'+this.subNumId] = gridApi;
                                //Grid selection method
                                gridApi.selection.on.rowSelectionChanged(gridApi.grid.appScope, function(row){
//                                    gridApi.selection.clearSelectedRows(event);
                                    //ciclo sull' array con gli oggetti api delle tabelle
                                    for(var ind = 0; ind < $scope.grigliaOrdini.data.length; ind++){
                                        if (ind != row.grid.options.subNumId){
                                            $scope['subgridApi'+ind].selection.clearSelectedRows(event);
                                        }
                                    };
                                    if (row.isSelected){
                                        $scope.ordiniSelected = [row];
                                        console.log(row.entity)
                                    }else{
                                        $scope.ordiniSelected = [];
                                    }
                                    gridApi.selection.selectRow(row, event);
                                });
                            }
//                            $scope.subgridApiOrdini['gridApi'+i].expandable.expandAllRows();
                        }
                        angular.forEach(data[i].dettaglio, function(item) {
                            item.qta = parseFloat(item.qta);
                        });


                    }
                    $scope.grigliaOrdini.data = data;
                    $timeout(function(){$scope.gridApiOrdini.expandable.expandAllRows();});
                    console.log(data);

                }, function(){
                    //$scope.gridOptions.data = [];
                });
            }else
                logger.error('Nessun articolo selezionato')
        };

        $scope.errorHandler = function(){
//            logger.error('Errore Inatteso, contattare il supporto tecnico');
		};

		$scope.closeModal = function ()
		{
			$uibModalInstance.dismiss('cancel');
		};

		$scope.modificaOrdiniDaFabb = function (){
		    var allSelectedRows = angular.copy($scope.grigliaOrdini.data.slice());
		    var errore_somme_tot_qta = false;
		    if (allSelectedRows.length > 0){
		        $(allSelectedRows).each(function(index, item){
		            var tot_qta = parseFloat(item.qta);
                    var somma_qta_dett = 0;
		            if (item.hasOwnProperty('dettaglio') && item.dettaglio.length > 0 && item.stato_ordine === 'INVIATO'){
		                $(item.dettaglio).each(function(iindex, iitem){
		                    somma_qta_dett += parseFloat(iitem.qta);
		                });
		            }
		            if(somma_qta_dett > tot_qta){
		                errore_somme_tot_qta = true;
		            }
		            if (item.hasOwnProperty('subgrigliaOrdini') && item.subgrigliaOrdini.length > 0){
                        delete item.subgrigliaOrdini;
		            }
                });
                if (!errore_somme_tot_qta){
                    SalvaService.modificaOrdiniDaFabb(allSelectedRows, function(data){
                        if(entity.hasOwnProperty('from_table_info')){
                            $rootScope.$emit("popup_submit_refresh:grids_refresh_panel", entity.from_table_info);
                        }else{
                            $rootScope.$emit("popup_submit_refresh:grids_refresh_all");
                        }
                        $scope.closeModal();
                    }, $scope.errorHandler());
                }else{
                    logger.error('Impossibile modificare ordine INVIATO:quantità in dettaglio maggiore della quantità totale')
                }
		    }else {
		        logger.error('Nessun ordine da modificare')
		    }
		};

        $scope.cambiaArticolo = function (){
            var presentiSelected = $scope.gridApiImpPresenti.selection.getSelectedRows();//jQuery.extend(true, [], $scope.gridApiImpPresenti.selection.getSelectedRows().slice());
            var ordiniSelected = $scope.ordiniSelected;//jQuery.extend(true, [], $scope.ordiniSelected.slice());

		    if (presentiSelected.length > 0 && ordiniSelected.length > 0){
		        var riga_art_presenti = presentiSelected[0];
		        var table_art_presenti = $scope.grigliaImpegniPresenti.data;
		        var riga_ord_preord = ordiniSelected[0].entity;
		        var table_ord_preord = ordiniSelected[0].grid.options.data;

		        var impegnoPresentiSelected = 0;
                if(riga_ord_preord.tipo === 'ORD'){
                    impegnoPresentiSelected = riga_art_presenti.ORD > 0 ? riga_art_presenti.ORD : riga_art_presenti.da_soddisfare;
                }else if(riga_ord_preord.tipo === 'SOC'){
                    impegnoPresentiSelected = riga_art_presenti.SOC > 0 ? riga_art_presenti.SOC : riga_art_presenti.da_soddisfare;
                };

		        //grep su data di table_art_presenti per ricavare la riga con stesso id della
		        //selezionata in ord_preord
		        var riga_orig_art_presenti = $.grep(table_art_presenti, function(riga, indice){ // just use arr
                  return riga.id === riga_ord_preord['id'];
                });
                if (riga_orig_art_presenti.length >0){
                    riga_orig_art_presenti = riga_orig_art_presenti[0];
                }else{
                    logger.error('Fabbisogno originale non trovato')
                }

                if (impegnoPresentiSelected <= riga_ord_preord['qta']){
                    var nuova_riga_ord_preord = angular.copy(riga_ord_preord);

                    riga_ord_preord['modello'] = riga_art_presenti.diba['modello_orig'];
                    riga_ord_preord['id_faccom'] = riga_art_presenti['id_faccom'];
                    riga_ord_preord['nome_modello'] = riga_art_presenti.diba['nome_modello_orig'];
                    riga_ord_preord['variante'] = riga_art_presenti.diba['variante_dt'];
                    riga_ord_preord['id'] = riga_art_presenti['id'];
                    riga_ord_preord['qta'] = impegnoPresentiSelected;

                    nuova_riga_ord_preord['qta'] -= impegnoPresentiSelected;
                    nuova_riga_ord_preord['id_facord_dett_orig'] = nuova_riga_ord_preord['id_facord_dett'];
                    /*var a = nuova_riga_ord_preord['id_faccom_list'].indexOf(riga_art_presenti['id_faccom'])
                    if (a > -1)  {
                        nuova_riga_ord_preord['id_faccom_list'] = nuova_riga_ord_preord['id_faccom_list'].splice(a, 1)
                    }
                    */
                    nuova_riga_ord_preord['id_facord_dett'] = '';
                    if (nuova_riga_ord_preord['qta'] > 0){
                        table_ord_preord.push(nuova_riga_ord_preord);
                    }
                }
                else if (impegnoPresentiSelected > riga_ord_preord['qta']){
                    if (riga_ord_preord['stato_ordine'] === 'BOZZA'){
                        //Aggiorno tab ord/preord
                        riga_ord_preord['modello'] = riga_art_presenti.diba['modello_orig'];
                        riga_ord_preord['nome_modello'] = riga_art_presenti.diba['nome_modello_orig'];
                        riga_ord_preord['variante'] = riga_art_presenti.diba['variante_dt'];
                        riga_ord_preord['id'] = riga_art_presenti['id'];
                        riga_ord_preord['id_faccom'] = riga_art_presenti['id_faccom'];
                        riga_ord_preord['qta'] = impegnoPresentiSelected;
                    }
                    else if (riga_ord_preord['stato_ordine'] === 'INVIATO'){
                        //Aggiorno tab ord/preord
                        riga_ord_preord['modello'] = riga_art_presenti.diba['modello_orig'];
                        riga_ord_preord['nome_modello'] = riga_art_presenti.diba['nome_modello_orig'];
                        riga_ord_preord['variante'] = riga_art_presenti.diba['variante_dt'];
                        riga_ord_preord['id_faccom'] = riga_art_presenti['id_faccom'];
                        riga_ord_preord['id'] = riga_art_presenti['id'];
                    }
                }
                //Aggiorno totali per riga e riga orig presenti
                aggiorna_totali_riga_orig(riga_art_presenti.id);
                aggiorna_totali_riga_orig(riga_orig_art_presenti.id);
//                if(riga_art_presenti.diba['nome_modello_orig'] !== 'Scorta sede'){
//                    riga_art_presenti['da_soddisfare'] = parseFloat(riga_art_presenti['fabbisogno'] - riga_art_presenti['SOC'] - riga_art_presenti['ORD'] - riga_art_presenti['DT']);
//                }
//                if(riga_orig_art_presenti.diba['nome_modello_orig'] !== 'Scorta sede'){
//                    riga_orig_art_presenti['da_soddisfare'] = parseFloat(riga_orig_art_presenti['fabbisogno'] - riga_orig_art_presenti['SOC'] - riga_orig_art_presenti['ORD'] - riga_orig_art_presenti['DT']);
//                }

		    }else {
		        logger.error('Selezionare un articolo presente e un ordine/preordine per lo scambio')
		    }
		};

        $scope.getRowFornitore = function(row, value){
			return AutoCompleteGFService.getFornitore({'query' : value, });
		};

		$scope.onSelectRowFornitore = function(row, item, value, label){
		    row.fornitore = item.fornitore;
			row.ragione_sociale = item.rag_soc_forn;
		};

		$scope.getRowArticolo = function(row, value){
			return AutoCompleteGFService.getArticoloFornitore({'query' : value, });
		};

		$scope.onSelectRowArticolo = function(row, item, value, label){
		    row.art_forn = item.articolo_fornitore;
		};

        $scope.grigliaImpegniPresenti = {
            multiSelect: false,
			enableColumnMenus: false,
			enableSelectAll: true,
			enableExpandAll: false,
			enableRowHeaderSelection: true,
			enableRowSelection : true,
			enableExpandableRowHeader: false,
//			enableFullRowSelection: true,
			headerHeight: 25,
			rowHeight: 25,
			gridName: 'grigliaImpegniPresenti',
			isRowSelectable: function(row){
                return row.entity.diba.nome_modello_orig !== 'Scorta sede' && row.entity.id !== '';
            },
			onRegisterApi: function (gridApi)
			{
				$scope.gridApiImpPresenti = gridApi;
			},
		};
		$scope.grigliaOrdini = {
			enableColumnMenus: false,
			enableSelectAll: false,
//			enableExpandAll: false,
			enableRowHeaderSelection: false,
			enableRowSelection : false,
			enableExpandableRowHeader: true,
			expandableRowTemplate: '<div ui-grid="row.entity.subgrigliaOrdini" ng-style="grid.appScope.getSubTableHeight(row)" ui-grid-selection ui-grid-auto-resize ui-grid-resize-columns></div>',
			headerHeight: 25,
			rowHeight: 25,
			gridName: 'grigliaOrdini',
			onRegisterApi: function (gridApi)
			{
				$scope.gridApiOrdini = gridApi;
			},
			expandableRowScope: {

                assegnaScortaSede: function(row){
                    var table_art_presenti = $scope.grigliaImpegniPresenti.data;
                    //grep su data di table_art_presenti per ricavare la riga con stesso id della
                    //selezionata in ord_preord
                    var riga_orig_art_presenti = $.grep(table_art_presenti, function(riga, indice){ // just use arr
                      return riga.id === row.entity.id;
                    });
                    if (riga_orig_art_presenti.length >0){
                        riga_orig_art_presenti = riga_orig_art_presenti[0];
                    }else{
                        logger.error('Fabbisogno originale non trovato');
                    }
                    //Se riga_orig è diversa da scorta sede sottraggo la quantità persa nel cambio modello
                    if (riga_orig_art_presenti.diba['nome_modello_orig'] !== "Scorta sede" && riga_orig_art_presenti.id !== ''){
                        if (row.entity.tipo === 'SOC'){
                            riga_orig_art_presenti['SOC'] -= row.entity['qta'];
                        }else if(row.entity.tipo === 'ORD'){
                            riga_orig_art_presenti['ORD'] -= row.entity['qta'];
                        }
                    }
                    $timeout(function(){aggiorna_totali_riga_orig(riga_orig_art_presenti.id)}, 200);
//                    if(riga_orig_art_presenti.diba['nome_modello_orig'] !== 'Scorta sede'){
//                        riga_orig_art_presenti['da_soddisfare'] = parseFloat(riga_orig_art_presenti['fabbisogno'] - riga_orig_art_presenti['SOC'] - riga_orig_art_presenti['ORD'] - riga_orig_art_presenti['DT']);
//                    }
                    //Sbianco campi Modello, Nome e Variante in riga e aggiorno qta scorta sede
                    row.entity.modello = '';
                    row.entity.nome_modello = 'Scorta sede';
                    row.entity.variante = '';
                    row.entity.id = '';
                    $timeout(function(){aggiorna_totali_riga_orig(row.entity.id)}, 200);
                },
                eliminaRigaOrdine: function(row){
                    var tableOrdiniData = row.grid.options.data;
                    tableOrdiniData.forEach( function( item, index, object ) {
                        if (tableOrdiniData.length > 1 && item != undefined && item['$$hashKey'] === row.entity['$$hashKey'] && item['stato_ordine'] === 'BOZZA'){
                            object.splice(index, 1);
                        };
                    });
                    aggiorna_riga_orig_art_presenti(row.entity);
                },
                aggiorna_art_presenti: function(entity, from){
                    aggiorna_riga_orig_art_presenti(entity);
                }
            }
		};

        function row_class_presenti(grid, row, col, rowRenderIndex, colRenderIndex) {
           var classe = '';
           if (entity.hasOwnProperty('listIdSelected')){
                if(entity.listIdSelected.indexOf(row.entity.id) >= 0 ){
                    classe = 'giallo_outlet_prog';
                }else if(row.entity.diba.nome_modello_orig === 'Scorta sede' && row.entity.id === ''){
                    classe = 'grigio_default';
                }else{
                    classe = 'bianco';
                }
           }
          return classe;
        }

        function row_class_mod_art(grid, row, col, rowRenderIndex, colRenderIndex) {
            var classe = '';
            if(row.entity.tipo === 'SOC'){
                classe = 'grigio_default';
            }else{
                classe = 'bianco';
            }
          return classe;
        }

        function row_class_dettaglio(grid, row, col, rowRenderIndex, colRenderIndex) {
           var classe = 'bianco';

          return classe;
        }

		var COLONNE_IMPEGNI_PRESENTI = [
            {displayName: 'Capocmat.', field: 'diba.capocmat', width: '15%', cellClass: row_class_presenti},
			{displayName: 'Cod.Modello', field: 'diba.modello_orig', cellClass: row_class_presenti},
			{displayName: 'Variante', field: 'diba.variante_dt', cellClass: row_class_presenti},
			{displayName: 'Nome Mod.', field: 'diba.nome_modello_orig', cellClass: row_class_presenti},
			{displayName: 'Fabbisogno.', field: 'fabbisogno', cellClass: row_class_presenti},
			{displayName: 'Da Sodd.',field: 'da_soddisfare', cellClass: row_class_presenti},
			{displayName: 'Imp. DT', field: 'DT', cellClass: row_class_presenti},
			{displayName: 'Imp. Soc.', field: 'SOC', cellClass: row_class_presenti},
			{displayName: 'Imp. Ord.', field: 'ORD', cellClass: row_class_presenti}
        ];

		var COLONNE_MOD_ORD = [
		    {  field : 'tipo', displayName : 'Tipo', cellClass: row_class_mod_art},
			{  field : 'data_ordine', displayName : 'Data Ordine', type: 'date', cellFilter: "date:'dd/MM/yyyy'", cellClass: row_class_mod_art},
			{  field : 'num_ordine', displayName : 'Num. Ord.', 'type' : 'string', cellClass: row_class_mod_art},
	        {  field : 'societa', displayName : 'Società', 'type' : 'string', cellClass: row_class_mod_art},
		    {  field : 'anno_stagione_dt', displayName : 'Anno Stagione DT',cellClass: row_class_mod_art},
		    {  field : 'collezione', displayName : 'Collezione DT', cellClass: row_class_mod_art},
		    {  field : 'capocmat', displayName : 'Capocmat', cellClass: row_class_mod_art},
		    {  field : 'fornitore', displayName : 'Fornitore', cellClass: row_class_mod_art},
		    {  field : 'art_forn', displayName : 'Articolo Fornitore', cellClass: row_class_mod_art},
		    {  field : 'tipo_ordine', displayName : 'Tipo', cellClass: row_class_mod_art},
		    {  field : 'stato_ordine', displayName : 'Stato', cellClass: row_class_mod_art},
		    {  field : 'qta', displayName : 'Qta Tot', cellClass: row_class_mod_art,
		        cellTemplate:
		        '<div class="ui-grid-cell-contents">'+
                    '<p ng-model="row.entity.qta"><b>{{row.entity.qta}}</b></p>'+
                '</div>'
		    }
		]

		var COLONNE_SOTTOTAB = [
			{  field : 'modello', displayName : 'Modello', enableHiding: false, cellClass: row_class_dettaglio,
			    cellTemplate:   '<div class="ui-grid-cell-contents">'+
                                    '<input type="text" class="form-control" ng-disabled="true" ng-model="row.entity.modello" name="row_modello" '+
                                '</div>'
            },
        	{  field : 'nome_modello', displayName : 'Nome', cellClass: row_class_dettaglio,
        	    cellTemplate:   '<div class="ui-grid-cell-contents">'+
                                    '<input type="text" class="form-control" ng-disabled="true" ng-model="row.entity.nome_modello" name="row_nome_modello" '+
                                '</div>'
        	},
	        {  field : 'variante', displayName : 'Variante', cellClass: row_class_dettaglio,
	            cellTemplate:   '<div class="ui-grid-cell-contents">'+
                                    '<input type="text" class="form-control" ng-disabled="true" ng-model="row.entity.variante" name="row_variante" '+
                                '</div>'
	        },
		    {  field : 'misura', displayName : 'Misura', cellClass: row_class_dettaglio},
		    {  field : 'colore', displayName : 'Colore', cellClass: row_class_dettaglio},
		    {  field : 'descrizione_colore', displayName : 'Descr. Colore', cellClass: row_class_dettaglio},
		    {displayName: 'Data Consegna',
                field: 'data_consegna',
                enableCellEdit: true,
                enableHiding: false,
                cellClass: row_class_dettaglio,
                type: 'date',
                cellFilter: "date:'dd/MM/yyyy'"
            },
		    {  field : 'qta', displayName : 'Qta', cellClass: row_class_dettaglio,
		        cellTemplate:
                '<div class="ui-grid-cell-contents">'+
                    '<input type="number:2" min="0" ng-model="row.entity.qta" ng-change="grid.appScope.aggiorna_art_presenti(row.entity)"'+
                '</div>'
		    },
		    {displayName: 'Azioni', field: 'azioni', category:'azioni', cellClass: row_class_dettaglio,
                enableHiding: false,
                enableCellEdit: false,
                enableSorting: false,
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Scorta sede" tooltip-placement="left" grid.parentRow.entity.stato_ordine != \'BOZZA\'" ng-click="grid.appScope.assegnaScortaSede(row)"'+
                        'class="btn btn-xs btn-default stretto">'+
                        '<i class="fa fa-cubes"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Elimina Riga" tooltip-placement="left" ng-disabled="grid.options.data.length <= 1 || grid.parentRow.entity.stato_ordine != \'BOZZA\'" ng-click="grid.appScope.eliminaRigaOrdine(row)"'+
                        'class="btn btn-xs btn-danger stretto">'+
                        '<i class="fa fa-trash"></i>'+
                    '</button>'+
                '</div>'
            }
		]

        // per modificare l'altezza della tabella dinamicamente, aggiungere ng-style="ctrl.getTableHeight()" agli
        // attributi della grid e scommentare qui sotto
        $scope.getTableHeightImpPres = function(tableData) {
           var rowHeight = 25; // your row height
           var headerHeight = 25; // your header height
           return {
              height: (tableData.length * rowHeight + headerHeight * 2.5) + "px",
              'min-height': 80 +'px',
              'max-height': 350 +'px'
           };
        };

        $scope.getTableHeightModOrd = function(tableData) {
           var rowHeight = 25; // your row height
           var headerHeight = 25; // your header height
           return {
              height: (tableData.length * 2 * rowHeight + headerHeight * 5) + "px",
              'min-height': 150 +'px',
              'max-height': 400 +'px'
           };
        };

        function getExpandableRowHeight(expRowHeight){
            if(expRowHeight < 50)
                return 50;
            else if(expRowHeight > 50 && expRowHeight < 200)
                return expRowHeight;
            else if(expRowHeight > 200)
                return 200;
        };

        $scope.getSubTableHeight = function(row) {
           var rowHeight = 25; // your row height
           var headerHeight = 25; // your header height
           var visibleDataSubTableLength = $.grep(angular.copy(row.entity.subgrigliaOrdini.data.slice()), function(riga, indice){ // just use arr
              return riga.visible == '1';
           }).length;
           var heightSubTable = (visibleDataSubTableLength * rowHeight + 2 * headerHeight)
           row.grid.options.expandableRowHeight = getExpandableRowHeight(heightSubTable);
           row.expandedRowHeight = getExpandableRowHeight(heightSubTable);
           return {
              height: heightSubTable + "px",
              'min-height': 50 +'px',
              'max-height': 200 +'px'
           };
        };

        var aggiorna_totali_riga_orig = function(id_orig){
            var somma_soc_dett = 0;
            var somma_ord_dett = 0;
            var table_art_presenti = $scope.grigliaImpegniPresenti.data;
            //grep su data di table_art_presenti per ricavare la riga con stesso id della
            //selezionata in ord_preord
            var riga_orig_art_presenti = $.grep(table_art_presenti, function(riga, indice){ // just use arr
              return riga.id === id_orig;
            });
            if (riga_orig_art_presenti.length >0){
                riga_orig_art_presenti = riga_orig_art_presenti[0];
            }else{
                logger.error('Fabbisogno modello non trovato');
            }
            angular.forEach($scope.grigliaOrdini.data, function(item, index, object) {
                if (item.hasOwnProperty('dettaglio') && item['dettaglio'].length > 0){
                    angular.forEach(item['dettaglio'], function(iitem, iindex, oobject) {
                        if(iitem['id'] === id_orig && iitem.visible == 1){
                            if (item['tipo'] === 'SOC'){
                                somma_soc_dett += parseFloat(iitem['qta']);
                            }
                            else if (item['tipo'] === 'ORD'){
                                somma_ord_dett += parseFloat(iitem['qta']);
                            }
                        }
                    });
                }
            });
            //AGGIORNO RIGA ORIG SCORTA SEDE IN TAB PRESENTI
            riga_orig_art_presenti['SOC'] = isNaN(somma_soc_dett) ? 0 : somma_soc_dett;
            riga_orig_art_presenti['ORD'] = isNaN(somma_ord_dett) ? 0 : somma_ord_dett;
            if(riga_orig_art_presenti.diba['nome_modello_orig'] !== 'Scorta sede'){
                riga_orig_art_presenti['da_soddisfare'] = parseFloat(riga_orig_art_presenti['fabbisogno'] - riga_orig_art_presenti['SOC'] - riga_orig_art_presenti['ORD'] - riga_orig_art_presenti['DT']);
            }
        };

        var aggiorna_riga_orig_art_presenti = function(row){
            var table_art_presenti = $scope.grigliaImpegniPresenti.data;
            //grep su data di table_art_presenti per ricavare la riga con stesso id della
            //selezionata in ord_preord
            var riga_orig_art_presenti = $.grep(table_art_presenti, function(riga, indice){ // just use arr
              return riga.id === row.id;
            });
            if (riga_orig_art_presenti.length >0){
                riga_orig_art_presenti = riga_orig_art_presenti[0];
            }else{
                logger.error('Fabbisogno originale non trovato')
            }
            aggiorna_totali_riga_orig(riga_orig_art_presenti.id);
//            if(riga_orig_art_presenti.diba['nome_modello_orig'] !== 'Scorta sede'){
//                riga_orig_art_presenti['da_soddisfare'] = parseFloat(riga_orig_art_presenti['fabbisogno'] - riga_orig_art_presenti['SOC'] - riga_orig_art_presenti['ORD'] - riga_orig_art_presenti['DT']).toFixed(2);
//
//            }
        };

        var aggrega_dati_griglia_presenti = function (data){
            var scorta_sede_dict = {};
            var scorta_sede_empty_dict = {  id: '',
                                            fabbisogno: 0,
                                            da_soddisfare: 0,
                                            DT: 0, SOC: 0, ORD: 0,
                                            isPresenti: true,
                                            diba:{  nome_modello_orig:'Scorta sede',
                                                    modello_orig:0,
                                                    variante_dt:0
                                            }
                                        };
            var final_list_presenti = [];
            angular.forEach(data, function(item, index, object) {
                if (item['diba']['nome_modello_orig'] === 'Scorta sede' && item['id'] === ''){
                    if ($.isEmptyObject(scorta_sede_dict)){
                        scorta_sede_dict = angular.copy(item);
                        scorta_sede_dict['isPresenti'] = true;
                        scorta_sede_dict['DT'] = parseFloat(item['DT']);
                        scorta_sede_dict['SOC'] = parseFloat(item['SOC']);
                        scorta_sede_dict['ORD'] = parseFloat(item['ORD']);
                        scorta_sede_dict['id'] = '';
                    }else{
                        scorta_sede_dict['DT'] += parseFloat(item['DT']);
                        scorta_sede_dict['SOC'] += parseFloat(item['SOC']);
                        scorta_sede_dict['ORD'] += parseFloat(item['ORD']);
                    }
                }else{
                    final_list_presenti.push(angular.copy(item));
                }
            });
            //Se ho scorta sede lo unisco aggregato in tabella altrimenti lo inizializzo a vuoto elo passo comunque
            if (!$.isEmptyObject(scorta_sede_dict)){
                final_list_presenti = final_list_presenti.concat([scorta_sede_dict]);
            }else{
                scorta_sede_empty_dict['diba']['capocmat'] = final_list_presenti[0]['diba']['capocmat']
                final_list_presenti = final_list_presenti.concat([scorta_sede_empty_dict]);
            }
            return final_list_presenti;
        }

        $scope.dataReset = function(){
            vm.getDatiDisimpegnaOrdini(angular.copy(entity));
            $scope.grigliaImpegniPresenti.data = aggrega_dati_griglia_presenti(angular.copy(entity.allTableData.slice()));
        };

		$scope.grigliaOrdini.columnDefs = COLONNE_MOD_ORD;
		$scope.grigliaOrdini.data = [];
		$scope.grigliaImpegniPresenti.columnDefs = COLONNE_IMPEGNI_PRESENTI;
		$scope.grigliaImpegniPresenti.data = aggrega_dati_griglia_presenti(angular.copy(entity.allTableData.slice()));
        console.log($scope);
        vm.getDatiDisimpegnaOrdini(angular.copy(entity));
	}
})();