(function ()
{
	'use strict';

	var controllerId = "PopoverColFornitoriCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', '$uibModal',
	                        'ServiceDatiVistaGiacenza',
	                        'ServiceDatiPopoverColoreFornitore', 'MACROCOLORE', 'uiGridConstants'];

	angular.module('app').controller(controllerId, ControllerFn);

	function ControllerFn($scope, $rootScope, LoggerFactory, $uibModal,
	                        ServiceDatiVistaGiacenza,
	                        ServiceDatiPopoverColoreFornitore, MACROCOLORE, uiGridConstants)
	{

		var vm = this;
        vm.fornitore = '';
        vm.colore_orig = '';
        vm.descr_colore = '';
        function getFilter(){
            return {
                fornitore_col_forn: vm.fornitore,
                colore_orig_col_forn: vm.colore_orig,
                descr_colore_col_forn: vm.descr_colore
            }
        };

		$scope.closePopover = function(ev){
		    var targetButton = ServiceDatiPopoverColoreFornitore.clickedButton;
		    setTimeout(function(){
                $(targetButton).trigger('click');
            }, 100);
		};

        $scope.cambiaColoreFornitore = function(rowEntity){
            $rootScope.$emit("popoverColFornitoreRicercaGiacenza:cambiaColFornitore", {
                riga_col_forn: {'id_articolo_fornitore_col_forn':rowEntity['id_articolo_fornitore_col_forn'],
                                'articolo_fornitore_col_forn':rowEntity['articolo_fornitore_col_forn'],
                                'conto_fornitore_col_forn':rowEntity['conto_fornitore_col_forn'],
                                'ragione_sociale_col_forn':rowEntity['ragione_sociale_col_forn'],
                                'colore_orig_col_forn':rowEntity['colore_orig_col_forn'],
                                'desc_colore_orig_col_forn':rowEntity['desc_colore_orig_col_forn'],
                                'desc_colore_forn_col_forn':rowEntity['desc_colore_forn_col_forn'],
                                'macrocolore_col_forn': rowEntity['macrocolore_col_forn']
                },
                tableRowID: ServiceDatiPopoverColoreFornitore.tableRowID,
                tableID: ServiceDatiPopoverColoreFornitore.tableID,
                clickButton: ServiceDatiPopoverColoreFornitore.clickedButton,
            });
            $scope.closePopover();
        };

    }
})();