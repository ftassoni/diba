import tools
from facon.ordpf.dao.decorators import query_with_result, query_without_result, insert_query_with_id_as_result

class StudioDibaDao():
    @staticmethod
    def __applica_filtri(params):
        where_clause = {
            'processo': '',
            'stato': '',
        }
        if params.get('processo') and params.get('processo') != '':
            where_clause['processo'] = " AND p.PACKAGE_ID='%s' " % params.get('processo')
        if params.get('stato') and params.get('stato') != '':
            where_clause['stato'] = " AND p.STATE='%s' " % params.get('stato')
        return where_clause


    @staticmethod
    @query_with_result
    def ricerca(filtri):

        #filter = StudioDibaDao.__applica_filtri(params=filtri)

        query = """
                    SELECT * FROM SCHEDE_MODELLO_RIGHE WHERE U_NAS='elisag';
                """
        return query