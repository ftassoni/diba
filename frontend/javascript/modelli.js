var HEADER_TABELLA_RICERCA = "<table id='table_risultati'> \
									<thead> \
										<tr> \
											<th>Societa</th> \
											<th>Modello</th> \
									 		<th>Nome</th> \
									 		<th>Classe</th> \
									 		<th>Collezione</th> \
									 		<th>Anno Stagione</th> \
									 		<th>Articolo</th> \
									 		<th>Descrizione</th> \
									 		<th>Azioni</th> \
										</tr> \
									</thead> \
								</table>";

var TABELLA_RICERCA = {
        "bProcessing": true,
        "bAutoWidth": false,
        "sAjaxDataProp": "ResultSet.Result",
        "oLanguage": ITALIAN_LANGUAGE,
        "aoColumns": [
                     { "mData": "societa", "sWidth": "5%"},
                     { "mData": "modello_base",  "sWidth": "10%" },
                     { "mData": "nome",  "sWidth": "10%" },
                     { "mData": "desc_classe",  "sWidth": "10%" },
                     { "mData": "collezione_descr",  "sWidth": "15%" },
                     { "mData": "anno_stagione", "sWidth": "5%"},
                     { "mData": "materiale", "sWidth": "5%"},
                     { "mData": "desc_articolo", "sWidth": "20%"},
                     { "mData": "modello_base", "sWidth": "10%" }
                     ],
        "aoColumnDefs": [
					{ "mRender": function (data, type, row) {
		            	btn = "<input type='button' class='btn_visualizza' title='Dettaglio Modello' onclick='mostraDettaglio(\""+row['societa']+"\","+row['modello_base']+")' />" +
		            		  "<input type='button' class='btn_image' title='Immagini Modello' onclick='viewImgModello(\""+row['societa']+"\","+row['modello_base']+")' />";
		            		  //"<input type='button' class='btn_importa' title='Importa' onclick='importaDB(\""+row['societa']+"\","+row['modello_base']+")' />"
		            	return btn;
					},
					  "aTargets": [8], 
					  "bSortable": false 
					}
		            ],
        "fnInitComplete": function (oSettings, json) {},
        "iDisplayLength": 20,
        "bPaginate": true,
        "bFilter": true,
        "bSortClasses": false,
        "aaSorting" : []
    }


function init(){
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	jQuery("#overlay").hide();
	jQuery("#overall_container").hide();
	
	jQuery("#btn_ricerca").unbind('click');
	jQuery("#btn_ricerca").click(ricercaModelli);
	
	setAutocomplete('gm');
   	jQuery('#param_gm').unbind('click');
   	jQuery('#param_gm').click(function() {
   		setAutocomplete('gm');
   	});
   	
   	setAutocomplete('articolo');
   	jQuery('#param_articolo').unbind('click');
   	jQuery('#param_articolo').click(function() {
   		setAutocomplete('articolo');
   	});
   	
   	setAutocomplete('collezione');
   	jQuery('#param_collezione').unbind('click');
   	jQuery('#param_collezione').click(function() {
   		setAutocomplete('collezione');
   	});
	
}



function setAutocomplete(param_nome) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	var societa = jQuery('#param_societa option:selected').val();
	var as = jQuery('#param_as  option:selected').val();
	
	jQuery('#param_'+param_nome).autocomplete({
   		source: function(request, response) {
			jQuery.ajax({
				url: app_server+"?",
				dataType: "json",
				data: {
					module: module,
					program: "ricerca_"+param_nome,
					sid: sid,
					menu_id: menu_id,
					societa: societa,
					anno_stagione: as,
					maxRows: 20,
					query: request.term
				},
				success: function(data) {
					if (data['sError']) {
						alert(data['sError']);
						jQuery('#param_'+param_nome ).removeClass('ui-autocomplete-loading');
					}
					else {
   					response(jQuery.map(data['ResultSet']['Result'], function(item) {
   						return {
   							label: item.label,
   							value: item.chiave
   						}
   						}));
					}
				}
			});
		},
		minLength: 0,
		select: function(event, ui) {
			jQuery('#param_'+param_nome).removeClass('ui-autocomplete-loading');
		},
		change: function(event, ui) {
			jQuery('#param_'+param_nome).removeClass('ui-autocomplete-loading');
		},
		open: function() {
			jQuery( this ).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close: function() {
			jQuery( this ).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});
}


function ricercaModelli() {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'ricerca_modelli'; 
	
	jQuery('#risultati_ricerca').empty();
	
	
	var params_ricerca = {}; 
	
	var societa = jQuery('#param_societa').val();
	if (societa!=""){
		params_ricerca['societa']=societa;
	}
	
	var as = jQuery('#param_as').val();
	if (as!=""){
		params_ricerca['as']=as;
	}
	
	if (societa == '' || as == '' || as == 'Tutti'){
		alert("Per la ricerca e' obbligatorio inserire societa e anno stagione.");
		return;
	}
	
	var classe = jQuery('#param_classe').val();
	if (classe!=""){
		params_ricerca['classe']=classe;
	}

	var gm = jQuery('#param_gm').val();
	if (gm!=""){
		params_ricerca['gm']=gm;
	}
	
	var art = jQuery('#param_articolo').val();
	if (art!=""){
		params_ricerca['articolo']=art;
	}
	
	var collezione = jQuery('#param_collezione').val();
	if (collezione!=""){
		params_ricerca['collezione']=collezione;
	}
	
	
	var modello = jQuery('#param_modello').val();
	if (modello!=""){
		params_ricerca['modello']=modello;
	}
	
	var nome_modello = jQuery('#param_nome').val();
	if (nome_modello!=""){
		params_ricerca['nome_modello']=nome_modello;
	}
	
//	var fornitore = jQuery('#param_fornitore').val();
//	if (fornitore!=""){
//		params_ricerca['fornitore']=fornitore;
//	}
	
	ricerca = $.toJSON(params_ricerca);
	
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&params_ricerca="+ricerca;
	
	jQuery('#risultati_ricerca').css('width','80%');
	jQuery('#risultati_ricerca').css('margin','auto');
	jQuery('#table_risultati').css('width','100%');
	
	
	
	jQuery('#risultati_ricerca').append(HEADER_TABELLA_RICERCA);

	TABELLA_RICERCA['sAjaxSource'] = url
	var oTable = jQuery('#table_risultati').dataTable(TABELLA_RICERCA);
	
	jQuery("#risultati_ricerca").show();
	
}


function indietro() {
	jQuery('#dettaglio_div').hide();
	jQuery('#ricerca_div').show();
}



function mostraDettaglio(societa, modello) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'render_scheda_modello'; 
	
	jQuery('#ricerca_div').hide();
	
	jQuery("#overlay").show();
	jQuery("#overall_container").show();
	
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&modello="+modello+"&societa="+societa;
	
	jQuery.ajax({
			type: "GET",
			async: true,
			url: url
	}).done( function(msg) {
		jQuery("#overlay").hide();
		jQuery("#overall_container").hide();		 //nascondo il pannello di attesa 
		/*Al ritorno della chiamata verifico il risultato*/
		jQuery('#dettaglio_div').empty();
		
		jQuery('#dettaglio_div').append(msg);
		jQuery('#dettaglio_div').show();
		
		jQuery("#global_tabs").tabs({selected: 0});
	});
		
}


function viewImgModello(societa, modello) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'get_link_img_modello'; 
	
	//jQuery("#overlay").show();
	//jQuery("#overall_container").show();
	
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&modello="+modello+"&societa="+societa;

	jQuery.ajax({
  		type: "GET",
		async: true,
		url: url,
		dataType: "json",
  		success: function(ret) {
			if (ret['result'] != 'OK') { 
				renderJSONresult(ret, '');
			} else {
				
				
				jQuery('#dialog_img').empty();
				
				img = "<img class='click_load_schizzo' height='300px' " +
						"title='Visualizza Schizzo' style='cursor:pointer;' " +
						"alt='Inline image' src='"+ret['message']+"' />"
				jQuery('#dialog_img').append(img);
				

				
				jQuery("#dialog_img").dialog({
					width:'auto',
					height: 'auto',
					title: 'Immagine Modello',
					modal: true,
					buttons: {
				    "Chiudi":function(e,ui) {
						jQuery(this).dialog("close");
						}
					}
				});

			}
		}
	});
		
}




//function importaDB(societa, modello) {
//	var sid = jQuery('#sid').val();
//	var app_server = jQuery('#app_server').val();
//	var menu_id = jQuery('#menu_id').val();
//	var module = jQuery('#module').val();
//	var program = 'render_scheda_modello'; 
//	
//	jQuery('#ricerca_div').hide();
//	
//	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&modello="+modello+"&societa="+societa;
//	
//	jQuery.ajax({
//			type: "GET",
//			async: true,
//			url: url
//	}).done( function(msg) {
//		jQuery("#overall_container").css('visibility', 'hidden');		 //nascondo il pannello di attesa 
//		/*Al ritorno della chiamata verifico il risultato*/
//		jQuery('#dettaglio_div').empty();
//		
//		jQuery('#dettaglio_div').append(msg);
//		jQuery('#dettaglio_div').show();
//		
//		jQuery("#global_tabs").tabs({selected: 0});
//	});
//		
//}


function importaDB(societa, modello) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'render_importa_diba'; 

	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&societa="+societa+"&modello="+modello;
	window.open(url);
}


//function mostraDB(societa, modello) {
//	var sid = jQuery('#sid').val();
//	var app_server = jQuery('#app_server').val();
//	var menu_id = jQuery('#menu_id').val();
//	var module = jQuery('#module').val();
//	var program = 'render_db_modello_originale'; 
//	
//	jQuery('#ricerca_div').hide();
//	
//	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&modello="+modello+"&societa="+societa;
//	
//	jQuery.ajax({
//			type: "GET",
//			async: true,
//			url: url
//	}).done( function(msg) {
//		jQuery("#overall_container").css('visibility', 'hidden');		 //nascondo il pannello di attesa 
//		/*Al ritorno della chiamata verifico il risultato*/
//		jQuery('#dettaglio_div').empty();
//		
//		jQuery('#dettaglio_div').append(msg);
//		jQuery('#dettaglio_div').show();
//	});
//		
//}












