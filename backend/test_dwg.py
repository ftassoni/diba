# -*- coding: UTF-8 -*-
import sys
#sys.path += [sys.path[0]+'/..']
sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/prorosa/bin')
sys.path.insert(2, '/home/re/mp/bin/workflow_mmfg/')


import db_access
import tools
import itertools
import operator
import config
import simplejson as json
import time
import copy
import re
import math
import notifica_mail
import ws_db_gruppo
import pprint
import mmfg_ws_config
import mmfg_ws_client

DEBUG = True

WS_MODULE = 'ws_interface'
WS_PROGRAM = 'exec_query_dt'

LABEL_CONFIG = 'db_gruppo' 
WS_RET_OK = 'Successo'
WS_RET_ERR = 'Fallimento'


def chiama_ws(program, diz_dati, societa, debug=False):

	ws_conf = mmfg_ws_config.get_config(LABEL_CONFIG)
	param_d = {
		'module': WS_MODULE,
		'program': program,
		'societa': societa,
	}
	param_d['data'] = json.dumps(diz_dati)
	r = mmfg_ws_client.send_request(param_d=param_d, ws_conf=ws_conf, debug=False)
	try:
		r = json.loads(r)
		if r[0]['result'] == WS_RET_ERR:
			b = False
		else:
			b = True
	except:
		mess = str(r)
		r = [{
			'result': WS_RET_ERR,
			'message': mess
		}]
		b = False

	return (r,b)


def main(pard):
	societa = 'MM'
	diz_dati = ["""select m.*, a.*, uc.*
				from mod_modello m 
				left join mod_uscita_collezione uc on m.societa=uc.societa and m.uscita_collezione=uc.uscita_collezione and m.anno=uc.anno and m.stagione=uc.stagione
				left join mod_articolo a on m.societa=a.societa and m.articolo=a.articolo
				where m.societa='%(societa)s' limit 1""" %vars()]
	pprint.pprint(chiama_ws(WS_PROGRAM , diz_dati, societa, debug=False))


if __name__ == "__main__":
	import config
	pard = config.global_parameters({})
	main(pard)
	
