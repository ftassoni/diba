# coding=utf-8
from datetime import datetime
import time
import itertools
import tools
import anno_stagione_lib

from Common import business_object
from Common.errors import BOException
from facon.diba.dao import diba_dao
from facon.ordpf.facade.pool_manager import Request




class DiBaEsplosa(business_object.BusinessObject):

	def __init__(self, data):
		pard = Request().get_pard()
		super(DiBaEsplosa, self).__init__(pard, data)
		self.set(data)

	def set(self, data):
		self.id = data.get('id', 0)
		self.modello_dt = str(data.get('modello_dt', ''))
		self.variante_dt = str(data.get('variante_dt', ''))
		self.societa_orig = str(data.get('societa_orig', ''))
		self.modello_orig = str(data.get('modello_orig', ''))
		self.ml = ''
		if self.modello_orig:
			self.ml = self.modello_orig[0]+ self.modello_orig[7]
		self.variante_orig = str(data.get('variante_orig', ''))
		self.nome_modello_orig = str(data.get('nome_modello_orig', ''))
		self.societa = str(data.get('societa', ''))
		self.anno_stagione = str(data.get('anno_stagione', ''))
		self.gm = str(data.get('gm', ''))
		self.articolo = str(data.get('articolo', ''))
		self.id_articolo = str(data.get('id_articolo', ''))
		self.misura = str(data.get('misura', ''))
		self.colore = str(data.get('colore', ''))
		self.capocmat = str(data.get('capocmat', ''))
		if self.capocmat and len(self.capocmat) == 15:
			self.societa = self.capocmat[:2]
			self.anno_stagione = anno_stagione_lib.decode_as(self.capocmat[4:6])
			self.gm = self.capocmat[2:4]
			self.articolo = self.capocmat[6:9]
			self.misura = self.capocmat[9:11]
			self.colore = self.capocmat[11:]
		self.descrizione = data.get('descrizione', '')
		self.macrocolore = str(data.get('macrocolore', ''))
		self.colore_descr = str(data.get('colore_descr', ''))
		self.consumo = float(data['consumo']) if data.get('consumo', '') != '' else 0.00
		self.consumo_dettaglio = str(data.get('consumo_dettaglio',''))
		self.taglia_base = str(data.get('taglia_base', ''))
		self.colonna = str(data.get('colonna', ''))
		self.posizione = data.get('posizione', '')
		self.pezzo = str(data.get('pezzo', ''))
		self.numero_riga = str(data.get('numero_riga', ''))


	@staticmethod
	def get_by_key(id):
		res = diba_dao.get_diba_esplosa_by_key(id)
		if not res:
			raise BOException('Distinta base esplosa non trovata %s' %id)

		return DiBaEsplosa(res[0])


	def salva(self):
		self.posizione = self.posizione.encode("latin-1") if type(self.posizione) is unicode else self.posizione
		self.descrizione = self.descrizione.encode("latin-1") if type(self.descrizione) is unicode else self.descrizione

		if self.id and self.id > 0:
			diba_dao.update_diba_esplosa(self.to_dictionary())
		else:
			self.id = diba_dao.insert_diba_esplosa(self.to_dictionary())

	def elimina(self):
		diba_dao.delete_diba_esplosa(self.id)