import simplejson as json
import sys, traceback
import time
import datetime
from datetime import date, timedelta
import caio_print as cp

from Common import errors


SOCIETA_LIST = [{'id':'MM','value':'MaxMara', 'cod': '006'}, 
				{'id':'MA','value':'Marella', 'cod': '002'}, 
				{'id':'MN','value':'Manif. del Nord', 'cod': '003'}, 
				{'id':'DT', 'value':'Diffusione Tessile', 'cod': '005' }]

AS_LIST = [{'id':'2012 P/E',}, {'id':'2012 A/I',}, {'id':'2013 P/E',}, {'id':'2013 A/I',},
			{'id':'2014 P/E',},{'id':'2014 A/I',},{'id':'2015 P/E',},{'id':'2015 A/I',},
			{'id':'2016 P/E',},{'id':'2016 A/I',}, {'id':'2017 P/E',}, {'id':'2017 A/I',},
           {'id': '2018 P/E',}, {'id': '2018 A/I',}, {'id':'2019 P/E',}, {'id':'2019 A/I',}]


TIPO_IMPEGNO = ['ORD', 'SOC', 'DT']
