(function ()
{
	'use strict';

	var controllerId = "PopoverDuplicaCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', '$compile', 'ServiceDatiDuplica'];

	angular.module('app').controller(controllerId, ControllerFn);

	function ControllerFn($scope, $rootScope, LoggerFactory, $compile, ServiceDatiDuplica)
	{
		var vm = this;
        var logger = LoggerFactory.getLogger(controllerId);
		// Qta fabb parziale da digitare nell' input del popover duplica
		vm.qta_fabb_parziale = 0;

		$scope.duplica_riga_articolo = function(){
		    if (vm.qta_fabb_parziale != null && vm.qta_fabb_parziale > 0){
		        if(vm.qta_fabb_parziale <= ServiceDatiDuplica.data_riga_da_duplicare.riga.entity['da_soddisfare']){
                    ServiceDatiDuplica.data_riga_da_duplicare['fabbisogno_parziale'] = vm.qta_fabb_parziale;
                    $rootScope.$emit("popover_duplica:duplica", ServiceDatiDuplica.data_riga_da_duplicare);
                }else{
                    logger.error('Fabbisogno parziale troppo elevato');
                }
		    }else{
		        logger.error('Inserisci un fabbisogno parziale');
		    }

		};
    }
})();