# coding=utf-8
import traceback
from datetime import datetime
import time
import itertools
import tools
import anno_stagione_lib

from Common import business_object
from Common.errors import BOException
from facon.diba.dao import fabbisogni_dao, diba_dao
from facon.diba.bo.distinta_base import DiBaEsplosa
from facon.ordpf.facade.pool_manager import Request
from facon.diba.diba_common import TIPO_IMPEGNO




class Fabbisogno(business_object.BusinessObject):

	def __init__(self, data):
		pard = Request().get_pard()
		super(Fabbisogno, self).__init__(pard, data)
		self.set(data)


	def set(self, data):
		self.set_testata(data)
		self.list_impegni = [Impegno(imp) for imp in data.get('list_impegni', [])]
		qta_lanciata = 0
		capi_lanciati = 0
		for imp in self.list_impegni:
			self.da_soddisfare -= float(imp.qta)
			if imp.id_faccom and str(imp.id_faccom) != '0' and imp.stato == 'valido' and imp.tipo== 'DT':
				qta_lanciata += float(imp.qta_faccom) if imp.qta_faccom else 0
				capi_lanciati += int(imp.capi_faccom) if imp.capi_faccom else 0
		if qta_lanciata > 0:
			self.fabbisogno = qta_lanciata
			self.tot_capi = capi_lanciati



	def set_testata(self, data):
		self.id = str(data.get('id', '0'))
		self.qta_prev = float(data['qta_prev']) if data.has_key('qta_prev') and data.get('qta_prev',0.00) != '' else 0.00  # qta_previsionale, ricavata dagli studi
		self.qta_teor = float(data['qta_teor']) if data.has_key('qta_teor') and data.get('qta_teor', 0.00) != '' else 0.00  # qta_teorica, ricavata dalle rich commerciali
		self.qta_reale = float(data['qta_reale']) if data.has_key('qta_reale') and data.get('qta_reale',0.00) != '' else 0.00  # qta_reale, ricavata dai lanci
		self.capi_prev = int(data['capi_prev']) if data.has_key('capi_prev') and data.get('capi_prev',0.00) != '' else 0.00
		self.capi_teor = int(data['capi_teor']) if data.has_key('capi_teor') and data.get('capi_teor',0.00) != '' else 0.00
		self.capi_lanciati = int(data['capi_lanciati']) if data.has_key('capi_lanciati') and data.get('capi_lanciati',0.00) != '' else 0.00
		self.tot_capi = 0
		if self.capi_lanciati > 0:
			self.tot_capi = self.capi_lanciati
		elif self.capi_teor > 0:
			self.tot_capi = self.capi_teor
		else:
			self.tot_capi = self.capi_prev
		self.fabbisogno = 0
		if self.qta_reale > 0:
			self.fabbisogno = self.qta_reale
		elif self.qta_teor > 0:
			self.fabbisogno = self.qta_teor
		else:
			self.fabbisogno = self.qta_prev
		self.da_soddisfare = self.fabbisogno
		self.stato = str(data.get('stato', ''))
		self.completato = str(data.get('completato', ''))
		self.fornitore = str(data.get('fornitore', ''))
		self.desc_col_fornitore = str(data.get('desc_col_fornitore', ''))
		self.id_diba = str(data.get('id_diba', ''))
		self.id_art_rich_comm = str(data['id_art_rich_comm']) if data.get('id_art_rich_comm', '0') and data['id_art_rich_comm'] else '0'
		self.diba = DiBaEsplosa(data.get('diba', {}))
		self.nota = str(data.get('nota', ''))

	@staticmethod
	def get_fabbisogni_by_tessuto(id_art_rcomm, tessuto):
		fabb = fabbisogni_dao.get_fabbisogno_tessuto(id_art_rcomm, tessuto)
		fab_obj = Fabbisogno(fabb)

		return fab_obj


	@staticmethod
	def get_fabbisogni_diba(id_art_rcomm):
		fabb_list = []
		fabb_diba = fabbisogni_dao.get_fabbisogno_diba(id_art_rcomm)
		for f in fabb_diba:
			f['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(f['id'])
			fab_obj = Fabbisogno(f)
			fabb_list.append(fab_obj)

		return fabb_list


	@staticmethod
	def get_fabbisogni_by_prelievo(id_faccom):
		fabb_list = []
		fabb_diba = fabbisogni_dao.get_fabbisogno_by_prelievo(id_faccom)
		for f in fabb_diba:
			f['list_impegni'] = []
			list_impegni = fabbisogni_dao.get_impegni_by_fabb(f['id'], id_faccom_list=id_faccom)
			for i in list_impegni:
				if i['tipo'] == 'DT' and float(i['qta'])>0.00:
					f['list_impegni'].append(i)
			if f['list_impegni']:
				fab_obj = Fabbisogno(f)
				fabb_list.append(fab_obj)

		return fabb_list

	@staticmethod
	def get_fabbisogni_by_prelievo_bp(id_faccom):
		fabb_list = []
		fabb_diba = fabbisogni_dao.get_fabbisogno_by_prelievo(id_faccom)
		for f in fabb_diba:
			f['list_impegni'] = []
			list_impegni = fabbisogni_dao.get_impegni_by_fabb(f['id'], id_faccom_list=id_faccom)
			for i in list_impegni:
				if i['tipo'] == 'DT' and float(i['qta_faccom'])>0.00:
					f['list_impegni'].append(i)
			if f['list_impegni']:
				fab_obj = Fabbisogno(f)
				fabb_list.append(fab_obj)

		return fabb_list


	@staticmethod
	def get_fabbisogni_by_articolo(params):
		fabb_list = []
		fabbs = fabbisogni_dao.get_fabbisogni_by_articolo_impegno(params)
		for f in fabbs:
			f['diba'] = {}
			diba = diba_dao.get_diba_esplosa_by_key(f['id_diba'])
			if diba:
				f['diba'] = diba[0]
			f['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(f['id'])
			fabb_obj = Fabbisogno(f)
			fabb_list.append(fabb_obj)

		return fabb_list


	@staticmethod
	def get_fabbisogno_by_ordine(id_facord_dett):
		fabb = fabbisogni_dao.get_fabbisogno_by_id_facord_dett(id_facord_dett)
		if not fabb:
			return None

		if len(fabb)>1:
			raise BOException("Errore: impossibile avere piu fabbisogni legati allo stesso ordine.")
		else:
			f = fabb[0]
			f['diba'] = {}
			diba = diba_dao.get_diba_esplosa_by_key(f['id_diba'])
			if diba:
				f['diba'] = diba[0]
			f['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(f['id'])
			fabb_obj = Fabbisogno(f)
			return fabb_obj


	@staticmethod
	def get_impegni_by_fabbisogno(fabb):
		fabb_obj = None
		if fabb.get('id', '') and fabb['id']:
			fabb['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(fabb['id'])
			fabb_obj = Fabbisogno(fabb)

		return fabb_obj


	@staticmethod
	def get_by_key(id, id_faccom_list=''):
		res = fabbisogni_dao.get_fabbisogno_by_key(id)
		if not res:
			raise BOException('Fabbisogno non trovato %s' %id)

		fab_diz = res[0]
		fab_diz['diba'] = {}
		diba = diba_dao.get_diba_esplosa_by_key(fab_diz['id_diba'])
		if diba:
			fab_diz['diba'] = diba[0]
		fab_diz['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(id, id_faccom_list)
		fabb_obj = Fabbisogno(fab_diz)

		return fabb_obj


	@staticmethod
	def get_by_id_diba(id_diba):
		res = fabbisogni_dao.get_fabbisogno_by_id_diba(id_diba)
		if not res:
			raise BOException('Fabbisogni non trovati %s' %id_diba)

		fabb_list = []
		for r in res:
			fab_diz = r
			fab_diz['diba'] = {}
			diba = diba_dao.get_diba_esplosa_by_key(fab_diz['id_diba'])
			if diba:
				fab_diz['diba'] = diba[0]
			fab_diz['list_impegni'] = fabbisogni_dao.get_impegni_by_fabb(r['id'])
			fabb_obj = Fabbisogno(fab_diz)
			fabb_list.append(fabb_obj)

		return fabb_list

	def check_giacenza_negativa(self):
		from facon.pao.facade.giacenza_mp import GiacenzaMPFacade
		facade_giacenza = GiacenzaMPFacade(Request().get_pard())
		facade_giacenza.check_giacenza_negativa_da_fabb(self.to_dictionary())

	def salva(self, FLAG_CHECK=True):
		self.set_completato()
		if self.id != '0' and self.id != '':
			self.diba.salva()
			fabbisogni_dao.update_fabbisogno(self.to_dictionary())
		else:
			self.diba.salva()
			self.id_diba = self.diba.id
			self.id = fabbisogni_dao.insert_fabbisogno(self.to_dictionary())

		presenza_bp = self.check_presenza_bp()
		# if presenza_bp:
		# 	self.scala_impegni_prev_e_teor()
		self.check_numero_impegni()
		for imp in self.list_impegni:
			imp.salva(self.id, presenza_bp)

		if presenza_bp:
			# in caso di id_faccom valorizzati
			# devo aggiornare la fabb.qta_reale come somma delle qta_faccom dei vari impegni sui bp
			# ma non ho il fabbisongo completo, solo gli impegni del FE, quindi va ricalcolato dopo il salva
			self.aggiorna_fabbisogno_reale()
		if FLAG_CHECK:
			self.check_giacenza_negativa()

	def aggiorna_fabbisogno_reale(self):
		res = fabbisogni_dao.get_qta_reale(self.id)
		if res:
			self.qta_reale = float(res[0]['qta_reale'])
		fabbisogni_dao.update_fabbisogno(self.to_dictionary())


	def check_numero_impegni(self):
		count = {}
		for imp in self.list_impegni:
			if not count.has_key((imp.tipo, imp.id_faccom)):
				count[(imp.tipo, imp.id_faccom)] = 0
			if imp.stato == 'valido':
				count[(imp.tipo, imp.id_faccom)] += 1

		check_imp = [k for (k, v) in count.items() if v > 1]
		if check_imp:
			raise BOException('Impossibile salvare due impegni dello stesso tipo sul medesimo fabbisogno %s (%s).' % (self.id, check_imp))


	def aggiorna_impegno(self, dict_data):
		for tipo in TIPO_IMPEGNO:
			if dict_data.has_key(tipo):
				trovato = False
				# tools.dump(self.to_dictionary())
				for imp in self.list_impegni:
					# uno stesso fabbisogno puo' essere legato a piu' bp
					# in caso ci sia il bp, devo aggiornare l'impegno corretto
					if dict_data.get('id_faccom', '') and dict_data['id_faccom'] and str(dict_data['id_faccom']) != '0':
						if tipo == imp.tipo and imp.stato == 'valido' and str(imp.id_faccom) == str(dict_data['id_faccom']):
							trovato = True
							if imp.qta != dict_data.get(tipo, '') and str(imp.rif_ord) != '' and str(imp.rif_ord) != '0':
								raise BOException("Impossibile modificare una quantita' legata ad un ordine (riford %s)." %imp.rif_ord)
							imp.qta = dict_data.get(tipo, '')
							imp.qta_faccom = dict_data.get('qta_faccom', '')
							imp.capi_faccom = dict_data.get('capi_faccom', '')
							imp.qta_prelevata = dict_data.get('qta_prelevata', '')
						elif imp.stato == 'valido' and str(imp.id_faccom) == str(dict_data['id_faccom']):
							imp.qta_faccom = dict_data.get('qta_faccom', '')
							imp.capi_faccom = dict_data.get('capi_faccom', '')
					else:
						if tipo == imp.tipo and imp.stato == 'valido':
							trovato = True
							imp.qta = dict_data.get(tipo, '')

				if not trovato:
					diz = {}
					diz['qta'] = dict_data.get(tipo, '')
					diz['tipo'] = tipo
					diz['id_faccom'] = dict_data.get('id_faccom', '')
					diz['qta_faccom'] = dict_data.get('qta_faccom', '')
					diz['capi_faccom'] = dict_data.get('capi_faccom', '')
					diz['qta_prelevata'] = dict_data.get('qta_prelevata', '')
					self.list_impegni.append(Impegno(diz))
		# elimino tutti gli impegni su una variante che non e' piu' quella nel bp
		for imp in self.list_impegni:
			if imp.tipo in ('ORD', 'DT', 'SOC') and imp.id_faccom and imp.id_faccom != '0':
				rec = imp.check_esiste_riga_bp()
				if rec and str(rec['variante']) != '0':
					if str(rec['variante']).zfill(3) != self.diba.variante_dt.zfill(3) or str(rec['capocmat']) != self.diba.capocmat:
						if imp.id:
							imp.elimina()
						else:
							self.list_impegni.remove(imp)

					fabbs = Fabbisogno.get_fabbisogni_by_prelievo(imp.id_faccom)
					for f in fabbs:
						if str(f.id) != str(self.id):
							for i in f.list_impegni:
								if i.id_faccom and str(i.id_faccom) == str(imp.id_faccom) and i.stato == 'valido' and \
									str(i.rif_ord == imp.rif_ord):
									i.elimina()


	def check_presenza_bp(self):
		presenza_bp = False

		for imp in self.list_impegni:
			if imp.id_faccom and str(imp.id_faccom) != '0':
				presenza_bp = True

		return presenza_bp


	def scala_impegni_prev_e_teor(self):
		# una volta creato un bp, devo scalare le qta impegnate con le rich commerciali (teorico e previsionale)
		# perche' il lancio, utilizza come prima risorsa gli impegni di tipo DT creati prima
		# inoltre verifico che esista sempre la riga del bp legata all'impegno, altrimenti elimino l'impegno
		qta_lancio = 0
		qta_dt = 0 # qta liberata dalla cancellazione di una riga di BP
		imp_orig = None
		for imp in self.list_impegni:
			if imp.id_faccom and str(imp.id_faccom) != '0' and imp.stato == 'valido':
				if not imp.check_esiste_riga_bp():
					imp.elimina()
					qta_dt += float(imp.qta)

			if imp.tipo == 'DT' and imp.stato == 'valido':
				if imp.id_faccom and str(imp.id_faccom) != '0':
					qta_lancio += float(imp.qta) if imp.qta else 0
				else:
					imp_orig = imp
		if imp_orig :
			if imp_orig.qta > 0.00:
				imp_orig.qta = imp_orig.qta_orig - qta_lancio
			imp_orig.qta += qta_dt
			if imp_orig.qta < 0.00:
				imp_orig.qta = 0



	def elimina_impegni_bp(self, id_righe_bp):
		qta_dt = 0
		imp_dt = None
		for imp in self.list_impegni:
			if str(imp.id_faccom)!='0' and (str(imp.id_faccom) in id_righe_bp or not imp.check_esiste_riga_bp()):
				imp.elimina()
				qta_dt += float(imp.qta)
			if imp.tipo == 'DT' and imp.stato == 'valido' and not (imp.id_faccom and str(imp.id_faccom) != '0'):
				imp_dt = imp

		if imp_dt:
			imp_dt.qta += qta_dt


	def aggiungi_qta_impegno(self, data):
		trovato = False
		# tools.dump(self.to_dictionary())
		for imp in self.list_impegni:
			if data.get('id_faccom', '') and str(data['id_faccom']) != '0':
				if data['tipo'] == imp.tipo and data['id_faccom'] == imp.id_faccom:
					trovato = True
					imp.qta += data.get('qta', 0.00)
			else:
				if data['tipo'] == imp.tipo:
					trovato = True
					imp.qta += data.get('qta', 0.00)

		if not trovato:
			self.list_impegni.append(Impegno(data))


	def aggiorna_fabbisogno_orig(self, fabb_data):
		if fabb_data['fabbisogno'] <= self.da_soddisfare:
			if self.qta_reale > 0:
				self.qta_reale = self.qta_reale - fabb_data['fabbisogno']
				self.capi_lanciati = int(self.qta_reale / self.diba.consumo)
				self.fabbisogno = self.qta_reale
				self.tot_capi = self.capi_lanciati
			elif self.qta_teor > 0:
				self.qta_teor = self.qta_teor - fabb_data['fabbisogno']
				self.capi_teor = int(self.qta_teor / self.diba.consumo)
				self.fabbisogno = self.qta_teor
				self.tot_capi = self.capi_teor
			else:
				self.qta_prev = self.qta_prev - fabb_data['fabbisogno']
				self.capi_prev = int(self.qta_prev / self.diba.consumo)
				self.fabbisogno = self.qta_prev
				self.tot_capi = self.capi_prev
		else:
			raise BOException("Fabbisogno parziale troppo elevato")


	def arretra_avanza_diba(self, stato):
		if stato:
			self.stato = stato

	def set_attributi_diba(self, elem):
		if elem:
			self.nota = elem['nota']
			self.diba.colore = elem['colore']
			self.diba.colore_descr = elem['colore_descrizione']
			self.diba.macrocolore = elem['macrocolore']
			self.diba.capocmat = str(self.diba.capocmat)[:11] + str(self.diba.colore).zfill(4)
		if self.diba.consumo_dettaglio:
			# aggiorno il consumo e ricalcolo le qta previsionali
			if elem['consumo'] and float(elem['consumo']) > 0.00:
				self.diba.consumo = float(elem['consumo'])
				self.qta_prev = self.capi_prev * float(self.diba.consumo)
				if self.qta_reale > 0:
					self.fabbisogno = self.qta_reale
				elif self.qta_teor > 0:
					self.fabbisogno = self.qta_teor
				else:
					self.fabbisogno = self.qta_prev


	def aggiorna_colore(self, new_colore):
		if new_colore:
			self.diba.colore = new_colore


	def elimina(self):
		self.diba.elimina()
		for imp in self.list_impegni:
			imp.elimina()
		fabbisogni_dao.delete_fabbisogno(self.id)


	def aggiorna_teorico(self, param):
		dati = self.diba.to_dictionary()
		dati.update(param)
		qta_tot_teor = 0
		#qta_tot_prev = 0
		res = fabbisogni_dao.get_tot_capi_teor(dati)
		if res:
			qta_tot_teor = int(res[0]['qta'])
		# res2 = fabbisogni_dao.get_tot_capi_prev(dati)
		# if res2:
		# 	qta_tot_prev = int(res2[0]['qta'])
		# if qta_tot_prev > 0:
		# 	self.capi_teor = int(self.capi_prev * qta_tot_teor / qta_tot_prev)
		# else:
		self.stato = 'valutazione'
		self.capi_teor = qta_tot_teor
		self.qta_teor = self.diba.consumo * self.capi_teor


	def impegna_tessuto(self, id_facord_dett):
		self.stato = 'valutazione'
		diz = {}
		diz['tipo'] = 'ORD'
		diz['qta'] = self.qta_prev
		diz['rif_ord'] = id_facord_dett
		self.list_impegni.append(Impegno(diz))



	def impegna_ordine(self, tipo, id_facord_dett, id_preord):
		trovato = False
		self.stato = 'valutazione'
		for i in self.list_impegni:
			if i.tipo == tipo and i.stato not in ('annullato', 'chiuso') \
					and i.rif_ord != '' and str(i.rif_ord) != '0':
				raise BOException("Impossibile impegnare: ordine gia' esistente (%s)." %i.to_dictionary())
			elif i.tipo == tipo and i.stato == 'valido' \
					and (i.rif_ord == '' or str(i.rif_ord) == '0'):
				i.rif_preord = id_preord
				i.rif_ord = id_facord_dett
				trovato = True
		if not trovato:
			raise BOException("Nessun impegno trovato.")


	def disimpegna_ordine(self, tipo, id_facord_dett):
		trovato = False
		for i in self.list_impegni:
			if i.tipo == tipo and str(i.rif_ord) == id_facord_dett and i.stato == 'valido':
				i.rif_ord = 0
				i.rif_preord = 0
				i.qta = 0
				trovato = True
		if not trovato:
			raise BOException("Nessun impegno trovato da eliminare.")


	def disimpegna_dt(self, qta_da_imp, id_faccom):
		imp_old = None
		imp_giac = None
		diff = qta_da_imp
		for i in self.list_impegni:
			if i.tipo == 'DT' and i.stato == 'valido':
				if str(i.id_faccom) == str(id_faccom):
					imp_old = i
				if not (i.id_faccom and str(i.id_faccom)!='0') and i.qta > 0:
					imp_giac = i
		# controllo se ho un precedente impegno relativo allo stesso bp
		if imp_old:
			qta_da_imp -= imp_old.qta
		# controllo se ho un impegno sulla giacenza (prenotazione)
		if qta_da_imp > 0:
			if imp_giac:
				if imp_giac.qta >= qta_da_imp:
					imp_giac.qta -= qta_da_imp
					diff = 0
				else:
					diff = qta_da_imp - imp_giac.qta
					imp_giac.qta = 0
		else:
			diff = 0

		return diff, qta_da_imp



	def aggiorna_impegno_ordine(self, tipo, id_facord_dett, id_preord, qta, id_faccom =''):
		trovato = False
		for i in self.list_impegni:
			if id_faccom and str(id_faccom) != '0':
				if i.tipo == tipo and i.stato == 'valido' and i.id_faccom == id_faccom:
					i.rif_preord = id_preord
					i.rif_ord = id_facord_dett
					i.qta = qta
					trovato = True
			else:
				if i.tipo == tipo and i.stato == 'valido':
					i.rif_preord = id_preord
					i.rif_ord = id_facord_dett
					i.qta = qta
					trovato = True
		if not trovato:
			new_imp = {}
			new_imp['tipo'] = tipo
			new_imp['rif_ord'] = id_facord_dett
			new_imp['rif_preord'] = id_preord
			if id_faccom:
				new_imp['id_faccom'] = id_faccom
			new_imp['qta'] = qta
			self.list_impegni.append(Impegno(new_imp))


	def set_completato(self):
		self.completato = ''
		qta_impegnata = 0
		for i in self.list_impegni:
			if i.tipo == 'SOC' and i.stato not in ('annullato', 'chiuso') \
					and i.rif_preord != '' and str(i.rif_preord) != '0':
				if i.qta:
					qta_impegnata += float(i.qta)
			if i.tipo == 'ORD' and i.stato not in ('annullato', 'chiuso') \
					and i.rif_ord != '' and str(i.rif_ord) != '0':
				if i.qta:
					qta_impegnata += float(i.qta)
			if i.tipo == 'DT':
				if i.qta:
					qta_impegnata += float(i.qta)

		if self.fabbisogno > 0.00 and float(self.fabbisogno - qta_impegnata) <= 0:
			self.completato = 'S'


	def archivia(self):
		self.stato = 'chiuso'
		self.completato = 'S'
		for imp in self.list_impegni:
			imp.archivia()


class Impegno(business_object.BusinessObject):

	def __init__(self, data):
		pard = Request().get_pard()
		super(Impegno, self).__init__(pard, data)
		self.set(data)


	def set(self, data):
		self.id = str(data.get('id', ''))
		self.tipo = str(data.get('tipo', ''))
		self.qta_orig = float(data.get('qta_orig', '0.00')) if data.get('qta_orig', '') not in ('', 'None', None) else 0.00
		self.qta = float(data.get('qta', '0.00')) if data.get('qta', '') not in ('', 'None', None) else 0.00
		self.rif_ord = str(data.get('rif_ord', '0'))
		self.rif_preord = str(data.get('rif_preord', '0'))
		self.id_faccom = str(data.get('id_faccom', '0'))
		self.capi_faccom = float(data.get('capi_faccom', '0')) if data.get('capi_faccom', '') not in ('', 'None', None) else 0.00
		self.qta_faccom = float(data.get('qta_faccom', '0')) if data.get('qta_faccom', '') not in ('', 'None', None) else 0.00
		self.qta_prelevata = float(data.get('qta_prelevata', '0')) if data.get('qta_prelevata', '') not in ('', 'None', None) else 0.00
		self.stato = str(data.get('stato', 'valido'))
		self.utente = str(data.get('utente', Request().get_val('sid_id_utente')))
		self.utente_mod = Request().get_val('sid_id_utente')
		if self.id_faccom and str(self.id_faccom) != '0':
			self.da_prelevare = self.qta_faccom - self.qta_prelevata
		else:
			self.da_prelevare = self.qta - self.qta_prelevata


	@staticmethod
	def get_by_key(id):
		res = fabbisogni_dao.get_impegno_by_key(id)
		if not res:
			raise BOException('Impegno non trovato %s' % id)
		return Impegno(res[0])


	def check_ordine_preordini(self, dati):
		if dati['rif_ord'] and str(dati['rif_ord']) != '0':
			res = fabbisogni_dao.check_ordine_preordine_in_impegni(dati)
			if res:
				raise BOException("Impossibile impegnare il medesimo ordine su piu' fabbisogni. %s" % res)


	def check_esiste_riga_bp(self):
		res = fabbisogni_dao.check_esiste_riga_bp(self.id_faccom)
		if res:
			return res[0]
		else:
			return {}


	def salva(self, id_fabb, presenza_bp=True):
		dati = self.to_dictionary()
		dati['id_fabb'] = id_fabb
		self.check_ordine_preordini(dati)
		if self.id !='' and str(self.id) != '0':
			if self.tipo == 'DT' and (not self.id_faccom or str(self.id_faccom) == '0') and not presenza_bp:
				# se non ci sono dei lanci vuol dire che sto di sicuro modificando l'impegno precedentemente fatto,
				# quindi aggiorno anche la qta originale (altrimenti non deve essere modificata)
				self.qta_orig = self.qta
			dati.update(self.to_dictionary())
			fabbisogni_dao.update_impegno(dati)
		else:
			# se e' un nuovo impegno inizializzo anche la qta iniziale
			self.utente = Request().get_val('sid_id_utente') if not self.utente else self.utente
			self.qta_orig = self.qta
			dati.update(self.to_dictionary())
			self.id = fabbisogni_dao.insert_impegno(dati)


	def elimina(self):
		if (self.rif_ord and str(self.rif_ord) != '0') or (self.rif_preord and str(self.rif_preord) != '0'):
			id_facord_dett = self.rif_ord if str(self.rif_ord) != '0' else self.rif_preord
			ordine_annullato = fabbisogni_dao.check_ordine_annullato(Request().get_pard(), id_facord_dett)
			if not ordine_annullato and self.qta > 0:
				raise BOException('Impossibile eliminare un impegno legato ad un ordine (%s).'%id_facord_dett)
		if self.id_faccom and str(self.id_faccom) != '0':
			bp_riga = self.check_esiste_riga_bp()
			# In caso di tessuto, controllo la quantita' prelevata sul BP, per impedire l'eliminazione
			# altrimenti controllo la quantita' prelevata sugli impegni
			if bp_riga and ((bp_riga['tipo_riga'] in ('T', 'E') and float(bp_riga['qta_prelevata']) > 0.00) or \
					(bp_riga['tipo_riga'] not in ('T', 'E') and  self.qta_prelevata > 0.00)):
					raise BOException("Impossibile eliminare un impegno legato ad un BP gia' prelevato (%s)."%self.id_faccom)
		self.stato = 'annullato'
		fabbisogni_dao.delete_impegno(self.to_dictionary())


	# def elimina_bp(self):
	# 	if (self.rif_ord and str(self.rif_ord) != '0') or (self.rif_preord and str(self.rif_preord) != '0'):
	# 		id_facord_dett = self.rif_ord if str(self.rif_ord) != '0' else self.rif_preord
	# 		ordine_annullato = fabbisogni_dao.check_ordine_annullato(Request().get_pard(), id_facord_dett)
	# 		if not ordine_annullato:
	# 			raise BOException('Impossibile eliminare un impegno legato ad un ordine (%s).'%id_facord_dett)
	# 	if self.id_faccom and str(self.id_faccom) != '0':
	# 		esiste = self.check_esiste_riga_bp()
	# 		if esiste and self.qta_prelevata > 0.00:
	# 			raise BOException("Impossibile eliminare un impegno legato ad un BP gia' prelevato (%s)."%self.id_faccom)
	# 	self.qta_orig = 0
	# 	self.id_faccom = ''
	# 	self.qta_faccom = 0
	# 	self.capi_faccom = 0
	# 	fabbisogni_dao.update_impegno(self.to_dictionary())


	def archivia(self):
		self.stato = 'chiuso'


