(function ()
{
	'use strict';

	var controllerId = "GestioneImpegniCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', '$timeout', 'LoggerFactory', '$location', '$uibModal', 'RicercaService',
	                        'ServiceDatiVistaGiacenza', 'ServiceDatiDuplica', 'ServiceDatiPopover', 'MACROCOLORE', 'UI_ANNI_STAGIONE_ID', 'uiGridConstants', 'SalvaService',
	                        'AutoCompleteGFService'];

	angular.module('app').controller(controllerId, ControllerFn)
	.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    // Regola la latenza tra eventi scroll nell' aggiunta di nuove tabelle
//    angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 100)

    angular.module('app').config(function($locationProvider) {
      $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
      });
    });
    angular.module('app').service('ServiceDatiVistaGiacenza', function() {
      this.tableRowID = null ;
      this.tableID = null ;
      this.riga = null ;
      this.isFornitore = false;
      this.diba = null;
    });
    angular.module('app').service('ServiceDatiPopover', function() {
      this.clickedButton = null ;
      this.clickedRow = {} ;
    });
    angular.module('app').service('ServiceDatiDuplica', function() {
      this.data_riga_da_duplicare = null ;
    });

	function ControllerFn($scope, $rootScope, $timeout, LoggerFactory, $location, $uibModal, RicercaService,
	                      ServiceDatiVistaGiacenza, ServiceDatiDuplica, ServiceDatiPopover, MACROCOLORE, UI_ANNI_STAGIONE_ID, uiGridConstants, SalvaService,
	                      AutoCompleteGFService)
	{

		var vm = this;
		var logger = LoggerFactory.getLogger(controllerId);
//		$scope.TablesLimit = 2;
		$scope.pageOffset = 300;
//		$scope.addTableDisabled = false;
		$scope.dataTabelleList = [];
        $scope.sottoGridSelections = [];
        $scope.nuovaRiga = {
            a_s: "", articolo: "", articolo_fornitore: "", short_capocmat: "", capocmat: "",
            colonna: "", colore: "", colore_descr: "", consumo: 0, descrizione: "",
            gm: "", id:"", id_articolo: "", macrocolore : "", misura: "", modello_dt: "",
            modello_orig: "", numero_riga: "", pezzo: "",
            posizione: "", societa: "", societa_orig: "",
            taglia_base: "", variante_dt: "", variante_orig: "",
            tipo: 'Nuovo', fornitori_list: []
        }
        $scope.loadingMisure = false;
        $scope.loadingColori = false;
        $scope.disableSbloccaImpegni = false;
        $scope.confirmImpegnaMulti = '';
        $scope.testoConfirmImpegnaSelezionatiGlobal = function (bozza_trovati, openConfirmBP) {
        var testo = '';
            if (bozza_trovati && openConfirmBP) {
                testo = 'Sicuro di voler impegnare articoli in stato bozza e di modificare i buoni prelievo associati?'
            } else if (bozza_trovati) {
                testo = 'Sicuro di voler impegnare articoli in stato bozza?'
            } else if (openConfirmBP) {
                testo = 'Sicuro di voler modificare i buoni prelievo associati?'
            }
            return testo;
        };

        $scope.initTableSelections = function(tableNumID, selectedIndexesList){
            if (selectedIndexesList != undefined && selectedIndexesList.length > 0){
                angular.forEach(selectedIndexesList, function(index) {
                    $timeout(function(){
                        $scope['sottoGridApi'+ tableNumID].selection.selectRowByVisibleIndex(index);
                    });
                });
            }
        };

//        $scope.addTable = function () {
//            console.log('scrolled');
//            $scope.loadingTables = true;
//            $scope.TablesLimit +=1;
//            if ($scope.TablesLimit < $scope.dataTabelleList.length && $scope.dataTabelleList.length > 0){
//                $scope.addTableDisabled = true;
//                $timeout(function(){
//                    $scope.addTableDisabled = false;
//                    $scope.loadingTables = false;
//                });
//            }else if ($scope.TablesLimit == $scope.dataTabelleList.length){
//                $scope.addTableDisabled = true;
//                $scope.loadingTables = false;
//            }
//        };

        $scope.testoConfirmImpegnaSelezionatiRow = function (rowEntity) {
            var testo = '';
            if (rowEntity.stato == 'bozza' && rowEntity.openConfirmBP != undefined && rowEntity.openConfirmBP) {
                testo = 'Sicuro di voler impegnare articoli in stato bozza e di modificare il BP '+String(rowEntity.rif_bp)+' associato?'
            } else if (rowEntity.stato == 'bozza') {
                testo = 'Sicuro di voler impegnare articoli in stato bozza?'
            } else if (rowEntity.openConfirmBP != undefined && rowEntity.openConfirmBP) {
                testo = 'Sicuro di voler modificare il BP '+String(rowEntity.rif_bp)+' associato?'
            }
            return testo;
        };

        $scope.popoverFornitore = {
            content: '',
            templateUrl: 'popoverFornitoreTemplate.html',
            title: 'Scegli Fornitore',
            placement: 'bottom',
        };

        $scope.popoverDuplica = {
            content: '',
            templateUrl: 'popoverDuplicaTemplate.html',
            title: 'Inserisci quantità fabbisogno parziale',
            placement: 'left',
        };

        var COLONNE_CUSTOM = [
            {displayName: '', field: 'apri_dettaglio', enableSorting: false,
                    category:"dettaglio", width: '2%',
                    cellTemplate: "<div style= \"height:32px \" "+
                                "class=\"ui-grid-row-header-cell\">"+
                                "<div class=\"ui-grid-cell-contents\" style=\"text-align: center\">"+
                                '<button uib-tooltip="Dettaglio" tooltip-placement="right" ng-hide="{{row.entity.tipo === \'Principale\'}}" ng-click=\"grid.appScope.apriDialogDettaglio(row)\"'+
                                    'class="btn btn-xs btn-default stretto">'+
                                    '<i class="fa fa-search"></i>'+
                                '</button>'+
                                "</div></div>"
            },
            {displayName: 'Capocmat Orig.', field: 'capocmat', category:"generic",
                            width: '5%',
                            enableCellEdit: false,
			                cellTemplate: '<div class="ui-grid-cell-contents" ng-model-options="{ debounce: 250 }" ng-model="row.entity.capocmat">'+
                            '<input class="form-control" type="text" maxlength="9" ng-model-options="{ debounce: 250 }" ng-model="row.entity.short_capocmat" ng-change="grid.appScope.componiCapocmat(row, \'input-change\')" ng-disabled="{{row.entity.principale}}">'+
                            '</div>'

            },
			{displayName: 'Mis', field: 'misura', category:"generic",
			                enableCellEdit: false,
			                cellTemplate: '<div class="ui-grid-cell-contents">'+
			                '<script type="text/ng-template" id="customTemplateMisure.html">'+
                                '<a>'+
                                    '<span ng-bind-html="match.model.mis | uibTypeaheadHighlight:query"></span>'+
                                '</a>'+
                            '</script>'+
                            '<input class="form-control" type="text" maxlength="2" ng-model-options="{ debounce: 250 }" ng-model="row.entity.misura" ng-change="grid.appScope.componiCapocmat(row, \'input-change\')" '+
                            'uib-typeahead="colore as colore.mis for colore in grid.appScope.getMisure(row, $viewValue)" typeahead-append-to-body="true"'+
                                           'typeahead-loading="loadingMisure" typeahead-min-length="1" typeahead-wait-ms="0" '+
                                           'typeahead-on-select="grid.appScope.onSelectMisura(row, $item, $model, $label)" '+
                                           'typeahead-template-url="customTemplateMisure.html" />'+
                                   '<i ng-show="grid.appScope.loadingMisure" class="fa fa-refresh"></i> '+
                            '</div>'
            },
			{displayName: 'Macrocol', field: 'macrocolore', category:'generic',
			                enableCellEdit: true,
			                editableCellTemplate: 'ui-grid/dropdownEditor',
                            editDropdownValueLabel: 'name',
                            editDropdownOptionsArray: MACROCOLORE,

			},
			{displayName: 'Col', field: 'colore', category:'generic',
			                enableCellEdit: false,
			                cellTemplate: '<div class="ui-grid-cell-contents">'+
			                '<script type="text/ng-template" id="customTemplateColori.html">'+
                                '<a>'+
                                    '<span ng-bind-html="match.model.col_capocmat | uibTypeaheadHighlight:query"></span>'+
                                '</a>'+
                            '</script>'+
                            '<input class="form-control" type="text" maxlength="4" ng-model-options="{ debounce: 250 }" ng-model="row.entity.colore" ng-change="grid.appScope.componiCapocmat(row, \'input-change\')" '+
                                    'uib-typeahead="colore as colore.col_capocmat for colore in grid.appScope.getColore(row, $viewValue)" typeahead-append-to-body="true"'+
                                           'typeahead-loading="loadingColori" typeahead-min-length="1" typeahead-wait-ms="0" '+
                                           'typeahead-on-select="grid.appScope.onSelectColore(row, $item, $model, $label)" '+
                                           'typeahead-template-url="customTemplateColori.html" />'+
                                   '<i ng-show="grid.appScope.loadingColori" class="fa fa-refresh"></i> '+
                            '</div>'
            },
			{displayName: 'Tipo', field: 'tipo',
			                category:'generic',
			                width: '6%',
			                enableCellEdit: false,
			                cellTemplate: '<div class="ui-grid-cell-contents" ng-model-options="{ debounce: 250 }" ng-model="row.entity.tipo"'+
                               'ng-class="{\'table-warning-class\': row.entity.tipo === \'Nuovo\'}">{{row.entity.tipo}}'+
                            '</div>'
            },
			{displayName: 'Fornitore Princ.', field: 'fornitore',
			            width: '10%',
			            category:'generic',
			            enableCellEdit: false,
            },
			{displayName: 'Articolo Forn.', field: 'articolo_fornitore',  width: '10%', category:'generic', enableCellEdit: false},
			{displayName: 'Altri Forn.', field: 'fornitore_popover',
			            width: '5%',
			            category:'generic',
			            enableCellEdit: false,
			            cellTemplate:
                        '<div class="ui-grid-cell-contents" align="center">'+
                            '<script type="text/ng-template" id="popoverFornitoreTemplate.html">'+
                                '<div class="form-group" ng-controller="PopupFornitoriCtrl as popUpCtrl">'+
                                '<button class="btn btn-xs btn-danger button-popover-close" ng-click="closePopover($event)">X</button>'+
//                                  '<label>Popup Title:</label>'+
                                  '<div id="gridPopoverFornitori" ui-grid="grid.appScope.grigliaPopoverFornitori" style="height:260px;"'+
                                  'ui-grid-auto-resize ui-grid-resize-columns ui-grid-cellnav'+
                                  'class="MyGrid"></div>'+
                                '</div>'+
                            '</script>'+
                            '<button id="scegliFornitore" uib-tooltip="Lista fornitori" tooltip-placement="left" ng-disabled="row.entity.fornitori_list.length === 0" ng-click="grid.appScope.scegliFornitore($event, row)" style="text-overflow: ellipsis;"'+
                                'popover-class="popover-fornitore" popover-placement="{{grid.appScope.popoverFornitore.placement}}" uib-popover-template="grid.appScope.popoverFornitore.templateUrl"'+
                                'popover-title="{{grid.appScope.popoverFornitore.title}}" popover-append-to-body='+true+' type="button" class="btn btn-xs btn-default">'+
                                '<i class="fa fa-arrow-circle-down"></i>'+
                            '</button>'+
                        '</div>'

            },
			{displayName: 'Fabb. Generale', field: 'FABB', category:'column8', width:'6%', subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Giac.', field: 'GIAC_DT', category:'dt_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Imp', field: 'IMP_DT', category:'dt_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Liberi', field: 'FREE_DT', category:'dt_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Giac.', field: 'GIAC_SOC', category:'societa_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Imp', field: 'IMP_SOC', category:'societa_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Liberi', field: 'FREE_SOC',  category:'societa_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Da Arr.', field: 'GIAC_ORD',  category:'ordini_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Imp', field: 'IMP_ORD', category:'ordini_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Liberi', field: 'FREE_ORD', category:'ordini_group',subcategory: 'none1', enableCellEdit: false},
			{displayName: 'Azioni', field: 'azioni', width: '5%', category:'azioni',
                enableHiding: false,
                enableCellEdit: false,
                enableSorting: false,
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<button uib-tooltip="Sostituisci Capocmat" tooltip-placement="left" ng-click="grid.appScope.sostituisci_capocmat(row)"'+
                        'class="btn btn-xs btn-primary stretto">'+
                        '<i class="fa fa-arrow-circle-down"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Sblocca Impegni" tooltip-placement="left" ng-disabled="grid.appScope.sbloccaImpegniDisableCondition(row)" ng-click="grid.appScope.sblocca_impegni(row)"'+
                        'class="btn btn-xs btn-default stretto">'+
                        '<i class="fa fa-unlock"></i>'+
                    '</button>'+
                '</div>'
            }
		];

		var COLONNE_TAB_SOTTOSTANTE = [
		    {displayName: 'Stato', field: 'stato', category:"generic", width: '2%',
		        cellTemplate: '<div class="ui-grid-cell-contents center-column-text">'+
                   '<i ng-show="{{row.entity.stato == \'bozza\'}}"'+
                    'tooltip-placement="right" ng-attr-uib-tooltip="{{row.entity.stato==\'bozza\' ? \'Bozza\' : \'\'}}"'+
                     'class="fa fa-flag red-flag-bozza" aria-hidden="true"></i>'+
                '</div>'
		    },
            {displayName: 'Societa.', field: 'diba.societa', category:"generic", width: '2%' },
			{displayName: 'Gm', field: 'diba.gm', category:"generic", width: '2%'},
			{displayName: 'AS', field: 'diba.anno_stagione', category:'generic', width: '4%'},
			{displayName: 'Articolo', field: 'diba.articolo', category:'generic', width: '3%'},
			{displayName: 'Mis', field: 'diba.misura', category:'generic', width: '2%'},
			{displayName: 'Colore', field: 'diba.colore', category:'generic', width: '3%'},
			{displayName: 'Col. Desc', field: 'diba.colore_descr', category:'generic', width: '6%'},
			{displayName: 'Col. Forn. Desc.', field: 'desc_col_fornitore', category:'generic', width: '6%'},
			{displayName: 'Ml', field: 'diba.ml', category:'generic', width: '2%'},
			{displayName: 'Cod. Mod.', field: 'diba.modello_orig', category:'generic'},
			{displayName: 'Vr', field: 'diba.variante_dt', category:'generic', width: '2%'},
			{displayName: 'Nome Mod.', field: 'diba.nome_modello_orig', category:'generic',

			    cellTemplate: '<div class="ui-grid-cell-contents" ng-model-options="{ debounce: 250 }" ng-model="row.entity.diba.nome_modello_orig"'+
                   'ng-class="{\'table-scorta-sede-class\': row.entity.diba.nome_modello_orig === \'Scorta sede\'}">{{row.entity.diba.nome_modello_orig}}'+
                '</div>'

			},
			{displayName: 'Bp.', field: 'rif_bp', category:'generic'},
			{displayName: 'Cons.', field: 'diba.consumo', width: '2%',
			                category:'generic',
			                aggregationType: uiGridConstants.aggregationTypes.sum,
			                footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue() | number:2 }}</div>'
            },
			{displayName: 'Tot. Capi.', field: 'tot_capi',
			            category:'generic',
			            aggregationType: uiGridConstants.aggregationTypes.sum,
			            footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue()}}</div>'
            },
			{displayName: 'Fabbisogno', field: 'fabbisogno',
			            headerCellClass: 'tot-fabb-custom-style',
			            category:'generic',
			            cellClass: 'par-fabb-custom-style',
			            cellTemplate:
                        '<div class="ui-grid-cell-contents" ng-model-options="{ debounce: 250 }" ng-model="row.entity.fabbisogno">'+
                        '{{row.entity.fabbisogno}}'+
                        '</div>',
			            aggregationType: uiGridConstants.aggregationTypes.sum,
                        footerCellTemplate: '<div class="ui-grid-cell-contents tot-fabb-custom-style">Tot: {{col.getAggregationValue() | number:2 }}</div>'
            },
			{displayName: 'Da Sodd.',
			            field: 'da_soddisfare',
			            category:'generic', aggregationType: uiGridConstants.aggregationTypes.sum,
			            CellFilter: 'number:2',
			            cellTemplate:
                        '<div class="ui-grid-cell-contents" ng-model-options="{ debounce: 250 }" ng-model="row.entity.da_soddisfare">'+
                        '{{row.entity.da_soddisfare}}'+
                        '</div>',
                        footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue() | number:2 }}</div>'
            },
			{displayName: 'DT', field: 'DT',
			                category:'col_giacenze',
                            subcategory: 'none1',
                            CellFilter: 'number:2',
                            headerCellClass: 'dt-column dt-column-header',
                            cellClass: 'dt-column',
                            aggregationType: uiGridConstants.aggregationTypes.sum,
                            footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue() | number:2 }}</div>',
                            cellTemplate: '<div class="ui-grid-cell-contents">'+
                            '<input class="form-control" type="number" min="0" ng-class="{\'cell-changed-class\': grid.appScope.isQtaDtChanged(row, \'InputClass\')}" ng-disabled="grid.appScope.getQtaDTCondition(row, \'CellDisabled\')"  ng-model-options="{ debounce: 250 }" ng-model="row.entity.DT" ng-change="grid.appScope.qtaChange(row)">'+
                            '</div>',

            },
			{displayName: 'Societa', field: 'SOC',
			                category:'col_giacenze',
			                subcategory: 'none1',
			                CellFilter: 'number:2',
			                headerCellClass: 'soc-column soc-column-header',
			                cellClass: 'soc-column',
			                aggregationType: uiGridConstants.aggregationTypes.sum,
			                footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue() | number:2 }}</div>',
			                cellTemplate: '<div class="ui-grid-cell-contents">'+
                            '<input class="form-control" type="number" min="0" ng-class="{\'cell-warning-class\': grid.appScope.getQtaSocCondition(row, \'InputClass\'), \'cell-changed-class\': grid.appScope.isQtaSocChanged(row, \'InputClass\')}" ng-disabled="grid.appScope.getQtaSocCondition(row, \'CellDisabled\')" ng-model-options="{ debounce: 250 }" ng-model="row.entity.SOC" ng-change="grid.appScope.qtaChange(row)">'+
                            '</div>'
            },
			{displayName: 'Ordini', field: 'ORD',
			            category:'col_giacenze',
			            subcategory: 'none1',
			            CellFilter: 'number:2',
			            headerCellClass: 'ord-column ord-column-header',
			            cellClass: 'ord-column',
			            aggregationType: uiGridConstants.aggregationTypes.sum,
			            footerCellTemplate: '<div class="ui-grid-cell-contents">Tot: {{col.getAggregationValue() | number:2 }}</div>',
			            cellTemplate: '<div class="ui-grid-cell-contents">'+
                            '<input class="form-control" type="number" min="0" ng-class="{\'cell-warning-class\': grid.appScope.getQtaOrdCondition(row, \'InputClass\'), \'cell-changed-class\': grid.appScope.isQtaOrdChanged(row, \'InputClass\')}" ng-disabled="grid.appScope.getQtaOrdCondition(row, \'CellDisabled\')" ng-model-options="{ debounce: 250 }" ng-model="row.entity.ORD" ng-change="grid.appScope.qtaChange(row)">'+
                            '</div>'
            },
            {displayName: 'Rif.Preord.', field: 'id_fac_preord_d', enableHiding: false,
                width: '7%',
                category:'col_riford',
                enableCellEdit: false,
                cellTemplate:
                '<div class="ui-grid-cell-contents" tooltip-placement="left" ng-attr-uib-tooltip="{{row.entity.id_fac_preord_d!==\'\' ? \'Modifica Preordine\' : \'\'}}" ng-click="row.entity.id_fac_preord_d!==\'\' ? grid.appScope.apriModificaOrdine(row.entity.id_preord) : \'\'" ng-model-options="{ debounce: 250 }" ng-model="row.entity.id_fac_preord_d" ng-class="(row.entity.id_fac_preord_d!==\'\') ? \'riford-pointer\' : \'\'">'+
                    '<a ng-show="row.entity.id_fac_preord_d!==\'\' && row.entity.id_fac_preord_d!== undefined">{{row.entity.id_fac_preord_d | limitTo:2:4}}/{{row.entity.id_fac_preord_d | limitTo:2:2}}/{{row.entity.id_fac_preord_d | limitTo:2:0}} - {{row.entity.id_fac_preord_d | limitTo:6:6}}</a>'+
                '</div>'
            },
            {displayName: 'Rif.Ord.', field: 'id_fac_d', enableHiding: false,
                width: '7%',
                category:'col_riford',
                enableCellEdit: false,
                cellTemplate:
                '<div class="ui-grid-cell-contents" tooltip-placement="left" ng-attr-uib-tooltip="{{row.entity.id_fac_d!==\'\' ? \'Modifica Ordine\' : \'\'}}" ng-click="row.entity.id_fac_d!==\'\' ? grid.appScope.apriModificaOrdine(row.entity.id_ord) : \'\'" ng-model-options="{ debounce: 250 }" ng-model="row.entity.id_fac_d" ng-class="(row.entity.id_fac_d!==\'\') ? \'riford-pointer\' : \'\'">'+
                    '<a ng-show="row.entity.id_fac_d!==\'\' && row.entity.id_fac_d!== undefined">{{row.entity.id_fac_d | limitTo:2:4}}/{{row.entity.id_fac_d | limitTo:2:2}}/{{row.entity.id_fac_d | limitTo:2:0}} - {{row.entity.id_fac_d | limitTo:6:6}}</a>'+
                '</div>'
            },
			{displayName: 'Azioni', field: 'azioni', category:'azioni',
                enableHiding: false,
                enableCellEdit: false,
                enableSorting: false,
				cellTemplate:
                '<div class="ui-grid-cell-contents" align="center">'+
                    '<script type="text/ng-template" id="popoverDuplicaTemplate.html">'+
                        '<div class="form-group" ng-controller="PopoverDuplicaCtrl as popOvCtrl">'+
//                                  '<label>Popup Title:</label>'+
                            '<form class="form-inline">'+
                              '<input type="number" min="0" step="0.01" class="form-control" ng-model-options="{ debounce: 250 }" ng-model="popOvCtrl.qta_fabb_parziale" placeholder="ins. qta" style="margin-right: 5px">'+
                              '<button type="submit" class="btn btn-xs btn-primary" ng-click="duplica_riga_articolo()">duplica</button>'+
                            '</form>'+
                        '</div>'+
                    '</script>'+
                    '<button id="duplicaButton" uib-tooltip="Duplica" tooltip-placement="left" ng-hide="grid.appScope.fabbAzioniDisableCondition(row, \'Duplica\')" ng-click="grid.appScope.apri_duplica_articolo($event, row)"'+
                        'popover-trigger="outsideClick" popover-class="popover-duplica" popover-placement="{{grid.appScope.popoverDuplica.placement}}" uib-popover-template="grid.appScope.popoverDuplica.templateUrl"'+
                        'popover-title="{{grid.appScope.popoverDuplica.title}}" popover-append-to-body='+false+' type="button" class="btn btn-xs btn-default">'+
                        '<i class="fa fa-files-o"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Impegna" tooltip-placement="left" '+
                        'ng-if="!grid.appScope.testoConfirmImpegnaSelezionatiRow(row.entity)"'+
                        'ng-hide="grid.appScope.fabbAzioniDisableCondition(row, \'Impegna\')" '+
                        'ng-click="grid.appScope.impegnaMultiArticoloPannello(row.entity)"'+
                        'class="btn btn-xs btn-info stretto">'+
                        '<i class="fa fa-lock"></i>'+
                    '</button>'+
                    '<button type="button" uib-tooltip="Impegna" tooltip-placement="left"'+
                            'ng-if="grid.appScope.testoConfirmImpegnaSelezionatiRow(row.entity)"'+
                            'ng-hide="grid.appScope.fabbAzioniDisableCondition(row, \'Impegna\')" '+
                            'class="btn btn-xs btn-info stretto" '+
                            'action="grid.appScope.impegnaMultiArticoloPannello(row.entity)"'+
                            'rel="{{grid.appScope.testoConfirmImpegnaSelezionatiRow(row.entity)}}"'+
                            'confirm>'+
                            '<i class="fa fa-lock"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Disimpegna" tooltip-placement="left" '+
                        'ng-hide="grid.appScope.fabbAzioniDisableCondition(row, \'Disimpegna\')" '+
                        'ng-click="grid.appScope.apriDialogDisimpegnaArticoliPannello(row.entity)"'+
                        'class="btn btn-xs btn-danger stretto">'+
                        '<i class="fa fa-unlock-alt"></i>'+
                    '</button>'+
                    '<button uib-tooltip="Elimina" tooltip-placement="left" '+
                        'ng-show="grid.appScope.eliminaDuplicatoCondition(row, \'Duplicato\')" '+
                        'ng-click="grid.appScope.eliminaDuplicato(row)"'+
                        'class="btn btn-xs btn-basic stretto">'+
                        '<i class="fa fa-trash"></i>'+
                    '</button>'+
                '</div>'
            }
		];

        var COL_LIST_FORNITORI = [
			{displayName: 'Fornitori', width:'20%', field: 'conto', category:'generic', enableCellEdit: false},
			{displayName: 'Rag. Soc.', width:'40%', field: 'fornitore', category:'generic', enableCellEdit: false},
			{displayName: 'Art.Forn', width:'40%', category:'generic', field: 'articolo_fornitore', enableCellEdit: false}
		];

        $scope.grigliaPopoverFornitori = {
            showGridFooter: false,
			enableColumnMenus: false,
			enableRowHeaderSelection: false,
			columnDefs: COL_LIST_FORNITORI,
			data: [],
		};

        $scope.CreateGrids = function (receivedTablesObject) {
            $scope.loadingTables = true;
            $scope.dataTabelleList = [];
            $scope.sottoGridSelections = [];
            
            angular.forEach(receivedTablesObject, function(item, key) {
                var tableID = "griddata"+key;
                // defining the grid control
                $scope.dataTabelleList.push({'giacenza':{
                    headerTemplate: '/javascripts/facon/diba/gestione_impegni/header-template.html',
                    expandableRowTemplate: '<div ui-grid="row.entity.subgrigliaImpegni" style="height:250px;" ui-grid-auto-resize ui-grid-resize-columns></div>',
                    enableExpandableRowHeader: false,
                    enableColumnMenus: false,
                    enableRowHeaderSelection: false,
                    treeRowHeaderAlwaysVisible: true,
                    //category definations
                    category: [
                      {name: 'dettaglio', displayName: 'DETTAGLIO', visible: true, showCatName: false},
                      {name: 'generic',displayName: 'Column 1', visible: true,showCatName: false},
                      {name: 'column8',displayName: 'FABB', visible: true,showCatName: true},
                      {name: 'dt_group',displayName: 'DT', cellClass:'dt-column dt-column-header', visible: true,showCatName: true},
                      {name: 'societa_group',displayName: 'SOCIETA', cellClass:'soc-column soc-column-header', visible: true,showCatName: true},
                      {name: 'ordini_group',displayName: 'ORDINI', cellClass:'ord-column ord-column-header', visible: true,showCatName: true},
                      {name: 'azioni', displayName: 'AZIONI', visible: true, showCatName: false},
                    ],
                    subcategory: [
                    // SECONDA INTESTAZIONE OPZIONALE PIENA
                      { name: 'none1', visible: true, showCatName: false},
                    // SECONDA INTESTAZIONE OPZIONALE PIENA
                      { name: 'sub1', displayName: 'Sub1', visible: true, showCatName: true },
                    ],
                    columnDefs: COLONNE_CUSTOM,
                    tableID: tableID,
                    tableNumID: key,
                    diba: item['diba'],
                    list_diba_ids: item['list_diba_ids'],
                    confirmImpegni: false,
                    isVisibleOnStart: key < 1,
                    data: vm.aggiorna_dati_impegni(item['giacenza'])

                }});
                $scope.dataTabelleList[key]['giacenza'].data.push(angular.copy($scope.nuovaRiga));

                //TABELLA SOTTOSTANTE
                var tableID = "sottogriddata"+key;
                $scope.sottoGridSelections.push([]);
                // defining the grid control
                $scope.dataTabelleList[key]['unique_id'] = key;
                $scope.dataTabelleList[key]['fabbList'] = {
                    headerTemplate: '/javascripts/facon/diba/gestione_impegni/header-template.html',
                    treeRowHeaderAlwaysVisible: true,
                    enableColumnMenus: false,
                    enableRowSelection: true,
                    enableSelectAll: true,
                    showColumnFooter: true,
                    tableNumID: key,
                    tableID: tableID,
                    selectedIndexes:[],
                    //category definations
                    category: [
                      {name: 'generic',displayName: 'Column 1', visible: true,showCatName: false},
                      {name: 'col_giacenze',displayName: 'IMPEGNI', visible: true,showCatName: true},
                      {name: 'col_riford',displayName: 'RIFORD', visible: true,showCatName: false},
                      {name: 'azioni', displayName: 'AZIONI', visible: true, showCatName: false},
                    ],
                    subcategory: [
                    // SECONDA INTESTAZIONE OPZIONALE PIENA
                      { name: 'none1', visible: true, showCatName: false },
                    // SECONDA INTESTAZIONE OPZIONALE PIENA
                      { name: 'sub1', displayName: 'Sub1', visible: true, showCatName: true },
                    ],
                    columnDefs: COLONNE_TAB_SOTTOSTANTE,
                    onRegisterApi: function (gridApi)
                    {
                        $scope['sottoGridApi' + key] = gridApi;
                        gridApi.selection.on.rowSelectionChanged($scope,function(row){
                            $scope.sottoGridSelections[key] = gridApi.selection.getSelectedRows();
                            var indiciSelezionati = $scope.dataTabelleList[key]['fabbList']['selectedIndexes']
                            if (row.isSelected && indiciSelezionati.indexOf(row.grid.options.data.indexOf(row.entity)) < 0){
                                indiciSelezionati.push(row.grid.options.data.indexOf(row.entity));
                            }else if (!row.isSelected && indiciSelezionati.indexOf(row.grid.options.data.indexOf(row.entity)) > -1){
                                var indexToRemove = row.grid.options.data.indexOf(row.entity);
                                $scope.dataTabelleList[key]['fabbList']['selectedIndexes'] = jQuery.grep(indiciSelezionati, function(value) {
                                    return value != indexToRemove;
                                });
                            }
                            // assegno true/false alle variabili per avere la confirm sui bottoni impegna
                            var tableID = "griddata"+key;
                            if ($scope.sottoGridSelections[key] != undefined && $scope.sottoGridSelections[key].length > 0){
                                var bozzaTrovatoLocal = false;
                                var bpTrovatoLocal = false;

                                $scope.dataTabelleList[key]['giacenza']['confirmImpegni'] = '';

                                angular.forEach($scope.sottoGridSelections[key], function(item, key) {
                                    if(item['stato'] == 'bozza') bozzaTrovatoLocal = true;
                                    if(item['openConfirmBP'] != undefined && item['openConfirmBP']) bpTrovatoLocal = true;
                                });

                                $scope.dataTabelleList[key]['giacenza']['confirmImpegni'] = $scope.testoConfirmImpegnaSelezionatiGlobal(bozzaTrovatoLocal, bpTrovatoLocal);

                            }else{
                                $scope.dataTabelleList[key]['giacenza']['confirmImpegni'] = ''
                            }

                            var bozzaTrovatoGlobal = false;
                            var bpTrovatoGlobal = false;
                            $scope.confirmImpegnaMulti = '';
                            angular.forEach($scope.sottoGridSelections, function(item, key) {
                                if(item.length > 0){
                                    angular.forEach(item, function(iitem, kkey) {
                                        if(iitem['stato'] == 'bozza') bozzaTrovatoGlobal = true;
                                        if(iitem['openConfirmBP'] != undefined && iitem['openConfirmBP']) bpTrovatoGlobal = true;
                                    });
                                }
                            });
                            $scope.confirmImpegnaMulti = $scope.testoConfirmImpegnaSelezionatiGlobal(bozzaTrovatoGlobal, bpTrovatoGlobal);
                        });

                        gridApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                            $scope.sottoGridSelections[key] = gridApi.selection.getSelectedRows();
                        });


                    },
                    data: item['fabb_list']
//                    isRowSelectable: function(row){
//                        return $scope.fabbAzioniDisableCondition(row, 'Selection');
//                    },
                };
                $timeout(function(){$scope.loadingTables = false;});
            });
        };

        vm.aggiorna_dati_impegni = function(dati_impegni){
            for(var i = 0; i < dati_impegni.length; i++){
                dati_impegni[i].fornitori_list = dati_impegni[i].fornitori_list ? dati_impegni[i].fornitori_list : []
            }
            return dati_impegni;
        };

        $scope.sbloccaImpegniDisableCondition = function (row){
            if (row.entity.capocmat == '' || row.entity.misura == '' || row.entity.colore == '' || $scope.disableSbloccaImpegni)
                return true;
            else
                return false;
        };

        function getDisimpCondition(row){
            var soc = false;
            var ord = false;
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if (item.tipo == 'ORD' && ['0',''].indexOf(item.rif_ord) == -1)
                        ord = true;
                    if (item.tipo == 'SOC' && ['0',''].indexOf(item.rif_preord) == -1)
                        soc = true;
                });
            }
            if (soc || ord){
                return false;
            }else {
                return true;
            }
        };

        $scope.getQtaDTCondition = function(row, from){
            if(row.entity.diba.nome_modello_orig === 'Scorta sede' && from === 'CellDisabled'){
                return true;
            }else{
                return false;
            }
        };

        $scope.getQtaOrdCondition = function(row, from){
            var inputDanger = false;
            var ord = false;
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if (item.tipo == 'ORD' && ['0',''].indexOf(item.rif_ord) == -1)
                        ord = true;
                    else if (item.tipo == 'ORD' && parseInt(row.entity.ORD) > 0 && ['0',''].indexOf(item.rif_preord) >= 0)
                        inputDanger = true;
                });
            }else if (parseInt(row.entity.ORD) > 0){
                inputDanger = true;
            }
            if ((ord && from == 'CellDisabled') || (row.entity.diba.nome_modello_orig === 'Scorta sede' && from == 'CellDisabled')){
                row.entity['apri_disimpegno'] = true;
                return true;
            }else if(inputDanger && from == 'InputClass'){
                return true
            }else {
                return false;
            }
        };

        $scope.getQtaSocCondition = function(row, from){
            var soc = false;
            var inputDanger = false;
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if (item.tipo == 'SOC' && ['0',''].indexOf(item.rif_preord) == -1)
                        soc = true;
                    else if (item.tipo == 'SOC' && parseInt(row.entity.SOC) > 0 && ['0',''].indexOf(item.rif_preord) >= 0)
                        inputDanger = true;
                });
            }else if (parseInt(row.entity.SOC) > 0){
                inputDanger = true;
            }
            if ((soc && from == 'CellDisabled') || (row.entity.diba.nome_modello_orig === 'Scorta sede' && from == 'CellDisabled')){
                return true;
            }else if(inputDanger && from == 'InputClass'){
                return true
            }else {
                return false;
            }
        };

        $scope.isQtaDtChanged = function(row, from){
            var dtChanged = false;
            var trovato = false;
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if(item.tipo == 'DT') trovato = true;
                    if (item.tipo == 'DT' && row.entity.DT != item.qta)
                        dtChanged = true;
                });
                if(!trovato && row.entity.DT > 0){
                    dtChanged = true;
                }
            }else if (row.entity.list_impegni.length === 0 && row.entity.DT > 0){
                dtChanged = true;
            }else {
                dtChanged = false;
            }
            return dtChanged;
        };

        $scope.isQtaSocChanged = function(row, from){
            var socChanged = false;
            var trovato = false
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if(item.tipo == 'SOC') trovato = true;
                    if (item.tipo == 'SOC' && row.entity.SOC != item.qta) {
                        socChanged = true;
                        };
                });
                if(!trovato && row.entity.SOC > 0){
                    socChanged = true;
                }
            }
            else if (row.entity.list_impegni.length === 0 && row.entity.SOC > 0){
                socChanged = true;
            }
            else{
                socChanged = false;
            }
            return socChanged;
        };

        $scope.isQtaOrdChanged = function(row, from){
            var ordChanged = false;
            var trovato = false;
            if (row.entity.list_impegni != undefined && row.entity.list_impegni.length > 0){
                angular.forEach(row.entity.list_impegni, function(item, key) {
                    if(item.tipo == 'ORD') trovato = true;
                    if (item.tipo == 'ORD' && parseInt(row.entity.ORD) != parseInt(item.qta)) {
                        ordChanged = true;
                        trovato = true;
                        };
                });
                if(!trovato && row.entity.ORD > 0){
                    ordChanged = true;
                }
            }else if (row.entity.list_impegni.length === 0 && parseInt(row.entity.ORD) > 0){
                ordChanged = true;
            }else {
                ordChanged = false;
            }
            return ordChanged;
        };

        $scope.eliminaDuplicatoCondition = function (row, from){
            var isEliminabile = false;
            if(row.entity.isDuplicato && row.entity['id'] === '' && from === 'Duplicato'){
                isEliminabile = true;
            };
            return isEliminabile;
        };

        $scope.fabbAzioniDisableCondition = function (row, from){
            if(row.entity.diba.nome_modello_orig === 'Scorta sede' && from === 'Impegna'){
                return true;
            }
            else if (row.entity.isBloccato && from === 'Duplica')
                return true;

            else if (from === 'Disimpegna'){
                var res = false;
                res = getDisimpCondition(row);
                return res;
            }

            else if(row.entity.isDuplicato && (from === 'Duplica' || from === 'Disimpegna'))
                return true;
            else
                return false;
        };

        $scope.scegliFornitore = function($event, row){
            $scope.grigliaPopoverFornitori.data = row.entity.fornitori_list;
            ServiceDatiPopover.clickedButton = $event.currentTarget;
        }

        $scope.componiDizCapocmat = function(capocmat){
            var finalObjCapocmat = {};
		    if (capocmat.length >= 9){
		        finalObjCapocmat['societa'] = capocmat.slice(0,2);
		        finalObjCapocmat['gm'] = capocmat.slice(2,4);
		        finalObjCapocmat['anno_stagione'] = capocmat.slice(4,6);
		        finalObjCapocmat['articolo'] = capocmat.slice(6,9);
		    }
		    return finalObjCapocmat;
		};

        $scope.getColore = function(row, value){
//                var capocmatDiz = $scope.componiDizCapocmat(row.entity.capocmat);
                return AutoCompleteGFService.getColori({
                    'query' : value,
                    'societa' : angular.copy(row.entity.societa),
                    'articolo' : angular.copy(row.entity.articolo),
                    'gm' : angular.copy(row.entity.gm),
                    'a_s' : angular.copy(row.entity.capocmat.slice(4,6)),
//                    'capocmat' : angular.copy(row.entity.capocmat.slice(0,9)),
                    'capocmat' : angular.copy(row.entity.short_capocmat),
                    'macrocolore': angular.copy(row.entity.macrocolore),
                });
        };

        $scope.getMisure = function(row, value){
                return AutoCompleteGFService.getMisure({
                    'query' : value,
//                    'capocmat' : angular.copy(row.entity.capocmat.slice(0,9))
                    'capocmat' : angular.copy(row.entity.short_capocmat),
                });
        };

        $scope.onSelectColore = function(row, item, value, label){
            row.entity['colore'] = item.col_capocmat;
            row.entity['colore_descr'] = item.col_descrizione;
            row.entity['capocmat'] = row.entity['capocmat'].slice(0,9) + row.entity['misura'] + row.entity['colore'];
            row.entity['short_capocmat'] = row.entity['capocmat'].slice(0,9);
            $scope.componiCapocmat(row);

		};

		$scope.onSelectMisura = function(row, item, value, label){
            row.entity['misura'] = item.mis;
            row.entity['capocmat'] = $scope.componiCapocmat(row, 'long');
            row.entity['short_capocmat'] = $scope.componiCapocmat(row, 'short');
            $scope.componiCapocmat(row);
		};

		$scope.componiCapocmat = function(row, tipo){
		    var capocmat = '';
		    if (tipo == 'short')
		        capocmat = row.entity['short_capocmat'];
		    else if (tipo == 'long')
		        capocmat = row.entity['capocmat'].slice(0,9) + row.entity['misura'] + row.entity['colore'];
            else if (tipo == 'input-change'){
                row.entity['capocmat'] = row.entity['short_capocmat'] + row.entity['misura'] + row.entity['colore'];
                if (row.entity['capocmat'].length >= 9){
                    row.entity['societa'] = row.entity['capocmat'].slice(0,2).toUpperCase();
                    row.entity['gm'] = row.entity['capocmat'].slice(2,4);
                    row.entity['a_s'] = row.entity['capocmat'].slice(4,6).toUpperCase();
                    row.entity['anno_stagione'] = '';
                    for (var i=0; i< UI_ANNI_STAGIONE_ID.length; i++){
                        if (row.entity['a_s'] == UI_ANNI_STAGIONE_ID[i].id)
                            row.entity['anno_stagione'] = UI_ANNI_STAGIONE_ID[i].name;
                    };
                    row.entity['articolo'] = row.entity['capocmat'].slice(6,9);
                }
            }
            if(row.entity['short_capocmat'].length == 9 && row.entity['misura'].length == 2 && row.entity['colore'].length == 4){
                    var objToSend = angular.copy(row.entity);
                    var tabellaGiacenza = row.grid.options.data;
        //            objToSend['capocmat'] = angular.copy(objToSend['capocmat'].slice(0,9));

                    RicercaService.ricerca_giacenze(objToSend, function(response){
                        if(response != undefined && response != '' && response.length > 0){
                            row.entity['GIAC_DT'] =  angular.copy(response[0]['GIAC_DT']);
                            row.entity['IMP_DT'] =   angular.copy(response[0]['IMP_DT']);
                            row.entity['FREE_DT'] =  angular.copy(response[0]['FREE_DT']);
                            row.entity['GIAC_SOC'] = angular.copy(response[0]['GIAC_SOC']);
                            row.entity['IMP_SOC'] =  angular.copy(response[0]['IMP_SOC']);
                            row.entity['FREE_SOC'] = angular.copy(response[0]['FREE_SOC']);
                            row.entity['GIAC_ORD'] = angular.copy(response[0]['GIAC_ORD']);
                            row.entity['IMP_ORD'] =  angular.copy(response[0]['IMP_ORD']);
                            row.entity['FREE_ORD'] = angular.copy(response[0]['FREE_ORD']);
                            row.entity['tipo'] = angular.copy(response[0]['tipo']);
                            row.entity['id_articolo'] = angular.copy(response[0]['id_articolo']);
                            row.entity['fornitore'] = angular.copy(response[0]['fornitore']);
                            row.entity['articolo_fornitore'] = angular.copy(response[0]['articolo_fornitore']);
                            row.entity['fornitori_list'] = angular.copy(response[0]['fornitori_list']);
                            row.entity['anno_stagione'] = angular.copy(response[0]['anno_stagione']);
                            //Se non ho più una riga con tipo Nuovo la aggiungo
                            checkAddNuovoRow(row);
                        }
                    }, function(){
                        emptyRow(row);
                        //Se non ho più una riga con tipo Nuovo la aggiungo
                        checkAddNuovoRow(row);
                        $scope.loadingColori = false;
                        $scope.loadingMisure = false;
                    }); // error handler;
                }
		    return capocmat;
        };

		var checkAddNuovoRow = function(row){
		    var tabellaGiacenza = row.grid.options.data;
            if(tabellaGiacenza != undefined && tabellaGiacenza.length > 0){
                var nuovoTrovato = false;
                angular.forEach(tabellaGiacenza, function(item, key) {
                    if (item['tipo'] === 'Nuovo') nuovoTrovato = true;
                });
                if (!nuovoTrovato) tabellaGiacenza.push(angular.copy($scope.nuovaRiga));
            }
        };

        var emptyRow = function(row){
            angular.forEach(row.entity, function(item, key) {
                if (key !== 'tipo'){
                    row.entity[key] = '';
                }else if (key === 'fornitori_list'){
                    row.entity[key] = [];
                }
            });
        };

        $scope.qtaChange = function(row){
            var tableNumID = this.obj.fabbList.tableNumID;
            var sommaQta = row.entity.DT + row.entity.SOC + row.entity.ORD;
            if (row.entity.diba.nome_modello_orig !== 'Scorta Sede'){
                row.entity.da_soddisfare = parseFloat(row.entity.fabbisogno - (row.entity.DT || 0) - (row.entity.SOC || 0) - (row.entity.ORD || 0)).toFixed(2);
                $scope['sottoGridApi'+ tableNumID].core.notifyDataChange(uiGridConstants.dataChange.ALL);
            }
            if (row.isSelected){
                //Simulo una deselezione e una successiva selezione per innescare di nuovo
                //il controllo per il confirm in caso fossero già state selezionate le righe
                //prima della sostituzione capocmat
                $scope['sottoGridApi'+ tableNumID].selection.unSelectRow(row.entity);
                $scope['sottoGridApi'+ tableNumID].selection.selectRow(row.entity);
            }
        };

        function getUrlIdFabbs() {
            vm.url_params = $location.search()||"Unknown";
            var id_richiesta = "";
            if (vm.url_params.hasOwnProperty('id_dibas'))
                id_richiesta = vm.url_params['id_dibas'].split(',');
            return id_richiesta;
        }

        function getUrlIdFabbsFromTables() {
            var id_richiesta = [];
            $.extend({
                distinct : function(anArray) {
                   var result = [];
                   $.each(anArray, function(i,v){
                       if ($.inArray(v, result) == -1) result.push(v);
                   });
                   return result;
                }
            });
            angular.forEach($scope.dataTabelleList, function(item, key) {
                id_richiesta = id_richiesta.concat(item['giacenza']['list_diba_ids'])
            });
            id_richiesta = $.distinct(id_richiesta);

            return id_richiesta;
        }

		vm.CercaImpegniDaFabb = function(dati){
            RicercaService.ricerca_impegni_da_fabbisogni(dati, function(data){
                angular.forEach(data, function(item, key) {
                    if (item.giacenza != undefined && item.giacenza.length > 0){
                        angular.forEach(item.giacenza, function(iiitem, kkkey) {
                            iiitem['short_capocmat'] = iiitem['capocmat'].slice(0,9);
                        });
                    }
                    item['list_diba_ids'] = [];
                    if (item.fabb_list != undefined && item.fabb_list.length > 0){
                        angular.forEach(item.fabb_list, function(iitem, kkey) {
                            if (iitem.hasOwnProperty('list_impegni') && iitem.list_impegni.length > 0){
                                angular.forEach(iitem.list_impegni, function(iiitem, kkkey) {
                                    if(iiitem.hasOwnProperty('tipo')){
                                        iitem[iiitem['tipo']] = iiitem.qta;
                                    }
                                });
                            };
                            if (iitem.hasOwnProperty('id_diba') && item['list_diba_ids'].indexOf(iitem.id_diba) === -1 && iitem.id_diba !== ''){
                                item['list_diba_ids'].push(iitem.id_diba);
                            }
                        });
                    };
                });
                $scope.CreateGrids(data);
                console.log(data);
            }, function(){
//                $scope.grigliaModelli.data = [];
            });
        };

        vm.refreshGiacenza = function(data){
            var righe_tabella = angular.copy($scope.dataTabelleList[data.tabella]['giacenza'].data);
            RicercaService.get_giacenza_complessiva_refresh({'art':data.articolo, 'tabData':righe_tabella}, function(response){
                angular.forEach(response, function(item, key) {
                    item['short_capocmat'] = item['capocmat'].slice(0,9);
                })
                $scope.dataTabelleList[data.tabella]['giacenza'].data = response;
                $scope.dataTabelleList[data.tabella]['giacenza'].data.push(angular.copy($scope.nuovaRiga));
            }, function(){
            });
        };

        vm.refreshFabbisoni = function(data){
            RicercaService.ricerca_impegni_da_fabbisogni_refresh(data.diba_ids, function(response){
                if(response != undefined){
                    angular.forEach(response, function(item, key) {
                        if (item.fabb_list != undefined && item.fabb_list.length > 0){
                            angular.forEach(item.fabb_list, function(iitem, kkey) {
                                if (iitem.hasOwnProperty('list_impegni') && iitem.list_impegni.length > 0){
                                    angular.forEach(iitem.list_impegni, function(iiitem, kkkey) {
                                        if(iiitem.hasOwnProperty('tipo')){
                                            iitem[iiitem['tipo']] = iiitem.qta;
                                        }
                                    });
                                };
                            });
                        };
                    });
                    $scope.dataTabelleList[data.tabella]['fabbList'].data = response[0].fabb_list;
                }
            }, function(){
//                    $scope.dataTabelle[data.tabella].data = [];
            });
        };

//        vm.salva_articolo_duplicato = function(tableNumID, tableID, riga_duplicata){
//            SalvaService.salva_articolo_duplicato(riga_duplicata, function(response){
//                    //Dopo aver ricevuto indietro la riga duplicata salvata da server
//                    // la aggiungo alle righe in tabella lato front-end e aggiorno il css
//                    // per altezza tabella
//                    $scope.dataTabelleList['fabbList'][tableID].data.push(response);
//                    $('#sottogrid'+tableNumID).css(
//                        $scope.getTableHeight($scope.dataSottoTabelle[tableID].data)
//                    );
//            }, function(){
//
//            });
//        };

        $scope.impegnaMultiArticoloPannello = function(rowEntity = null){
            var tableNumID = this.obj.fabbList.tableNumID;
            var tableID = this.obj.fabbList.tableID;
            var dibaIds = this.obj.giacenza.list_diba_ids;
            var ListSelectedObjects = $scope.sottoGridSelections[tableNumID];
            var finalListToSend = rowEntity != null ? [rowEntity] : ListSelectedObjects;
            var tableData = angular.copy(this.obj.fabbList.data);
            var idList = [];

            if (finalListToSend != undefined && finalListToSend.length > 0){
                    SalvaService.impegna_articolo(finalListToSend, function(response){
                        var tableToRefresh = tableNumID;
                        var subtableToRefresh = tableNumID;
                        vm.refreshGiacenza({tabella: tableToRefresh, articolo: response[0].diba});
                        angular.forEach(response, function(item){
                            if (dibaIds.indexOf(item.id_diba) < 0 && item.id_diba != undefined)
                                dibaIds.push(String(item.id_diba));
                        });
                        vm.refreshFabbisoni({tabella: subtableToRefresh, diba_ids: dibaIds});
                        $scope['sottoGridApi'+ tableNumID].core.notifyDataChange(uiGridConstants.dataChange.ALL);
                        $scope['sottoGridApi'+ tableNumID].core.refresh();

                    }, function(){

                    });
            }else{
                logger.error('Nessun articolo selezionato');
            };
        };

        $scope.impegnaMultiArticolo = function(){
            var selectedList = $scope.sottoGridSelections;
            var ListContainsSelectedObjects = false;
            var finalListPassed = [];
            if (selectedList != undefined && selectedList.length > 0){
                angular.forEach(selectedList, function(item, key) {
                    if (!$.isEmptyObject(item))
                        ListContainsSelectedObjects = true;
                        finalListPassed = finalListPassed.concat(item);
                });
            }
            if (ListContainsSelectedObjects){
                SalvaService.impegna_articolo(finalListPassed, function(response){
                // Per adesso ridisegno tutto
                    $timeout(function(){vm.CercaImpegniDaFabb(getUrlIdFabbsFromTables());}, 500);
                }, function(){ }); // error handler


                console.log($scope);

            }else {
                logger.error('Nessun articolo selezionato');
            }

        };

        $scope.duplica_articolo = function(info_riga){
            var tableData = info_riga.tableData;
            var tableID = info_riga.tableID;
            var tableNumID = info_riga.tableNumID;
            var riga = info_riga.riga;

            var riga_duplicata = {};
            if (riga.grid.options.data !== undefined && riga.grid.options.data.length > 0){
                riga_duplicata = angular.copy(riga.entity);
                riga_duplicata['duplicato'] = true;
                riga_duplicata['fabbisogno'] = angular.copy(riga_duplicata['da_soddisfare']);

                //setto il fabbisogno al duplicato
                if (riga_duplicata['qta_reale'] > 0) {
                    //Aggiorno qta e fabbisogno
                    riga_duplicata['qta_reale'] = info_riga['fabbisogno_parziale'];
                    riga_duplicata['capi_lanciati'] = parseInt(riga_duplicata['qta_reale'] / riga_duplicata.diba['consumo']);
                    riga.entity['qta_reale'] = riga.entity['qta_reale'] - info_riga['fabbisogno_parziale'];
                    riga.entity['capi_lanciati'] = riga.entity['capi_lanciati'] -  riga_duplicata['capi_lanciati'];
                    riga.entity['fabbisogno'] = angular.copy(riga.entity['qta_reale']);
                    //ricalcolo da soddisfare in riga originaria
                    riga.entity['da_soddisfare'] = parseFloat(riga.entity['qta_reale'] - (riga.entity['DT'] || 0)
                                            - (riga.entity['SOC'] || 0) - (riga.entity['ORD'] || 0)).toFixed(2);
                    //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                    riga.entity['tot_capi'] = parseInt(riga.entity['qta_reale'] / riga.entity.diba['consumo'])
                }else if (riga_duplicata['qta_teor'] > 0) {
                    //Aggiorno qta e fabbisogno
                    riga_duplicata['qta_teor'] = info_riga['fabbisogno_parziale'];
                    riga_duplicata['capi_teor'] = parseInt(riga_duplicata['qta_teor'] / riga_duplicata.diba['consumo']);
                    riga.entity['qta_teor'] = riga.entity['qta_teor'] - info_riga['fabbisogno_parziale'];
                    riga.entity['capi_teor'] = riga.entity['capi_teor'] -  riga_duplicata['capi_teor'];
                    riga.entity['fabbisogno'] = angular.copy(riga.entity['qta_teor']);
                    //ricalcolo da soddisfare in riga originaria
                    riga.entity['da_soddisfare'] = parseFloat(riga.entity['qta_teor'] - (riga.entity['DT'] || 0)
                                        - (riga.entity['SOC'] || 0) - (riga.entity['ORD'] || 0)).toFixed(2);
                    //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                    riga.entity['tot_capi'] = parseInt(riga.entity['qta_teor'] / riga.entity.diba['consumo'])
                }else {
                    //Aggiorno qta e fabbisogno
                    riga_duplicata['qta_prev'] = info_riga['fabbisogno_parziale'];
                    riga_duplicata['capi_prev'] = parseInt(riga_duplicata['qta_prev'] / riga_duplicata.diba['consumo']);
                    riga.entity['qta_prev'] = riga.entity['qta_prev'] - info_riga['fabbisogno_parziale'];
                    riga.entity['capi_prev'] = riga.entity['capi_prev'] -  riga_duplicata['capi_prev'];
                    riga.entity['fabbisogno'] = angular.copy(riga.entity['qta_prev']);
                    //ricalcolo da soddisfare in riga originaria
                    riga.entity['da_soddisfare'] = parseFloat(riga.entity['qta_prev'] - (riga.entity['DT'] || 0)
                                        - (riga.entity['SOC'] || 0) - (riga.entity['ORD'] || 0)).toFixed(2);
                    //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                    riga.entity['tot_capi'] = parseInt(riga.entity['qta_prev'] / riga.entity.diba['consumo']);
                }

                //Inizializzo i campi restanti della riga duplicata
                riga_duplicata['id'] = '';
                riga_duplicata['id_fabb_orig'] = riga.entity['id'];
                riga_duplicata['da_soddisfare'] = info_riga['fabbisogno_parziale'];
                riga_duplicata['fabbisogno'] = info_riga['fabbisogno_parziale'];
                riga_duplicata['tot_capi'] = parseInt(info_riga['fabbisogno_parziale'] / riga_duplicata.diba['consumo']);
                riga_duplicata['list_impegni'] = [];
                riga_duplicata['id_ord'] = '';
				riga_duplicata['id_fac_d'] = '';
				riga_duplicata['id_preord'] = ''
				riga_duplicata['id_fac_preord_d'] = '';
                riga_duplicata['isDuplicato'] = true;
                riga_duplicata['DT'] = 0;
                riga_duplicata['SOC'] = 0;
                riga_duplicata['ORD'] = 0;

                tableData.push(riga_duplicata);
                $('#sottogrid'+tableNumID).css(
                    $scope.getTableHeight($scope.dataTabelleList[tableNumID]['fabbList'].data)
                );
//                vm.salva_articolo_duplicato(tableNumID, tableID, riga_duplicata);
            }
        };

        $scope.apri_duplica_articolo = function($event, riga){
            var tableData = riga.grid.options.data;
//            var tableID = riga.grid.options.tableID;
            var tableNumID = riga.grid.options.tableNumID;
            var fromButton = $event.target;

            var info_righe = {
                'riga': riga,
//                'tableID': tableID,
                'tableData': tableData,
                'tableNumID': tableNumID,
                'fromButton': fromButton
            };

            ServiceDatiDuplica.data_riga_da_duplicare = info_righe;
            console.log(ServiceDatiDuplica.data_riga_da_duplicare);
        };

        $scope.sostituisci_capocmat = function(riga){
            var tableNumID = this.obj.giacenza.tableNumID;
            var tableArticoliData = $scope.dataTabelleList[tableNumID]['fabbList'].data;
            var tableArticoliDataRows = this.$$childTail.grid.rows;
            var itemsSelected = false;
            if (tableArticoliData.length > 0){
                 if (!riga.entity['colore'] || !riga.entity['misura']) {
                        alert('Capocmat senza misura/colore: impossibile sostituire!');
                        return;
                  };
                if ((!riga.entity['colore'] || riga.entity['colore'] == '0000') && riga.entity['gest_colore'] == 'S') {
                    alert('Capocmat gestito a colore: impossibile sostituire!');
                    return;
                };
                if ((!riga.entity['misura'] || riga.entity['misura'] == '00') && riga.entity['gest_misura'] == 'S') {
                    alert('Capocmat gestito a misura: impossibile sostituire!');
                    return;
                };
                angular.forEach(tableArticoliDataRows, function(item, key) {
                    if (item.isSelected){
                        itemsSelected = true;
                        if(item.entity['id_fac_d'] === '' && item.entity['id_fac_preord_d'] === ''){
                            if (riga.entity.col_forn_dict != undefined) {
                                item.entity['id_articolo_fornitore_col_forn'] = angular.copy(riga.entity['col_forn_dict']['id_articolo_fornitore_col_forn']) || '';
                                item.entity['articolo_fornitore_col_forn'] = angular.copy(riga.entity['col_forn_dict']['articolo_fornitore_col_forn']) || '';
                                item.entity['fornitore'] = angular.copy(riga.entity['col_forn_dict']['conto_fornitore_col_forn']) || '';
                                item.entity['ragione_sociale_col_forn'] = angular.copy(riga.entity['col_forn_dict']['ragione_sociale_col_forn']) || '';
                                item.entity['colore_orig_col_forn'] = angular.copy(riga.entity['col_forn_dict']['colore_orig_col_forn']) || '';
                                item.entity['desc_col_fornitore'] = angular.copy(riga.entity['col_forn_dict']['desc_colore_forn_col_forn']) || '';
                                item.entity['desc_colore_forn_col_forn'] = angular.copy(riga.entity['col_forn_dict']['desc_colore_forn_col_forn']) || '';
                            }else{
                                item.entity['fornitore'] = '';
                                item.entity['desc_col_fornitore'] = '';
                            }
                            item.entity.diba['id_articolo'] = angular.copy(riga.entity['id_articolo']);
                            item.entity.diba['capocmat'] = angular.copy(riga.entity['capocmat'].toUpperCase());
                            item.entity.diba['societa'] = angular.copy(riga.entity['societa']);
                            item.entity.diba['gm'] = angular.copy(riga.entity['gm']);
                            item.entity.diba['anno_stagione'] = angular.copy(riga.entity['anno_stagione']);
                            item.entity.diba['articolo'] = angular.copy(riga.entity['articolo']);
                            item.entity.diba['misura'] = angular.copy(riga.entity['misura']);
                            item.entity.diba['colore'] = angular.copy(riga.entity['colore']);
                            item.entity.diba['colore_descr'] = angular.copy(riga.entity['colore_descr']);
                            item.entity.DT = 0;
                            item.entity.SOC = 0;
                            item.entity.ORD = 0;
                            if (item.entity['isDuplicato']) {
                                item.entity.diba['id'] = '';
                                item.entity.id_diba = '';
                            }
                            if(item.entity.rif_bp != ""){
                                item.entity['openConfirmBP'] = true;
                                //Simulo una deselezione e una successiva selezione per innescare di nuovo
                                //il controllo per il confirm in caso fossero già state selezionate le righe
                                //prima della sostituzione capocmat
                                $scope['sottoGridApi'+ tableNumID].selection.unSelectRow(item.entity);
                                $scope['sottoGridApi'+ tableNumID].selection.selectRow(item.entity);
                            }
                            logger.success('Capocmat sostituito correttamente per articoli selezionati');
                        }else {
                            logger.error('Impossibile sostituire capocmat su un articolo già ordinato / preordinato');
                        }
                    }
                });
                if (!itemsSelected){
                    logger.error('Nessun articolo selezionato per sostituzione capocmat');
                }
            }
        };

        $scope.sblocca_impegni = function(row){
            //Disabilito bottone per evitre di ricliccarci prima della fine della richiesta
            $scope.disableSbloccaImpegni = true;
//            var tableID = this.obj.giacenza.tableID;
            var tableNumID = this.obj.giacenza.tableNumID;
            var tableArticoliData = this.obj.fabbList.data;
            var tableDibaIds = row.grid.options.list_diba_ids;
            var paramsToSend = {};
            var idList = [];
            var idListDett = [];
            if (tableArticoliData != undefined && tableArticoliData.length > 0){
                angular.forEach(tableArticoliData, function(item, key) {
                    if (item.hasOwnProperty('id')){
                        idList.push(item.id);
                    }
                     if (item.hasOwnProperty('id_facord_dett')){
                        idListDett.push(item.id_facord_dett);
                    }
                });
            };
            paramsToSend['societa'] = row.entity.societa || '';
            paramsToSend['anno_stagione'] = row.entity.anno_stagione || '';
            paramsToSend['gm'] = row.entity.gm || '';
            paramsToSend['misura'] = row.entity.misura || '';
            paramsToSend['colore'] = row.entity.colore || '';
            paramsToSend['articolo'] = row.entity.articolo || '';
            paramsToSend['capocmat'] = row.entity.capocmat || '';
            paramsToSend['id_fabbs'] = idList || [];
            paramsToSend['id_facord_detts'] = idListDett || [];
            RicercaService.getSbloccaImpegni(paramsToSend, function(data){
                if (data != undefined && data.length > 0){
                    angular.forEach(data, function(item, key) {
                        if (item.hasOwnProperty('list_impegni') && item.list_impegni.length > 0){
                            angular.forEach(item.list_impegni, function(iitem, kkkey) {
                                if(iitem.hasOwnProperty('tipo')){
                                    item[iitem['tipo']] = iitem.qta;
                                }
                            });
                        };
                        item['isBloccato'] = true;
                        tableArticoliData.push(item);

                        if (tableDibaIds.indexOf(item['id_diba']) === -1 && item['id_diba'] !== ''){
                            tableDibaIds.push(item['id_diba']);
                        }
                    });
                    $('#sottogrid'+tableNumID).css(
                        $scope.getTableHeight($scope.dataTabelleList[tableNumID]['fabbList'].data)
                    );
                    //Riabilito bottone dopo la fine della richiesta
                    $scope.disableSbloccaImpegni = false;
                }else
                    logger.error('Nessun impegno trovato')
                    //Riabilito bottone dopo la fine della richiesta
                    $scope.disableSbloccaImpegni = false;
            }, function(){
//                $scope.grigliaModelli.data = [];
            });

        };

        $scope.eliminaDuplicato = function(row){
//            var tableID = this.obj.fabbList.tableID;
            var tableNumID = this.obj.fabbList.tableNumID;
            var tableArticoliData = $scope.dataTabelleList[tableNumID]['fabbList'].data;
            tableArticoliData.forEach( function( item, index, object ) {
                if (item['id'] === row.entity['id_fabb_orig']){
                    if (item['qta_reale'] > 0) {
                        item['qta_reale'] += row.entity['fabbisogno'];
                        item['fabbisogno'] = angular.copy(item['qta_reale']);
                        item['da_soddisfare'] = parseFloat(item['qta_reale'] - (item['DT'] || 0)
                                            - (item['SOC'] || 0) - (item['ORD'] || 0)).toFixed(2);
                        //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                        item['capi_lanciati'] += row.entity['capi_lanciati'];
                        item['tot_capi'] = parseInt(parseFloat(item['qta_reale']) / parseFloat(item.diba['consumo']));
                    }else if (item['qta_teor'] > 0) {
                        item['qta_teor'] += row.entity['fabbisogno'];
                        item['fabbisogno'] = angular.copy(item['qta_teor']);
                        item['da_soddisfare'] = parseFloat(item['qta_teor'] - (item['DT'] || 0)
                                            - (item['SOC'] || 0) - (item['ORD'] || 0)).toFixed(2);
                        //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                        item['capi_teor'] += row.entity['capi_teor'];
                        item['tot_capi'] = parseInt(parseFloat(item['qta_teor']) / parseFloat(item.diba['consumo']));
                    }else{
                        item['qta_prev'] += row.entity['fabbisogno'];
                        item['fabbisogno'] = angular.copy(item['qta_prev']);
                        item['da_soddisfare'] = parseFloat(item['qta_prev'] - (item['DT'] || 0)
                                            - (item['SOC'] || 0) - (item['ORD'] || 0)).toFixed(2);
                        //setto tot capi dopo aggiornamento fabbisogno alla riga originaria
                        item['capi_prev'] += row.entity['capi_prev'];
                        item['tot_capi'] = parseInt(parseFloat(item['qta_prev']) / parseFloat(item.diba['consumo']));
                    }
                }
                if (item != undefined && item['$$hashKey'] === row.entity['$$hashKey']){
                    object.splice(index, 1);
                }
            });
            $scope['sottoGridApi'+ tableNumID].core.notifyDataChange(uiGridConstants.dataChange.ALL);
        };

        // Dialog dettaglio
        $scope.apriDialogDettaglio = function(row){
            ServiceDatiVistaGiacenza.tableRowID = row.entity.$$hashKey;
            ServiceDatiVistaGiacenza.tableID = row.grid.options.tableNumID;
            ServiceDatiVistaGiacenza.riga = row.entity;
            ServiceDatiVistaGiacenza.isFornitore = false;
            ServiceDatiVistaGiacenza.diba = this.obj.giacenza.diba;

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'javascripts/facon/diba/gestione_impegni/vista-cerca-giacenza/vista-cerca-giacenza.html',
                controller: 'DialogCercaGiacenzaCtrl as dialog_cerca_giacenza_ctrl',
                size: 'max-size',
                resolve: { /* @ngInject */
                    entity: {
                              'riga' : row.entity
                            }
                }
            });
            modalInstance.result.then(function () {
            });
        };

        // Dialog crea ordini da Pannello
        $scope.apriDialogCreaOrdiniPannello = function(){
            var tableNumID = this.obj.fabbList.tableNumID;
            var tableToRefresh = tableNumID;
            var subtableToRefresh = tableNumID;
            var dibaIds = this.obj.giacenza.list_diba_ids;
            var diba = this.obj.giacenza.diba;


            var ListSelectedObjects = $scope.sottoGridSelections[tableNumID]

            if (ListSelectedObjects.length > 0){
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'javascripts/facon/diba/gestione_impegni/vista-crea-ordini/vista-crea-ordini.html',
                    controller: 'DialogCreaOrdiniCtrl as dialog_crea_ordini_ctrl',
                    size: 'max-size',
                    resolve: { /* @ngInject */
                        entity: {
                            'righe_selezionate' : ListSelectedObjects,
                            'from_table_info' : {
                                'diba': diba,
                                'diba_ids': dibaIds,
                                'tabGiacenze': tableToRefresh,
                                'tabFabbisogni': subtableToRefresh
                            }
                        }
                    }
                });
                modalInstance.result.then(function () {
                });
            }else
                logger.error('Nessun articolo selezionato')

		};

        // Dialog disimpegna articoli da Pannello
        $scope.apriDialogDisimpegnaArticoliPannello = function(riga = null){
            var tableNumID = this.obj.fabbList.tableNumID;
            var tableData = this.obj.fabbList.data;
            var listSelectedObjects = $scope.sottoGridSelections[tableNumID]

            var tableToRefresh = tableNumID;
            var subtableToRefresh = tableNumID;
            var dibaIds = this.obj.giacenza.list_diba_ids;
            var diba = this.obj.giacenza.diba;

            var articolo_senza_disimpegno = false;
            var listIdSelected = [];
            if (riga == null){
                angular.forEach(listSelectedObjects, function(item, key) {
                    if(listIdSelected.indexOf(item['id']) === -1 && item.hasOwnProperty('apri_disimpegno') && item['apri_disimpegno']){
                        if(item['id'] !== '') listIdSelected.push(item['id']);
                    }else{
                        articolo_senza_disimpegno = true;
                    }
                });
            }else{
                listIdSelected = [riga['id']]
            }
            if (riga || listSelectedObjects.length > 0 && !articolo_senza_disimpegno){
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'javascripts/facon/diba/gestione_impegni/vista-disimpegna-articoli/vista-disimpegna-articoli.html',
                    controller: 'DialogDisimpegnaArticoliCtrl as dialog_disimpegna_articoli_ctrl',
                    size: 'max-size',
                    resolve: { /* @ngInject */
                        entity: {
                            'righe_selezionate' : riga ? [riga] : listSelectedObjects,
                            'allTableData': tableData,
                            'listIdSelected': listIdSelected,
                            'from_table_info' : {
                                'diba': diba,
                                'diba_ids': dibaIds,
                                'tabGiacenze': tableToRefresh,
                                'tabFabbisogni': subtableToRefresh
                            }
                        }
                    }
                });
                modalInstance.result.then(function () {
                });
            }else if (articolo_senza_disimpegno){
                logger.error('Articoli senza ordine/preordine selezionati erroneamente')
            }else{
                logger.error('Nessun articolo selezionato')
            }
		};

		// Dialog crea ordini Globale
        $scope.apriDialogCreaOrdini = function(){
            var ListContainsSelectedObjects = false;
            var finalListPassed = [];
            if ($scope.sottoGridSelections.length > 0){
                angular.forEach($scope.sottoGridSelections, function(item, key) {
                    if (!$.isEmptyObject(item)){
                        ListContainsSelectedObjects = true;
                        finalListPassed = finalListPassed.concat(item);
                    }
                });
                if (ListContainsSelectedObjects){
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'javascripts/facon/diba/gestione_impegni/vista-crea-ordini/vista-crea-ordini.html',
                        controller: 'DialogCreaOrdiniCtrl as dialog_crea_ordini_ctrl',
                        size: 'max-size',
                        resolve: { /* @ngInject */
                            entity: { 'righe_selezionate' : finalListPassed}
                        }
                    });
                    modalInstance.result.then(function () {
                    });
                }else
                    logger.error('Nessun articolo selezionato')
            }else
                logger.error('Errore Imprevisto, contattare il supporto tecnico')
		};

		// Dialog disimpegna articoli Globale
        $scope.apriDialogDisimpegnaArticoli = function(){
            var ListContainsSelectedObjects = false;
            var finalListPassed = [];
            if ($scope.sottoGridSelections.length > 0){
                angular.forEach($scope.sottoGridSelections, function(item, key) {
                    if (!$.isEmptyObject(item))
                        ListContainsSelectedObjects = true;
                        finalListPassed = finalListPassed.concat(item);
                });
                if (ListContainsSelectedObjects){
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'javascripts/facon/diba/gestione_impegni/vista-disimpegna-articoli/vista-disimpegna-articoli.html',
                        controller: 'DialogDisimpegnaArticoliCtrl as dialog_disimpegna_articoli_ctrl',
                        size: 'max-size',
                        resolve: { /* @ngInject */
                            entity: { 'righe_selezionate' : finalListPassed}
                        }
                    });
                    modalInstance.result.then(function () {
                    });
                }else
                    logger.error('Nessun articolo selezionato')
            }else
                logger.error('Errore Imprevisto, contattare il supporto tecnico')
		};

        $scope.apriModificaOrdine = function(id_ordine){
            var sid = jQuery('#sid').val();
            var app_server = jQuery('#app_server').val();
            var menu_id = jQuery('#menu_id').val();
            var module = 'facon/pao.ordini';
            var program = 'visualizza_ordine';
            var action = 'modifica_ordine';
            //var id_ordine = jQuery(this).attr('id').replace('btn_apri_ordine_','');
            var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&id_ordine="+id_ordine+"&action="+action;
            window.open(url);
        }

        // per modificare l'altezza della tabella dinamicamente, aggiungere ng-style="ctrl.getTableHeight()" agli
        // attributi della grid e scommentare qui sotto
        $scope.getTableHeight = function(tableData) {
           var rowHeight = 30; // your row height
           var headerHeight = 30; // your header height
           return {
              height: (tableData.length * rowHeight + headerHeight * 3.5) + "px",
              'min-height': 200 +'px',
              'max-height': 500 +'px'
           };
        };

        vm.refreshPanelGridsFromPopup = function(event, tab_info){
            vm.refreshGiacenza({tabella: tab_info.tabGiacenze, articolo: tab_info.diba});
            vm.refreshFabbisoni({tabella: tab_info.tabFabbisogni, diba_ids: tab_info.diba_ids});
        };

        vm.refreshAllGridsFromPopup = function(event){
//        	if($scope.TablesLimit != undefined){
//               $scope.TablesLimit = 2;
//               $scope.addTableDisabled = false;
//            }
            $timeout(function(){$scope.loadingTables = false;});
            vm.CercaImpegniDaFabb(getUrlIdFabbsFromTables());
        };

        vm.duplicaDaPopover = function(event, info_riga){
            $scope.duplica_articolo(info_riga);
            $timeout(function(){$(info_riga.fromButton).trigger('click');}, 200);
        };

        vm.scegliArticolo = function(event, dati){
            var newTableData = $scope.dataTabelleList[dati['tableID']]['giacenza'].data;
            var isNuovo = false;
            angular.forEach(newTableData, function(item, key) {
                //Se trovo riga di uguale id sostituisco
                if (item.$$hashKey === dati.tableRowID){
                    if(item.hasOwnProperty('tipo') && item.tipo === 'Nuovo'){
                        isNuovo = true;
                    }
                    newTableData[key] = dati.riga;
                    ServiceDatiVistaGiacenza.tableRowID = newTableData[key]['$$hashKey'];
                }
            });
            $scope.dataTabelleList[dati['tableID']]['giacenza'].data = vm.aggiorna_dati_impegni(newTableData);
            if (isNuovo) newTableData.push(angular.copy($scope.nuovaRiga));

        };

        vm.CercaImpegniDaFabb(getUrlIdFabbs());
        //Invio true come valore per la variabile testataSalvata
        //quando carico dati studio da url correttamente
        $rootScope.$on("vistaGiac:scegli_articolo", vm.scegliArticolo);
        $rootScope.$on("popup_submit_refresh:grids_refresh_panel", vm.refreshPanelGridsFromPopup);
        $rootScope.$on("popup_submit_refresh:grids_refresh_all", vm.refreshAllGridsFromPopup);
        $rootScope.$on("popover_duplica:duplica", vm.duplicaDaPopover);
        //Aggiugo ai link di percorso l'attributo target
		//per raggiungere la pagina
        $('.breadcrumb li a').attr('target', '_self');
        $('.dropdown-user .box-user-info a').attr('target', '_self');
        console.log($scope);



    }
})();


