(function()
{
	'use strict';

	GetDatiStudioService.$inject = ['DTHttp', 'LoggerFactory', '$q'];

	var factoryId = "GetDatiStudioService";

	angular.module('app').factory(factoryId, GetDatiStudioService);

	function GetDatiStudioService(DTHttp, LoggerFactory, $q)
	{
		var logger = LoggerFactory.getLogger(factoryId);

		return {
			getDatiStudio: function (params, successHandler, errorHandler) {
				DTHttp
					.get('facon/ordpf.richieste_pf_ctrl', 'get_dati_studio_diba', params)
					.then(
						function successCallback(response) {
							if (successHandler != null)
								successHandler(response.data.ResultSet.Result);
						},
						function errorCallback(response) {
							logger.error(response.data.message);
							if (errorHandler != null)
								errorHandler();
						}
				);
			},
			getColori: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/diba.fabbisogni_ctrl', 'autocomplete_colore', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			},
			getCollezioniOrig: function(param)
			{
				var deferred = $q.defer();
				var payload = { dati: JSON.stringify(param)};
				DTHttp.get('facon/ordpf.richieste_pf_ctrl', 'autocomplete_collezione_orig', payload).then(
					function successCallback(response){
						deferred.resolve(response.data.ResultSet.Result);
						logger.debug(response.data.ResultSet.Result);
					},
					function errorCallback(response){
						logger.error(response.data.message);
						deferred.reject(response)
					}
				);
				return deferred.promise;
			}
		};
	}
})();