# -*- coding: UTF-8 -*-
import sys
#sys.path += [sys.path[0]+'/..']
sys.path.insert(0, '/home/re/mp/bin')
import string
import pprint
import StringIO
import cStringIO
import base64
import os
import re
import itertools
import time
import copy
import types
import zipfile
#from svglib.svglib import svg2rlg
import reportlab
from decimal import *
import tools
import parametri_lib
from datetime import datetime, date
from facon.ordpf.errors import ForwardException
from facon.diba.diba_common import SOCIETA_LIST
import warnings
warnings.simplefilter("ignore", DeprecationWarning)
import pyPdf

from reportlab.platypus import Spacer, SimpleDocTemplate, Table, TableStyle, Paragraph, PageBreak, Frame, KeepTogether, CondPageBreak
from reportlab import platypus
from reportlab.platypus import Image as plImage
from reportlab.lib.units import cm
from reportlab.rl_config import defaultPageSize
from reportlab.lib.pagesizes import A4, landscape, portrait
from reportlab.lib import colors
from reportlab.graphics.shapes import *
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT, TA_JUSTIFY
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfutils
from reportlab.lib.units import cm, inch

#KespansionePdf = 0.8932 # serve a compensare la distorsione in stampa dei pdf
KespansionePdf = 0.96 # serve a compensare la distorsione in stampa dei pdf

import config


import datetime
import cjson
import cgi
import locale
import tools
import itertools

import caio_print as cp
from facon import ws_client_mmfg

# PARAMETRI A4 = (840,594)
LEFT_MARGIN = 0.5 * cm
RIGHT_MARGIN = 0.5 * cm
TOP_MARGIN = 1 * cm
BOTTOM_MARGIN = 1 * cm



style_data = TableStyle([])
style_data.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_data.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_data.add('BOTTOMPADDING', (0, 5), (-1, -1), 0)
style_data.add('GRID', (0, 0), (-1, -1), 0.25, colors.gray)

style_data_highlighted = TableStyle([])
style_data_highlighted.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_data_highlighted.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_data_highlighted.add('BOTTOMPADDING', (0, 5), (-1, -1), 0)
style_data_highlighted.add('GRID', (0, 0), (-1, -1), 0.25, colors.gray)
style_data_highlighted.add('BACKGROUND', (0, 0), (-1, -1), colors.yellow)

style_title = TableStyle([])
style_title.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_title.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_title.add('BOTTOMPADDING', (0, 0), (-1, -1), 0)
style_title.add('LEFTPADDING', (0, 0), (-1, -1), 0)
style_title.add('RIGHTPADDING', (0, 0), (-1, -1), 2)
style_title.add('GRID', (0, 0), (-1, -1), 0.25, colors.gray)
style_title.add('BACKGROUND', (0, 0), (-1, -1), colors.lightgrey)

style_intest = TableStyle([])
style_intest.add('VALIGN', (0, 0), (-1, -1), 'BOTTOM')
style_intest.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_intest.add('BOTTOMPADDING', (0, 0), (-1, -1), 5)
style_intest.add('TOPPADDING', (0, 0), (-1, -1), 5)
style_intest.add('LINEBELOW', (0, 0), (-1, -1), 0.25, colors.gray)

style_nota = TableStyle([])
style_nota.add('VALIGN', (0, 0), (-1, -1), 'BOTTOM')
style_nota.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_nota.add('BOTTOMPADDING', (0, 0), (-1, -1), 0)
style_nota.add('TOPPADDING', (0, 0), (-1, -1), 2)
style_nota.add('LINEBELOW', (0, -1), (-1, -1), 0.5, colors.black)

style_content = TableStyle([])
style_content.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_content.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_content.add('BOTTOMPADDING', (0, 0), (-1, -1), 1)
style_content.add('TOPPADDING', (0, 0), (-1, -1), 1)

style_color =  TableStyle([])
style_color.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_color.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_color.add('BOTTOMPADDING', (0, 0), (-1, -1), 2)
style_color.add('TOPPADDING', (0, 0), (-1, -1), 2)
style_color.add('BACKGROUND', (0, 0), (-1, -1), colors.lightgrey)

style_color2 =  TableStyle([])
style_color2.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_color2.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_color2.add('BOTTOMPADDING', (0, 0), (-1, -1), 2)
style_color2.add('TOPPADDING', (0, 0), (-1, -1), 2)
style_color2.add('BACKGROUND', (-1, 0), (-1, -1), colors.lightgrey)

style_content2 = TableStyle([])
style_content2.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_content2.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
style_content2.add('BOTTOMPADDING', (0, 0), (-1, -1), 1)
style_content2.add('TOPPADDING', (0, 0), (-1, -1), 2)
style_content2.add('LINEABOVE', (0, 0), (-1, -1), 0.25, colors.gray)

style_img = TableStyle([])
style_img.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_img.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
#style_img.add('BOTTOMPADDING', (0, 0), (-1, -1), 1)
#style_img.add('TOPPADDING', (0, 0), (-1, -1), 2)

style_etichetta_lavaggio = TableStyle([])
style_etichetta_lavaggio.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
style_etichetta_lavaggio.add('ALIGN', (0, 0), (-1, -1), 'LEFT')
style_etichetta_lavaggio.add('BOTTOMPADDING', (0, 0), (-1, -1), 0)
style_etichetta_lavaggio.add('TOPPADDING', (0, 0), (-1, -1), 4)
style_etichetta_lavaggio.add('RIGHTPADDING', (0, 0), (-1, -1), 4)
style_etichetta_lavaggio.add('LEFTPADDING', (0, 0), (-1, -1), 0)


def build_pdf_scheda(pard, dati, dati_orig):
	global pagina
	global count_pagine

	PAGE_HEIGHT = A4[1]
	PAGE_WIDTH = A4[0]
	prog_documento = 0
	lista_pdf = []
	
	story = []
	buffer_IO = StringIO.StringIO()
	prog_documento += 1
	doc = SimpleDocTemplate(buffer_IO, pagesize=portrait(A4), showBoundary=0)
	doc.leftMargin = LEFT_MARGIN
	doc.rightMargin = RIGHT_MARGIN
	doc.topMargin = TOP_MARGIN
	doc.bottomMargin = BOTTOM_MARGIN
	dt_int = ''
	n_pagine = 1
	pagina = 0
	for p in xrange(n_pagine):
		if p > 0:
			story.append(PageBreak())
		story.extend(build_pdf_scheda_data(pard, dati, dati_orig))
	
	# Generazione del documento pdf
	doc.build(story, onFirstPage=any_page, onLaterPages=any_page)
	# impostazione file pdf da generare	
	
	name = 'SchedaModello_' + dati['modello'] + '.pdf'
	lista_pdf.append((name, buffer_IO.getvalue()))
	buffer_IO.close()
	
	nome_file = lista_pdf[0][0]
	contenuto = lista_pdf[0][1]
	anno = dati['anno_stagione'][:4]
	if not os.path.isdir(pard['DOC_BASE_DIR'] + '/ordmp/diba/' + anno + '/'):
		os.system('mkdir -p ' + pard['DOC_BASE_DIR'] + '/ordmp/diba/' + anno + '/')
	fullpath = pard['DOC_BASE_DIR'] + "/ordmp/diba/" + anno + '/' + nome_file
	
	j = open(fullpath, 'w')
	j.write(contenuto)
	j.close()
	
	return {'name':nome_file, 'path':fullpath}






def build_pdf_scheda_data(pard, dati, dati_orig):
	#cp.dump(input_list)
	story = []
	
	compos = []

	dati_orig.setdefault('modello_orig', dati_orig.get('modello_orig', ''))
	dati_orig.setdefault('gruppo_merceologico', dati_orig.get('gruppo_merceologico', '').zfill(2))
	dati_orig.setdefault('progr_articolo', dati_orig.get('progr_articolo', '').zfill(3))
	dati_orig.setdefault('desc_articolo', dati_orig.get('desc_articolo', ''))
	
	dati.setdefault('tessuto_descr_orig', dati.get('tessuto_descr_orig', dati_orig['desc_articolo']))
	dati.setdefault('tessuto_orig', dati.get('tessuto_orig', dati_orig['gruppo_merceologico']+dati_orig['progr_articolo']))
	
	for c in dati['composizioni']:
		riga_compos = [[par_nero("<b>Pezzo:</b> %(pz)s" %c,font=9, align=TA_LEFT, ), par_nero("<b>Comp.:</b> %(componente)s" %c,font=9, align=TA_LEFT, )]]
		compos.append([Table(riga_compos, (2*cm, 5*cm), style=style_content2)])
		riga_compos = [[par_nero("<b>Composiz.:</b> %(compos)s" %c,font=9, align=TA_LEFT, )]]
		compos.append([Table(riga_compos, (7*cm), style=style_content)])
	if compos:
		table_compos = Table(compos, (7*cm), style=style_color)
	else:
		table_compos = Spacer(0,5)
	
	intest_1 = [[par_nero("<b>Cod. Mod. Ricodificato</b>",9, align=TA_LEFT), par_nero("<b>Nome Mod. Ricodificato</b>",9, align=TA_LEFT)],
				[par_nero("%(modello)s" %dati,font=9, align=TA_LEFT, ), par_nero("%(nome)s" %dati, font=9, align=TA_LEFT, )]]
	table_intest_1 = Table(intest_1, (7*cm, 7*cm), style=style_color)
	
	intest_2 = [[par_nero("<b>Cod. Mod. Originale</b>",8, align=TA_LEFT), par_nero("<b>Nome Mod. Originale</b>",8, align=TA_LEFT)],
				[par_nero("%(modello_orig)s" %dati,8, align=TA_LEFT), par_nero("%(nome_orig)s" %dati,8, align=TA_LEFT)]]
	table_intest_2 = Table(intest_2, (7*cm, 7*cm), style=style_content)
	
	intest_3 = [[par_nero("<b>Costo Industriale</b>",8, align=TA_LEFT), par_nero("<b>Sell-Out Originale</b>",8, align=TA_LEFT), par_nero("",8, align=TA_LEFT)],
				[par_nero("%(costo_ind)s" %dati,8, align=TA_LEFT), par_nero("<b>M1:</b> %(prezzo_m1)s" %dati['prezzi_orig'],8, align=TA_LEFT), par_nero("<b>M2:</b> %(prezzo_m2)s" %dati['prezzi_orig'],8, align=TA_LEFT)]]
	table_intest_3 = Table(intest_3, (7*cm, 4*cm, 3*cm), style=style_content)
	
	intest_4 = [[par_nero("",8, align=TA_LEFT), par_nero("<b>Sell-Out Ricodificato</b>",9, align=TA_LEFT), par_nero("",8, align=TA_LEFT)],
				[par_nero("",8, align=TA_LEFT), par_nero("<b>M1:</b> %(prezzo_m1)s" %dati['prezzi'],8, align=TA_LEFT), par_nero("<b>M2:</b> %(prezzo_m2)s" %dati['prezzi'],font=9, align=TA_LEFT, )]]
	table_intest_4 = Table(intest_4, (7*cm, 4*cm, 3*cm), style=style_color2)
	
	intest_5 = [[par_nero("<b>Art. Tessuto Mod. Originale</b>",8, align=TA_LEFT), par_nero("<b>Descrizione Tessuto Mod. Originale</b>",8, align=TA_LEFT)],
				[par_nero("%(tessuto_orig)s" %dati,8, align=TA_LEFT), par_nero("%s" %dati['tessuto_descr_orig'].decode('latin-1').encode('utf8'),8, align=TA_LEFT)]]
	table_intest_5 = Table(intest_5, (7*cm, 7*cm), style=style_content)
	
	intest_6 = [[par_nero("<b>Faconista</b>",8, align=TA_LEFT), par_nero("<b>Composizione</b>",9, align=TA_LEFT)],
				[par_nero("%(faconista)s - %(ragsoc)s" %dati,8, align=TA_LEFT), table_compos]]
	table_intest_6 = Table(intest_6, (7*cm, 7*cm), style=style_color2)
	
	intest =[[table_intest_1],[table_intest_2],[table_intest_3],[table_intest_4], [table_intest_5], [table_intest_6]]
	table_intest = Table(intest, (14*cm), style=style_intest)
	
	if dati.get('modello_orig', '') and pard.get('AMBIENTE') != 'ot':
		img = get_img_schizzo(pard, {'societa': dati.get('societa',''), 'modello': str(dati.get('modello_orig', ''))[:8]+'000'})
	else:
		img = Spacer(0,10)
	intestazione = [img, table_intest]
	
	story.append(Table([intestazione], (6*cm, 14*cm), style=style_img))

	et_tbl_tot =[]
	count = 0
	if dati['etichetta_layout']:
		for elem in dati['etichetta_layout']:
			count += 1
			trasc = ''
			col1 = Spacer(0,0)
			col2 = Spacer(0,0)
			et_tbl = []
			for k,g in itertools.groupby(elem, key= lambda k: (k['tipo'])):
				d = {'tipo': k,
				     'dati': list(g)}
				if d['tipo'] == 'std':
					riga = []
					#trasc = d['dati'][0]['trasc']
					for dd in d['dati']:
						riga.append(plImage(pard['IMG_DIR']+'/care_symbol_jpg/%s' %dd['img'].replace('gif', 'jpg'), 0.7*cm, 0.7*cm))
					if len(d['dati']) < 5:
						for i in range(len(d['dati']), 5):
							riga.append(Spacer(0,0))
					col1 = Table([riga], (1*cm, 1*cm, 1*cm, 1*cm, 1*cm), style=style_content)
				else:
					for dd in d['dati']:
						et_tbl.append([par_nero('<b>%(tipo)s</b>'%dd,7, align=TA_LEFT ), par_nero('%(desc)s'%dd, 7, align=TA_LEFT)])

			#et_tbl_tot.append([Table([[par_nero('<b>Pezzo %s - CODICE %s/%s</b>'%(count, trasc[:2], trasc[2:]), 8, align=TA_LEFT)]], (13*cm), style=style_color2)])
			et_tbl_tot.append([Table([[par_nero('<b>Pezzo %s</b>' %(count), 8, align=TA_LEFT)]], (13 * cm), style=style_color2)])
			if et_tbl:
				col2 = Table(et_tbl, (2.5 * cm,5.5 * cm), style=style_content)
			et_tbl_tot.append([Table([[col1, col2]], (5*cm, 8*cm), style=style_content)])
		table_etichetta = Table(et_tbl_tot, (13*cm), style=style_content)
	else:
		table_etichetta = Spacer(0,5)

	note = [[par_nero("<b>Etichetta Lavaggio Modello DT</b>",8, align=TA_LEFT), par_nero("<b>Note</b>",8, align=TA_LEFT)],
			[table_etichetta, par_nero("%(nota)s" %dati,7, align=TA_LEFT)]]
	story.append(Table(note, (13*cm, 7*cm), style=style_nota))
	
	story.append(Spacer(0,5))

	tessuto = [[par_nero("<b>Ns. Art. Tessuto</b>",8, align=TA_LEFT), par_nero("<b>Descrizione Tessuto</b>",8, align=TA_LEFT), par_nero("<b>CAPOCMAT</b>",8, align=TA_LEFT), par_nero("<b>Etichetta Lavaggio</b>",8, align=TA_LEFT)]]
	capocmat_list = []
	for t in dati['tessuto']:
		if t.get('capocmat') not in capocmat_list:
			capocmat_list.append(t.get('capocmat'))
			cod_lavaggio = t.get('codice_lavaggio', '')
		else:
			cod_lavaggio = ''
		etichette_lavaggio = render_img_lavaggio(pard, cod_lavaggio)
		tessuto.append([par_nero("%(ns_tessuto)s" %t,8, align=TA_LEFT), par_nero("%(ns_tessuto_descr)s" %t,8, align=TA_LEFT), par_nero("%(capocmat)s" %t,8, align=TA_LEFT), etichette_lavaggio])
	story.append(Table(tessuto, (5*cm, 7*cm, 3*cm, 5*cm), style=style_intest))
	
	story.append(Spacer(0,10))

	titolo = [[ par_nero("<b>Referenza</b>",9, align=TA_CENTER),
				par_nero("<b>CMAT</b>",9, align=TA_CENTER),
				par_nero("<b>Desc. Colore</b>",9, align=TA_CENTER),
				par_nero("<b>Variante</b>",9, align=TA_CENTER),
				par_nero("<b>Tale Quale</b>",7, align=TA_CENTER),
				par_nero("<b>Tot. Capi Lanciati</b>",9, align=TA_CENTER),
				par_nero("<b>Negozi</b>",9, align=TA_CENTER)
				]]
	story.append(Table(titolo, (6*cm, 2*cm, 3.5*cm, 2*cm, 1*cm, 2.5*cm, 3*cm), style=style_title))
	
	num_var = 0
	for var in dati['varianti']:
		riga = [[Spacer(0,5),
				par_nero("%s" %var.get('cmat', ''), 8, align=TA_CENTER),
				par_nero("%s" %var.get('variante_descr', ''), 8, align=TA_CENTER),
				par_nero("%s" %var.get('variante_modello', '').zfill(3), 9, align=TA_CENTER),
				par_nero("%s" %var.get('tale_quale', ''), 9, align=TA_CENTER),
				par_nero("%s" %var.get('qta', ''), 9, align=TA_CENTER),
				par_nero("""501, 502, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 
							520, 521, 522, 523, 524, 51, 901, 902, 904, 905, 906, 907, 908""",7, align=TA_CENTER)
				]]
		
		col_w = (6*cm, 2*cm, 3.5*cm, 2*cm, 1*cm, 2.5*cm, 3*cm)
		col_h = (3*cm)
		
		if num_var >= 3:
			story.append(PageBreak())
			story.append(Table([intestazione], (6*cm, 14*cm)))
			story.append(Table(note, (13*cm, 7*cm), style=style_nota))
			story.append(Spacer(0,5))
			story.append(Table(tessuto, (5*cm, 7*cm, 3*cm, 5*cm),style=style_intest))
			story.append(Spacer(0,5))
			story.append(Table(titolo, (6*cm, 2*cm, 3.5*cm, 2*cm, 1*cm, 2.5*cm, 3*cm), style=style_data))
			num_var = 0
			
		story.append(Table(riga, col_w, col_h, style=style_data))
		num_var +=1
		
	if num_var > 0 and num_var < 3:
		for i in range (num_var, 3):
			riga = [[Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5)]]
			
			col_w = (6*cm, 2*cm, 3.5*cm, 2*cm, 1*cm, 2.5*cm, 3*cm)
			col_h = (3*cm)
			story.append(Table(riga, col_w, col_h, style=style_data))
	
	if not dati['varianti']:
		for i in range (0, 3):
			riga = [[Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5),
					Spacer(0,5)]]
			
			col_w = (6*cm, 2*cm, 3.5*cm, 2*cm, 1*cm, 2.5*cm, 3*cm)
			col_h = (3*cm)
			story.append(Table(riga, col_w, col_h, style=style_data))
		
			
	
	return story


def render_img_lavaggio(pard, cod_lavaggio):
	table_etichetta = Spacer(0, 5)
	if cod_lavaggio:
		img_list = []
		for pos, char in enumerate(cod_lavaggio):
			tipo = pos + 1
			imgPath = pard['IMG_DIR'] + """/paxar/standard_%s%s.png""" % (tipo, char)
			if not os.path.exists(imgPath):
				img_list.append(Spacer(0, 0))
			else:
				max_h= 0.5 * cm
				max_w= 0.5 * cm
				imgObj = plImage(imgPath, max_h, max_w)
				img_list.append(imgObj)

		if len(img_list) < 5:
			for i in range(len(img_list), 5):
				img_list.append(Spacer(0, 0))
		table_etichetta = Table([img_list], (0.8 * cm, 0.8 * cm, 0.8 * cm, 0.8 * cm, 0.8 * cm), (0.4*cm), style=style_etichetta_lavaggio)

	return table_etichetta


def get_img_schizzo(pard, dati, flag_schizzo=False):
	max_h = 6*cm
	max_w = 4*cm
	url_img = ws_client_mmfg.get_img_modello(pard, 'model', dati['societa'], dati['modello'][:11], flag_schizzo=flag_schizzo)
	img = plImage(url_img)
	x,y = img.wrap(max_w, max_h)
	rapp_x = float(x)/max_w
	rapp_y = float(y)/max_h
	if rapp_x > rapp_y:
		x_out = max_w
		y_out = int(float(y) / rapp_x)
	else:
		x_out = int(float(x) / rapp_y)
		y_out = max_h
	img = plImage(url_img, x_out, y_out)
	
	return img


def par_nero(l, font=10, align=TA_CENTER, destra=5, lead=10, righe_vuote=False, fontName='Helvetica'):
	par_style = ParagraphStyle('Normal')
	par_style.fontName = fontName
	par_style.fontSize = font
	par_style.alignment = align
	par_style.textColor = colors.black
	par_style.leftIndent = destra
	par_style.leading = lead
	par_style.space_before = 2
	par_style.space_after = 2
	if type(l) == type([]) or type(l) == type(()):
		data = []
		for item in l:
			if righe_vuote and ((item or "").strip() <= ""):
				data.append(Spacer(0, 15))
			else:
				data.append(Paragraph(item, par_style))
		return data
	else:
		p = Paragraph(l, par_style)
		return p
	
	
def par_color(l, font=10, align=TA_CENTER, destra=5, lead=10, righe_vuote=False, fontName='Helvetica', color=colors.black, backColor = None):
	par_style = ParagraphStyle('Normal')
	par_style.fontName = fontName
	par_style.fontSize = font
	par_style.alignment = align
	par_style.textColor = color
	par_style.backColor = backColor
	par_style.leftIndent = destra
	par_style.leading = lead
	par_style.space_before = 2
	par_style.space_after = 2
	if type(l) == type([]) or type(l) == type(()):
		data = []
		for item in l:
			if righe_vuote and ((item or "").strip() <= ""):
				data.append(Spacer(0, 15))
			else:
				data.append(Paragraph(item, par_style))
		return data
	else:
		p = Paragraph(l, par_style)
		return p


def par_normale(l, allign=TA_JUSTIFY, size=11, leading=2, firstLeftIndent=0, leftIndent=0):
	par_style = ParagraphStyle('Normal')
	par_style.fontName = 'Times-Roman'
	par_style.fontSize = size
	par_style.leading = size + leading
	par_style.leftIndent = leftIndent
	par_style.firstLineIndent = firstLeftIndent
	par_style.rightIndent = 0
	par_style.alignment = allign
	par_style.textColor = colors.black
	if isinstance(l, str):
		p = Paragraph(l, par_style)
		return p
	else:
		data = []
		for item in l:
			try:
				data.append(Paragraph(item, par_style))
			except Exception, e:
				print e
				raise
		return data
	
	
def any_page(canvas, doc):
	global pagina
	pagina += 1
	
	PAGE_HEIGHT = A4[1]
	PAGE_WIDTH = A4[0]
	vpos = PAGE_HEIGHT
	hpos = 4.5 * cm
	# footer	
	canvas.saveState()
	canvas.setFillColorRGB(0.501961, 0.501961, 0.471961) 

	canvas.setFont('Times-Roman', 6)
	canvas.drawRightString(29 * cm, 1 * cm, 'Pagina %d' % pagina)
	canvas.restoreState()




def render_doc(pard, doc):
	j = open(doc['path'], 'r')
	contenuto = j.read()
	j.close()
	tipo = doc['path'].split('.')
	pard['header'] = 'Content-type: data:application/pdf;\n'
	if len(tipo)==2 and tipo[1] not in ('pdf', 'jpeg', 'png', 'gif'):
		pard['header'] = 'Content-type: data:text/plain;\n'
 	pard['header'] = pard['header'] + 'Content-Disposition: attachment; filename=' + doc['name'] + '\n\n'
	pard['html'] = contenuto
	return pard

def get_parametro(pard, type, name):
	if type == 'costanti':
		if name == 'Carta Intestata':
			return """Diffusione Tessile srl unipersonale - Gruppo MaxMara - Capitale sociale 2.500.000 Euro i.v.<br>
					  Sede Legale: 42025 Cavriago (RE) Via Santi 8, Z.I. Corte Tegge<br>
					  Registro Imprese n, RE01044120358 - R.E.A. 156506/RE<br>
					  Cod. Fisc./P.IVA: 01044120358 - Tel.: 0522 494611 - Fax: 0522 944358 - Fax Facon: 0522 494697 - Email: info@diffusionetessile.it - Web: www.diffusionetessile.it
					"""
		elif name == 'Firma Lettera':
			return """ Diffusione Tessile srl /n - Gruppo Max Mara - """
		elif name == 'Intestazione Lettera':
			return """ Diffusione Tessile srl - Gruppo Max Mara /n Via Santi n. 8 - Zona Ind. Corte Tegge  /n 42025 Cavriago (RE) Italy """
		else:
			return ''

def any_page_dt_std(canvas, doc):
	global count_pag
	global pag
	pag += 1
	if pag > count_pag:
		pag = 1
	PAGE_HEIGHT = A4[1]
	PAGE_WIDTH = A4[0]
	vpos = PAGE_HEIGHT - 2.5 * cm
	hpos = 4 * cm

	# header
	if logo:
		canvas.saveState()
		canvas.setFillColorRGB(0.501961, 0.501961, 0.471961)
		canvas.drawImage(logo, hpos, vpos)


	# footer
	if pag == count_pag:
		w, h = trasport_tbl.wrapOn(canvas, 4 * cm, PAGE_WIDTH)
		trasport_tbl.drawOn(canvas, (PAGE_WIDTH - w) / 2.0, 3 * cm)
		canvas.saveState()

	ind = 0
	canvas.setFont('Times-Roman', 6)

	data = string.split(dt_int, '<br>')
	for item in data:
		indent = len(item.strip())
		canvas.drawCentredString(PAGE_WIDTH / 2, 50 - ind * 8, item.strip())
		ind += 1
	canvas.drawCentredString(PAGE_WIDTH / 2, 8, "Pagina %s di %%s" % pag % count_pag)


def build_stampa_studio_diba(pard, dati_list):
	global count_pag
	global pag
	count_pag = 0
	story = []

	titolo = [[par_nero("<b>Art.</b>", 9, align=TA_CENTER),
	           par_nero("<b>Misura</b>", 9, align=TA_CENTER),
			   par_nero("<b>Col.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Vr. Modello</b>", 9, align=TA_CENTER),
			   par_nero("<b>Fabb</b>", 9, align=TA_CENTER),
			   par_nero("<b>Descr/posiz</b>", 9, align=TA_CENTER),
			   par_nero("<b>Cons.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Pz.</b>", 9, align=TA_CENTER),
			   par_nero("<b>C.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Note</b>", 9, align=TA_CENTER)
			   ]]

	for modello in dati_list:
		count_pag += 1
		riga_left = [
			[par_nero("<b>Cod. Mod.</b>", 7, align=TA_CENTER), par_nero(modello[0]['modello'], 7, align=TA_CENTER)],
			[par_nero("<b>Nome Mod.</b>", 7, align=TA_CENTER), par_nero(modello[0]['nome_modello'], 7, align=TA_CENTER)],
		]

		riga_left_var = []

		for gruppo in modello:
			riga_left_var.append([par_nero("<b>Var. "+ gruppo['variante'] +"</b>", 7, align=TA_CENTER), par_nero("<b>Descr:</b> "+ gruppo['descr_col_variante'], 7, align=TA_CENTER), par_nero("<b>Tot.Capi:</b> "+str(gruppo['somma_capi']), 7, align=TA_CENTER)])

		if modello[0].get('modello', '') and pard['AMBIENTE'] not in ('ot'):
			img = get_img_schizzo(pard, {'modello': modello[0]['modello'][:8]+'000', 'societa': modello[0]['societa']}, flag_schizzo=True)
		else:
			img = Drawing(200, 100)
			img.add(Rect(20, 0, 200, 114, fillColor=colors.white, strokeColor=colors.lightgrey))
			img.add(String(110, 55, 'IMG', fontSize=10, fillColor=colors.grey))

		tab_left = [[Table(riga_left, (2 * cm, 6 * cm), (1 * cm), style=style_data), Table(riga_left_var, (2 * cm, 4 * cm, 2 * cm), (1 * cm), style=style_data)]]
		tab_intestazione = [[tab_left, img]]


		story.append(Table(tab_intestazione))
		story.append(Spacer(0, 50))

		story.append(Table(titolo, (2.5 * cm, 1.5 * cm, 1.5 * cm, 2 * cm, 2 * cm, 4.5 * cm,  1.5 * cm, 1 * cm, 1 * cm, 2 * cm),
						   (0.6 * cm), style=style_title))
		count = 0
		page_n = 1
		for gruppo in modello:

			for art in sorted(gruppo['variante_group'], key= lambda k:(k['capocmat'], k['misura'], k['colore'])):
				riga = [[
					par_nero("%s" % art.get('capocmat', '')[0:9], 8, align=TA_CENTER),
					par_nero("%s" % art.get('misura', ''), 8, align=TA_CENTER),
					par_nero("%s" % art.get('colore', ''), 8, align=TA_CENTER),
					par_nero("%s" % art.get('variante', '').zfill(3), 8, align=TA_CENTER),
					par_nero("%s" % art.get('fabbisogno', ''), 8, align=TA_CENTER),
					par_nero("%s - %s" % (art.get('descrizione', '').decode('utf-8').encode('utf8'),
					                      art.get('posizione', '').decode('utf-8').encode('utf8')), 7, align=TA_LEFT),
					par_nero("%s" % art.get('consumo', ''), 8, align=TA_CENTER),
					par_nero("%s" % art.get('pezzo', ''), 8, align=TA_CENTER),
					par_nero("%s" % art.get('colonna', ''), 8, align=TA_CENTER),
					par_nero("%s" % art.get('nota', ''), 8, align=TA_CENTER)
				 ]]

				col_w = (2.5 * cm, 1.5 * cm, 1.5 * cm, 2 * cm, 2 * cm, 4.5 * cm,  1.5 * cm, 1 * cm, 1 * cm, 2 * cm)
				col_h = (0.8 * cm)

				line = 1
				if (len(art['posizione'])+len(art['descrizione']))> 60:
					line = (len(art['posizione'])+len(art['descrizione']))/60 + 1 #una riga 60 caratteri circa

				if ((count+line != 0 and (count+line) / 20 > 0 and page_n == 1) or (count+line != 0 and (count+line) / 26 > 0 and page_n > 1)):
					story.append(PageBreak())
					story.append(Spacer(0, 10))
					story.append(Table(titolo, (2.5 * cm, 1.5 * cm, 1.5 * cm, 2 * cm, 2 * cm, 4.5 * cm,  1.5 * cm, 1 * cm, 1 * cm, 2 * cm),
					   (0.8 * cm), style=style_title))
					count = 0
					count_pag +=1
					page_n += 1
				story.append(Table(riga, col_w, style= (style_data, style_data_highlighted)[art['stato'] == 'valutazione']))
				count += line

		story.append(PageBreak())
	return story


def build_studio_diba_doc(pard, data):
	global dt_int, logo
	global count_pag
	global pag
	global trasport_tbl
	pag = 0
	count_pag = 0
	dt_int = get_parametro(pard, 'costanti', '')
	logo ='%(IMG_DIR)s/logo_menu_gif.jpg' % pard
	PAGE_HEIGHT = A4[0]
	PAGE_WIDTH = A4[1]
	prog_documento = 0
	lista_pdf = []
	prog_documento = 0

	trasport = [['']]

	col_w = (20 * cm)

	trasport_tbl = Table(trasport, col_w)
	style_data = TableStyle([])
	style_data.add('BOTTOMPADDING', (0, 0), (-1, -1), 0)
	style_data.add('TOPPADDING', (0, 0), (-1, -1), 0)
	style_data.add('LEFTPADDING', (0, 0), (-1, -1), 0)
	style_data.add('RIGHTPADDING', (0, 0), (-1, -1), 0)
	trasport_tbl.setStyle(style_data)

	story = []
	buffer_IO = StringIO.StringIO()
	prog_documento += 1
	doc = SimpleDocTemplate(buffer_IO, pagesize=portrait(A4), showBoundary=0)
	doc.leftMargin = LEFT_MARGIN
	doc.rightMargin = RIGHT_MARGIN
	doc.topMargin = 100
	doc.bottomMargin = 40
	n_pagine = 1

# 	for p in xrange(n_pagine):
# 		if p > 0:
# 			story.append(PageBreak())

	story.extend(build_stampa_studio_diba(pard, data))

	# Generazione del documento pdf
	doc.build(story, onFirstPage=any_page_dt_std, onLaterPages=any_page_dt_std)
	# impostazione file pdf da generare
	# now_str = datetime.now().strftime("%Y%m%d%H%M%S")
	diz = {}
	if type(data[0]) == list:
		diz['modello'] = ''
		diz['variante'] = ''
		name = 'StampaStudioDibaMultiplo.pdf'
	else:
		diz['modello'] = data[0].get('modello', '')
		diz['variante'] = data[0].get('variante', '')
		name = 'StampaStudioDiba%(modello)s.pdf' % data[0]


	lista_pdf.append((name, buffer_IO.getvalue()))
	buffer_IO.close()

	nome_file = lista_pdf[0][0]
	contenuto = lista_pdf[0][1]

	anno = str(date.today().year)

	partial_path = "%(DOC_BASE_DIR)s/diba/"%pard + anno + '/'
	try:
		if not os.path.isdir(partial_path):
			os.makedirs(partial_path)
	except:
		raise ForwardException("Impossibile creare la cartella per i file pdf.")
	fullpath = partial_path + nome_file

	j = open(fullpath, 'w')
	j.write(contenuto)
	j.close()

	diz = {}
	diz['NAME_FILE'] = nome_file
	diz['PATH'] = fullpath
	return diz


def build_stampa_modello(pard, dati_list):
	global count_pag
	global pag
	count_pag = 0
	count_pag += 1
	count = 0
	page_n = 1
	story = []

	titolo = [[par_nero("<b>Soc.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Gm</b>", 9, align=TA_CENTER),
			   par_nero("<b>Art.</b>", 9, align=TA_CENTER),
			   par_nero("<b>A/S</b>", 9, align=TA_CENTER),
	           par_nero("<b>Mis.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Col.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Descr.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Pos.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Cons.</b>", 9, align=TA_CENTER),
			   par_nero("<b>Pz.</b>", 9, align=TA_CENTER),
			   par_nero("<b>C.</b>", 9, align=TA_CENTER),
			   ]]

	riga_left = [
		[par_nero("<b>Cod. Mod.</b>", 8, align=TA_CENTER), par_nero(dati_list[0]['prodotto'], 8, align=TA_CENTER)],
		[par_nero("<b>Nome Mod.</b>", 8, align=TA_CENTER), par_nero(dati_list[0]['nome'], 8, align=TA_CENTER)],
		[par_nero("<b>Variante</b>", 8, align=TA_CENTER), par_nero(dati_list[0]['variante_prodotto'], 8, align=TA_CENTER)]
	]

	if dati_list[0].get('prodotto', '') and pard['AMBIENTE'] not in ('ot'):
		img = get_img_schizzo(pard, {'modello': dati_list[0]['prodotto'][:8] + '000', 'societa': dati_list[0]['societa']})
	else:
		img = Drawing(200, 100)
		img.add(Rect(20, 0, 200, 114, fillColor=colors.white, strokeColor=colors.lightgrey))
		img.add(String(110, 55, 'IMG', fontSize=10, fillColor=colors.grey))

	tab_left = Table(riga_left, (3 * cm, 5 * cm), (1 * cm), style=style_data)
	tab_intestazione = [[tab_left, img]]

	story.append(Table(tab_intestazione))
	story.append(Spacer(0, 50))

	story.append(
		Table(titolo, (1.5 * cm, 1 * cm, 1.5 * cm, 1 * cm, 1.5 * cm, 1.5 * cm,  4.5 * cm, 4.5 * cm, 1.5 * cm, 1 * cm, 1 * cm),
			  (0.6 * cm), style=style_title))

	for modello in dati_list:
		riga = [[
			par_nero("%s" % modello.get('societa', '')[0:9], 8, align=TA_CENTER),
			par_nero("%s" % modello.get('gm', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('articolo', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('a_s', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('misura_prodotto_componente', '')[-2:], 8, align=TA_CENTER),
			par_nero("%s" % modello.get('variante_prodotto_componente', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('descrizione', '').decode('latin-1').encode('utf8'), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('posizione', '').decode('latin-1').encode('utf8'), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('consumo_netto', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('progr_pezzo', ''), 8, align=TA_CENTER),
			par_nero("%s" % modello.get('progr_componente_coloritura', ''), 8, align=TA_CENTER)
		 ]]

		col_w = (1.5 * cm, 1 * cm, 1.5 * cm, 1 * cm, 1.5 * cm, 1.5 * cm,  4.5 * cm, 4.5 * cm, 1.5 * cm, 1 * cm, 1 * cm)

		if ((count / 20 > 0 and page_n == 1) or (count / 28 > 0 and page_n > 1)):
			story.append(PageBreak())
			story.append(Spacer(0, 10))
			story.append(Table(titolo, (1.5 * cm, 1 * cm, 1.5 * cm, 1 * cm, 1.5 * cm, 1.5 * cm,  4.5 * cm, 4.5 * cm, 1.5 * cm, 1 * cm, 1 * cm),
			   (0.8 * cm), style=style_title))
			count = 0
			count_pag +=1
			page_n += 1

		if len(modello.get('posizione')) > 200 or len(modello.get('descrizione', '')) > 200:
			count += 3
		elif (len(modello.get('posizione')) > 100 and len(modello.get('posizione')) < 200) or (len(modello.get('descrizione', '')) > 100 and len(modello.get('descrizione', '')) < 200):
			count += 2
		else:
			count += 1
		# story.append(Table(riga, col_w, style= (style_data, style_data_highlighted)[modello['stato'] == 'valutazione']))
		# count += 1
		story.append(Table(riga, col_w, style= style_data))

	return story


def build_studio_modello_doc(pard, data):
	global dt_int, logo
	global count_pag
	global pag
	global trasport_tbl
	pag = 0
	count_pag = 0
	dt_int = get_parametro(pard, 'costanti', '')
	logo ='%(IMG_DIR)s/logo_menu_gif.jpg' % pard
	PAGE_HEIGHT = A4[0]
	PAGE_WIDTH = A4[1]
	prog_documento = 0
	lista_pdf = []
	prog_documento = 0

	trasport = [['']]

	col_w = (20 * cm)

	trasport_tbl = Table(trasport, col_w)
	style_data = TableStyle([])
	style_data.add('BOTTOMPADDING', (0, 0), (-1, -1), 0)
	style_data.add('TOPPADDING', (0, 0), (-1, -1), 0)
	style_data.add('LEFTPADDING', (0, 0), (-1, -1), 0)
	style_data.add('RIGHTPADDING', (0, 0), (-1, -1), 0)
	trasport_tbl.setStyle(style_data)

	story = []
	buffer_IO = StringIO.StringIO()
	prog_documento += 1
	doc = SimpleDocTemplate(buffer_IO, pagesize=portrait(A4), showBoundary=0)
	doc.leftMargin = LEFT_MARGIN
	doc.rightMargin = RIGHT_MARGIN
	doc.topMargin = 100
	doc.bottomMargin = 40
	n_pagine = 1

# 	for p in xrange(n_pagine):
# 		if p > 0:
# 			story.append(PageBreak())

	story.extend(build_stampa_modello(pard, data))

	# Generazione del documento pdf
	doc.build(story, onFirstPage=any_page_dt_std, onLaterPages=any_page_dt_std)
	# impostazione file pdf da generare
	# now_str = datetime.now().strftime("%Y%m%d%H%M%S")
	diz = {}
	diz['modello'] = data[0].get('modello', '')
	diz['variante'] = data[0].get('variante', '')
	name = 'StampaDiba%(societa)s_%(prodotto)s_%(variante_prodotto)s.pdf' % data[0]


	lista_pdf.append((name, buffer_IO.getvalue()))
	buffer_IO.close()

	nome_file = lista_pdf[0][0]
	contenuto = lista_pdf[0][1]

	anno = str(date.today().year)

	partial_path = "%(DOC_BASE_DIR)s/diba/"%pard + anno + '/'
	try:
		if not os.path.isdir(partial_path):
			os.makedirs(partial_path)
	except:
		raise ForwardException("Impossibile creare la cartella per i file pdf.")
	fullpath = partial_path + nome_file

	j = open(fullpath, 'w')
	j.write(contenuto)
	j.close()

	diz = {}
	diz['NAME_FILE'] = nome_file
	diz['PATH'] = fullpath
	return diz