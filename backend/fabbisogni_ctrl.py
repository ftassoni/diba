import traceback

from Common import decorators
from facon.ordpf.facade.pool_manager import Request
from facon.diba.view import gestione_fabbisogni as views_gest_fabbisogni
from facon.diba.facade.fabbisogni_facade import FabbFacade
from facon.pao.dao import ordiniDB
from facon.pao.facade.giacenza_mp import GiacenzaMPFacade
from facon.diba.doc import diba_doc
from facon.pao.facade.ordine_mp import OrdineMateriaPrimaFacade

import tools, cjson


def vista_gestione_fabbisogni(pard):
	"""
	controller della pagina principale dei listini
	:param pard:
	:return:
	"""
	try:
		pard['html'] = views_gest_fabbisogni.gestione_fabbisogni(pard)
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard


@decorators.buildJsonResultSet
def autocomplete_classi(pard):
	facade = FabbFacade(pard)
	a = facade.autocomplete_classi()
	# tools.dump(a)
	return a


@decorators.buildJsonResultSet
def autocomplete_colore(pard):
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	a = facade.autocomplete_colore(params)
	# tools.dump(a)
	return a


@decorators.buildJsonResultSet
def autocomplete_misura(pard):
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	a = facade.autocomplete_misura(params)
	# tools.dump(a)
	return a


@decorators.buildJsonResultSet
def ricerca_fabbisogni(pard):
	facade = FabbFacade(pard)
	dict_params = cjson.decode(Request().get_val('dati', "{}"))
	result = facade.ricerca_fabbisogni(dict_params)
	return result


@decorators.buildJsonResultSet
def get_fabbisogni(pard):
	facade = FabbFacade(pard)
	id_dibas = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.get_fabbisogni_da_gestire(id_dibas)

	giac_mp_facade = GiacenzaMPFacade(pard)
	for r in result:
		r['giacenza'] = giac_mp_facade.get_giacenza_complessiva(r['diba'])

	return result


@decorators.buildJsonResultSet
def get_fabbisogni_refresh(pard):
	# fa la get normale di tutti i fabbisogni
	# ma li unisce in un unico risultato in modo da compilare un'unica tabella a frontend
	facade = FabbFacade(pard)
	id_dibas = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.get_fabbisogni_da_gestire(id_dibas)
	if result:
		r = result[0].copy()
		giac_mp_facade = GiacenzaMPFacade(pard)
		r['giacenza'] = giac_mp_facade.get_giacenza_complessiva(r['diba'])
		r['fabb_list'] = []
		for rr in result:
			r['fabb_list'].extend(rr['fabb_list'])
		return [r]
	else:
		return []


@decorators.buildJsonResultSet
def ricerca_giacenza(pard):
	giac_mp_facade = GiacenzaMPFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	result = giac_mp_facade.ricerca_giacenza_complessiva(params)

	return result


@decorators.buildJsonResultSet
def ricerca_giacenza_da_impegni(pard):
	giac_mp_facade = GiacenzaMPFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	giac_result = giac_mp_facade.ricerca_giacenza_complessiva(params)
	return giac_result


@decorators.buildJsonResultSet
def get_giacenza_articolo(pard):
	giac_mp_facade = GiacenzaMPFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	result = giac_mp_facade.get_giacenza_articolo(params)

	return result

@decorators.buildJsonResultSet
def get_giacenza_complessiva(pard):
	giac_mp_facade = GiacenzaMPFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	result = giac_mp_facade.get_giacenza_complessiva(params)

	return result

@decorators.buildJsonResultSet
def get_giacenza_complessiva_refresh(pard):
	giac_mp_facade = GiacenzaMPFacade(pard)
	params = cjson.decode(Request().get_val('dati', "{}"))
	result = giac_mp_facade.get_giacenza_complessiva_refresh(params)
	return result


# @decorators.buildJsonResultSet
# def get_studio_diba(pard):
#     facade = FabbFacade(pard)
#     id_righe_r_comm = cjson.decode(Request().get_val('dati', "[]"))
#     result = facade.get_studio_diba(id_righe_r_comm)
#     return result


@decorators.buildJsonResultSet
def salva_impegno(pard):
	"""
	salva - crea/aggiorna richiesta studio
	:param pard:
	:return result:  {'result': '', 'errors': ''}
	"""
	facade = FabbFacade(pard)
	params = cjson.decode(Request().get_val('dati', "[]"))
	result = facade.salva_impegno(params)
	return result


@decorators.buildJsonResultSet
def salva_articolo_duplicato(pard):
	"""
	salva - crea/aggiorna richiesta studio
	:param pard:
	:return result:  {'result': '', 'errors': ''}
	"""
	facade = FabbFacade(pard)
	dict_params = cjson.decode(Request().get_val('dati', "{}"))
	result = facade.salva_articolo_duplicato(dict_params)
	return result


@decorators.buildJsonResultSet
def get_ordini_da_gestione_impegni(pard):
	dict_params = cjson.decode(pard.get('dati', '{}'))
	facade = FabbFacade(pard)
	result = facade.get_ordini_da_fabbisogni(dict_params)
	return result


@decorators.buildJsonResultSet
def get_disimpegno_ordini_da_gestione_impegni(pard):
	dict_params = cjson.decode(pard.get('dati', '{}'))
	facade = FabbFacade(pard)
	result = facade.get_disimpegno_ordini_da_gestione_impegni(dict_params)
	return result

@decorators.buildJsonResultSet
def get_sblocca_impegni(pard):
	dict_params = cjson.decode(pard.get('dati', '{}'))
	facade = FabbFacade(pard)
	result = facade.get_sblocca_fabbisogni(dict_params)
	return result


@decorators.buildJsonResultSet
def crea_ordini_da_fabb(pard):
	dict_params = cjson.decode(pard.get('dati', '{}'))
	facade = FabbFacade(pard)
	result = facade.crea_ordini_preordine_by_fabb(dict_params)
	return result


@decorators.buildJsonResultSet
def modifica_ordini_da_fabb(pard):
	dati = cjson.decode(pard.get('dati', '[]'))
	facade = FabbFacade(pard)
	result = facade.modifica_ordini_preordini_by_fabb(dati)
	return result


@decorators.buildSimpleJson
def stampa_distinta_base(pard):
	facade = FabbFacade(pard)
	dati = cjson.decode(Request().get_val('dati', '{}'))
	result =[]
	for d in dati:
		query_modello = facade.get_dati_studio_diba_pdf(d)
		result.append(query_modello)

	pdf = diba_doc.build_studio_diba_doc(Request().get_pard(), result)

	return {'result': 'OK', 'message': pdf}


def render_doc(pard):
	"""
	:param pard:
	:return:
	"""
	try:
		doc = cjson.decode(pard.get('doc', {}))
		diz = {'path': doc['PATH'], 'name': doc['NAME_FILE']}
		diba_doc.render_doc(pard, diz)

	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())

	return pard