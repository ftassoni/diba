import sys
#sys.path.insert(0, '/Users/fabiotassoni/dt-home/re/mp/bin')
#sys.path.insert(1, '/Users/fabiotassoni/dt-home/re/mp/bin/workflow_mmfg/')
sys.path.insert(0, '/home/re/mp/bin')
sys.path.insert(1, '/home/re/mp/bin/workflow_mmfg/')

from Common import constants, logger

import tools
import datetime
import traceback
import time
import notifica_mail
import pprint
from facon.diba.facade import diba_facade

DESTINATARI_MAIL = ['dtsupport@otconsulting.com']


def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end
       

def main(pard, dati):
	try:
		start = datetime.time(7, 30, 0)
		midday_end = datetime.time(12, 20, 0)
		midday_start = datetime.time(12, 40, 0)
		end = datetime.time(18, 30, 0)
		today = datetime.datetime(*time.localtime()[:3])
		
		tools.batch_logging('Avvio esecuzione...')
		dbfac = diba_facade.DiBaFacade(pard)
		resd = dbfac.importa_capocmat_diba(dati)

	except Exception, e:
		tools.batch_logging(traceback.format_exc())
		if pard['AMBIENTE'] != 'svil':
			body = "Messaggio dal server: %(IP_ADDRESS)s <br><br> " %pard
			body += "Errore nel batch di importazione dei capocmat delle diba (batch_importa_capocmat_diba.py) <br><br> "
			body += traceback.format_exc() + "<br><br>"
			body += "Consultare il log in /home/re/mp/log/batch_importa_capocmat_diba.log"
			subject = "[BATCH] Errore: Importazione CAPOCMAT DIBA"
			notifica_mail.invia('debug@diffusionetessile.it',DESTINATARI_MAIL,
						subject=subject,corpo=body,rispondi_a=[],tipo='html',pre=False)
	finally:
		tools.batch_logging('Fine esecuzione...')
		
if __name__ == "__main__" :
	import config
	pard = config.configura_batch_crontab(user='CRON')
	dati = {
		'capocmat': ''
	}
	if len(sys.argv) > 2 and sys.argv[1] == '-c':
		dati['capocmat'] = sys.argv[2]
	main(pard, dati)	
