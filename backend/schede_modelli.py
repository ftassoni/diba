# -*- coding: utf-8 -*-
import tools
import time
import copy
import cjson
import caio_print as cp
import os
import traceback
import itertools
import db_access
import simplejson as json

from anno_stagione_lib import decode_as
from facon.diba.view import schede_modelli_view
from facon.diba.dao import dwg_dao
from facon.diba.dao import diba_dao
from facon.diba.doc import diba_doc
from facon.ordpf.dao.richiesteDB import RigaRichiestaCommercialeDAO

from Common import errors
from Common import decorators



def main(pard):
	'''
	Funzione base che si occupa di disegnare la pagina di ricerca
	'''
	try:
		pard['html'] = schede_modelli_view.pagina_base(pard)
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard



@decorators.buildJsonResultSet
def ricerca_bp(pard):
	dict_params = cjson.decode(pard.get('params_ricerca', {}))
	res = diba_dao.ricerca_bp(pard, dict_params)
	return res


@decorators.buildJsonResultSet	
def ricerca_collezione(pard):
	return diba_dao.get_collezione_dt_autocomplete(pard, pard.get('query',''))
	

def crea_scheda(pard):
	'''
	Funzione base che si occupa di disegnare la pagina della scheda modello
	'''
	try:
		dati = diba_dao.crea_scheda_modello(pard, pard.get('modello', ''))
		dati['id_scheda_old'] = pard.get('id_scheda_old', '')
		if dati['id_scheda_old']:
			dati['id_scheda'] = ''
		dati['prezzi'] = diba_dao.get_prezzi_modello(pard, pard.get('modello', ''))
		dati['composizioni'] = diba_dao.get_compos_modello(pard, pard.get('modello', ''))
		dati['prezzi_orig'] = diba_dao.get_prezzi_modello(pard, pard.get('modello_orig', ''))
		dati['etichetta'] = diba_dao.get_etichetta_lavaggio_completa(pard, pard.get('modello', ''))
		dati_orig = {}
		if pard.get('societa', '') and pard['societa'] in ('MA', 'MM', 'MN'):
			try:
				dati_orig = dwg_dao.get_modello_originale(pard, pard['societa'], pard.get('modello_orig', ''))
			except errors.DAOException, e:
				dati_orig['desc_articolo'] = 'Modello NON trovato su DB gruppo.'
# 			dati_articolo = diba_dao.get_articolo_by_id(pard, dati_orig['articolo'], pard['societa'])
# 			dati_orig.update(dati_articolo)

		if not dati.get('tessuto', []) or not dati['tessuto'][0].get('capocmat', '') or not dati_orig.get('progr_articolo', '') or dati['tessuto'][0]['capocmat'][6:9] != dati_orig['progr_articolo']:
			for var in dati.get('varianti', []):
				var['variante_orig'] = ''
		pard['html'] = schede_modelli_view.pagina_base_scheda(pard, dati, dati_orig)
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard



def visualizza_scheda(pard):
	'''
	Funzione base che si occupa di disegnare la pagina della scheda modello
	'''
	try:
		dati = diba_dao.get_scheda_modello(pard, pard.get('id_scheda', ''))
		dati['prezzi'] = diba_dao.get_prezzi_modello(pard, dati.get('modello', ''))
		dati['composizioni'] = diba_dao.get_compos_modello(pard, dati.get('modello', ''))
		dati['prezzi_orig'] = diba_dao.get_prezzi_modello(pard, dati.get('modello_orig', ''))
		dati['etichetta'] = diba_dao.get_etichetta_lavaggio_completa(pard, dati.get('modello', ''))
		dati_orig = {}
		if dati.get('societa', '') and dati['societa'] in ('MA', 'MM', 'MN'):
			try:
				dati_orig = dwg_dao.get_modello_originale(pard, dati['societa'], dati.get('modello_orig', ''))
			except errors.DAOException, e:
				dati_orig['desc_articolo'] = 'Modello NON trovato su DB gruppo.'
			#dati_articolo = diba_dao.get_articolo_by_id(pard, dati_orig['articolo'], dati['societa'])
			#dati_orig.update(dati_articolo)
		pard['html'] = schede_modelli_view.pagina_base_scheda(pard, dati, dati_orig)
	except Exception, e:
		pard['html'] = tools.format_errore(traceback.format_exc())
		return pard
	return pard


@decorators.buildSimpleJson
def salva_scheda(pard):
	try:
		conn = db_access.mysql_connect(pard)
		dati = cjson.decode(pard.get('params', {}))
		
		if dati.get('ID_SCHEDA_OLD', ''):
			diba_dao.delete_scheda_modello(pard, dati['ID_SCHEDA_OLD'], conn)
		else:
			dati['ID_SCHEDA'] = diba_dao.check_scheda_modello(pard, dati['MODELLO'])
		
		if dati['ID_SCHEDA']:
			id_scheda = diba_dao.update_scheda_modello(pard, dati, conn)
		else:
			id_scheda = diba_dao.insert_scheda_modello(pard, dati, conn)
			
	except errors.BOException, e:
		conn.rollback()
		conn.close()
		return {'result':'KO', 'message': e.value}
	except Exception, ed:
		conn.rollback()
		conn.close()
		return {'result':'KO', 'message': traceback.format_exc()}
	
	conn.commit()
	conn.close()
	return {'result':'OK','message': {'id_scheda' : id_scheda}}


@decorators.buildSimpleJson		
def crea_scheda_pdf(pard):	
	try:
		dati = {}
		if pard.get('id_scheda', ''):
			dati = diba_dao.get_scheda_modello(pard, pard.get('id_scheda', ''))
		else:
			dati = diba_dao.crea_scheda_modello(pard, pard.get('modello', ''))
		if dati.get('modello') in ['None', None, '', 0]:
			raise errors.FacadeException('Modello %s non presente in anagrafica' % pard.get('modello'))
		dati['prezzi'] = diba_dao.get_prezzi_modello(pard, dati.get('modello', ''))
		dati['composizioni'] = diba_dao.get_compos_modello(pard, dati.get('modello', ''))
		dati['prezzi_orig'] = diba_dao.get_prezzi_modello(pard, dati.get('modello_orig', ''))
		dati['etichetta'] = diba_dao.get_etichetta_lavaggio_completa(pard, dati.get('modello', ''))
		for t in dati['tessuto']:
			capocmat = t.get('capocmat')
			anno_stagione = decode_as(capocmat[4:6])
			codice_lavaggio = RigaRichiestaCommercialeDAO.get_info_articolo_capocmat(pard, soc_orig=capocmat[0:2],
																					 art_orig=capocmat[6:],
																					 gm=capocmat[2:4],
																					 a_s=anno_stagione)
			if codice_lavaggio:
				t['codice_lavaggio'] = codice_lavaggio[0]['codice_lavaggio']
			else:
				t['codice_lavaggio'] = ''

		dati['etichetta_layout'] = []
		for e in dati['etichetta']:
			dati['etichetta_layout'].append(diba_dao.get_img_etichetta_lavaggio_completa(pard, e['etichetta']))
		dati_orig = {}
		if dati.get('societa', '') and dati['societa'] in ('MA', 'MM', 'MN'):
			try:
				dati_orig = dwg_dao.get_modello_originale(pard, dati['societa'], dati.get('modello_orig', ''))
			except errors.DAOException, e:
				dati_orig['desc_articolo'] = 'Modello NON trovato su DB gruppo.'
			#dati_articolo = diba_dao.get_articolo_by_id(pard, dati_orig['articolo'], dati['societa'])
			#dati_orig.update(dati_articolo)
		
		doc = diba_doc.build_pdf_scheda(pard, dati, dati_orig)
	except errors.FacadeException, e:
		return {'result':'KO', 'message': e.value }
	except Exception, ed:
		return {'result':'KO', 'message': traceback.format_exc()}
	else:
		return {'result':'OK','message': doc}
	

def visualizza_pdf(pard):
	try:
		doc_name = pard['doc_name']
		doc_path = pard['doc_path']
		diba_doc.render_doc(pard, {'name':doc_name, 'path':doc_path})
	except Exception, ed:
		pard['html'] = tools.format_errore(traceback.format_exc())
	return pard












