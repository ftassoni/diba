(function ()
{
	'use strict';

	var controllerId = "StudioDibaCtrl";

	ControllerFn.$inject = ['$scope', '$rootScope', 'LoggerFactory', '$window', '$location', 'ServiceTipoStudio'];

	angular.module('app').controller(controllerId, ControllerFn);
    angular.module('app').service('ServiceTipoStudio', function() {
      this.tipoStudio = 'diba' ;
    });
	function ControllerFn($scope, $rootScope, LoggerFactory, $window, $location, ServiceTipoStudio)
	{

		var vm = this;

		function getUrlTipoStudio() {
            vm.url_params = $location.search()||"Unknown";
            return vm.url_params.hasOwnProperty('tipo_studio') ? vm.url_params['tipo_studio'] : "";
        }

        ServiceTipoStudio.tipoStudio = getUrlTipoStudio();

        // Se studio permanenti cambio il titolo della pagina
        if (getUrlTipoStudio() === 'permanenti') {
            $('head title').html('Studio Permanenti');
            $('.page-title').html('Studio Permanenti');
        }

        //Aggiugo ai link di percorso l'attributo target
		//per raggiungere la pagina
        $('.breadcrumb li a').attr('target', '_self');
        $('.dropdown-user .box-user-info a').attr('target', '_self');

	}
})();