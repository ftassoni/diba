import db_access
import tools
import itertools
import time
from Common import errors
import math

import anno_stagione_lib
from facon.ordpf.dao import decorators
from facon.ordpf.facade import pool_manager
from facon.ordpf.facade.pool_manager import Request, ConnPool, CONN_KEY

from facon.diba.dao import dwg_dao
from facon.origana import importa_anagrafica


def ricerca_bp(pard, dict_params):
	q_societa = ''
	societa = ''
	if dict_params.get('societa','') and  dict_params['societa'] != 'Tutti':
		if dict_params['societa'] == 'MM':
			societa = '006'
		elif  dict_params['societa'] == 'MA':
			societa = '002'
		elif  dict_params['societa'] == 'MN':
			societa = '003'
		elif  dict_params['societa'] == 'DT':
			societa = '013'
		q_societa = " and SUBSTR(F.C_MAT_A, 10, 3)  like '%s' " %societa
	
	q_as = ''
	if dict_params.get('as','') and dict_params['as']!='Tutti':
		anno = dict_params['as'][3]
		if dict_params['as'][5:] == 'P/E':
			stag = '2'
		else:
			stag = '4'
		q_as = "and (A1.ANNO = '%s' and A1.STAG = '%s') " %(anno, stag)
		
	q_gm_ns_tessuto = ''
	if dict_params.get('gm_ns_tessuto',''):
		q_gm_ns_tessuto = " and substr(F.TESSUTO, 1,2) = '%(gm_ns_tessuto)s' " %dict_params
		
	q_art_ns_tessuto = ''
	if dict_params.get('art_ns_tessuto',''):
		q_art_ns_tessuto = " and substr(F.TESSUTO, 3, 3) = '%(art_ns_tessuto)s' " %dict_params
		
	q_ns_tessuto_descr = ''
	if dict_params.get('ns_tessuto_descr',''):
		q_ns_tessuto_descr = ' and FA.ART_ORIG_DESCR like "%%%(ns_tessuto_descr)s%%" ' %dict_params
		
	q_tessuto_orig = ''
	if dict_params.get('gm_tessuto_orig','') or dict_params.get('art_tessuto_orig','') or dict_params.get('tessuto_orig_descr',''):
		gm = dict_params.get('gm_tessuto_orig','')
		art = dict_params.get('art_tessuto_orig','')
		descr = dict_params.get('tessuto_orig_descr','')
		modelli_orig_list = dwg_dao.get_modello_originale_by_articolo(pard, dict_params.get('societa',''), art, gm, descr)
		
		q_tessuto_orig = " and substring(A2.C_MAT, 1, 11) in ('%s') " %"','".join(modelli_orig_list)
		
		
	q_classe = ''
	if dict_params.get('classe',''):
		q_classe = " and SUBSTR(A1.C_MAT, 2, 2)  like '%(classe)s' " %dict_params
	
	q_collezione = ''
	if dict_params.get('collezione',''):
		q_collezione = " and F.CMATSET like '%%%(collezione)s%%' " %dict_params
	
	q_modello_orig = ''
	if dict_params.get('modello_orig',''):
		q_modello_orig = " and A2.C_MAT like '%%%(modello_orig)s%%'  " %dict_params
	
	q_modello = ''
	if dict_params.get('modello',''):
		q_modello = " and A1.C_MAT like '%%%(modello)s%%' " %dict_params	
	
	q_nome = ''
	if dict_params.get('nome_modello',''):
		q_nome = " and A1.D_MAT like '%%%s%%' " %dict_params['nome_modello'].upper()
	
	q_nome_orig = ''
	if dict_params.get('nome_modello_orig',''):
		q_nome_orig = " and A2.D_MAT like '%%%s%%' " %dict_params['nome_modello_orig'].upper()
		
#	q_fornitore = ''
#	if dict_params.get('fornitore',''):
#		q_fornitore = " and m.articolo = '%(fornitore)s' " %dict_params
	
	query = """select A1.TI_ROWID,
					substr(A1.C_MAT, 1,12) as modello, 
					A1.D_MAT as nome,
					IFNULL(S.MODELLO_ORIG, ifnull(substr(A2.C_MAT,1, 12), ifnull(substring(M.D_MAT_F,1,12), '')))  as modello_orig,
					IFNULL(S.MODELLO_ORIG_DESCR, ifnull(A2.D_MAT, ifnull(substring(M.D_MAT_F, 13, 25), ''))) as nome_orig,
					CASE WHEN (S.FACONISTA=0 or S.FACONISTA is NULL) THEN ifnull(F.CONTO, '') ELSE S.FACONISTA END as faconista, 
					ifnull(F.CMATSET, '') as collezione, 
					ifnull(A1.ANNO, '') as ANNO, 
					ifnull(A1.STAG, '') as STAG,
					ifnull(F.TESSUTO, '')  as ns_tessuto,
					CASE WHEN (S.FACONISTA=0 or S.FACONISTA is NULL) THEN ifnull(C2.RAGSOC, '') ELSE ifnull(C1.RAGSOC, '') END as ragsoc,
					ifnull(S.TI_ROWID, '') as ID_SCHEDA
				from ANAMAT A1 
				left join FACBP F on (F.C_MAT = A1.C_MAT)
				left join FACBP_EST FE on (F.RIF_INTR = FE.RIF_INTR)
				left join ANAMAT A2 on (F.C_MAT_A = A2.C_MAT)
				left join SCHEDE_MODELLO S ON (SUBSTRING(A1.C_MAT, 1,12) = S.MODELLO)
				left join FAC_ANACON C1 on (S.FACONISTA = C1.CONTO)
				left join FAC_ANACON C2 on (F.CONTO = C2.CONTO)
				left join FACCOL CL on (F.CMATSET = CL.CMATSET)
				left join FACORD FA on (FA.C_MAT = substr(F.TESSUTO, 1, 5))
				left join MATFOR M  on (M.C_MAT = A1.C_MAT)
				where 1=1
				AND ( CL.GRUPPO IN ('OUTLET', 'INTREND') or CL.GRUPPO is NULL)
				AND length(A1.C_MAT) = 15
				AND FE.TIPO_BP != 'integrazione'
				%(q_societa)s 
				%(q_as)s
				%(q_classe)s
				%(q_collezione)s
				%(q_modello)s
				 %(q_nome)s
				 %(q_modello_orig)s
				 %(q_nome_orig)s
				 %(q_gm_ns_tessuto)s
				 %(q_art_ns_tessuto)s
				 %(q_ns_tessuto_descr)s
				 %(q_tessuto_orig)s
				GROUP BY substr(A1.C_MAT, 1, 12) 
				limit 1000
				 """ %vars()			
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	for r in resd:
		r['anno_stagione'] = ''
		r['classe'] = r['modello'][1:3]
		if r['ANNO'] and r['STAG']:
			if r['STAG'] == '2':
				r['STAG'] = '1'
			elif r['STAG'] == '4':
				r['STAG'] = '2'
			r['anno_stagione'] = anno_stagione_lib.decode_as(r['ANNO']+r['STAG'])
		r['societa']=''
		r['nome_orig'] = r['nome_orig'].strip()
		if len(r['modello_orig']) == 12: 
			if r['modello_orig'][9:12] == '006':
				r['societa'] = 'MM'
			elif r['modello_orig'][9:12] == '002':
				r['societa'] = 'MA'
			elif r['modello_orig'][9:12] == '003':
				r['societa'] = 'MN'	
			elif r['modello_orig'][9:12] == '013':
				r['societa'] = 'DT'	
	return resd


def get_gm_autocomplete(pard, query, soc):
	pard['sql'] = """ select distinct concat(gm, '-', descrizione) as label, gm as chiave
						from impo_gm 
						where societa = '%s'
						and ( gm like '%s' or descrizione like '%%%s%%' ) 
				"""%(soc, query, query)
	
	result = db_access.mysql_dict(pard)
	return list(result)


def get_articolo_autocomplete(pard, query, soc):
	pard['sql'] = """ select distinct concat(articolo, '-', descrizione) as label, articolo as chiave
						from impo_articoli
						where societa = '%s'
						and ( articolo = '%s' or descrizione like '%%%s%%' ) 
						limit 100
				"""%(soc, query, query)
	
	result = db_access.mysql_dict(pard)
	for r in result:
		r['label'] = r['label'].encode('latin-1')
	return list(result)

	
def get_articolo_descr(pard, conn, dati):
	"""dati: {'consumo_unitario_netto': '1', 'progr_pezzo': '1', 
			 'materiale': '82210916P', 'societa': 'MN', 'stagione': '1',
			 'materiale_componente': '840100180', 'prodotto_componente': '840100180', 
			 'gruppo_merceologico': '84', 'anno': '2010', 'prodotto': '82210916PE00', 
			 'misura_prodotto': '38', 'numero_riga': '212', 'progr_articolo': '180',
			 'variante_prodotto_componente': '1', 'misura_prodotto_componente': None, 
			 'variante_prodotto': '1', 'consumo_unitario_lordo': '1.02'}"""
	descr = ''
	dati['a_s'] = ''
	dati['anno_stagione'] = ''
	if dati['anno'] == '0' and dati['stagione'] == '0':
		dati['a_s'] = 'PP'
	else:
		dati['a_s'] = str(dati['anno'][3])+str(dati['stagione'])
		if dati['societa'] == 'MN' and dati['anno'] < '2012':
			dati['a_s'] = 'PP'
	if dati['a_s'] == 'PP':
		dati['anno_stagione'] = 'permanente'
	else:
		dati['anno_stagione'] = anno_stagione_lib.decode_as(dati['a_s'])
	pard['sql'] = """ select descrizione
						from impo_articoli a
						join impo_societa s on (s.soc_orig = a.societa)
						where s.soc_dt = '%(societa)s'
						and a.gm = '%(gruppo_merceologico)s'
						and a.articolo = '%(progr_articolo)s'
						and a.anno_stagione = '%(anno_stagione)s'
				"""%dati
	result = db_access.mysql_dict_conn(pard, conn)
	if result:
		descr = str(result[0]['descrizione']).capitalize()
	return descr


def get_articolo_by_id(pard, id_articolo, soc):
	pard['sql'] = """ select gm as gm_orig, 
							articolo as articolo_orig, 
							descrizione as descrizione_orig,
							difettosita as difettosita
						from impo_articoli
						where societa = '%s'
						and id_articolo = '%s'
				"""%(soc, id_articolo)
	
	result = db_access.mysql_dict(pard)
	if result:
		r = result[0]
		r['tessuto_orig'] = r['gm_orig']+' - '+r['articolo_orig']
		r['descrizione_orig'] = r['descrizione_orig'].decode('latin-1').encode('latin-1')
		return r
	else:
		return {}


def get_articolo_variante_by_id(pard, soc, id_articolo, ml, colore, variante):
	
	w_variante = ''
	if soc != 'MN':
		if variante:
			w_variante = """ and iav.colore_std = '%s' and iav.variante = '%s' """ %(colore[-3:], variante)
		pard['sql'] = """ select ia.gm as gm_orig, 
								ia.articolo as articolo_orig, 
								ia.descrizione as descrizione_orig,
								iav.descrizione as descrizione_var,
								ic.colore as colore_orig,
								ic.descrizione as descrizione_colore
							from impo_articoli ia
								join impo_articoli_varianti iav 
									on ia.id_articolo=iav.id_articolo
									and ia.societa=iav.societa
								join impo_colori ic
									on iav.societa=ic.societa
									and iav.colore=ic.colore
								join impo_societa ims
									on ia.societa=ims.SOC_ORIG
							where ims.SOC_DT = '%s'
							and ia.id_articolo = %s
							and iav.ml = '%s'
							%%s
					"""%(soc, id_articolo, ml) %w_variante
	else:
		if variante:
			colore = colore.zfill(4)
			w_variante = """ and ic.colore = '%s' """ %colore
		pard['sql'] = """ select ia.gm as gm_orig, 
								ia.articolo as articolo_orig, 
								ia.descrizione as desc_articolo,
								ic.colore as colore_orig,
								ic.descrizione as descrizione_var
							from impo_articoli ia
							join impo_articoli_colori iac
								on ia.societa=iac.societa
								and iac.id_articolo=ia.id_articolo
							join impo_colori ic 
								on iac.societa=ic.societa 
								and ic.colore=iac.colore
							join impo_societa ims
									on ia.societa=ims.SOC_ORIG
							where ims.SOC_DT = '%s'
							and ia.id_articolo = %s
							%%s
					"""%(soc, id_articolo) %w_variante
		#tools.dump(pard['sql'])
	
	result = db_access.mysql_dict(pard)
	if result:
		r = result[0]
		if r.get('descrizione_orig', ''):
			r['descrizione_orig'] = r['descrizione_orig'].decode('latin-1').encode('latin-1')
		if r.get('desc_articolo', ''):
			r['desc_articolo'] = r['desc_articolo'].decode('latin-1').encode('latin-1')
		return r
	else:
		return {}


def get_collezione_autocomplete(pard, query, soc, anno_stagione):
	pard['sql'] = """ select distinct concat(ml, '-', descrizione) as label, ml as chiave
						from impo_collezioni
						where societa = '%s' and anno_stagione = '%s'
						and ( ml = '%s' or descrizione like '%%%s%%' ) 
						limit 100
				"""%(soc, anno_stagione, query, query)
	#tools.dump(pard['sql'])
	result = db_access.mysql_dict(pard)
	for r in result:
		r['label'] = r['label'].encode('latin-1')
	return list(result)


def get_collezione_dt_autocomplete(pard, query):
	pard['sql'] =""" select distinct CMATSET as label, CMATSET as chiave
					from FACCOL
					where GRUPPO IN ('OUTLET', 'INTREND') 
					and CMATSET like '%%%s%%' """ %query
	
	result = db_access.mysql_dict(pard)
	
	for r in result:
		r['label'] = r['label'].encode('latin-1')
		r['chiave'] = r['chiave'].encode('latin-1')
	return list(result)

@decorators.query_with_result
def get_impo_collezione_mn(soc, ml_mn):
	query = """ select ml, ml_mn
				from impo_collezioni c
				join impo_societa s on (s.soc_orig = c.societa)
				where s.soc_dt = '%s'
				and c.ml_mn = '%s'
			"""%(soc, ml_mn)
	return query


def check_db_esplosa(pard, conn, societa, modello, variante, tg):
	modelli = []
	lista = dwg_dao.get_modelli_variante_prod(pard, societa, modello, variante)
	new_lista = []
	for l in lista:
		if l['tipo_produzione'] == 'N':
			new_lista.insert(0, l)
		else:
			new_lista.append(l)
		if l['modello_permanente'] and l['modello_permanente'] != '':
			modelli.append(l)
	if not modelli:		
		new_lista.sort(key = lambda k : (k['modello']), reverse=True)
		new_lista2 = []
		new_lista3 = []
		for n in new_lista:
			if n['made_in'] == 'IT':
				new_lista2.insert(0, n)
			elif n['made_in']:
				new_lista2.append(n)
			else:
				new_lista3.append(n)

		new_lista2.sort(key = lambda k : (k['versione_modello']), reverse=True)
		new_lista3.sort(key=lambda k: (k['versione_modello']), reverse=True)

		modelli = new_lista2
		modelli.extend(new_lista3)
	
	where_clause = ''
	if tg == 'tg_base':
		tipo_tg_dt = get_tipo_tg_dt(pard, societa, modelli[0]['tipo_taglia'])
		if tipo_tg_dt == 'T34':
			where_clause += " and de.misura_prodotto in ('34')"
		elif tipo_tg_dt in ('CAP', 'CA2'):
			where_clause += " and de.misura_prodotto in ('57')"
		elif tipo_tg_dt in ('A25'):
			where_clause += " and de.misura_prodotto in ('28')"
		else:
			where_clause += " and de.misura_prodotto in ('42', 'SM', 'M')"
	else:
		where_clause += " and de.misura_prodotto = '%s' " %tg
	
	trovato = False
	for m in modelli:	
		pard['sql'] = """ select de.societa, de.misura_prodotto as tg, 
								de.prodotto as modello_produzione, de.variante_prodotto as variante	
							from impo_diba_esplosa de
							where de.societa='%(societa)s' 
							and de.prodotto='%(modello_produzione)s' 
							and de.variante_prodotto = '%(variante)s'
							%%s					
						""" %m %where_clause
		result = db_access.mysql_dict_conn(pard, conn)
		if result:
			trovato = True
			return result[0]
	if not trovato:
		raise errors.DAOException('Nessuna distinta base trovata %s-%s. (modelli_prod: %s)' %(societa, modello, set([m['modello_produzione'] for m in modelli])))





def get_db_modello_originale(pard, conn, societa, modello, variante='', tg='tg_base'):
	""" societa							-> societa
		prodotto						-> modello8 + '000'
		variante_prodotto 				-> variante_modello
		progr_pezzo						-> num pezzo
		numero_riga
		misura_prodotto 				-> taglia
		materiale			
		materiale_componente 
		misura_prodotto_componente		-> misura(3)
		prodotto_componente				-> soc+anno+.+gm+a_s+articolo
		variante_prodotto_componente 	-> colore(3)
		gruppo_merceologico				
		anno							
		stagione					
		progr_articolo	
		descrizione						-> descrizione articolo
		consumo_unitario_netto			-> 0 
		consumo_unitario_lordo			-> consumo
	"""
	db_list_tot = []

	where_clause = ''
	
#	ret_dict = dwg_dao.get_modello_variante_originale(pard, societa, modello, variante)
#	if ret_dict.get('variante', '') != '':
#		where_clause += " and variante_prodotto = '%s' " %ret_dict['variante']
#		
#	if tg == 'tg_base':
#		tipo_tg_dt = get_tipo_tg_dt(pard, societa, ret_dict['tipo_taglia'])
#		if tipo_tg_dt == 'T34':
#			where_clause += " and misura_prodotto in ('34')"
#		else:
#			where_clause += " and misura_prodotto in ('42', 'SM', 'M')"
#	elif tg != '':
#		where_clause += " and misura_prodotto = '%s' " %tg
#		
	ret_dict = check_db_esplosa(pard, conn, societa, modello, variante, tg)
	if ret_dict.get('variante', '') != '':
		where_clause += " and variante_prodotto = '%s' " %ret_dict['variante']	
	if ret_dict.get('tg', '') != '':
		where_clause += " and misura_prodotto = '%s' " %ret_dict['tg']	
# 	tools.dump(ret_dict)
	
	pard['sql'] = """ select de.societa, de.prodotto_componente, de.misura_prodotto as taglia, de.prodotto, de.variante_prodotto,
							de.misura_prodotto_componente as misura, de.variante_prodotto_componente as colore, 
							de.progr_articolo as articolo, de.gruppo_merceologico as gm, de.anno, de.stagione,
							de.consumo_unitario_lordo as consumo, de.numero_riga, de.descrizione as d_mat	
						from impo_diba_esplosa de
						where de.societa='%(societa)s' 
						and de.prodotto='%(modello_produzione)s' 
						and de.gruppo_merceologico >=10
						%%s					
					""" %ret_dict %where_clause
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(pard['sql'])
	result = db_access.mysql_dict_conn(pard, conn)
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(result)
	result = sorted(result, key = lambda k : (k['societa'],  k['prodotto'], k['variante_prodotto'], k['prodotto_componente'], k['taglia'],
												k['misura'], k['colore'], k['articolo'], k['gm'], k['anno'], k['stagione'], k['numero_riga']))
	for k, g in itertools.groupby(result, key = lambda k : (k['societa'],  k['prodotto'], k['variante_prodotto'], k['prodotto_componente'], k['taglia'],
															 k['misura'], k['colore'], k['articolo'], k['gm'], k['anno'], k['stagione'])):
		db_list = list(g)
		materiale = db_list[0].copy()
		if (materiale['gm'], materiale['societa']) in (('20','MM'), ('21', 'MM'), ('20', 'MA'), ('21','MA'),
																	('30', 'MN'), ('31', 'MN'), ('32', 'MN')):
			materiale['consumo'] = sum(float(d['consumo']) for d in db_list)
			#tools.dump(materiale)
			db_list = [materiale]
		#tools.dump(db_list)
		for db_riga in db_list:
			if db_riga['colore']:
				db_riga['colore'] = str(db_riga['colore']).zfill(4)
			
			if db_riga['misura'] and db_riga['misura'] != 'None':
				db_riga['misura'] = str(int(db_riga['misura'])).zfill(2)
			else:
				db_riga['misura'] = '00'
			
			if db_riga['anno'] == '0' and db_riga['stagione'] == '0':
				db_riga['a_s'] = 'PP'
			else:
				db_riga['a_s'] = str(db_riga['anno'][3])+str(db_riga['stagione'])
				if db_riga['societa'] == 'MN' and db_riga['anno'] < '2010':
					db_riga['a_s'] = 'PP'
			db_riga['anno_stagione'] = ''
			if db_riga['a_s'] == 'PP':
				db_riga['anno_stagione'] = 'permanente'
			else:
				db_riga['anno_stagione'] = anno_stagione_lib.decode_as(db_riga['a_s'])
				
			db_riga['societa_old'] = ''
			if db_riga['societa'] == 'MM':
				db_riga['societa_old'] = 'OM'
			elif db_riga['societa'] == 'MN':
				db_riga['societa_old'] = 'ON'
			elif db_riga['societa'] == 'MA':
				db_riga['societa_old'] = 'OA'
			
			regole_gestione = get_regole_capocmat(pard, db_riga, conn)
			db_riga.update(regole_gestione)
			
			dati_capocmat = get_dati_capocmat(pard, conn, db_riga)
			if not dati_capocmat.get('capocmat', ''):
				#tools.dump('qui')
				db_riga['id_articolo_old'] = ''
				db_riga['id_articolo_new'] = ''
				db_riga['id_artforn_old'] = ''
				db_riga['id_artforn_new'] = ''
	
				pard['batch_mode'] = True
				importa_anagrafica.importa_articolo(pard, db_riga, conn)
				
				dati_capocmat = get_dati_capocmat(pard, conn, db_riga)
			db_riga.update(dati_capocmat)
			
			db_riga['gestione_taglie'] = 'N'
			if str(db_riga['taglia']).zfill(3) == str(db_riga['misura']).zfill(3):
				pard['sql'] = """ select de.societa, de.prodotto_componente, de.misura_prodotto as taglia, de.prodotto, de.variante_prodotto,
								de.misura_prodotto_componente as misura, de.variante_prodotto_componente as colore, 
								de.consumo_unitario_lordo as consumo, de.progr_articolo as articolo, 
								de.gruppo_merceologico as gm, de.anno, de.stagione, de.descrizione as d_mat
							from impo_diba_esplosa de
							where de.societa='%(societa)s' 
							and de.prodotto='%(modello_produzione)s' 
							and de.variante_prodotto = '%(variante)s' 	
							and de.misura_prodotto	!= '%%(taglia)s'	
							and de.prodotto_componente = '%%(prodotto_componente)s'
							""" %ret_dict %db_riga
				#tools.dump(pard['sql'])
				db_list_art = db_access.mysql_dict_conn(pard, conn)
				lista_misure = set([t['misura'] for t in db_list_art])
				if len(lista_misure)>1:
					db_riga['gestione_taglie'] = 'S'
					for db_art in db_list_art:
						db_art['flag_gest_misure'] = db_riga['flag_gest_misure']
						db_art['flag_gest_colori'] = db_riga['flag_gest_colori']
						db_art['a_s'] = db_riga['a_s']
						db_art['anno_stagione'] = db_riga['anno_stagione']
						db_art['gestione_taglie'] = db_riga['gestione_taglie']
						
						if db_art['colore']:
							db_art['colore'] = str(db_art['colore']).zfill(4)
						else:
							db_art['colore'] = '0000'
	
						if db_art['misura'] and db_art['misura'] != 'None':
							#tools.dump(db_art['misura'])
							db_art['misura'] = str(int(db_art['misura'])).zfill(2)
						else:
							db_art['misura'] = '00'
						dati_capocmat_art = get_dati_capocmat(pard, conn, db_art)
						db_art.update(dati_capocmat_art)
						
						db_list_tot.append(db_art)
			
			db_list_tot.append(db_riga)
		
	#tools.dump(db_list_tot)
	return db_list_tot


def get_tipo_tg_dt(pard, societa, tipo_tg_soc):
	tipo_tg_dt = ''
	pard['sql'] = """select TIPO_TG_DT 
					from TIPI_TG_ASSO
					where SOC_TIPO_TG = '%s'
					and SOC = '%s' 
					"""%(tipo_tg_soc, societa)
	result = db_access.mysql_dict(pard)
	if result:
		tipo_tg_dt = result[0]['TIPO_TG_DT']
	return tipo_tg_dt
	

def get_db_tessuto_originale(pard, conn, societa, modello, variante='', tg='tg_base'):
	""" societa							-> societa
		prodotto						-> modello8 + '000'
		variante_prodotto 				-> variante_modello
		progr_pezzo						-> num pezzo
		numero_riga
		misura_prodotto 				-> taglia
		materiale			
		materiale_componente 
		misura_prodotto_componente		-> misura(3)
		prodotto_componente				-> soc+anno+.+gm+a_s+articolo
		variante_prodotto_componente 	-> colore(3)
		gruppo_merceologico				
		anno							
		stagione					
		progr_articolo	
		descrizione						-> descrizione articolo
		consumo_unitario_netto			-> 0 
		consumo_unitario_lordo			-> consumo
	"""
	db_riga = {}

#	where_clause = ''
#	if tg == 'tg_base':
#		where_clause += " and misura_prodotto in ('34', '42', 'SM', 'M')"
#	elif tg != '':
#		where_clause += " and misura_prodotto = '%s' " %tg
	
	try:
		ret_dict = check_db_esplosa(pard, conn, societa, modello, variante, tg)
		#ret_dict = dwg_dao.get_modello_variante_originale(pard, societa, modello, variante)
	except:
		return db_riga
	
	where_clause = ''
	if ret_dict.get('variante', '') != '':
		where_clause += " and variante_prodotto = '%s' " %ret_dict['variante']
	if ret_dict.get('tg', '') != '':
		where_clause += " and misura_prodotto = '%s' " %ret_dict['tg']	
	
	pard['sql'] = """ select de.societa, de.prodotto_componente, de.misura_prodotto as taglia, de.prodotto, de.variante_prodotto,
							de.misura_prodotto_componente as misura, de.variante_prodotto_componente as colore, 
							de.progr_articolo as articolo, de.gruppo_merceologico as gm, de.anno, de.stagione,
							de.consumo_unitario_lordo as consumo, de.numero_riga, de.descrizione as d_mat	
						from impo_diba_esplosa de
						where de.societa='%(societa)s' 
						and de.prodotto='%(modello_produzione)s' 
						and de.numero_riga = 1110
						%%s					
					""" %ret_dict %where_clause
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(pard['sql'])
	result = db_access.mysql_dict_conn(pard, conn)
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(result)
	if result:
		db_riga = result[0]
		if db_riga['colore']:
			db_riga['colore'] = str(db_riga['colore']).zfill(4)
		
		if db_riga['misura'] and db_riga['misura'] != 'None':
			db_riga['misura'] = str(int(db_riga['misura'])).zfill(2)
		else:
			db_riga['misura'] = '00'
		
		if db_riga['anno'] == '0' and db_riga['stagione'] == '0':
			db_riga['a_s'] = 'PP'
		else:
			db_riga['a_s'] = str(db_riga['anno'][3])+str(db_riga['stagione'])
			if db_riga['societa'] == 'MN' and db_riga['anno'] < '2012':
				db_riga['a_s'] = 'PP'
		db_riga['anno_stagione'] = ''
		if db_riga['a_s'] == 'PP':
			db_riga['anno_stagione'] = 'permanente'
		else:
			db_riga['anno_stagione'] = anno_stagione_lib.decode_as(db_riga['a_s'])
			
		db_riga['societa_old'] = ''
		if db_riga['societa'] == 'MM':
			db_riga['societa_old'] = 'OM'
		elif db_riga['societa'] == 'MN':
			db_riga['societa_old'] = 'ON'
		elif db_riga['societa'] == 'MA':
			db_riga['societa_old'] = 'OA'
		
		db_riga['flag_gest_misure'] = 'N'
		db_riga['flag_gest_colore'] = 'S'
		
		dati_capocmat = get_dati_capocmat(pard, conn, db_riga)
# 	non dovrei mai passare di qui
# 		if not dati_capocmat.get('capocmat', ''):
# 			#tools.dump('qui')
# 			db_riga['id_articolo_old'] = ''
# 			db_riga['id_articolo_new'] = ''
# 			db_riga['id_artforn_old'] = ''
# 			db_riga['id_artforn_new'] = ''
# 
# 			pard['batch_mode'] = True
# 			importa_anagrafica.importa_articolo(pard, db_riga, conn)
# 			
# 			dati_capocmat = get_dati_capocmat(pard, conn, db_riga)
		db_riga.update(dati_capocmat)
	
		
	#tools.dump(db_list_tot)
	return db_riga


def get_dati_capocmat(pard, conn, param):
	diz_out = {}	
	
	pard['sql'] = """ select M.CAPOCMAT as capocmat, 
							M.D_COL as d_mat_col 
					from MP_CAPOCMAT M
					where M.SOC = '%(societa)s'
					and M.GM = '%(gm)s'
					and M.A_S = '%(a_s)s'
					and M.ART = '%(articolo)s'
					and M.MIS = '%(misura)s'
					and M.COL = '%(colore)s'
					"""	%param
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(pard['sql'])
	res_capocmat = db_access.mysql_dict_conn(pard, conn)
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(res_capocmat)
	if res_capocmat:
		giacenza_diz = get_giacenza_capocmat(pard, res_capocmat[0]['capocmat'], conn = conn)
		#TODO: vedere se la misura e' eliminabile
		if giacenza_diz['giac'] == '0' and param['misura'] == '00' and param['colore'] == '0000':
			diz_out = get_capocmat_correlato(pard, res_capocmat[0], conn, param['misura'])
		else:
			diz_out = res_capocmat[0]
	return diz_out


def exist_capocmat(pard, conn, param):
	diz_out = {}

	pard['sql'] = """ select M.CAPOCMAT as capocmat,
							M.D_COL as d_mat_col
					from MP_CAPOCMAT M
					where M.SOC = '%(societa)s'
					and M.GM = '%(gm)s'
					and M.A_S = '%(a_s)s'
					and M.ART = '%(articolo)s'
					and M.MIS = '%(misura)s'
					and M.COL = '%(colore)s'
					"""	%param
	res_capocmat = db_access.mysql_dict_conn(pard, conn)
	if res_capocmat:
		diz_out = res_capocmat[0]
	return diz_out


@decorators.query_with_result
def get_articoli_diba_esplosa(dati):
	sql_where = ""
	if dati['capocmat']:
		sql_where += " and d.CAPOCMAT = '%(capocmat)s' " %dati

	query = """ select d.*
				from DIBA_ESPLOSA d left join MP_CAPOCMAT c on d.CAPOCMAT=c.CAPOCMAT
				where c.CAPOCMAT is null
				%s """ %sql_where
	return query
	

def get_regole_capocmat(pard, param, conn):
	diz_out = {'flag_gest_misure' : '', 'flag_gest_colori' : ''}
	pard['sql'] = """select a.flag_gest_misure, g.flag_gest_misure as flag_gest_misure_gm, 
							a.flag_gest_colori, g.flag_gest_colori as flag_gest_colori_gm
							from impo_articoli a  
							join impo_gm g on (g.societa = a.societa and g.gm = a.gm)
							where a.societa = '%(societa_old)s'
							and a.gm = '%(gm)s' 
							and a.articolo = '%(articolo)s'
							and a.anno_stagione = '%(anno_stagione)s'
						""" %param
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(pard['sql'])
	res_regole = db_access.mysql_dict_conn(pard, conn)	
# 	if pard['sid_id_utente'] == 'elifus':
# 		tools.dump(res_regole)
	if res_regole:
		diz_out['flag_gest_colori'] = res_regole[0]['flag_gest_colori']
		if not diz_out['flag_gest_colori']:
			diz_out['flag_gest_colori'] = res_regole[0]['flag_gest_colori_gm']
		diz_out['flag_gest_misure'] = res_regole[0]['flag_gest_misure']
		if not diz_out['flag_gest_misure']:
			diz_out['flag_gest_misure'] = res_regole[0]['flag_gest_misure_gm']
	return diz_out


def get_giacenza_capocmat(pard, capocmat, rif_bp='', c_mat='', dati_modello={}, conn=None):
	giac_diz = {'giac': '0', 'qta_giac': 0, 'qta_imp_altri_mod': 0}
	cmat_list = ''
	# tools.dump(capocmat)
	new_conn = False
	if not conn:
		conn = db_access.mysql_connect(pard)
		new_conn = True
	
	if c_mat and c_mat != '0':
		cmat_list = c_mat
	else:
		pard['sql'] = """ select ifnull(group_concat(distinct C_MAT), '') as cmat_list
							from MP_CMAT_CAPOCMAT
							where CAPOCMAT = '%s'
					"""%capocmat
		res = db_access.mysql_dict_conn(pard, conn)
		if res and res[0]['cmat_list']:
			cmat_list = res[0]['cmat_list']


	qta_impegnata_mod = 0
	qta_impegnata_riga = 0
	qta_imp_ord_riga = 0
	qta_imp_ord = 0
	if dati_modello.get('modello', '') and dati_modello['modello']:
		res_impegno = get_qta_impegnata_new(pard, capocmat, dati_modello, conn)
		#tools.dump(res_impegno)
		if 'imp_riga' in res_impegno.keys():
			qta_impegnata_riga = float(res_impegno['imp_riga']['IMP_DT'])
			qta_imp_ord_riga = float(res_impegno['imp_riga']['IMP_ORD'])
		if 'imp_modello_altri_bp' in res_impegno.keys():
			qta_impegnata_mod = float(res_impegno['imp_modello_altri_bp']['IMP_DT'])
			# Get qta nello stesso bp, degli altri id_faccom che pero' non sono stati considerati nella get sopra
			# la sommo alla qta_impegnata_mod
			if rif_bp:
				qta_bp = get_qta_stesso_bp(pard, rif_bp, capocmat, res_impegno['imp_modello_altri_bp']['id_faccoms'], conn)
				qta_impegnata_mod += qta_bp
			qta_imp_ord = float(res_impegno['imp_modello_altri_bp']['IMP_ORD'])
		if 'imp_altri_modelli' in res_impegno.keys():
			giac_diz['qta_imp_altri_mod'] = float(res_impegno['imp_altri_modelli']['IMP_DT']) + qta_impegnata_mod
			
	else:
		giac_diz['qta_imp_altri_mod'] = get_qta_impegnata(pard, capocmat, conn)

	if cmat_list:

		pard['sql'] = """select ifnull(sum(GC_Q), 0) as giacenza
						from SITMP 
						WHERE C_MAT in (%s)
						and MAG = 9
					"""%cmat_list
		res2 = db_access.mysql_dict_conn(pard, conn)
		if res2 and res2[0]['giacenza'] != '':
			qta_giac = float(res2[0]['giacenza'])/100

			qta_bbpp = 0.00
			if rif_bp:
				qta_bbpp = get_qta_bbpp(pard, rif_bp, cmat_list, conn)
			giac_diz['qta_giac'] = qta_giac + float(qta_bbpp)
			qta_giac_netta = giac_diz['qta_giac'] - float(giac_diz['qta_imp_altri_mod'])
			if qta_giac_netta > 0.00:
				giac_diz['giac'] = '1'
			elif qta_giac_netta - float(qta_impegnata_riga) <= 0 and (qta_imp_ord + qta_imp_ord_riga)>0:
				giac_diz['giac'] = '3'
			elif (giac_diz['qta_giac'] + giac_diz['qta_imp_altri_mod']) > 0.00 and giac_diz['qta_imp_altri_mod'] > 0 and qta_giac_netta <= 0.00:
				giac_diz['giac'] = '2'
			else:
				giac_diz['giac'] = '0'
		else:
			giac_diz['giac'] ='0'
	else:
		if qta_imp_ord > 0:
			giac_diz['giac'] = '3'
		else:
			giac_diz['giac'] = '0'

		
	if new_conn:
		conn.close()
	return giac_diz


def get_qta_giacenza_capocmat(pard, capocmat, rif_bp='', c_mat='', conn=None):
	qta_giac = 0
	cmat_list = ''
	
	new_conn = False
	if not conn:
		conn = db_access.mysql_connect(pard)
		new_conn = True
	
	if c_mat and c_mat != '0':
		cmat_list = c_mat
	else:
		pard['sql'] = """ select ifnull(group_concat(distinct C_MAT), '') as cmat_list
							from MP_CMAT_CAPOCMAT
							where CAPOCMAT = '%s'
					"""%capocmat
		res = db_access.mysql_dict_conn(pard, conn)
		if res and res[0]['cmat_list']:
			cmat_list = res[0]['cmat_list']
	
	if cmat_list:
		pard['sql'] = """select ifnull(sum(GC_Q/100), 0) as giacenza
						from SITMP 
						WHERE C_MAT in (%s)
						and MAG = 9
					"""%cmat_list
		#tools.dump(pard['sql'])
		res2 = db_access.mysql_dict_conn(pard, conn)
		if res2 and res2[0]['giacenza'] != '':
			qta_bbpp = 0.00
			qta_impegnata = 0.00
			if rif_bp:
				qta_impegnata = get_qta_impegnata(pard, capocmat, conn, key=rif_bp)
				qta_bbpp = get_qta_bbpp(pard, rif_bp, cmat_list, conn)
# 			tools.dump(qta_impegnata)
# 			tools.dump(qta_bbpp)
# 			tools.dump(res2[0]['giacenza'])
			qta_giac = float(res2[0]['giacenza'])+float(qta_bbpp)-float(qta_impegnata)
		else:
			qta_giac = 0
	else:
		qta_giac = 0
		
	if new_conn:
		conn.close()	
		
	return qta_giac


def get_qta_impegnata(pard, capocmat, conn, key= ''):
	where_clause = ''
	if key:
		where_clause += " and RIF_INTR != %s " %key
		
	pard['sql'] = """ select ifnull(sum(Q-Q_C)/100,0) as qta_impegnata 
					from FACCOM
					where Q_C < Q
						and CAPOCMAT = '%%s'
						and C_MAT = 0
						%s
					group by CAPOCMAT, C_MAT """  %where_clause %capocmat
	resd = db_access.mysql_dict_conn(pard, conn)

	qta_impegnata = 0.00
	if resd:
		qta_impegnata = float(resd[0]['qta_impegnata'])
	return qta_impegnata


def get_qta_impegnata_new(pard, capocmat, dati_modello, conn):
	pard['sql'] = """ select
					ifnull(if(SUM(F.QTA_FABB_REALE)>0,SUM(F.QTA_FABB_REALE),if(SUM(F.QTA_FABB_TEOR)>0, SUM(F.QTA_FABB_TEOR), SUM(F.QTA_FABB_PREV))), 0) as qta_FABB,
					D.MODELLO_ORIG as modello,
					D.NOME_MODELLO_ORIG as nome_modello,
					D.CAPOCMAT as capocmat,
					rc.ANNO_STAGIONE as anno_stagione,
					IFNULL(COL.CMATSET, rc.COLLEZIONE) as collezione,
					GROUP_CONCAT(DISTINCT F.ID) AS ID_FABBS,
					IF (I.TIPO='DT', ifnull(IF(SUM(I.QTA-I.QTA_PRELEVATA)<0,0,SUM(I.QTA-I.QTA_PRELEVATA)), 0) ,0) AS IMP_DT,
					IF (I.TIPO='SOC', ifnull(SUM(I.QTA), 0) ,0) AS IMP_SOC,
					IF (I.TIPO='ORD', ifnull(SUM(I.QTA), 0) ,0) AS IMP_ORD,
					ifnull(I.ID_FACCOM, '') as id_faccom
					from DIBA_ESPLOSA D
					JOIN FABBISOGNI F ON (F.ID_DIBA = D.ID)
					join RICHIESTE_COMM_PF_ART rar on (rar.id = F.id_art_comm)
					join RICHIESTE_COMM_PF_RIGHE rcr on (rcr.id=rar.id_riga)
					JOIN RICHIESTE_COMM_PF rc on (rc.id=rcr.id_richiesta AND rc.STATO !='chiusa')
					left JOIN FACCOL COL ON (rc.COLLEZIONE = if(rc.ente_richiesta='Commerciale Facon',COL.CMATSET, COL.ML))
					LEFT JOIN IMPEGNI I ON (I.ID_FABBISOGNO = F.ID AND I.STATO ='valido')
					WHERE CAPOCMAT = '%s'
					GROUP BY D.MODELLO_ORIG, D.NOME_MODELLO_ORIG, D.SOCIETA_ORIG, rc.ANNO_STAGIONE, rc.COLLEZIONE, I.TIPO, I.ID_FACCOM
					 """%capocmat
	resd = db_access.mysql_dict_conn(pard, conn)
	qta_impegnata_diz = {}

	for fabb in resd:
		if str(dati_modello['modello'])[:8] == str(fabb['modello'])[:8] and dati_modello['anno_stagione'] == fabb['anno_stagione'] and \
				dati_modello['collezione'] == fabb['collezione']:
			if str(fabb['id_faccom']) in str(dati_modello['ti_rowid']).split(',') or str(fabb['id_faccom'])=='0':
				if 'imp_riga' in qta_impegnata_diz.keys():
					qta_impegnata_diz['imp_riga']['IMP_SOC'] += float(fabb['IMP_SOC'])
					qta_impegnata_diz['imp_riga']['IMP_ORD'] += float(fabb['IMP_ORD'])
					qta_impegnata_diz['imp_riga']['IMP_DT'] += float(fabb['IMP_DT'])
				else:
					qta_impegnata_diz['imp_riga'] = {
						'capocmat': fabb['capocmat'],
						'IMP_SOC': float(fabb['IMP_SOC']),
						'IMP_ORD': float(fabb['IMP_ORD']),
						'IMP_DT': float(fabb['IMP_DT'])
					}
			else:
				if 'imp_modello_altri_bp' in qta_impegnata_diz.keys():
					if fabb['id_faccom']:
						qta_impegnata_diz['imp_modello_altri_bp']['id_faccoms'].append(fabb['id_faccom'])
					qta_impegnata_diz['imp_modello_altri_bp']['IMP_SOC'] += float(fabb['IMP_SOC'])
					qta_impegnata_diz['imp_modello_altri_bp']['IMP_ORD'] += float(fabb['IMP_ORD'])
					qta_impegnata_diz['imp_modello_altri_bp']['IMP_DT'] += float(fabb['IMP_DT'])
				else:
					qta_impegnata_diz['imp_modello_altri_bp'] = {
						'capocmat': fabb['capocmat'],
						'IMP_SOC': float(fabb['IMP_SOC']),
						'IMP_ORD': float(fabb['IMP_ORD']),
						'IMP_DT': float(fabb['IMP_DT']),
						'id_faccoms':[fabb['id_faccom']] if fabb['id_faccom'] else []
					}
		else:
			if 'imp_altri_modelli' in qta_impegnata_diz.keys():
				qta_impegnata_diz['imp_altri_modelli']['IMP_SOC'] += float(fabb['IMP_SOC'])
				qta_impegnata_diz['imp_altri_modelli']['IMP_ORD'] += float(fabb['IMP_ORD'])
				qta_impegnata_diz['imp_altri_modelli']['IMP_DT'] += float(fabb['IMP_DT'])
			else:
				qta_impegnata_diz['imp_altri_modelli'] = {
					'capocmat': fabb['capocmat'],
					'IMP_SOC': float(fabb['IMP_SOC']),
					'IMP_ORD': float(fabb['IMP_ORD']),
					'IMP_DT': float(fabb['IMP_DT'])
				}
	return qta_impegnata_diz


def get_qta_bbpp(pard, rif_bp, cmat_list, conn):
	pard['sql'] = """select
						ifnull(sum(Q*(-1)/100),0) as qta_movmag
					from
						MOVMAG
					where
						MAG = 9
						and C_MAT in (%s)
						and C_OP = 'BBPP'
						and RIF_ORD = %s
					"""%(cmat_list, rif_bp)
	result = list(db_access.mysql_dict_conn(pard, conn))
	return result[0]['qta_movmag']

@decorators.query_with_result
def get_qta_bbpp_new(rif_bp, cmat_list):
	query = """select
						ifnull(sum(Q*(-1)/100),0) as qta_movmag
					from
						MOVMAG
					where
						MAG = 9
						and C_MAT in (%s)
						and C_OP = 'BBPP'
						and RIF_ORD = %s
					"""%(cmat_list, rif_bp)
	return query


def get_capocmat_correlato(pard, dati, conn, misura =''):
	#TODO: gestire la misura
	dati['id_correlazione'] = ''
	pard['sql'] = """ select ID_CORR as id_correlazione
						from  MP_CAPOCMAT_CORRELATI CC
						where CAPOCMAT = '%(capocmat)s'
				""" %dati
	res_corr = db_access.mysql_dict_conn(pard, conn)
	if res_corr:
		dati['id_correlazione'] = res_corr[0]['id_correlazione']
	pard['sql'] = """ select distinct CAPOCMAT AS capocmat
						FROM MP_CAPOCMAT_CORRELATI CC 
						where ID_CORR = '%(id_correlazione)s'
						and CAPOCMAT != '%(capocmat)s'
				""" %dati
# 	pard['sql'] = """ select distinct msoc1.CAPOCMAT AS capocmat
# 							FROM MP_CAPOCMAT CC 
# 							LEFT JOIN MP_CAPOCMAT_CMATSOC msoc1 on substring(msoc1.CAPOCMAT_SOC,1,9)=substring(CC.CAPOCMAT,1,9)
# 							where msoc1.CAPOCMAT is not null
# 							and CC.CAPOCMAT = '%(capocmat)s'
# 							
# 							UNION
# 							
# 							select distinct msoc2.CAPOCMAT_SOC AS capocmat
# 							FROM MP_CAPOCMAT CC 
# 							LEFT JOIN MP_CAPOCMAT_CMATSOC msoc2 on substring(msoc2.CAPOCMAT,1,9)=substring(CC.CAPOCMAT,1,9)
# 							where msoc2.CAPOCMAT_SOC is not null
# 							and CC.CAPOCMAT = '%(capocmat)s'
# 					""" %dati
	res = db_access.mysql_dict_conn(pard, conn)
	trovato = False
	for r in res:
		if r['capocmat'] != dati['capocmat']:
			giac_corr_diz = get_giacenza_capocmat(pard, r['capocmat'], dati_modello={}, conn = conn)
			if giac_corr_diz['giac'] == '1':
				trovato = True
				pard['sql'] = """ select CAPOCMAT as capocmat, 
										D_COL as d_mat_col 
								from MP_CAPOCMAT
								where CAPOCMAT = '%(capocmat)s'
						""" %r
				res2 = db_access.mysql_dict(pard)
				return res2[0]
	if not trovato:
		return dati



def get_qta_stesso_bp(pard, rif_bp, capocmat, id_faccoms, conn):
	where_clause=''
	if id_faccoms:
		where_clause = ' and C.TI_ROWID not in (%s) '%','.join(id_faccoms)
	pard['sql'] = """ select sum(Q-Q_C)/100 as qta
					FROM FACCOM C
					JOIN FACCOM_EST CE ON (C.TI_ROWID=CE.ID_FACCOM)
					WHERE (CAPOCMAT = '%s' OR CAPOCMAT_CMAT LIKE '%s')
					and C.RIF_INTR = '%s'
					%%s
					and ((C.C_MAT!=0 and C.CAPOCMAT='') or (C.C_MAT=0 and C.CAPOCMAT!='') or (C.C_MAT=0 and CE.CAPOCMAT_CMAT!=''))
				"""%(capocmat, capocmat, rif_bp) %where_clause

	res = db_access.mysql_dict_conn(pard, conn)
	if res and res[0]['qta']:
		return float(res[0]['qta'])
	else:
		return 0.00


	
def get_db_modello_originale_ana(pard, societa, modello, variante=''):
	if not variante:
		pard['sql'] = """ select distinct societa, prodotto as modello, variante_prodotto as variante from impo_diba_esplosa where societa ='%s' and prodotto like '%s%%' """ %(societa, modello)
		result = db_access.mysql_dict(pard)
	else:
		result = [{'societa': societa, 'modello': modello, 'variante': variante}]
	ret_dict = {}
	for rec in result:
		ret_dict[(rec['modello'], rec['variante'])] = []
		pard['sql'] = """ select '' as pezzo, '' as colonna, anno, stagione, societa,
								'' as id_articolo, gruppo_merceologico as gm, progr_articolo as articolo, '' as anno_stagione_art, 
								'' as ricerca_colore, misura_prodotto_componente as misura_base, '' as style, '' as tipo_segnata, 
								sum(consumo_unitario_lordo) as consumo_base
						from impo_diba_esplosa where societa='%(societa)s' and prodotto='%(modello)s' and variante_prodotto='%(variante)s' 
						group by societa, gm, articolo, anno, stagione
						order by gm, articolo """ %rec
		db_list = db_access.mysql_dict(pard)
		for db_elem in db_list:
			if len(str(db_elem['anno'])) == 4:
				db_elem['a_s'] = str(db_elem['anno'])[3]+str(db_elem['stagione'])
				db_elem['anno_stagione_art'] = anno_stagione_lib.decode_as(db_elem['a_s'])
			elif db_elem['anno'] == '0' and db_elem['stagione']=='0':
				db_elem['a_s'] = 'PP'
				db_elem['anno_stagione_art'] = 'permanente'
			pard['sql'] = """ select a.*, f.*
						from impo_articoli a 
						join impo_societa s on (a.societa = s.soc_orig)
						left join impo_fornitori f	on a.fornitore_principale=f.fornitore and a.societa=f.societa 
						where s.soc_dt='%(societa)s' 
						and a.articolo = '%(articolo)s'
						and a.anno_stagione = '%(anno_stagione_art)s'
						and a.gm = '%(gm)s'
						""" %db_elem
			art_dict = db_access.mysql_dict(pard)
			if art_dict:
				db_elem.update(art_dict[0])
		ret_dict[(rec['modello'], rec['variante'])] = db_list
	return ret_dict  


def get_prezzi_modello(pard, modello):
	diz_out = {'prezzo_m1' : '', 'prezzo_m2' : ''}
	query = """ select 	CASE WHEN PRZ_V_4=0
						THEN PRZ_V_1/100 
						ELSE PRZ_V_4/100 
						END as prezzo,
						MERC as mercato
				from PREZZI 
				where C_MAT = %s000
				and (MERC=1 OR MERC=2)
			"""%modello
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	for r in resd:
		if r['mercato'] == '1':
			diz_out['prezzo_m1'] = round(float(r['prezzo']),2)
		if r['mercato'] == '2':
			diz_out['prezzo_m2'] = round(float(r['prezzo']),2)

	return diz_out


def get_made_in_modello(pard, modello):
	query = """ select 	GROUP_CONCAT(DISTINCT made_in) as made_in
				from datge.DATI_MADE_IN
				where MODELLO_NEW = %s000
			"""%modello
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	if resd:
		return resd[0]['made_in']
	else:
		return ''


def get_compos_modello(pard, modello):
	diz_out = {'compos' : '', 'componente' : '', 'pz' : ''}
	query = """ select c.PROGR_PEZZO, c.CODICE_COMPONENTE, c.PROGR_COMPONENTE, a.DESCR_COMPONENTE, 
						c.PROGR_COMPOSIZIONE, c.PERC_COMPOSIZIONE, c.SIGLA_COMPOSIZIONE
				from COMPOS_NEW c
				join ANAG_COMPONENTI a on (c.CODICE_COMPONENTE = a.CODICE_COMPONENTE and a.LINGUA='ITAL')
				where substring(MODELLO, 1, 12) = %s
				ORDER BY PROGR_PEZZO, CODICE_COMPONENTE,PROGR_COMPONENTE, PROGR_COMPOSIZIONE
			"""%modello
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	result_list = []
	for k, g in itertools.groupby(resd, key = lambda k : (k['PROGR_PEZZO'], k['CODICE_COMPONENTE'], k['PROGR_COMPONENTE'], k['DESCR_COMPONENTE'])):
		compos = list(g)
		diz = {}
		diz['compos'] = ''
		diz['pz'] = k[0]
		diz['componente'] = k[3]
		for c in compos:
			diz['compos'] += c['PERC_COMPOSIZIONE']+c['SIGLA_COMPOSIZIONE']
		result_list.append(diz)
	return result_list


def get_compos_modello_traduzioni(pard, modello):
	diz_out = {'compos' : '', 'componente' : '', 'pz' : ''}
	query = """ select c.PROGR_PEZZO, c.CODICE_COMPONENTE, c.PROGR_COMPONENTE, group_concat(distinct a.DESCR_COMPONENTE separator ', ') as DESCR_COMPONENTE,
				c.PROGR_COMPOSIZIONE, c.PERC_COMPOSIZIONE, c.SIGLA_COMPOSIZIONE
				from COMPOS_NEW c
				join ANAG_COMPONENTI a on (c.CODICE_COMPONENTE = a.CODICE_COMPONENTE)
				where substring(MODELLO, 1, 12) = %s
				ORDER BY PROGR_PEZZO, CODICE_COMPONENTE,PROGR_COMPONENTE, PROGR_COMPOSIZIONE
			"""%modello
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	result_list = []
	for k, g in itertools.groupby(resd, key = lambda k : (k['PROGR_PEZZO'], k['CODICE_COMPONENTE'], k['PROGR_COMPONENTE'], k['DESCR_COMPONENTE'])):
		compos = list(g)
		diz = {}
		diz['compos'] = []
		diz['pz'] = k[0]
		diz['componente'] = k[3]
		for c in compos:
			diz['compos'].append([c['PERC_COMPOSIZIONE'], get_composizioni_tradotte(pard, c['SIGLA_COMPOSIZIONE'])])
		result_list.append(diz)
	return result_list


def get_composizioni_tradotte(pard, sigla_composizione):
	from config import db_to_db_datge
	db = db_to_db_datge(pard['MYSQL_DB'])
	query = """
			select group_concat(distinct descrizione separator ', ') as sigla_composiz_lingue
			from %s.PF_SIGLE_COMPOS where sigla = '%s'
			""" %(db, sigla_composizione)
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	if resd:
		return resd[0]['sigla_composiz_lingue'].upper() if resd[0]['sigla_composiz_lingue'] not in [None, 'None', ''] \
														else resd[0]['sigla_composiz_lingue']
	else:
		return ''


def get_ns_tessuto(pard, cmat, testata):
	query = """ select distinct ifnull(ART_ORIG_DESCR, '') as ART_ORIG_DESCR, CAPOCMAT
				from FACORD
				where C_MAT = '%s'
				and RIF_INTR in (%%(rif_ord_list)s)
				"""%cmat %testata #anno_stagione
				#and ANNO_STAGIONE_DT = '%s'
	pard['sql'] = query
	result = db_access.mysql_dict(pard)
# 	if pard['sid_id_utente'] == 'fabio':
# 		tools.dump(pard['sql'])
	if result:
		return result[0]['ART_ORIG_DESCR'], result[0]['CAPOCMAT']
	else:
		return '', ''
				
	

def crea_scheda_modello(pard, modello):
	query = """select substring(A.C_MAT, 1, 12) as modello, 
						A.D_MAT as nome,
						ifnull(substring(A2.C_MAT, 1, 12), IFNULL(concat(SUBSTR(M.D_MAT_F, 1, 8), '00', SUBSTR(M.D_MAT_F, -9, 2)), ''))  as modello_orig,
						ifnull(A2.D_MAT, IFNULL(SUBSTR(M.D_MAT_F, -7, 7), '')) as nome_orig,
						ifnull(F.CONTO, IFNULL(M.CONTO, '')) as faconista, 
						ifnull(C1.RAGSOC, ifnull(C2.RAGSOC, '')) as ragsoc, 
						ifnull(F.CMATSET, '') as collezione, 
						ifnull(A.ANNO, '') as anno, 
						ifnull(A.STAG, '') as stagione,
						ifnull(A.CST_STD/100, '') as costo_ind,
						ifnull(group_concat(distinct substring(FC.C_MAT, 1, 5)), '')  as cmat_list,
						ifnull(GROUP_CONCAT(DISTINCT F.RIF_INTR), '') as rif_intr_bp,
						ifnull(GROUP_CONCAT(DISTINCT S.RIF_ORD), '') as rif_ord_list,
						'' as nota
				from  ANAMAT A
				left join FACBP F  on (substring(F.C_MAT,1,12)=substring(A.C_MAT,1,12))
				left join ANAMAT A2 on (F.C_MAT_A = A2.C_MAT)
				left join FACCOM FC on (F.RIF_INTR = FC.RIF_INTR )
				left join FAC_ANACON C1 on (C1.CONTO=F.CONTO)
				left join MATFOR M on (M.C_MAT = A.C_MAT)
				left join FAC_ANACON C2 on (C2.CONTO=M.CONTO)
				LEFT join FACPEZ P ON (F.RIF_INTR = P.NR_RIF)
				LEFT join SITPEZ S ON (P.NR_PZ = S.NR_PZ)
				where 1=1
				and A.C_MAT like '%%%s%%'
				AND ( FC.T_MAT='T' or FC.T_MAT is NULL or FC.T_MAT = '')
				 """ %modello
#				%(q_societa)s 
#				%(q_gm)s %(q_art)s
#				 %(q_classe)s
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	# if pard['sid_id_utente'] == 'elifus':
	# 	tools.dump(query)
		# tools.dump(resd)
	if resd:
		r = resd[0]
		r['classe'] = str(r['modello'])[1:3]
		if r['costo_ind']:
			r['costo_ind'] = round(float(r['costo_ind']), 2)
		r['anno_stagione'] = ''
		if r['anno'] and r['stagione']:
			if r['stagione'] == '2':
				r['stagione'] = '1'
			elif r['stagione'] == '4':
				r['stagione'] = '2'
			r['anno_stagione'] = anno_stagione_lib.decode_as(r['anno']+r['stagione'])
			
		r['tessuto'] = []
		cmat_list = r['cmat_list'].split(',')
		for c in cmat_list:
			if c and c != '0':
				diz = {'ns_tessuto' : '', 'ns_tessuto_descr' : '', 'capocmat' : ''}
				if r['rif_ord_list']:
					diz['ns_tessuto'] = c
					diz['ns_tessuto_descr'], diz['capocmat'] = get_ns_tessuto(pard, c, r)
				r['tessuto'].append(diz)
			
		r['societa']=''
		r['nome_orig'] = r['nome_orig'].strip()
		if len(str(r['modello_orig'])) == 12: 
			if str(r['modello_orig'])[9:12] == '006':
				r['societa'] = 'MM'
			elif str(r['modello_orig'])[9:12] == '002':
				r['societa'] = 'MA'
			elif str(r['modello_orig'])[9:12] == '003':
				r['societa'] = 'MN'	
			elif str(r['modello_orig'])[9:12] == '013':
				r['societa'] = 'DT'	
		r['varianti'] = crea_modello_var(pard, r['rif_intr_bp'], r['cmat_list'])
		return r
	else:
		return {}


def check_scheda_modello(pard, modello):
	query = """select TI_ROWID
				from SCHEDE_MODELLO S
				where MODELLO = %s
			""" %modello
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	if resd:
		return resd[0]['TI_ROWID']
	else:
		return ''
	

def get_scheda_modello(pard, id_scheda):
	query = """select S.TI_ROWID  as id_scheda, 
					S.MODELLO_ORIG  as modello_orig, 
					S.MODELLO as modello, 
					S.MODELLO_DESCR as nome,
					S.MODELLO_ORIG_DESCR as nome_orig,
					CASE WHEN S.FACONISTA=0 THEN ifnull(F.CONTO, '') ELSE S.FACONISTA END as faconista, 
					CASE WHEN S.FACONISTA=0 THEN ifnull(C2.RAGSOC, '') ELSE ifnull(C1.RAGSOC, '') END as ragsoc,
					ifnull(F.CMATSET, '') as collezione, 
					A.ANNO as anno, 
					A.STAG as stagione,
					A.CST_STD/100 as costo_ind,
					S.TESSUTO_ORIG  as tessuto_orig,
					S.TESSUTO_DESCR_ORIG as tessuto_descr_orig,
					S.TESSUTO_DT as tessuto,
					S.NOTE as nota,
					IFNULL(group_concat(distinct substring(FC.C_MAT, 1, 5), ''), '')  as cmat_list,
					ifnull(GROUP_CONCAT(DISTINCT T.RIF_ORD), '') as rif_ord_list
				from SCHEDE_MODELLO S
				join ANAMAT A on (S.MODELLO = SUBSTRING(A.C_MAT, 1,12))
				left join FACBP F on (A.C_MAT = F.C_MAT) 
				left join FACCOM FC on (F.RIF_INTR = FC.RIF_INTR)
				left join FAC_ANACON C1 on (S.FACONISTA=C1.CONTO)
				left join FAC_ANACON C2 on (F.CONTO=C2.CONTO)
				LEFT join FACPEZ P ON (F.RIF_INTR = P.NR_RIF)
				LEFT join SITPEZ T ON (P.NR_PZ = T.NR_PZ)
				where 1=1
				and S.TI_ROWID = %s
				AND (FC.T_MAT='T' OR FC.T_MAT is NULL)
				 """ %id_scheda
	pard['sql'] = query
	resd = db_access.mysql_dict(pard)
	if resd:
		r = resd[0]
		r['classe'] = str(r['modello'])[1:3]
		if r['costo_ind']:
			r['costo_ind'] = round(float(r['costo_ind']), 2)
		r['anno_stagione'] = ''
		if r['anno'] and r['stagione']:
			if r['stagione'] == '2':
				r['stagione'] = '1'
			elif r['stagione'] == '4':
				r['stagione'] = '2'
			r['anno_stagione'] = anno_stagione_lib.decode_as(r['anno']+r['stagione'])
		
		if r['tessuto']:
			r['tessuto'] = eval(r['tessuto'])
			for t in r['tessuto']:
				t['ns_tessuto_descr'] = t['ns_tessuto_descr'].encode('latin1')			
		
		if not r['tessuto']:	
			r['tessuto'] = []
			cmat_list = r['cmat_list'].split(',')
			for c in cmat_list:
				if c and c != '0':
					diz = {'ns_tessuto' : '', 'ns_tessuto_descr' : '', 'capocmat' : ''}
					if r['rif_ord_list']:
						diz['ns_tessuto'] = c
						diz['ns_tessuto_descr'], diz['capocmat'] = get_ns_tessuto(pard, c, r)
					r['tessuto'].append(diz)
			
		r['societa']=''
		if len(str(r['modello_orig'])) == 12: 
			if str(r['modello_orig'])[9:12] == '006':
				r['societa'] = 'MM'
			elif str(r['modello_orig'])[9:12] == '002':
				r['societa'] = 'MA'
			elif str(r['modello_orig'])[9:12] == '003':
				r['societa'] = 'MN'	
			elif str(r['modello_orig'])[9:12] == '013':
				r['societa'] = 'DT'	
		r['varianti'] = get_modello_var(pard, id_scheda)
		#r['nota'] = r['nota'].decode('latin-1')
		
		return r
	else:
		return {}


def crea_modello_var(pard, rif_intr_bp, cmat_list):
	abilita_dump = False
	resd = []
	if rif_intr_bp:
		query = """select F.VAR_DIS,
							C.C_MAT, 
							B.C_MAT_A as modello_orig,
							B.C_MAT as modello_dt,
							B.ANNO,
							B.STAG,
							B.CMATSET,
							group_concat(DISTINCT S.RIF_ORD) as ordini_list
					from FACCAP F 
					join FACCOM C ON (F.RIF_INTR = C.RIF_INTR AND F.VAR_DIS=C.VAR_DIS)
					join FACBP B ON (F.RIF_INTR = B.RIF_INTR)
					join FACPEZ P ON (C.C_MAT = P.C_MAT)
					join SITPEZ S ON (P.NR_PZ = S.NR_PZ)
					where 1=1
					and F.RIF_INTR in (%s)
					and C.T_MAT = 'T'
					group by F.VAR_DIS
					 """ %rif_intr_bp
		pard['sql'] = query
		if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
			tools.dump(query)
		resd = list(db_access.mysql_dict(pard))
		if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
			tools.dump(resd)
	result = []
	for r in resd:
		diz = {'variante': r['VAR_DIS'],
			   'qta': 0,
			   'variante_descr': '',
			   'variante_modello': r['VAR_DIS'],
			   'variante_orig': '',
			   'cmat': r['C_MAT'],
			   'tale_quale': 'N'}

		query = """ select sum(F.QT1+F.QT2+F.QT3+F.QT4+F.QT5+F.QT6+F.QT7+F.QT8+F.QTC1+F.QTC2+F.QTC3+F.QTC4+F.QTC5+F.QTC6+F.QTC7+F.QTC8) as QTA
					from FACCAP F
					where 1=1
					and F.RIF_INTR in (%s)
					and F.VAR_DIS = '%%s'
					"""%rif_intr_bp %r['VAR_DIS']
		pard['sql'] = query
		res_q = list(db_access.mysql_dict(pard))
		if res_q:
			diz['qta'] = res_q[0]['QTA']
		
		tale_quale = False
		if r['CMATSET'] in ('OUTLET RIAS','OUTLET PROG') and r['modello_dt'][9:12] == '005':
			tale_quale = True
		
		query = """ select distinct FD.COL_ORIG_DESC, F.CAPOCMAT, FD.COL_ORIG, F.SOC_ART_ORIG
				from FACORD F
				join FACORD_DETT FD ON (F.ID = FD.ID_FACORD)
				where F.C_MAT in (%s)
				and FD.CMAT_VAR = %s
				AND F.RIF_INTR in (%s)
				and FD.MODELLO = '%s'
				"""%(cmat_list, r['C_MAT'][5:8], r['ordini_list'], str(r['modello_orig'])[:8])
				
		pard['sql'] = query
		res_descr = db_access.mysql_dict(pard)
		if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
			tools.dump(query)
			tools.dump(res_descr)
		if res_descr:
			anno_stagione_orig = anno_stagione_lib.decode_as(str(res_descr[0]['CAPOCMAT'])[4:6])
			ml = str(r['modello_orig'])[0] + str(r['modello_orig'])[7]
			
			gm_orig = str(res_descr[0]['CAPOCMAT'])[2:4]
			art_orig = str(res_descr[0]['CAPOCMAT'])[6:9]
			col_orig = res_descr[0]['COL_ORIG']
			soc_orig = res_descr[0]['SOC_ART_ORIG']
			if col_orig and int(col_orig)>1000 and soc_orig in ('MM','MA'):
				col_orig = str(col_orig)[-3:]
			
			diz['variante_descr'] = res_descr[0]['COL_ORIG_DESC']
			
			query = """ select ml, variante_commerciale
				from impo_articoli ia
				join impo_articoli_varianti iv on (ia.societa = iv.societa and ia.id_articolo = iv.id_articolo)
				where ia.anno_stagione = '%s'
				and ia.articolo = '%s'
				and ia.gm = '%s'
				and iv.colore = '%s'
				and ia.societa = '%s'
				"""%(anno_stagione_orig, art_orig, gm_orig, col_orig, soc_orig)
			pard['sql'] = query
			if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
				tools.dump(query)
			res_var = db_access.mysql_dict(pard)
			if res_var:
				trovato = False
				for rv in res_var:
					if str(rv['ml']) == str(ml):
						trovato = True
						diz['variante_orig'] = rv['variante_commerciale']
						if tale_quale:
							diz['tale_quale'] = 'S'
				if not trovato:
					diz['variante_orig'] = res_var[0]['variante_commerciale']
		else:
			query = """ select distinct FD.COL_ORIG_DESC, F.CAPOCMAT, FD.COL_ORIG, F.SOC_ART_ORIG
				from FACORD F
				join FACORD_DETT FD ON (F.ID = FD.ID_FACORD)
				where F.C_MAT in (%s)
				and FD.CMAT_VAR = %s
				AND F.RIF_INTR in (%s)
				"""%(cmat_list, r['C_MAT'][5:8], r['ordini_list'])
				
			pard['sql'] = query
			if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
				tools.dump(query)
			res_descr2 = db_access.mysql_dict(pard)
			if res_descr2:
				
				gm_orig = str(res_descr2[0]['CAPOCMAT'])[2:4]
				art_orig = str(res_descr2[0]['CAPOCMAT'])[6:9]
				anno_stagione_orig = anno_stagione_lib.decode_as(str(res_descr2[0]['CAPOCMAT'])[4:6])
				ml = str(r['modello_orig'])[0] + str(r['modello_orig'])[7]
				col_orig = res_descr2[0]['COL_ORIG']
				soc_orig = res_descr2[0]['SOC_ART_ORIG']
				if col_orig and int(col_orig)>1000 and soc_orig in ('MM','MA'):
					col_orig = str(col_orig)[-3:]
					
				
				diz['variante_descr'] = res_descr2[0]['COL_ORIG_DESC']
				
				query = """ select ml, variante_commerciale
					from impo_articoli ia
					join impo_articoli_varianti iv on (ia.societa = iv.societa and ia.id_articolo = iv.id_articolo)
					where ia.anno_stagione = '%s'
					and ia.articolo = '%s'
					and ia.gm = '%s'
					and iv.colore = '%s'
					and ia.societa = '%s'
					"""%(anno_stagione_orig, art_orig, gm_orig, col_orig, soc_orig)
				pard['sql'] = query
				res_var = db_access.mysql_dict(pard)
				if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
					tools.dump(query)
					tools.dump(res_var)
					tools.dump(ml)
					tools.dump(tale_quale)
				if res_var:
					trovato = False
					for rv in res_var:
						if str(rv['ml']) == str(ml):
							trovato = True
							diz['variante_orig'] = rv['variante_commerciale']
# 							if tale_quale:
# 								diz['tale_quale'] = 'S'
					if not trovato:
						diz['variante_orig'] = res_var[0]['variante_commerciale']
		if pard['sid_id_utente'] in ['elifus', 'matbri'] and abilita_dump == True:
			tools.dump(diz)
		result.append(diz)
	
	return result


def get_modello_var(pard, id_scheda):
	query = """select MODELLO_VAR as variante_modello, COLORE_DESCR as variante_descr, QTA as qta,
					VAR_ORIG as variante_orig, C_MAT as cmat, VAR_DIS as variante, TALE_QUALE as tale_quale
				from SCHEDE_MODELLO_RIGHE
				where 1=1
				and ID_SCHEDA = %s
				 """ %id_scheda
	pard['sql'] = query
	resd = list(db_access.mysql_dict(pard))
	return resd



def get_scheda_modello_righe(pard, id_scheda):
	query= """ select ID_SCHEDA, MODELLO_VAR, COLORE_DESCR, QTA,
				VAR_ORIG, C_MAT, VAR_DIS, TALE_QUALE,
				TS_NAS, U_NAS, TS_VAR, U_VAR
				from SCHEDE_MODELLO_RIGHE
				where ID_SCHEDA = %s
			"""%id_scheda
	pard['sql'] = query
	res = list(db_access.mysql_dict(pard))
	return res


def insert_scheda_modello(pard, dati, conn):
	if not dati['MODELLO_ORIG'] or len(dati['MODELLO_ORIG']) < 12:
		raise errors.BOException('Impossibile salvare una scheda senza modello originale (codice a 15).')
	
	if dati['MODELLO_ORIG'][9:12] not in ('002', '003', '006', '013'):
		raise errors.BOException('Societa modello originale errata (002-MA, 003-MN, 006-MM, 013-DT).')
	
	if not dati['FACONISTA']:
		raise errors.BOException('Impossibile salvare una scheda senza faconista.')
	
	for k in ('NOTE', 'MODELLO_ORIG_DESCR', 'MODELLO_DESCR', 'TESSUTO_DESCR_ORIG', 'tessuto'):
		if k not in ('tessuto'):
			dati[k] = dati[k].encode('latin1')
		dati[k] = conn.escape_string(str(dati[k]))
	
	query= """ insert into SCHEDE_MODELLO
				(MODELLO_ORIG, MODELLO_ORIG_DESCR, MODELLO, MODELLO_DESCR,
				TESSUTO_ORIG, TESSUTO_DESCR_ORIG, TESSUTO_DT, FACONISTA, NOTE, TS_NAS, U_NAS, TS_VAR, U_VAR)
				VALUES
				('%(MODELLO_ORIG)s', '%(MODELLO_ORIG_DESCR)s', '%(MODELLO)s', '%(MODELLO_DESCR)s', 
				'%(TESSUTO_ORIG)s', '%(TESSUTO_DESCR_ORIG)s', '%(tessuto)s', '%(FACONISTA)s', '%(NOTE)s', 
				CURRENT_TIMESTAMP, '%%(sid_id_utente)s', CURRENT_TIMESTAMP, '%%(sid_id_utente)s')
			"""%dati %pard
	
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)
	
	pard['sql'] = """SELECT LAST_INSERT_ID() as id_scheda"""
	id_scheda = db_access.mysql_dict_conn(pard, conn)[0]['id_scheda']
	for var in dati['varianti']:
		insert_scheda_modello_riga(pard, id_scheda, var, conn)
		
	return id_scheda
		


def insert_scheda_modello_riga(pard, id_scheda, dati, conn):
	if not dati['MODELLO_VAR']:
		raise errors.BOException('Impossibile salvare una scheda modello senza variante')
	
	dati['COLORE_DESCR'] = conn.escape_string(str(dati['COLORE_DESCR']))
	
	query= """ insert into SCHEDE_MODELLO_RIGHE
				(ID_SCHEDA, MODELLO_VAR, COLORE_DESCR, QTA,
				VAR_ORIG, TALE_QUALE, C_MAT, VAR_DIS,
				TS_NAS, U_NAS, TS_VAR, U_VAR)
				VALUES
				('%s', '%%(MODELLO_VAR)s', "%%(COLORE_DESCR)s", '%%(QTA)s',
				'%%(VAR_ORIG)s', '%%(TALE_QUALE)s', '%%(C_MAT)s', '%%(VAR_DIS)s',
				CURRENT_TIMESTAMP, '%%%%(sid_id_utente)s', CURRENT_TIMESTAMP, '%%%%(sid_id_utente)s')
			"""%id_scheda %dati %pard
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)
 



def update_scheda_modello(pard, dati, conn):
	if not dati['MODELLO_ORIG'] or len(dati['MODELLO_ORIG']) < 12:
		raise errors.BOException('Impossibile salvare una scheda senza modello originale (codice a 15).')
	
	if dati['MODELLO_ORIG'][9:12] not in ('002', '003', '006', '013'):
		raise errors.BOException('Societa modello originale errata (002-MA, 003-MN, 006-MM, 013-DT).')
	
	if not dati['FACONISTA']:
		raise errors.BOException('Impossibile salvare una scheda senza faconista.')
	
	for k in ('NOTE', 'MODELLO_ORIG_DESCR', 'MODELLO_DESCR', 'TESSUTO_DESCR_ORIG', 'tessuto'):
		if k != 'tessuto':
			dati[k] = dati[k].encode('latin1')
		dati[k] = conn.escape_string(str(dati[k]))
	
	query= """ update SCHEDE_MODELLO
				set
				MODELLO_ORIG='%(MODELLO_ORIG)s', 
				MODELLO_ORIG_DESCR='%(MODELLO_ORIG_DESCR)s', 
				MODELLO='%(MODELLO)s', 
				MODELLO_DESCR='%(MODELLO_DESCR)s',
				TESSUTO_ORIG='%(TESSUTO_ORIG)s', 
				TESSUTO_DESCR_ORIG='%(TESSUTO_DESCR_ORIG)s', 
				TESSUTO_DT='%(tessuto)s', 
				FACONISTA='%(FACONISTA)s',
				NOTE='%(NOTE)s', 
				TS_VAR=CURRENT_TIMESTAMP, 
				U_VAR='%%(sid_id_utente)s'
				where TI_ROWID = %(ID_SCHEDA)s
			"""%dati %pard
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)
	modello_righe_old = get_scheda_modello_righe(pard, dati['ID_SCHEDA'])
	varianti_old = [int(r['MODELLO_VAR']) for r in modello_righe_old]
	varianti_new = []
	for variante in dati['varianti']:
		if int(variante['MODELLO_VAR']) not in varianti_old:
			insert_scheda_modello_riga(pard, dati['ID_SCHEDA'], variante, conn)
			varianti_new.append(int(variante['MODELLO_VAR']))
		else:
			update_scheda_modello_riga(pard, dati['ID_SCHEDA'], variante, conn)
			varianti_new.append(int(variante['MODELLO_VAR']))
			
	for var in varianti_old:
		if int(var) not in varianti_new:
			delete_scheda_modello_riga(pard, dati['ID_SCHEDA'], conn, var=int(var))
	
	return 	dati['ID_SCHEDA']


def update_scheda_modello_riga(pard, id_scheda, dati, conn):
	if not dati['MODELLO_VAR']:
		raise errors.BOException('Impossibile salvare una scheda modello senza variante')
	
	dati['COLORE_DESCR'] = conn.escape_string(str(dati['COLORE_DESCR']))
	
	query= """ update SCHEDE_MODELLO_RIGHE
				set
				MODELLO_VAR='%(MODELLO_VAR)s', 
				VAR_ORIG='%(VAR_ORIG)s',
				TALE_QUALE='%(TALE_QUALE)s', 
				C_MAT='%(C_MAT)s',
				VAR_DIS='%(VAR_DIS)s',
				COLORE_DESCR="%(COLORE_DESCR)s", 
				QTA='%(QTA)s',
				TS_VAR=CURRENT_TIMESTAMP, 
				U_VAR='%%(sid_id_utente)s'
				where ID_SCHEDA=%%%%s
				and MODELLO_VAR='%(MODELLO_VAR)s'
			"""%dati %pard %id_scheda
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)



def delete_scheda_modello(pard, id_scheda, conn):
	query= """ delete from SCHEDE_MODELLO
				where TI_ROWID=%s
			"""%(id_scheda)
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)
	
	delete_scheda_modello_riga(pard, id_scheda, conn)
	

	
def delete_scheda_modello_riga(pard, id_scheda, conn, var=''):
	where_clause = ''
	if var:
		where_clause += " and MODELLO_VAR='%s' "
	query= """ delete from SCHEDE_MODELLO_RIGHE
				where ID_SCHEDA=%s
				%s
			"""%(id_scheda, where_clause)
	pard['sql'] = query
	db_access.mysql_send_conn(pard, conn)


def get_modelli_schede(pard, conn, params):
	where_clause = " 1=1 "
	where_clause_divise = " "
	if params.get('modello', '') and params['modello']:
		where_clause += " and r.MODELLO_ORIG = %(modello)s " %params
		where_clause_divise += " and opr.MODELLO_ORIG = %(modello)s " %params
	else:
		where_clause += """ and r.FLAG_IMPORT_DIBA = '' """
	pard['sql'] = """ select distinct 
						r.SOCIETA_ORIG as societa, 
						r.MODELLO_ORIG as modello,
						r.AS_ORIG as anno_stagione
				from RICHIESTE_COMM_PF_RIGHE r
				where
				%s
			"""%(where_clause)
# 	pard['sql'] = """ select distinct 
# 							r.SOCIETA_ORIG as societa, 
# 							r.MODELLO_ORIG as modello,
# 							r.AS_ORIG as anno_stagione
# 					from RICHIESTE_COMM_PF_RIGHE r
# 					where
# 					%s
# 					
# 					union
# 					
# 					select distinct 
# 							opr.SOCIETA as societa, 
# 							opr.MODELLO_ORIG as modello, 
# 							o.AS_ORIG as anno_stagione
# 					from ORDINI_PF o 
# 					join ORDINI_PF_RIGHE opr on o.ID=opr.ID_ORDINE_PF
# 					where o.COLLEZIONE_FACON like 'MAXMARA DIV'
# 					and o.COLLEZIONE_DT = '70'
# 					%s
# 				"""%(where_clause, where_clause_divise)
	resd = list(db_access.mysql_dict_conn(pard, conn))
	
	return resd


def update_import_modelli_schede(pard, conn, data):
	pard['sql'] = """ update RICHIESTE_COMM_PF_RIGHE r
					set r.FLAG_IMPORT_DIBA = '1'
					where r.SOCIETA_ORIG = '%(societa)s'
					and r.MODELLO_ORIG = '%(modello)s'
					and r.AS_ORIG = '%(anno_stagione)s'
				"""%data
	db_access.mysql_send_conn(pard, conn)


def get_etichetta_lavaggio_completa(pard, cmat):
	pard['sql'] = """select group_concat(CODICE_LAVAGGIO) as etichetta, PROGR_PEZZO
						from LAVAGGIO_MODELLO
						where MODELLO = %s000
						group by PROGR_PEZZO
						order by PROGR_LAVAGGIO
					""" % cmat
	result_lavaggio = db_access.mysql_dict(pard)
	return result_lavaggio


def get_img_etichetta_lavaggio_completa(pard, etichetta):
	res = []
	for e in etichetta.split(','):
		if e.isdigit():
			# pard['sql'] = """select GM_ART as trasc
			# 						from TRASC_GM_LAV
			#                         where COD_LAV_5 = %s
			# 			            """ % e
			# result_tras = db_access.mysql_dict(pard)
			for x in range(len(e)):
				progressivo_lavaggio = x
				codice_lavaggio = e[x]
				pard['sql'] = """select *
								from ANA_CARE_SYMBOL
								where COD_LAVAGGIO = %s
								and POSIZ = %s
				""" %(codice_lavaggio, progressivo_lavaggio)
				result_simboli = db_access.mysql_dict(pard)
				if result_simboli:
					res.append({'tipo': 'std',
					            'trasc': '', #result_tras[0]['trasc'] if result_tras else '',
					            'img': result_simboli[0]['IMG_SRC'],
					            'desc': str(result_simboli[0]['DESC_SIMBOLO']).decode('latin-1').encode('utf-8')})
		else:
			# Etichette lavaggio aggiuntive:
			# TIPO_ETIC = 'L' per prendere solo le etichette relative ai lavaggi
			# TIPO_ETIC = 'C' per prendere anche le etichette relativa alle composizioni
			pard['sql'] = """select *
							from ANA_ETIAGG
							where COD_LAYOUT = "%s"
							and TIPO_ETIC in ('L','C')
						""" %e
			result_diciture = db_access.mysql_dict(pard)
			if result_diciture:
				for diz_d in result_diciture:
					res.append({'tipo': diz_d['TIPO'],
					            'img': '',
					            'desc': str(diz_d['DESC_ITA'].lower().capitalize()).decode('latin-1').encode('utf-8')})
	return res


@decorators.query_with_result
def get_modelli_autocomplete(soc='', param='', nome= ''):
	sel_clause = """ distinct substr(B.C_MAT_A,10,3) as societa,
							substr(B.C_MAT_A,1,8) as modello_orig,
							A.D_MAT as nome_modello_orig"""
	if nome:
		sel_clause = " distinct A.D_MAT as nome_modello_orig "
	societa = ''
	if soc == 'MM':
		societa = '006'
	elif soc == 'MA':
		societa = '002'
	elif soc == 'MN':
		societa = '003'
	elif soc == 'DT':
		societa = '013'
	query = """ select %s
				from FACBP B
				join ANAMAT A on (B.C_MAT_A = A.C_MAT)
				where (substr(B.C_MAT_A,1,8) like '%s%%' or A.D_MAT like '%s%%')
				and substr(B.C_MAT_A,10,3) like '%s%%'
			""" %(sel_clause, param, param.upper(), societa)
# 	tools.dump(query)
	return query


@decorators.query_with_result
def get_fornitore_pricipale_by_id_articolo(id_articolo, soc):
	query = """ select ifnull(f.conto_fornitore, '') as fornitore, ifnull(f.ragione_sociale, '') as ragione_sociale,
						ifnull(af.articolo_fornitore, '') as articolo_fornitore, ifnull(af.id_articolo_fornitore, '') as id_articolo_fornitore
				from    impo_articoli a
						left join impo_articoli_fornitore af on (a.societa = af.societa and a.id_articolo = af.id_articolo and af.stato not in ('Bozza', 'Incompleto', 'Annullato'))
						join impo_fornitori f on (f.societa='DT' and af.fornitore = f.fornitore)
				where a.societa = '%s'
				and a.id_articolo = '%s'
				""" % (soc, id_articolo)
	return query


@decorators.query_with_result
def get_diba_esplosa_by_key(id):
	query = """ select ID as id,
					MODELLO_DT as modello_dt,
					VARIANTE_DT as variante_dt,
					SOCIETA_ORIG as societa_orig,
					MODELLO_ORIG as modello_orig,
					NOME_MODELLO_ORIG as nome_modello_orig,
					VARIANTE_ORIG as variante_orig,
					CAPOCMAT as capocmat,
					DESCRIZIONE as descrizione,
					SOCIETA as societa,
					GM as gm,
					ANNO_STAGIONE as anno_stagione,
					ARTICOLO as articolo,
					ifnull(ID_ARTICOLO, '') as id_articolo,
					MISURA as misura,
					ifnull(MACROCOLORE, '') as macrocolore,
					COLORE as colore,
					ifnull(COLORE_DESCR, '') as colore_descr,
					CONSUMO as consumo,
					CONSUMO_DETTAGLIO as consumo_dettaglio,
					NUMERO_RIGA as numero_riga,
					TAGLIA_BASE as taglia_base,
					PEZZO as pezzo,
					ifnull(COLONNA,'') as colonna,
					ifnull(POSIZIONE, '') as posizione
			  from DIBA_ESPLOSA D
			  where ID = %s
			"""%id
	return query


@decorators.query_with_result
def get_tessuto_diba_esplosa(id, tessuto):
	query = """ select ID as id,
						MODELLO_DT as modello_dt,
						VARIANTE_DT as variante_dt,
						SOCIETA_ORIG as societa_orig,
						MODELLO_ORIG as modello_orig,
						NOME_MODELLO_ORIG as nome_modello_orig,
						VARIANTE_ORIG as variante_orig,
						CAPOCMAT as capocmat,
						DESCRIZIONE as descrizione,
						SOCIETA as societa,
						GM as gm,
						ANNO_STAGIONE as anno_stagione,
						ARTICOLO as articolo,
						ifnull(ID_ARTICOLO, '') as id_articolo,
						MISURA as misura,
						ifnull(MACROCOLORE, '') as macrocolore,
						COLORE as colore,
						COLORE_DESCR as colore_descr,
						CONSUMO as consumo,
						CONSUMO_DETTAGLIO as consumo_dettaglio,
						NUMERO_RIGA as numero_riga,
						TAGLIA_BASE as taglia_base,
						PEZZO as pezzo,
						COLONNA as colonna,
						POSIZIONE as posizione
				from DIBA_ESPLOSA D
				where ID = %s
				and SOCIETA = '%%(societa)s'
				and GM = %%(gm)s
				and ANNO_STAGIONE = '%%(anno_stagione)s'
				and ARTICOLO = %%(articolo)s
				and COLORE = %%(colore)s
				and MISURA = 00
				""" % id %tessuto
	return query


@decorators.query_without_result
def update_diba_esplosa(dati):
	conn = pool_manager.ConnPool().get_connection(Request().get_val(pool_manager.CONN_KEY))
	dati['colore_descr'] = conn.escape_string(dati['colore_descr'])
	dati['posizione'] = conn.escape_string(dati['posizione'])
	dati['descrizione'] = conn.escape_string(dati['descrizione'])
	query = {}
	query['query'] = ''' update DIBA_ESPLOSA
						set MODELLO_DT = '%(modello_dt)s',
						  VARIANTE_DT  = '%(variante_dt)s',
						  SOCIETA_ORIG  = '%(societa_orig)s',
						  MODELLO_ORIG  = '%(modello_orig)s',
						  NOME_MODELLO_ORIG  = '%(nome_modello_orig)s',
						  VARIANTE_ORIG  = '%(variante_orig)s',
						  CAPOCMAT  = '%(capocmat)s',
						  DESCRIZIONE  = '%(descrizione)s',
						  SOCIETA  = '%(societa)s',
						  GM  = '%(gm)s',
						  ANNO_STAGIONE  = '%(anno_stagione)s',
						  ID_ARTICOLO = '%(id_articolo)s',
						  ARTICOLO  = '%(articolo)s',
						  MISURA  = '%(misura)s',
						  MACROCOLORE = '%(macrocolore)s',
						  COLORE  = '%(colore)s',
						  COLORE_DESCR = '%(colore_descr)s',
						  CONSUMO = '%(consumo)s',
						  CONSUMO_DETTAGLIO = '%(consumo_dettaglio)s',
						  NUMERO_RIGA = '%(numero_riga)s',
						  TAGLIA_BASE  = '%(taglia_base)s',
						  PEZZO  = '%(pezzo)s',
						  COLONNA  = '%(colonna)s',
						  POSIZIONE  = '%(posizione)s'
					where ID = %(id)s 
					'''%dati
	return query


@decorators.insert_query_with_id_as_result
def insert_diba_esplosa(dati):
	#tools.dump(dati)
	conn = pool_manager.ConnPool().get_connection(Request().get_val(pool_manager.CONN_KEY))
	dati['colore_descr'] = conn.escape_string(dati['colore_descr'])
	dati['posizione'] = conn.escape_string(dati['posizione'])
	dati['descrizione'] = conn.escape_string(dati['descrizione'])

	query = {}
	query['query'] = ''' insert into DIBA_ESPLOSA
						set MODELLO_DT = '%(modello_dt)s',
						  VARIANTE_DT  = '%(variante_dt)s',
						  SOCIETA_ORIG  = '%(societa_orig)s',
						  MODELLO_ORIG  = '%(modello_orig)s',
						  NOME_MODELLO_ORIG  = '%(nome_modello_orig)s',
						  VARIANTE_ORIG  = '%(variante_orig)s',
						  CAPOCMAT  = '%(capocmat)s',
						  DESCRIZIONE  = '%(descrizione)s',
						  SOCIETA  = '%(societa)s',
						  GM  = '%(gm)s',
						  ANNO_STAGIONE  = '%(anno_stagione)s',
						  ID_ARTICOLO = '%(id_articolo)s',
						  ARTICOLO  = '%(articolo)s',
						  MISURA  = '%(misura)s',
						  MACROCOLORE = '%(macrocolore)s',
						  COLORE  = '%(colore)s',
						  COLORE_DESCR = '%(colore_descr)s',
						  CONSUMO = '%(consumo)s',
						  CONSUMO_DETTAGLIO = '%(consumo_dettaglio)s',
						  NUMERO_RIGA = '%(numero_riga)s',
						  TAGLIA_BASE  = '%(taglia_base)s',
						  PEZZO  = '%(pezzo)s',
						  COLONNA  = '%(colonna)s',
						  POSIZIONE  = '%(posizione)s'
				 ''' %dati
	return query


@decorators.query_without_result
def delete_diba_esplosa(id):
	query = {}
	query['query'] = ''' delete from DIBA_ESPLOSA
					where ID = %s
					'''%id
	return query


@decorators.query_with_result
def get_id_art_orig(dati):
	query = """ select a.id_articolo as id_articolo,
						ifnull(a.descrizione, '') as descrizione
				from impo_articoli a
				where a.societa = '%(societa)s'
				and a.articolo = '%(articolo)s'
				and a.gm = '%(gm)s'
				and a.anno_stagione = '%(anno_stagione)s'
		""" %dati
	return query


@decorators.query_with_result
def get_dati_articolo_orig(dati):
	query = """ select a.id_articolo as id_articolo,
						ifnull(a.descrizione, '') as descrizione,
						ifnull(af.id_articolo_fornitore, '') as id_articolo_fornitore,
						ifnull(af.articolo_fornitore, '') as articolo_fornitore,
						ifnull(f.ragione_sociale, '') as fornitore,
						ifnull(f.conto_fornitore, '') as conto
				from impo_articoli a
				left join impo_articoli_fornitore af on (a.societa = af.societa and a.id_articolo = af.id_articolo
														and af.stato not in ('Bozza', 'Annullato'))
				left join impo_fornitori f on (f.societa = 'DT' and af.fornitore = f.fornitore)
				LEFT JOIN FAC_ANACON FF ON (FF.CONTO = f.conto_fornitore)
				where a.societa = '%(societa)s'
				and a.articolo = '%(articolo)s'
				and a.gm = '%(gm)s'
				and a.anno_stagione = '%(anno_stagione)s'
				AND (FF.FLAG_SOSPESO != 'SI' or FF.FLAG_SOSPESO is null)
		""" %dati
	return query


@decorators.query_with_result
def get_dati_articolo_societa(dati):
	query = """ select      ifnull(af.id_articolo_fornitore, 0) as id_articolo_fornitore,
							ifnull(af.articolo_fornitore, concat(g.descrizione, ' ' ,a.articolo)) as articolo_fornitore,
							ifnull(f.ragione_sociale, '') as fornitore
					from impo_articoli a
					join impo_societa s on (s.soc_dt = a.societa)
					join impo_gm g on (s.soc_orig = g.societa and g.gm=a.gm)
					left join impo_fornitori f on (f.societa = 'DT' and f.conto_fornitore = %(fornitore)s)
					left join impo_articoli_fornitore af on (af.societa = a.societa and f.fornitore = af.fornitore
															and af.id_articolo = a.id_articolo)
					where a.societa = '%(soc_art_orig)s'
					and a.id_articolo = '%(id_articolo)s'
			""" % dati
	return query



def get_articoli_fornitore(dati):
	forn_list = []
	res_orig = get_articoli_fornitore_orig(dati)
	forn_list.extend(res_orig)
	res_dt = get_articoli_fornitore_dt(dati)
	forn_list.extend(res_dt)
	return forn_list


@decorators.query_with_result
def get_articoli_fornitore_orig(dati):
	query = """ select ifnull(af.id_articolo_fornitore, '') as id_articolo_fornitore,
						ifnull(af.articolo_fornitore, '') as articolo_fornitore,
						ifnull(f.ragione_sociale , '') as fornitore,
						ifnull(f.fornitore, '') as conto,
						ifnull(af.stato, '') as stato,
						'SOC' as orig
				from impo_articoli a
				join impo_societa s on (a.societa = s.soc_orig)
				left join impo_articoli_fornitore af on (a.societa = af.societa and a.id_articolo = af.id_articolo)
				left join impo_fornitori f on (f.societa = s.soc_orig and af.fornitore = f.fornitore)
				where s.soc_dt = '%(societa)s'
				and a.articolo = '%(articolo)s'
				and a.gm = '%(gm)s'
				and a.anno_stagione = '%(anno_stagione)s'
				and af.stato not in ('Bozza', 'Incompleto', 'Annullato')
		""" %dati
	return query


@decorators.query_with_result
def get_articoli_fornitore_dt(dati):
	query = """ select ifnull(af.id_articolo_fornitore, '') as id_articolo_fornitore,
						ifnull(af.articolo_fornitore, '') as articolo_fornitore,
						ifnull(f.ragione_sociale , '') as fornitore,
						ifnull(f.conto_fornitore, '') as conto,
						ifnull(af.stato, '') as stato,
						'DT' as orig
				from impo_articoli a
				left join impo_articoli_fornitore af on (a.societa = af.societa and a.id_articolo = af.id_articolo)
				left join impo_fornitori f on (f.societa = 'DT' and af.fornitore = f.fornitore)
				left join FAC_ANACON FF ON (FF.CONTO = f.conto_fornitore)
				where a.societa = '%(societa)s'
				and a.articolo = '%(articolo)s'
				and a.gm = '%(gm)s'
				and a.anno_stagione = '%(anno_stagione)s'
				and af.stato not in ('Bozza', 'Incompleto', 'Annullato')
				AND (FF.FLAG_SOSPESO != 'SI' or FF.FLAG_SOSPESO is null)
		""" %dati
	return query


@decorators.query_with_result
def get_fornitore_pricipale_by_id_articolo_forn(dati):
	query = """ select ifnull(af.id_articolo_fornitore, '') as id_articolo_fornitore,
						ifnull(af.articolo_fornitore, '') as articolo_fornitore,
						ifnull(f.ragione_sociale, '') as ragione_sociale,
						ifnull(f.fornitore, '') as conto
				from impo_articoli_fornitore af
				left join impo_fornitori f on (f.societa ='DT' and af.fornitore = f.fornitore)
				where af.societa = '%(societa)s'
				and af.id_articolo_fornitore = '%(id_articolo_fornitore)s'
		""" %dati
	return query


@decorators.query_with_result
def get_macrocolore(dati):
	query = """ select C.DESC_COLORE as macrocolore, ifnull(ic.descrizione, C.DESC_COLORE) as colore_descr
				from MP_COLORE C
				join impo_colori ic on (ic.id_raggr_colore = C.COLORE)
				join impo_societa soc on (soc.soc_orig = ic.societa)
				where soc.soc_dt = '%(societa)s'
				and lpad(ic.colore, 4, '0') = '%(colore)s'
		""" %dati
	return query


@decorators.query_with_result
def get_descr_gm_orig(soc, gm):
	query = """ select descrizione
				from impo_gm g
				join impo_societa s on (s.soc_orig = g.gm)
				where s.soc_dt = '%s'
				and g.gm = '%s'
			""" % (soc, gm)
	return query


@decorators.query_with_result
def ricerca_studio_diba(dict_params):
	q_cod_modelli = ''
	if dict_params.get('cod_modelli', ''):
		q_cod_modelli = " and d.modello_orig in (%(cod_modelli)s) " % dict_params

	q_id = ''
	if dict_params.get('id_richiesta_studio', ''):
		q_id = " and rc.id in (%(id_richiesta_studio)s) " % dict_params

	q_classe = ''
	if dict_params.get('classe', '') and dict_params['classe']:
		q_classe = " and substring(d.modello_orig,2,2) IN (%s) " % ','.join(dict_params['classe'])

	q_modello = ''
	if dict_params.get('modello', ''):
		q_modello = " and (d.modello_orig like'%(modello)s%%' or rcr.MODELLO_TRASCODIFICA like '%(modello)s%%') " % dict_params

	q_nome_modello = ''
	if dict_params.get('nome_modello', ''):
		q_nome_modello = " and d.nome_modello_orig ='%(nome_modello)s' " % dict_params

	q_variante = ''
	if dict_params.get('variante', ''):
		q_variante = " and d.variante_orig ='%(variante)s' " % dict_params

	q_societa = ''
	if dict_params.get('societa', ''):
		q_societa = " and d.societa ='%(societa)s' " % dict_params

	q_fam_gm = ''
	if dict_params.get('fam_gm', ''):
		q_fam_gm = " and ffgm.fam_gm ='%(fam_gm)s' " % dict_params

	q_gm = ''
	if dict_params.get('gm', ''):
		q_gm = " and d.gm ='%(gm)s' " % dict_params

	q_articolo = ''
	if dict_params.get('articolo', ''):
		q_articolo = " and d.articolo ='%(articolo)s' " % dict_params

	q_colore = ''
	if dict_params.get('colore', ''):
		q_colore = " and d.colore ='%(colore)s' " % dict_params

	q_art_princ = ''
	if dict_params.get('art_princ', ''):
		q_art_princ = " and rcr.art_princ IN (%(art_princ)s) " % dict_params

	q_collez_orig = ''
	if dict_params.get('collezione', ''):
		q_collez_orig = ' and rcr.collezione_orig like "%%%(collezione)s%%" ' % dict_params

	q_stato = ''
	if dict_params.get('stato', ''):
		q_stato = " and f.stato ='%(stato)s' " % dict_params

	q_avanzamento = ''
	if dict_params.get('avanzamento', ''):
		avanzamento = dict_params.get('avanzamento', '')
		if avanzamento == 'ordinato':
			q_avanzamento = " and i.rif_ord not in (0,'0','') "
		elif avanzamento == 'da_ordinare':
			q_avanzamento = " and (i.rif_ord in (0,'0','') or i.rif_ord is null) "
		elif avanzamento == 'impegnato':
			q_avanzamento = " and imp.impegni > 0 "
		elif avanzamento == 'da_impegnare':
			q_avanzamento = " and (imp.impegni = 0 or imp.impegni is null) "

	query = """select
				d.capocmat as capocmat,
				d.societa as societa,
				d.gm as gm,
				d.articolo as articolo,
				d.id_articolo as id_articolo,
				d.macrocolore as macrocolore,
				d.colore as colore,
				d.colore_descr as colore_descrizione,
				d.misura as misura,
				d.descrizione as descrizione,
				d.modello_orig as modello,
				d.variante_dt as variante,
				d.nome_modello_orig as nome_modello,
				d.consumo as consumo,
				d.consumo_dettaglio as consumo_dettaglio,
				d.posizione as posizione,
				d.anno_stagione as anno_stagione,
				if(sum(f.CAPI_FABB_TEOR)>0, SUM(f.CAPI_FABB_TEOR), sum(f.CAPI_FABB_PREV)) as tot_capi,
				if(sum(f.QTA_FABB_TEOR)>0, SUM(f.QTA_FABB_TEOR), SUM(f.QTA_FABB_PREV)) as fabbisogno,
				ifnull(imp.impegni, 0) as impegni,
				f.stato as stato,
				ffgm.fam_gm as fam_gm,
				f.id as id_fabb,
				d.id as id_diba,
				f.nota as nota,
				rcr.flag_non_originale as flag_variante,
				rcr.MODELLO_TRASCODIFICA as modello_trascodifica,
				ifnull(i.rif_ord, '') as id_facord_dett,
				rcr.art_princ as art_princ,
				rcr.collezione_orig as collezione_orig

				FROM FABBISOGNI f
				join DIBA_ESPLOSA d
					on (f.id_diba = d.id)
				join RICHIESTE_COMM_PF_ART rar
					on (rar.id = f.id_art_comm)
				join RICHIESTE_COMM_PF_RIGHE rcr
						on (rcr.id=rar.id_riga)
				JOIN RICHIESTE_COMM_PF rc
					on (rc.id=rcr.id_richiesta)
				join FACFAMGM_ASSO ffgm
				on (d.societa = ffgm.soc and d.gm = ffgm.soc_gm)
				left join IMPEGNI i
					on (f.id = i.id_fabbisogno and i.tipo = 'ORD')
				left join (select i2.id_fabbisogno, ifnull(sum(i2.qta), 0) as impegni
							from IMPEGNI i2
							where i2.stato='valido'
							group by i2.id_fabbisogno) as imp on (imp.id_fabbisogno = f.id)
				left join impo_articoli ia
				on (d.id_articolo = ia.id_articolo and d.societa = ia.societa)

				where 1=1
				%(q_cod_modelli)s
				%(q_id)s
				%(q_classe)s
				%(q_modello)s
				%(q_nome_modello)s
				%(q_variante)s
				%(q_societa)s
				%(q_fam_gm)s
				%(q_gm)s
				%(q_articolo)s
				%(q_collez_orig)s
				%(q_colore)s
				%(q_art_princ)s
				%(q_stato)s
				%(q_avanzamento)s
				group by d.id
				 """ % vars()
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_dati_studio_diba_pdf(dict_params):
	q_id = ''
	if dict_params.get('id', ''):
		q_id = " and rc.id ='%(id)s' " % dict_params
	q_modello = ''
	if dict_params.get('modello_orig', ''):
		q_modello = " and d.modello_orig ='%(modello_orig)s' " % dict_params

	query = """select
				d.societa as societa,
				d.gm as gm,
				d.capocmat as capocmat,
				d.articolo as articolo,
				d.articolo as id_articolo,
				d.macrocolore as macrocolore,
				d.colore as colore,
				d.colore_descr as colore_descrizione,
				d.misura as misura,
				d.descrizione as descrizione,
				d.societa_orig as societa_orig,
				d.modello_orig as modello,
				d.variante_dt as variante,
				d.nome_modello_orig as nome_modello,
				d.consumo as consumo,
				d.consumo_dettaglio as consumo_dettaglio,
				d.posizione as posizione,
				d.anno_stagione as anno_stagione,
				d.pezzo as pezzo,
				d.colonna as colonna,
				d.id as id_diba,
				rcr.nome_variante_orig as descr_col_variante,
				f.nota as nota,
				ifnull(if(f.CAPI_LANCIATI>0,f.CAPI_LANCIATI,if(f.CAPI_FABB_TEOR>0, f.CAPI_FABB_TEOR, f.CAPI_FABB_PREV)), 0) as tot_capi,
				ifnull(if(f.QTA_FABB_REALE>0,f.QTA_FABB_REALE,if(f.QTA_FABB_TEOR>0, f.QTA_FABB_TEOR, f.QTA_FABB_PREV)), 0) as fabbisogno,
				f.stato as stato,
				f.id as id_fabb

				from RICHIESTE_COMM_PF rc
				join RICHIESTE_COMM_PF_RIGHE rcr
					 on (rc.id=rcr.id_richiesta)
				join RICHIESTE_COMM_PF_ART rar
					on (rcr.id=rar.id_riga)
				join FABBISOGNI f
					on (rar.id = f.id_art_comm)
				join DIBA_ESPLOSA d
				on (f.id_diba = d.id and lpad(rar.variante, '0', 3) = lpad(d.variante_dt, '0', 3))

				where 1=1
				%(q_id)s
				%(q_modello)s

				limit 1000
				 """ % vars()
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_ordini_impegni(id, dict_params):

	query = """select
				distinct F.ANNO_STAGIONE_DT anno_stagione_dt,
				IFNULL(COL.CMATSET, '') as collezione_dt,
				F.SOC as societa,
				concat(F.C_MAT,FD.CMAT_VAR) as c_mat,
				FD.TI_ROWID as id_facord_dett,
				FD.FLAG_MODELLO as flag_modello,
				FD.modello as modello_orig,
				FD.modello_desc as nome_modello,
				FD.var_mod_orig as variante,
				FD.qta_ord,
				FD.qta_ord_eff,
				FD.qta_arriv,
				ifnull(i.QTA, '0') as qta_impegno,
				ifnull(i.UTENTE, '') as utente_impegno,
				ifnull(d.CAPOCMAT, '') as capocmat,
				ifnull(if(fabb.QTA_FABB_REALE>0,fabb.QTA_FABB_REALE,if(fabb.QTA_FABB_TEOR>0, fabb.QTA_FABB_TEOR, fabb.QTA_FABB_PREV)), 0) as fabb,
				ifnull(fac.RIF_INTR, '') as rif_bp,
				(FD.qta_ord - FD.qta_arriv) as qta_mancanti
			from FACORD F JOIN FACORD_DETT FD ON F.ID=FD.ID_FACORD
			join IMPEGNI i on (FD.TI_ROWID=i.RIF_ORD and i.tipo in ('ORD','SOC'))
			left join FACCOL COL ON (F.ID_COL=COL.ID_COL)
			left join FABBISOGNI fabb on (i.ID_FABBISOGNO=fabb.ID)
			left join IMPEGNI i2 on (fabb.ID=i2.ID_FABBISOGNO and i2.ID_FACCOM!='0' and i2.tipo = 'DT')
			left join FACCOM fac on (i2.ID_FACCOM=fac.TI_ROWID)
			left join DIBA_ESPLOSA d on fabb.ID_DIBA=d.ID
			where
				((F.STATO = 'DEFINITO'
				and FLAG_A_SALDO != 'S') or FD.TI_ROWID=%s)
				and F.C_MAT = '%s'
				and FD.CMAT_VAR = '%s'
				 """ %(id, str(dict_params['c_mat'])[:-3], str(dict_params['c_mat'])[-3:])
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_articoli_carico_pdf(dict_params):
	query = """select
					d.capocmat as capocmat,
					c.c_mat as c_mat,
					d.societa_orig as societa_orig,
					d.modello_orig as modello_orig,
					d.nome_modello_orig as nome_modello,
					d.id as id_diba,
					f.id as id_fabb,
					d.societa as societa,
					d.anno_stagione as anno_stagione,
					d.gm as gm,
					d.articolo as articolo,
					d.misura as misura,
					d.colore as colore,
					if(d.variante_dt='',d.variante_orig, d.variante_dt) as variante,
					ifnull(if(SUM(f.QTA_FABB_REALE)>0,SUM(f.QTA_FABB_REALE),if(SUM(f.QTA_FABB_TEOR)>0, SUM(f.QTA_FABB_TEOR), SUM(f.QTA_FABB_PREV))), 0) as FABB,
					IF (I.TIPO='DT', ifnull(SUM(I.QTA), 0) ,0) AS IMP_DT,
					IF (I.TIPO='SOC', ifnull(SUM(I.QTA), 0) ,0) AS IMP_SOC,
					IF (I.TIPO='ORD', ifnull(SUM(I.QTA), 0) ,0) AS IMP_ORD,
					ifnull(I.UTENTE, '') as utente_impegno,
					IFNULL(COL.CMATSET, t.COLLEZIONE) as collezione_dt,
					IFNULL(I.ID_FACCOM, '0') as id_faccom
				from MP_CMAT_CAPOCMAT c
				join DIBA_ESPLOSA d on (c.capocmat = d.capocmat)
				join FABBISOGNI f on (d.id = f.id_diba)
				join IMPEGNI I1 on (I1.id_fabbisogno = f.id and (I1.RIF_ORD>0 OR I1.RIF_PREORD>0))
				left join IMPEGNI I on (I.id_fabbisogno = f.id)
				left join RICHIESTE_COMM_PF_ART a on (f.ID_ART_COMM=a.ID)
				left join RICHIESTE_COMM_PF_RIGHE r on (a.ID_RIGA = r.ID)
				left join RICHIESTE_COMM_PF t on (t.ID = r.ID_RICHIESTA)
				LEFT JOIN FACCOL COL ON (t.COLLEZIONE=COL.ML)
				where 1=1
				and c.c_mat = %(C_MAT)s
				GROUP BY c.capocmat,d.MODELLO_ORIG, d.NOME_MODELLO_ORIG, d.SOCIETA_ORIG,if(d.variante_dt='',d.variante_orig, d.variante_dt) ,I.TIPO, I.ID_FACCOM
				limit 1000
				 """ % dict_params
	# tools.dump(query)
	return query


def calcola_consumo_netto(pard, societa, id_articolo, consumo_lordo):
	soc_orig = ''
	if societa == 'MM':
		soc_orig = 'OM'
	elif societa == 'MA':
		soc_orig = 'OA'
	elif societa == 'MN':
		soc_orig = 'ON'

	consumo_netto = consumo_lordo
	art_orig_data = get_articolo_by_id(pard, id_articolo, soc_orig)
	if art_orig_data and art_orig_data['difettosita']:
		difettosita = float(art_orig_data['difettosita'])
		consumo_netto = round(
			(1.00 - float(difettosita) / 100.00) * float(consumo_lordo), 2)

	return consumo_netto


@decorators.query_with_result
def get_id_fabbisogno_by_id_art_rcomm(id_art, capocmat):
	query = """select
					f.id
					from FABBISOGNI f
					join DIBA_ESPLOSA d on (d.id = f.id_diba)
					where 1=1
					and f.id_art_comm = '%s'
					and d.capocmat = '%s'
					 """ % (id_art, capocmat)
	return query


@decorators.query_with_result
def get_collezione_orig(param):
	where_clause = ''
	param['query'] = ConnPool().escape_string(param.get('query', ''))
	if param.get('anno_stagione', '') and param['anno_stagione']:
		where_clause += " and a.anno_stagione = '%(anno_stagione)s' " %param
	if param.get('societa', '') and param['societa']:
		where_clause += " and s.soc_dt = '%(societa)s' " %param
	if param.get('ml', '') and param['ml']:
		if param.get('societa', '') and param['societa'] == 'MN':
			where_clause += " and a.ml_mn like '%%%(ml)s' " % param
		else:
			where_clause += " and a.ml like '%%%(ml)s' " %param
	if param.get('query', '') and param['query']:
		if param.get('societa', '') and param['societa'] == 'MN':
			where_clause += """ and (a.descrizione like "%%%(query)s%%"
									or a.ml_mn like "%%%(query)s%%"
									or concat(a.ml, '-', a.descrizione, '(', a.ml_mn, ')') like "%%%(query)s%%") """ % param
		else:
			where_clause += """ and (a.descrizione like "%%%(query)s%%"
								or a.ml like "%%%(query)s%%"
								or concat(a.ml, '-', a.descrizione) like "%%%(query)s%%") """ %param

	query = """SELECT distinct a.ml as ml,
				concat(a.ml, '-', a.descrizione, if(a.societa='ON', concat(' (',a.ml_mn, ')'), '')) as descrizione
			FROM impo_collezioni a
			join impo_societa s on (s.soc_orig = a.societa)
			where 1=1
			%s
			limit 100
		""" %where_clause
	# import tools
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_modello_variante(anno_stagione, societa, gm, articolo, variante=''):
	query = """
				SELECT DISTINCT
					iav.id_articolo as id_articolo,
					ia.societa as societa,
					ia.anno_stagione as anno_stagione,
					ia.gm as gm,
					ia.articolo as articolo,
					iav.variante as variante,
					iav.colore as colore,
					ia.descrizione as desc_articolo,
					iav.descrizione as desc_variante
				FROM impo_articoli_varianti iav
				LEFT JOIN impo_articoli ia ON (iav.societa = ia.societa AND iav.id_articolo = ia.id_articolo)
				LEFT JOIN impo_societa isc ON (isc.soc_orig = ia.societa)
				WHERE 1=1
				AND ia.anno_stagione = '%s'
				AND isc.soc_dt = '%s'
				AND ia.gm = '%s'
				AND ia.articolo = '%s'
				AND iav.variante = '%s';
			""" % (anno_stagione, societa, gm, articolo, variante)
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_ricodifiche_dt(params):
	query = '''
			SELECT DISTINCT a.anno_stagione as anno_stagione, a.articolo as articolo, a.gm as gm, 'DT' as societa
			FROM MP_CAPOCMAT c
			JOIN MP_CMAT_CAPOCMAT cc ON (c.capocmat = cc.capocmat)
			JOIN articoli a ON (substring(cc.c_mat,1,5) = concat(a.gm,a.articolo))
			WHERE SOC = '%(societa)s'
			AND c.A_S = '%(anno_stagione)s'
			AND c.GM = '10'
			AND c.ART = '%(articolo)s'
		''' % params
	# tools.dump(query)
	return query


@decorators.query_with_result
def get_ricodifiche_orig(params):
	where_clause = ''
	if params.get('gm', '') and params['gm']:
		where_clause += " and a.gm = '%(gm)s' " % params
	if params.get('articolo', '') and params['articolo']:
		where_clause += " and a.articolo = '%(articolo)s' " % params
	if params.get('c_mat_col', '') and params['c_mat_col']:
		where_clause += " and substring(cc.c_mat,6,3) = '%(c_mat_col)s' " % params

	query = '''
			SELECT DISTINCT c.A_S_MM as anno_stagione, c.ART as articolo, c.GM as gm, c.SOC as societa, c.COL as colore
			FROM articoli a
			JOIN MP_CMAT_CAPOCMAT cc  ON (substring(cc.c_mat,1,5) = concat(a.gm,a.articolo))
			join MP_CAPOCMAT c ON (c.capocmat = cc.capocmat)
			WHERE 1=1
			%s
		''' %where_clause
	# tools.dump(query)
	return query