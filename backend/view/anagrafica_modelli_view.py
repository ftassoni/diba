import tools
import time
import pprint
import menu
import cjson
import get_static
import caio_print as cp

from Common import utilities

from facon.diba.diba_common import *
from facon import ws_client_mmfg


def render_page_base(pard):
	# jquery-ui-1.8.12.custom.js
	# jquery.ui.core.js
	# jq.js
	# jquery-1.4.4.min.js
	
    html = """
    <?xml version="1.0" encoding="UTF-8"?>
    <HTML>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <HEAD>
        		<SCRIPT type="text/javascript" src="javascripts/jq/jq.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-ui-1.8.10.custom.min.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery.ui.core.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery.json-2.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/ui.datepicker-it.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/jquery-1.8.3.js"></SCRIPT>
				<SCRIPT type="text/javascript" src="javascripts/jq/ui/jquery-ui-1.9.2.js"></SCRIPT>
				<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
                %%(page_scripts)s
                %%(page_css)s
                <title>%%(page_title)s</title>
        </HEAD>
        <BODY>
                %%(body_page)s
        </BODY>
    </HTML>""" %pard
    
    return html

def pagina_base(pard):
	"""Definisco il campo head della pagina, generalmente piuttosto statico"""
	page_head = {}
	
	js1 = get_static.get_link(pard, "ordini-common.js", "facon.pao")
	js2 = get_static.get_link(pard, "modelli.js", "facon.diba")
	
	page_head['page_scripts']="""
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="%s"></SCRIPT>
						<SCRIPT src="tablesorter/jquery.tablesorter.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.js"></SCRIPT>
						<SCRIPT src="media/js/jquery.dataTables.columnFilter.js"></SCRIPT>
						<SCRIPT type='text/javascript'>
							  jQuery.noConflict();	
							  jQuery(document).ready(function() {init();});
						</SCRIPT>
						""" %(js1, js2)
	
	css1 = get_static.get_link(pard, "diba.css", "facon.diba")				
	page_head['page_css']="""
						<link rel="stylesheet" type="text/css" href="web.css">
						<link rel="stylesheet" type="text/css" href="%s">
						<link rel="stylesheet" type="text/css" href="css/smoothness/jquery-ui-1.9.2.custom.min.css">
						<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
						<link rel="stylesheet" type="text/css" href="tablesorter/themes/blue/style_DT.css">
						"""%(css1)
		
	page_head['page_title']="""
							Anagrafica Modelli Originali
							"""
	"""Chiave dell'head che serve solo perche il % ammette solo due occorrenze consecutive e non tre"""
	page_head['body_page']="""
							%(body_page)s
							"""
						
	"""Recupero alcuni parametri di utility dal pard"""
	pard['menu'] = menu.menu_bar(pard)
	pard['TABLE_WIDTH'] = '98%'
	pard['separator'] = "<hr width='%(TABLE_WIDTH)s' noshade>" % pard
	
	
	"""Costruisco la pagina base  e poi innesto tutti i vari elementi in modo incrementale"""
	html = render_page_base(pard)
	html = html % page_head
	
	common_body = _make_common_body(pard)
	
	"""Costruisco la tabella e richiamo un metodo di utility per generare le varie righe"""

	page_elements = {}
	page_elements['panel_ricerca'] = render_panel_ricerca(pard, {})
	page_elements['render_wait_div'] = render_wait_div(pard)

	
	page_body={}
	
	page_body['body_page']= common_body %page_elements  %pard
	
	html = html % page_body
	return html



def render_wait_div(pard):
	''' stringa di costruzione del pannello di attesa durante la ricerca ''' 
	html= """
		<div id="overall_container" >
			<div id="loading_box">
				<center>
					<h1>Elaborazione in corso</h1>
					<img src="img/loading/ajax_loader_bar.gif"/>
				</center>
			</div>	
		</div>
		"""
	return html



def _make_common_body(pard):
	common_body = """
				<input type="hidden" id="sid" value="%%(sid)s">
				<input type="hidden" id="app_server" value="%%(APPSERVER)s">
				<input type="hidden" id="menu_id" value="%%(menu_id)s">
				<input type="hidden" id="module" value="%%(module)s">
				
				<div id='overlay'> </div>
						
				<div class="pagina_dynamic">
					%%(menu)s
					<form autocomplete='off'> 
					<br>
					<div id='errors'>
					 </div>
					 <div id='message'>
					 </div>
					 %(render_wait_div)s
					 <div id = 'ricerca_div'>
						 %(panel_ricerca)s
						 <br>
						 %%(separator)s		
						 <br>
						  <div id='dialog_img'>
						</div>
						 <div id='risultati_ricerca'>
						</div>
					</div>
					<div id = 'dettaglio_div'>
					</div>
					</form>
				</div> 
			"""
	return common_body


def render_panel_ricerca(pard, valori_parametri):
	
	valori_parametri.setdefault('gm','')
	valori_parametri.setdefault('articolo','')
	valori_parametri.setdefault('anno_stagione','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('societa','')
	valori_parametri.setdefault('fornitore','')
	valori_parametri.setdefault('modello','')
	valori_parametri.setdefault('modello_nome','')
	valori_parametri.setdefault('collezione','')
	
	parametri_details = {}
	parametri_details['html_societa_options'] = utilities.build_options__(SOCIETA_LIST)
	parametri_details['html_as_options'] = utilities.build_options__(AS_LIST, selected=valori_parametri['anno_stagione'])
	#parametri_details['html_classe_options'] = utilities.build_options__(CLASSE_LIST, tutti='Tutti')
	
	panel_ricerca = """ <div>
				<table align=CENTER id='table_ricerca'>
					<tr align=CENTER>
							<td colspan='3' class="lbl">Societa'</td>
							<td width= "50px"></td>
							<td class="lbl">Anno Stagione</td>
							<td width= "10px"></td>
							<td class="lbl">Classe</td>
							<td width= "50px"></td>
							<td class="lbl">Modello (Base)</td>
							<td width= "10px"></td>
							<td class="lbl">Nome</td>
							<td width= "50px"></td>
							<td></td>
					</tr>
					<tr>
						 <td colspan='3' class="lbl">
						  <select id="param_societa">
	                           %(html_societa_options)s
	                      </select>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
						  <select id="param_as">
	                           %(html_as_options)s
	                      </select>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="10" name="classe" id="param_classe" value='%%(classe)s'>
						</td>
						<td width= "50px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" maxlength='8' name="modello" id="param_modello" value='%%(modello)s'>
						</td>
						<td width= "10px"></td>
						<td class="lbl">
						 	<input type="text" class="ui-autocomplete-input ui-corner-all" size="20" name="nome" id="param_nome" value='%%(modello_nome)s'>
						</td>
						<td width= "50px"></td>
						<td></td>
					</tr>
					<tr height='10'></tr>
					<tr>
						<td class="lbl">Gm Princ.</td>
						<td width= "10px"></td>
						<td class="lbl">Articolo Princ.</td>
						<td width= "50px"></td>
						
						<td colspan='3' class="lbl">Collezione</td>
						<td width= "50px"></td>
						<td></td>
					</tr>
					<tr>   
						<td class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="5" name="gm" id="param_gm" value='%%(gm)s'>
						</td>
						<td class="lbl" width= "10px"> - </td>
						<td class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="10" name="articolo" id="param_articolo" value='%%(articolo)s'>
						</td>
						<td width= "10px"></td>
						<td colspan='3' class="lbl">
						  <input type="text" class="ui-autocomplete-input ui-corner-all" size="35" name="collezione" id="param_collezione" value='%%(collezione)s'>
						</td>
						<td width= "50px"></td>
						<td></td>
						<td></td>
						<td class="lbl">
						 	 <input class='btn_ricerca' type="button" size="15" name="btn_ricerca" id="btn_ricerca" value='   Ricerca' title='Ricerca'>
						</td>
					</tr>
				</table> 
			</div>
			<br>
			""" %parametri_details %valori_parametri 
#			<td colspan='3' class="lbl">Fornitore Princ.</td>
#			<td width= "50px"></td>
#
#			<td colspan='3' class="lbl">
#			  <input type="text" class="ui-autocomplete-input ui-corner-all" size="30" name="fornitore" id="param_fornitore" value='%%(fornitore)s'>
#			</td>
#			<td width= "10px"></td>
	
	return panel_ricerca



def get_scheda_modello(pard, valori_parametri={}, db={}):
	valori_parametri.setdefault('modello_base','')
	valori_parametri.setdefault('anno','')
	valori_parametri.setdefault('stag','')
	valori_parametri.setdefault('stagione','')
	if valori_parametri['stagione'] == '1':
		valori_parametri['stag'] = 'P/E'
	elif valori_parametri['stagione'] == '2':
		valori_parametri['stag'] = 'A/I'
	valori_parametri.setdefault('uscita_collezione','')
	valori_parametri.setdefault('desc_uscita_collezione','')
	valori_parametri.setdefault('desc_classe','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('gruppo_merceologico','')
	valori_parametri.setdefault('progr_articolo','')
	valori_parametri.setdefault('desc_articolo','')
	valori_parametri.setdefault('nome','')
	valori_parametri.setdefault('modalita_produzione','')
	valori_parametri.setdefault('desc_tipo_produzione','')
	
	valori_parametri['url_img'] = ws_client_mmfg.get_img_modello(pard, 'model',valori_parametri['societa'], valori_parametri['modello_base'])
	
	html = """ %(btn_indietro)s
				<br>
				%(intestazione)s 
			   <br>
			    <div id="global_tabs">
					 	<ul>
					 		<li><a href="#tab_scheda_modello">Scheda Modello</a></li>
					 	</ul> 
					<div id="tab_scheda_modello">
					   %(dati_generali)s
					   <br> 
					   %(produzione)s
					    <br> 
					   %(pezzi)s
					    <br>
					   %(varianti)s
					 </div>
				</div>
			   """
	
	html_details = {}
	html_details['btn_indietro'] = """ <div id="div_back" style="width:62%; margin:auto;">
											<input type='button' id="btn_back" class="btn_back" value="    Indietro" onclick='indietro()'>
										</div>
									"""
	html_details['intestazione'] = """<fieldset  style="width:60%%%%; margin:auto;">
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										
										<tr>
											<td width="80px">
												<img class="click_load_schizzo" height="150px" title="Visualizza Schizzo" style="cursor:pointer;" alt="Inline image" src="%(url_img)s">
											</td>
											<td align='center'>
												<b><span style="font-size:12;color:#444444;">Modello: </span>
													<span style="font-size:15;color:red;">%(modello_base)s000 - %(nome)s</span>
													</b>
												<br>
												<b>
												<span style="font-size:11;color:#444444;">%(classe)s - %(desc_classe)s (%(uscita_collezione)s - %(desc_uscita_collezione)s %(anno)s %(stag)s )</span>
												</b>
													<br>
													Articolo principale:
													<b>%(gruppo_merceologico)s - %(progr_articolo)s</b>
													(%(desc_articolo)s)
											</td>
											<td width="80px">
											</td>
										</tr>
									
									</table>
								</fieldset>
								"""%valori_parametri
	
	valori_parametri['desc_tecnica_estesa'] = ''
								
	html_details['dati_generali'] = ''
	html_details['dati_generali'] = """<fieldset  style="width:60%%%%; margin:auto;">
										<legend><b>DATI GENERALI</b></legend>
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										
										<tr>
											<td class='dbl' width="20%%%%">
												Nome:
											</td>
											<td class='dl' width="30%%%%">
												%(nome)s
											</td>
											<td class='dbl' width="20%%%%">
												Modalita' Produzione:
											</td>
											<td class='dl' width="30%%%%">
												%(modalita_produzione)s - <i>%(desc_tipo_produzione)s</i>
											</td>
										</tr>
										
										<tr>
											<td class='dbl' width="20%%%%">
												Tipo Imballo:
											</td>
											<td class='dl' width="30%%%%">
												%(tipo_imballo)s - <i>%(desc_tipo_imballo)s</i>
											</td>
											<td class='dbl' width="20%%%%">
												Numero Pezzi:
											</td>
											<td class='dl' width="30%%%%">
												%(num_pezzi)s
											</td>
										</tr>
										
										<tr>
											<td class='dbl' width="20%%%%">
												Validita':
											</td>
											<td class='dl' width="30%%%%">
												%(validita)s
											</td>
											<td class='dbl' width="20%%%%">
												Tipo Taglia:
											</td>
											<td class='dl' width="30%%%%">
												%(desc_tipo_taglia)s
											</td>
										</tr>
										
										<tr>
											<td class='dbl' width="20%%%%">
												Peso:
											</td>
											<td class='dl' width="30%%%%">
												%(peso)s
											</td>
											<td class='dbl' width="20%%%%">
												Prima Taglia:
											</td>
											<td class='dl' width="30%%%%">
												%(prima_tg)s
											</td>
										</tr>
										
										<tr>
											<td class='dbl' width="20%%%%">
												Peso Netto:
											</td>
											<td class='dl' width="30%%%%">
												%(peso_netto)s
											</td>
											<td class='dbl' width="20%%%%">
												Ultima Taglia:
											</td>
											<td class='dl' width="30%%%%">
												%(ultima_tg)s
											</td>
										</tr>
										
										<tr>
											<td class='dbl' width="20%%%%">
												Desc. Scheda Tecnica:
											</td>
											<td class='dl' colspan='3' width="80%%%%">
												%(desc_tecnica_estesa)s
											</td>
										</tr>
									
									</table>
								</fieldset>
								"""%valori_parametri
	
	
	html_details['produzione'] = ''
	html_details['produzione'] = """<fieldset  style="width:60%%; margin:auto;">
										<legend><b>PRODUZIONE</b></legend>
										<br>
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										<thead>
											<th class='hc'>Modello Comm.</th>
											<th class='hc'>Est Prod</th>
											<th class='hc'>Made In</th>
											<th class='hc'>Orig. Pref.</th>
										</thead> """	
	for modello_comm in valori_parametri['modelli_commerciali']:
		for modello_prod in modello_comm['modelli_produttivi']:
			html_details['produzione'] += """
											<tr>
												<td class='dl'>%(modello)s</td>
												<td class='dl'>%%%%s</td>
												<td class='dl'>%%(made_in)s</td>
												<td class='dl'>%%(f_origine_preferenziale)s</td>
											</tr> 
											""" %modello_comm %modello_prod %modello_prod['modello_produzione'][-3:]	
	
	html_details['produzione'] += """								
									</table>
								</fieldset>
								"""%valori_parametri
	
	
	html_details['pezzi'] = ''
	html_details['pezzi'] = """<fieldset  style="width:60%%; margin:auto;">
										<legend><b>PEZZI</b></legend>
										<br>
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										<thead>
											<th class='hc'>Modello Comm.</th>
											<th class='hc'>N. Pezzo</th>
											<th class='hc'>Codice Pezzo</th>
											<th class='hc'>Composizioni</th>
										</thead> """	
	for modello_comm in valori_parametri['modelli_commerciali']:
		for pezzo in modello_comm['pezzi']:
			str_compos = """ <br> """.join(["%s - <i>%s</i>" %(c['desc_tipo_materiale'], c['composizione']) for c in pezzo['composizioni']])
			html_details['pezzi'] += """
											<tr>
												<td class='dl'>%(modello)s</td>
												<td class='dl'>%%(progr_pezzo)s</td>
												<td class='dl'>%%(codice_pezzo)s - <i>%%(desc_pezzo)s</i></td>
												<td class='dl'>%%%%s</td>
											</tr> 
											""" %modello_comm %pezzo %str_compos	
	
	html_details['pezzi'] += """								
									</table>
								</fieldset>
								"""%valori_parametri
								
	
	html_details['varianti'] = ''
	html_details['varianti'] = """<fieldset  style="width:60%%; margin:auto;">
										<legend><b>VARIANTI</b></legend>
										<br>
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										<thead>
											<th class='hc'>Colore</th>
											<th class='hc'>Modello Comm.</th>
											<th class='hc'>Variante</th>
											<th class='hc'>Desc. Variante</th>
											<th class='hc'>Colore Variante</th>
											<th class='hc'>Tema</th>
											<th class='hc'>Tipo Consiglio</th>
											<th class='hc'>Tipo Vetrina</th>
										</thead> """	
	for modello_comm in valori_parametri['modelli_commerciali']:
		for variante in modello_comm['varianti']:
			variante['url_img'] = ws_client_mmfg.get_img_modello(pard, 'color', valori_parametri['societa'], valori_parametri['modello_base'], variante['variante'])
			html_details['varianti'] += """
											<tr>
												<td class='dl'><img class="click_load_color" height="30px" style="cursor:pointer;" alt="Inline image" src="%%(url_img)s"></td>
												<td class='dl'>%(modello)s</td>
												<td class='dl'>%%(variante)s</td>
												<td class='dl'>%%(desc_variante)s</td>
												<td class='dl'>
													<table cellspacing=0 cellpadding=1 style="border-collapse:collapse;">
														<tr>
															<td valign=center>
																<span class="talloncino" style="background-color: %%(html_pantone)s;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
																
															</td>
															<td valign=center nowrap>
																%%(colore_variante)s - %%(desc_colore_variante)s
															</td>
														</tr>
													</table>
												</td>
												<td class='dl'>%%(tema)s - <i>%%(desc_tema)s</i></td>
												<td class='dl'>%%(tipo_consiglio)s</td>
												<td class='dl'>%%(tipo_vetrina)s</td>
											</tr> 
											""" %modello_comm %variante	
	
	html_details['varianti'] += """								
									</table>
								</fieldset>
								"""%valori_parametri
								
	
	html_details['db'] = ''
	if db:
		html_details['db'] = """<fieldset  style="width:80%%; margin:auto;">
											<legend><b>DISTINTA BASE</b></legend>
											<br>
											<table id="result_table" cellpadding='3px' align='center' class="" width="1100px" style="border-collapse:collapse;">
											<thead>
												<th class='hc'>Pezzo</th>
												<th class='hc'>Colonna</th>
												<th class='hc'>Gm</th>
												<th class='hc'>AS</th>
												<th class='hc'>Art</th>
												<th class='hc'>Desc. Art.</th>
												<th class='hc'>Compos</th>
												<th class='hc'>Fornitore Princ.</th>
												<th class='hc'>Misura Base</th>
												<th class='hc'>Style</th>
												<th class='hc'>Tipo Segnata</th>
												<th class='hc'>Ricerca Colore</th>
												<th class='hc'>Consumo Base</th>
											</thead> """	
											
		chiave = db['db'].keys()[0]
		for db_art in db['db'][chiave]:
			tr_style = ''
			if int(db_art['id_articolo']) == int(valori_parametri['articolo']):
				tr_style = """ style="background-color:  #FFFFCC;" """
			html_details['db'] += """
											<tr %%s>
												<td class='dl'>%(pezzo)s</td>
												<td class='dl'>%(colonna)s</td>
												<td class='dl'>%(gm)s</td>
												<td class='dl'>%(anno_stagione)s</td>
												<td class='dl'>%(articolo)s</td>
												<td class='dl'>%(descrizione)s</td>
												<td class='dl'>%(composizione)s</td>
												<td class='dl'>%(fornitore_principale)s - %(ragione_sociale)s</td>
												<td class='dl'>%(misura_base)s</td>
												<td class='dl'>%(style)s</td>
												<td class='dl'>%(tipo_segnata)s</td>
												<td class='dl'>%(ricerca_colore)s</td>
												<td class='dl'>%(consumo_base)s</td>
											</tr> 
											""" %db_art %tr_style
		
		html_details['db'] += """								
										</table>
									</fieldset>
									"""
																							
								
	html = html %html_details
	html = html.replace('%%','%')
	return html



def get_db_modello(pard, valori_parametri, db):
	
	valori_parametri.setdefault('modello_base','')
	valori_parametri.setdefault('anno','')
	valori_parametri.setdefault('stag','')
	valori_parametri.setdefault('stagione','')
	if valori_parametri['stagione']=='1':
		valori_parametri['stag'] = 'P/E'
	elif valori_parametri['stagione']=='2':
		valori_parametri['stag'] = 'A/I'
	valori_parametri.setdefault('uscita_collezione','')
	valori_parametri.setdefault('desc_uscita_collezione','')
	valori_parametri.setdefault('desc_classe','')
	valori_parametri.setdefault('classe','')
	valori_parametri.setdefault('gruppo_merceologico','')
	valori_parametri.setdefault('progr_articolo','')
	valori_parametri.setdefault('desc_articolo','')
	
	html = """ %(btn_indietro)s
				<br>
				%(intestazione)s 
			   <br> 
			   %(db)s
			   <br>
			   """
	
	html_details = {}
	html_details['btn_indietro'] = """ <div id="div_back" style="width:63%; margin:auto;">
											<input type='button' id="btn_back" class="btn_back" value="    Indietro" onclick='indietro()'>
										</div>
									"""
	html_details['intestazione'] = """<fieldset  style="width:60%%%%; margin:auto;">
										<table id="result_table" align='center' class="" width="950px" style="border-collapse:collapse;">
										
										<tr>
											<td width="80px">
												<img class="click_load_schizzo" height="70px" title="Visualizza Schizzo" style="cursor:pointer;" alt="Inline image" src="">
											</td>
											<td align='center'>
												<b><span style="font-size:12;color:#444444;">Modello: </span>
													<span style="font-size:15;color:red;">%(modello_base)s000</span>
													</b>
												<br>
												<b>
												<span style="font-size:11;color:#444444;">%(classe)s - %(desc_classe)s (%(uscita_collezione)s - %(desc_uscita_collezione)s %(anno)s %(stag)s )</span>
												</b>
													<br>
													Articolo principale:
													<b>%(gruppo_merceologico)s - %(progr_articolo)s</b>
													(%(desc_articolo)s)
											</td>
											<td width="80px">
											</td>
										</tr>
									
									</table>
								</fieldset>
								"""%valori_parametri
	
	
	html_details['db'] = """<fieldset  style="width:80%; margin:auto;">
										<legend><b>DISTINTA BASE</b></legend>
										<br>
										<table id="result_table" align='center' cellpadding='3px' class="" width="1100px" style="border-collapse:collapse;">
										<thead>
											<th class='hc'>Pezzo</th>
											<th class='hc'>Colonna</th>
											<th class='hc'>Gm</th>
											<th class='hc'>AS</th>
											<th class='hc'>Art</th>
											<th class='hc'>Desc. Art.</th>
											<th class='hc'>Compos</th>
											<th class='hc'>Fornitore Princ.</th>
											<th class='hc'>Misura Base</th>
											<th class='hc'>Style</th>
											<th class='hc'>Tipo Segnata</th>
											<th class='hc'>Ricerca Colore</th>
											<th class='hc'>Consumo Base</th>
										</thead> """	
	chiave = db.keys()[0]
	tools.dump(db[chiave])
	for db_art in db[chiave]:
		tr_style = ''
		if int(db_art['id_articolo']) == int(valori_parametri['articolo']):
			tr_style = """ style="background-color: #FFFFCC;" """
		html_details['db'] += """
										<tr %%s>
											<td class='dl'>%(pezzo)s</td>
											<td class='dl'>%(colonna)s</td>
											<td class='dl'>%(gm)s</td>
											<td class='dl'>%(anno_stagione)s</td>
											<td class='dl'>%(articolo)s</td>
											<td class='dl'>%(descrizione)s</td>
											<td class='dl'>%(composizione)s</td>
											<td class='dl'>%(fornitore_principale)s - %(ragione_sociale)s</td>
											<td class='dl'>%(misura_base)s</td>
											<td class='dl'>%(style)s</td>
											<td class='dl'>%(tipo_segnata)s</td>
											<td class='dl'>%(ricerca_colore)s</td>
											<td class='dl'>%(consumo_base)s</td>
										</tr> 
										""" %db_art %tr_style
	
	html_details['db'] += """								
									</table>
								</fieldset>
								"""
															
								
	html = html %html_details
	html = html.replace('%%','%')
	return html




