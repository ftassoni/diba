var HEADER_TABELLA_RICERCA = "<table id='table_risultati'> \
									<thead> \
										<tr> \
											<th>Societa</th> \
											<th>Anno Stagione</th> \
											<th>Classe</th> \
											<th>Collezione</th> \
											<th>Conto</th> \
											<th>Faconista</th> \
											<th>Modello</th> \
									 		<th>Nome</th> \
											<th>Modello Orig.</th> \
									 		<th>Nome Orig.</th> \
									 		<th>Art. Ns. Tessuto</th> \
									 		<th>Azioni</th> \
										</tr> \
									</thead> \
								</table>";

var TABELLA_RICERCA = {
        "bProcessing": true,
        "bAutoWidth": false,
        "sAjaxDataProp": "ResultSet.Result",
        "oLanguage": ITALIAN_LANGUAGE,
        "aoColumns": [
                     { "mData": "societa", "sWidth": "5%"},
                     { "mData": "anno_stagione", "sWidth": "5%"},
                     { "mData": "classe",  "sWidth": "5%" },
                     { "mData": "collezione",  "sWidth": "10%" },
                     { "mData": "faconista",  "sWidth": "5%" },
                     { "mData": "ragsoc",  "sWidth": "15%" },
                     { "mData": "modello",  "sWidth": "10%" },
                     { "mData": "nome",  "sWidth": "10%" },
                     { "mData": "modello_orig",  "sWidth": "10%" },
                     { "mData": "nome_orig",  "sWidth": "10%" },
                     { "mData": "ns_tessuto", "sWidth": "10%"},
                     { "mData": "TI_ROWID", "sWidth": "10%" }
                     ],
        "aoColumnDefs": [
					{ "mRender": function (data, type, row) {
						var btn = '';
						if (row['ID_SCHEDA'] != '') {
							btn += "<input type='button' class='btn_apri' title='Apri Scheda Modello' onclick='apriScheda("+row['ID_SCHEDA']+")' />" ;
							btn += "<input type='button' class='btn_pdf' title='Pdf Modello' onclick='getSchedaPdf("+row['modello']+", "+row['ID_SCHEDA']+")' />";
							btn += "<input type='button' class='btn_crea' title='Crea Scheda Modello' onclick='creaScheda(\""+row['societa']+"\",\""+row['modello']+"\",\""+row['modello_orig']+"\", "+row['ID_SCHEDA']+")' />";
						} else
							btn += "<input type='button' class='btn_crea' title='Crea Scheda Modello' onclick='creaScheda(\""+row['societa']+"\",\""+row['modello']+"\",\""+row['modello_orig']+"\", \"\")' />";
		            		 
		            	return btn;
					},
					  "aTargets": [11], 
					  "bSortable": false 
					}
		            ],
        "fnInitComplete": function (oSettings, json) {},
        "iDisplayLength": 20,
        "bPaginate": true,
        "bFilter": true,
        "bSortClasses": false,
        "aaSorting" : []
    }


function init(){
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	jQuery("#overlay").hide();
	jQuery("#overall_container").hide();
	
	jQuery('#table_ricerca').keypress(function(e) {
		if(e.which == 13) {
			ricercaSchede();
		}
	});
	
	jQuery("#btn_ricerca").unbind('click');
	jQuery("#btn_ricerca").click(ricercaSchede);
	
   	
//		setAutocomplete('gm');
//   	jQuery('#param_gm').unbind('change');
//   	jQuery('#param_gm').change(function() {
//   		setAutocomplete('gm');
//   	});
//   	
//   	setAutocomplete('articolo');
//   	jQuery('#param_articolo').unbind('change');
//   	jQuery('#param_articolo').change(function() {
//   		setAutocomplete('articolo');
//   	});
   	
   	setAutocomplete('collezione');
   	jQuery('#param_collezione').unbind('change');
   	jQuery('#param_collezione').change(function() {
   		setAutocomplete('collezione');
   	});
   	
   	
   	jQuery( "#param_conto" ).autocomplete({
   		source: function(request, response) {
   			jQuery.ajax({
   				url: app_server+"?",
   				dataType: "json",
   				data: {
   					module: 'facon/pao.ordini',
   					program: "ricerca_fornitori_shortlist",
   					sid: sid,
   					menu_id: menu_id,
   					maxRows: 20,
   					query: request.term
   				},
   				success: function(data) {
   					if (data['sError']) {
   						alert(data['sError']);
   						jQuery( "#param_conto" ).removeClass('ui-autocomplete-loading');
   					}
   					else {
	   					response(jQuery.map(data['ResultSet']['Result'], function(item) {
	   						return {
	   							label: item.ragsoc,
	   							value: item.conto
	   						}
	   						}));
   					}
   				}
   			});
   		},
   		minLength: 4,
   		select: function(event, ui) {
   			jQuery('#param_conto').removeClass('ui-autocomplete-loading');
   			jQuery("#param_faconista").val(ui.item.label);
   		},
   		change: function(event, ui) {
   			jQuery('#param_conto').removeClass('ui-autocomplete-loading');
   			jQuery('#param_conto').val((ui.item ? ui.item.value : ""));
   			if (jQuery('#param_conto').val()==''){
   				jQuery("#param_faconista").val('');
   			}
   		},
   		open: function() {
   			jQuery( this ).removeClass("ui-corner-all").addClass("ui-corner-top");
   		},
   		close: function() {
   			jQuery( this ).removeClass("ui-corner-top").addClass("ui-corner-all");
   		}
   	});
	
}



function setAutocomplete(param_nome) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	var societa = jQuery('#param_societa option:selected').val();
	var as = jQuery('#param_as').val();
	
	jQuery('#param_'+param_nome).autocomplete({
   		source: function(request, response) {
			jQuery.ajax({
				url: app_server+"?",
				dataType: "json",
				data: {
					module: module,
					program: "ricerca_"+param_nome,
					sid: sid,
					menu_id: menu_id,
					societa: societa,
					anno_stagione: as,
					maxRows: 20,
					query: request.term
				},
				success: function(data) {
					if (data['sError']) {
						alert(data['sError']);
						jQuery('#param_'+param_nome ).removeClass('ui-autocomplete-loading');
					}
					else {
   					response(jQuery.map(data['ResultSet']['Result'], function(item) {
   						return {
   							label: item.label,
   							value: item.chiave
   						}
   						}));
					}
				}
			});
		},
		minLength: 0,
		select: function(event, ui) {
			jQuery('#param_'+param_nome).removeClass('ui-autocomplete-loading');
		},
		change: function(event, ui) {
			jQuery('#param_'+param_nome).removeClass('ui-autocomplete-loading');
		},
		open: function() {
			jQuery( this ).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close: function() {
			jQuery( this ).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});
}


function ricercaSchede() {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'ricerca_bp'; 
	
	jQuery('#risultati_ricerca').empty();
	
	
	var params_ricerca = {}; 
	
	var societa = jQuery('#param_societa').val();
	if (societa!="" && societa!='Tutti'){
		params_ricerca['societa']=societa;
	}
	
	var as = jQuery('#param_as').val();
	if (as!="" && as !='Tutti'){
		params_ricerca['as']=as;
	}
	
	var classe = jQuery('#param_classe').val();
	if (classe!=""){
		params_ricerca['classe']=classe;
	}

	
	var collezione = jQuery('#param_collezione').val();
	if (collezione!=""){
		params_ricerca['collezione']=collezione;
	}
	
	var descrizione = jQuery('#param_descrizione').val();
	if (descrizione!=""){
		params_ricerca['descrizione']=descrizione;
	}
	
	
	var modello_orig = jQuery('#param_modello_orig').val();
	if (modello_orig!=""){
		params_ricerca['modello_orig']=modello_orig;
	}
	
	var nome_modello_orig = jQuery('#param_nome_orig').val();
	if (nome_modello_orig!=""){
		params_ricerca['nome_modello_orig']=nome_modello_orig;
	}
	
	var modello = jQuery('#param_modello').val();
	if (modello!=""){
		params_ricerca['modello']=modello;
	}
	
	var nome_modello = jQuery('#param_nome').val();
	if (nome_modello!=""){
		params_ricerca['nome_modello']=nome_modello;
	}
	
	
	var gm_ns_tessuto = jQuery('#param_gm_ns_tessuto').val();
	if (gm_ns_tessuto!=""){
		params_ricerca['gm_ns_tessuto']=gm_ns_tessuto;
	}
	
	var art_ns_tessuto = jQuery('#param_art_ns_tessuto').val();
	if (art_ns_tessuto!=""){
		params_ricerca['art_ns_tessuto']=art_ns_tessuto;
	}
	
	var ns_tessuto_descr = jQuery('#param_ns_tessuto_descr').val();
	if (ns_tessuto_descr!=""){
		params_ricerca['ns_tessuto_descr']=ns_tessuto_descr;
	}
	
	var gm_tessuto_orig = jQuery('#param_gm_tessuto_orig').val();
	if (gm_tessuto_orig!=""){
		params_ricerca['gm_tessuto_orig']=gm_tessuto_orig;
	}
	
	var art_tessuto_orig = jQuery('#param_art_tessuto_orig').val();
	if (art_tessuto_orig!=""){
		params_ricerca['art_tessuto_orig']=art_tessuto_orig;
	}
	
	var tessuto_orig_descr = jQuery('#param_tessuto_orig_descr').val();
	if (tessuto_orig_descr!=""){
		params_ricerca['tessuto_orig_descr']=tessuto_orig_descr;
	}
	
	if (!societa && !as && !modello && !nome_modello) {
		alert('Per la ricerca inserire almeno societa o anno stagione o codice o nome del modello ricodificato.')
		return;
	}
	
//	var fornitore = jQuery('#param_fornitore').val();
//	if (fornitore!=""){
//		params_ricerca['fornitore']=fornitore;
//	}
	
	ricerca = $.toJSON(params_ricerca);
	
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&params_ricerca="+ricerca;
	
	jQuery('#risultati_ricerca').css('width','85%');
	jQuery('#risultati_ricerca').css('margin','auto');
	jQuery('#table_risultati').css('width','100%');
	
	
	
	jQuery('#risultati_ricerca').append(HEADER_TABELLA_RICERCA);

	TABELLA_RICERCA['sAjaxSource'] = url
	var oTable = jQuery('#table_risultati').dataTable(TABELLA_RICERCA);
	
	jQuery("#risultati_ricerca").show();
	
}




function creaScheda(societa, modello, modello_orig, id_scheda_old){
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'crea_scheda'; 
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&societa="+societa+"&modello="+modello+"&modello_orig="+modello_orig+"&id_scheda_old="+id_scheda_old;
	window.open(url);
	
}


function apriScheda(id_scheda){
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'visualizza_scheda'; 
	var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&id_scheda="+id_scheda;
	window.open(url);
	
}


function chiudi() {
	window.close();
}

function getDatiScheda() {
	var dati_scheda = {};
	dati_scheda['ID_SCHEDA'] = jQuery('#param_id_scheda').val();
	dati_scheda['MODELLO_ORIG'] = jQuery('#param_modello_orig').val(); 
	dati_scheda['MODELLO_ORIG_DESCR'] = jQuery('#param_nome_orig').val(); 
	dati_scheda['MODELLO'] = jQuery('#param_modello').val();
	dati_scheda['MODELLO_DESCR'] = jQuery('#param_nome').val();
	dati_scheda['TESSUTO_ORIG'] = jQuery('#param_tessuto').val() ;
	dati_scheda['TESSUTO_DESCR_ORIG'] = jQuery('#param_tessuto_descr').val();
	dati_scheda['FACONISTA'] = jQuery('#param_conto').val();
	dati_scheda['NOTE'] = jQuery('#param_nota').val();
	dati_scheda['varianti'] = [];
	dati_scheda['tessuto'] = [];
	
	var varianti = jQuery('[id^="param_variante_modello"]');
	varianti.each(function() {
		var riga = jQuery(this).parent().parent();
		var variante = {};
		variante['MODELLO_VAR'] = jQuery(this).val();
		variante['COLORE_DESCR'] = jQuery('#param_colore_descr', riga).val();
		variante['C_MAT'] = jQuery('#param_cmat', riga).val();
		variante['VAR_DIS'] = jQuery('#param_variante', riga).val();
		variante['VAR_ORIG'] = jQuery('#param_var_orig', riga).val();
		variante['TALE_QUALE'] = 'N';
		if (jQuery('#param_tale_quale', riga).is(':checked'))
			variante['TALE_QUALE'] = 'S';
		variante['QTA'] = jQuery('#param_capi', riga).val();
		if (variante['MODELLO_VAR'] != '' || variante['VAR_DIS'] != '')
			dati_scheda['varianti'].push(variante);
	});
	
	var tessuto = jQuery('[id="param_ns_tessuto"]');
	tessuto.each(function() {
		var riga = jQuery(this).parent().parent();
		var tess = {};
		tess['ns_tessuto'] = jQuery(this).val();
		tess['ns_tessuto_descr'] = jQuery('#param_ns_tessuto_descr', riga).val();
		tess['capocmat'] = jQuery('#param_capocmat', riga).val();
		if (tess['ns_tessuto'] != '' || tess['ns_tessuto_descr'] != '' || tess['capocmat'] != '')
			dati_scheda['tessuto'].push(tess);
		
	});
	
	return dati_scheda
}




function salva(id_scheda_old) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	
	
	scheda_dict = getDatiScheda();
	scheda_dict['ID_SCHEDA_OLD'] = id_scheda_old;
	
	
	
	ajax_data = {};
	ajax_data['params'] = $.toJSON(scheda_dict);
	ajax_data['module']=module;
	ajax_data['program']='salva_scheda'; 
	ajax_data['menu_id']=menu_id;
	ajax_data['sid']=sid;
	
	
	jQuery.ajax({
  		type: "POST",
		async: true,
		dataType: "json",
  		url: app_server+"?",
  		data: ajax_data
	}).done(function(ret) {
		
		if (ret['result']=='OK'){
			if (ret['message']['id_scheda'] != "") {
				jQuery('#param_id_scheda').val(ret['message']['id_scheda']);
			}
			//ret['message']='';
			//renderJSONresult(ret, 'Scheda modello salvato con successo.', false);
			alert('Scheda modello salvato con successo.');
			var sid = jQuery('#sid').val();
			var app_server = jQuery('#app_server').val();
			var menu_id = jQuery('#menu_id').val();
			var module = jQuery('#module').val();
			var program = 'visualizza_scheda'; 
			var url = app_server+"?module="+module+"&program="+program+"&menu_id="+menu_id+"&sid="+sid+"&id_scheda="+ret['message']['id_scheda'];
			//location.reload(); 
			location.replace(url);
		}
		else
			renderJSONresult(ret, '', false);
		}
	);
}


function addVar() {
	var last_row = jQuery('#table_articolo tr:last');
	var td = jQuery("#btn_add").parent();
	var variante = jQuery('#param_variante_modello', last_row).val();
	
	if (!variante) {
		alert('Impossibile aggiungere una variante vuota.');
		return;
	}
	
	td.empty();
	td.append("<input type='button' class='btn_delete' id='btn_delete_"+variante+"' onclick='deleteVar(\""+variante+"\")' title='Cancella Variante'  />");
	var new_row = "<tr>\
						<td width='100px' rowspan='2' style='text-align: center;'>\
							<input type='button' id='btn_add' class='btn_add' onclick='addVar()' title='Aggiungi Variante'  />\
						</td>\
						<td class='lbl'>Variante Modello</td>\
						<td class='lbl'>Variante Orig.</td>\
						<td width='80px' class='lbl'>Variante Orig. </td>\
						<td class='lbl'>Descrizione Colore</td>\
						<td class='lbl'>CMAT</td>\
						<td class='lbl'>Variante Dis.</td>\
						<td class='lbl'>Tot. Capi Lanciati</td>\
					</tr>\
					<tr height='60px'>\
						<td><input class='il' id='param_variante_modello' size='15' value='' /></td>\
						<td><input class='il' id='param_var_orig' size='15' value='' /></td>\
						<td style='text-align: center'><input type='checkbox' id='param_tale_quale' /></td>\
						<td><input class='il' id='param_colore_descr' size='50' value='' /></td>\
						<td><input class='il' id='param_cmat' size='15' value='' /></td>\
						<td><input class='il' id='param_variante' size='15' value='' /></td>\
						<td><input class='il' id='param_capi' size='20' value='' /></td>\
					</tr> ";
	last_row.after(new_row);
	
}


function deleteVar(variante) {
	var row = jQuery('#btn_delete_'+variante).parent().parent();
	var row_next = row.next();
	
	row_next.remove();
	row.remove();
	
}



function getSchedaPdf(modello, id_scheda) {
	var sid = jQuery('#sid').val();
	var app_server = jQuery('#app_server').val();
	var menu_id = jQuery('#menu_id').val();
	var module = jQuery('#module').val();
	var program = 'crea_scheda_pdf';
	
	if (!id_scheda)
		id_scheda = jQuery('#param_id_scheda').val();
	var url = app_server+'?module='+module+'&program='+program+'&sid='+sid+'&menu_id='+menu_id+'&modello='+modello+'&id_scheda='+id_scheda;
	jQuery.ajax({
  		type: "GET",
		async: true,
		url: url,
		dataType: "json",
  		success: function(ret) {
			if (ret['result'] != 'OK') { 
				renderJSONresult(ret, '');
			} else {
				program = 'visualizza_pdf';
				var doc_name = ret['message']['name'];
				var doc_path = ret['message']['path'];
				var url2 = app_server+'?module='+module+'&program='+program+'&sid='+sid+'&menu_id='+menu_id+'&doc_name='+doc_name+'&doc_path='+doc_path;
	
				window.location.href = url2;
			}
		}
	});
	
}















